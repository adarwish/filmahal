$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    autoplay: true,
    autoplayTimeout: 1500,
    autoplayHoverPause:true,
    dotsEach:true,
    responsive:{
        0:{
            items:2,
            nav:false,
            loop:true
        },
        
        320: {
            items: 1,
            nav:false,
            loop:true
        },
        600:{
            items:3,
            nav:false,
            loop:true
        },
        1000:{
            items:5,
            nav:false,
            loop:true
        }
    }
});


    $('.container').addClass('container-fluid').removeClass('container');

});

$('.video').parent().click(function () {
  if($(this).children(".video").get(0).paused){
          $(this).children(".video").get(0).play();
          $(this).children(".playpause").fadeOut();
    	  } else{
    	  	$(this).children(".video").get(0).pause();
  		$(this).children(".playpause").fadeIn();
    	  }
});
$('#exampleModal').click(function () {
	setTimeout(function () {
		if (!$('#exampleModal').hasClass('in')) {
			$('.video').parent().children(".video").get(0).pause();
  			$('.video').parent().children(".playpause").fadeIn();
		} else {
			console.log('');
		}}, 500);
});