$(function() {
    var typed = new Typed('.opening-soon', {
        strings: ["الافتتاح قريباً &nbsp;"],
        typeSpeed: 40,
        loop: true,
        backSpeed: 10,
        fadeOut: true,
        backDelay: 1000
    });
    
    
    var dateToCountDownTo = new Date("May 1, 2018 23:59:59").getTime();

    var counter = setInterval(function() {
        function h() {
            hours = '0' + hours;
        }
        
        function s() {
            seconds = '0' + seconds;
        }
        
        function m() {
            minutes = '0' + minutes;
        }
        
        function d() {
            days = '0' + days;
        }
        
        var currentTime = new Date().getTime();
        var length = dateToCountDownTo - currentTime;
        
        var days = Math.floor(length / (1000 * 60 * 60 * 24));
        var hours = Math.floor((length % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((length % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((length % (1000 * 60)) / 1000);
        if(days < 10 && hours < 10 && minutes < 10 && seconds < 10) {
            d();
            h();
            m();
            s();
        } else if(days < 10 && hours < 10 && minutes  < 10) {
            d();
            h();
            m();
        } else if(days < 10 && hours < 10 && seconds < 10) {
            d();
            h();
            s();
        } else if(days < 10 && hours < 10) {
            d();
            h();
        } else if(days  < 10) {
            d();
        } else if(hours < 10 && minutes < 10 && seconds < 10) {
            h();
            m();
            s();
        } else if(hours < 10 && seconds < 10 ) {
            h();
            s();
        } else if(hours < 10 && minutes < 10) {
            h();
            m();
        } else if(hours < 10) {
            h();
        } else if(minutes < 10 && seconds < 10) {
            m();
            s();
        } else if(minutes < 10 && days < 10 ) {
            m();
            d();
        } else if(minutes < 10) {
            m();
        } else if(days < 10 && seconds < 10) {
            d();
            s();
        } else if(seconds < 10) {
            s();
        }
        

        if(length < 0) {
            clearInterval(counter);
            hours = "00";
            minutes = "00";
            seconds = "00";
            days = "00";
        }
        
        $('.days').html(days + '<div class="word text-center">يوم</div>');
        $('.hours').html(hours + '<div class="word text-center">ساعة</div>');
        $('.minuts').html(minutes + '<div class="word text-center">دقيقة</div>');
        $('.seconds').html(seconds + '<div class="word text-center">ثانية</div>');

    }, 1000);
});