"use strict";

$(document).ready(function () {

    function print(x) {
//        console.log(x);
    }
    
    setTimeout(function () {
        $('#emoji').css('display', 'inline-block').css('height', '0').css('width', '0');
    }, 1000);
    

    /*Start Img Uploader*/
    // function readURL(input, width, height) {

    //     let flag;
    //     let thisImg;
    //     const thisDiv = $(input).parent().parent().find("div");
    //     const target = $(input).parents();
    //     const targetID = $(target[9]).attr("id");

    //     let main = thisDiv[2];
    //     const img = thisDiv[3];

    //     print(main);

    //     if($(input).val()) {
    //         const extentions = [".jpg", ".jpeg", ".png", ".gif"];
    //         const x = $(input).val();
    //         let y = x.match(/(.png)|(.jpg)|(.jpeg)|(.gif)/ig);


    //         if(y) {
    //             y = y.slice(-1);
    //                 for(const exten of extentions) {
    //                     if(exten == y) {
    //                         console.log("founded");
    //                         var reader = new FileReader();
    //                         reader.onload = function(e) {

    //                         thisImg = new Image();
    //                         $(thisImg).attr('src', e.target.result);
    //                         $(thisImg).attr('class', "img-responsive");
    //                         $(img).find("img").remove();

    //                         thisImg.onload = function() {
    //                             if(targetID == "navbar") {
    //                                 if(this.width == width && this.height == height) {
    //                                     $(main).css("display", "none");

    //                                     $(img).append(thisImg);
    //                                     swal(
    //                                         'Supported Format!',
    //                                         'You Uploaded an Image!',
    //                                         'success'
    //                                         );

    //                                 } else {
    //                                     $(main).css("display", "block");
    //                                     swal(
    //                                         'Dimensions MUST be 400 X 400!',
    //                                         'You Uploaded Image With unaccepted Dimensions!',
    //                                         'error'
    //                                         );
    //                                       }
    //                             } else {
    //                                 main = $(target[1]).find(".main");
    //                                 $(target[1]).css("padding", 0);

    //                                 if(this.width >= 1349 && this.height >= 471) {
    //                                     $(main).css("display", "none");
    //                                     $(img).append(thisImg);
    //                                     swal(
    //                                         'Supported Format!',
    //                                         'You Uploaded an Image!',
    //                                         'success'
    //                                         );
    //                                     $(".owl-carousel").eq(0).addClass("owl-theme");
    //                                 } else {
    //                                     $(main).css("display", "block");

    //                                     swal(
    //                                         'Dimensions Small!',
    //                                         'You Uploaded an Image is SMALL!',
    //                                         'error'
    //                                         );
    //                                       }
    //                             }
    //                                 }
    //                             }
    //                         reader.readAsDataURL(input.files[0]);
    //                         break;
    //                 }    
    //             }
    //         }
    //         else {
    //             flag = 0;
    //             console.log("Not Valid");
    //             $(thisImg).attr("src", "");
    //             $(main).css("display", "block");
    //             swal(
    //                   'Not Supported Format!',
    //                   '.jpg, .jpeg, .png, .gif!',
    //                   'error'
    //                 );
    //             }

    //     } else {
    //         console.log("Enter SomeThing");
    //         swal(
    //               '!Canceled',
    //               'Still on Previous Cover',
    //               'question'
    //             );
    //         }
    // }

    $("#uploader input[type='file']").change(function () {
        readIMG(this, 1349, 471);
    });

    $("#navbar input[type='file']").change(function () {
        readIMG(this, 400, 400);
    });

    $("#uploadProduct input[type='file']").change(function () {
        readIMG(this, 400, 400);
        setTimeout(function () {
            $('.input-slogo').each(function () {
                $(this).val($(this).parents('.parent-slogo').find('.preview-slogo img').attr('src'));
//                console.log($(this));
//                console.log($(this).eq(0));
//                console.log($(this).parents('.parent-slogo'));
//                console.log($(this).parents('.parent-slogo').find('.preview-slogo img'));
//                console.log($(this).parents('.parent-slogo').find('.preview-slogo img').attr('src'));
            });
        }, 1000);
    });

    $("#restInfo input[type='file']").change(function () {
        readIMG(this, 400, 400);
    });

    $("#editProduct input[type='file']").change(function () {
        readIMG(this, 400, 400);
    });
    /*End Img Uploader*/

    /*Start Of 1st Carousel */
    $('.owl-carousel').eq(0).owlCarousel({
        loop: false,
        margin: 0,
        responsiveClass: true,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
                loop: false
            }
        }
    });
    /*End Of 1st Carousel*/

    /*Start Of 2nd Carousel */
    $('.owl-carousel').eq(1).owlCarousel({
        navText: ['<img src="/image/SVG/arrow.svg" class="img-responsive">', '<img src="/image/SVG/arrow.svg" class="img-responsive">'],
        loop: false,
        margin: 30,
        stagePadding: 15,
        responsiveClass: true,
        autoplay: false,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        startPosition: -1,
        responsive: {
            0: {
                items: 1,
                nav: false,
                loop: false
            },

            480: {
                items: 2,
                nav: false,
                loop: false
            },

            600: {
                items: 3,
                nav: false,
                loop: false
            },

            850: {
                items: 4,
                nav: false,
                loop: false
            },

            1000: {
                items: 5,
                nav: false,
                loop: false
            },

            1270: {
                items: 5,
                nav: true,
                loop: false
            }
        }
    });
    $('.slid-cates .owl-carousel .owl-stage').children().each(function (index) {
        $(this).attr('data-position', index);
//        console.log($(this).attr("data-position"));
    });

  /*
    $(".slid-cates .brand-name").click(function () {
        var position = $(this).parent().parent();
        $(position).prependTo(".slid-cates .owl-carousel .owl-stage");
        $('.slid-cates .owl-carousel .owl-stage').trigger('to.owl.carousel', -1);
    }); 
    
    	*/

    $(".slid-cates .owl-next, .slid-cates .owl-prev").hover(function () {
        if ($(this).hasClass("disabled")) {
            $(this).find("i").css("color", "#11998e");
            $(this).css("cursor", "no-drop");
        } else {
            $(this).find("i").css("color", "#30baaf");
            $(this).css("cursor", "pointer");
        }
    }, function () {
        $(this).find("i").css("color", "#11998e");
        if ($(this).hasClass("disabled")) {
            $(this).css("cursor", "no-drop");
        } else {
            $(this).css("cursor", "pointer");
        }
    });

    /*End Of 2nd Carousel*/

    /*Start Of Hide Element*/
    $(".fa-ellipsis-v").click(function () {

        var firstParent = $(this).parent();
        var popUp = $(firstParent).find(".popup");

        if ($(popUp).hasClass("show") && $(popUp).hasClass("hidden")) {
            $(".popup").addClass("hidden").removeClass("show");
            $(popUp).removeClass("hidden");
//            console.log("1");
        } else if ($(popUp).hasClass("show")) {
            $(".popup").addClass("hidden").removeClass("show");
            $(popUp).removeClass("show").addClass("hidden");
//            console.log("2");
        } else if ($(popUp).hasClass("hidden")) {
            $(".popup").addClass("hidden").removeClass("show");
            setTimeout(function () {
                $(popUp).removeClass("hidden").addClass("show");
            }, 1);
        }
    });

    $(".mainSection").on("click", ".fa-ellipsis-v", function () {
        var firstParent = $(this).parent();
        var popUp = $(firstParent).find(".popup");
        if ($(popUp).hasClass("show hidden")) {
            setTimeout(function () {
                $(popUp).removeClass("hidden");
            }, 1);
        } else if ($(popUp).hasClass("show")) {
            $(".popup").addClass("hidden").removeClass("show");
            setTimeout(function () {
                $(popUp).removeClass("show").addClass("hidden");
            }, 1);
        } else {
            $(".popup").addClass("hidden").removeClass("show");
            setTimeout(function () {
                $(popUp).removeClass("hidden").addClass("show");
            }, 1);
        }
    });

    $("#navbar .contain p").click(function () {
        var parents = $(this).parents();
        var popUp = parents[1];
        var targetParent = parents[3];
//        console.log(targetParent);
        $(popUp).toggleClass("hidden");

        $(targetParent).appendTo(".owl-stage:eq(1)");
    });

    $(".attrsVal .contain p").click(function () {
        var parent = $(this).parent().parent().parent();
        $(parent).remove();
    });

    /*End Of Hide Element*/
    function testImgContainer() {
        var containers = $(".img");

        containers.each(function (index) {
            if ($(containers[index]).find("img").length) {
                var src = $(containers[index]).find("img").attr("src");
                if (src.search(/jpg/i) != -1 || src.search(/gif/i) != -1 || src.search(/jpeg/i) != -1 || src.search(/png/i) != -1) {
                    $(containers[index]).show();
                    $(containers[index]).parent().find(".main").addClass("hidden");
                } else {
                    $(containers[index]).hide();
                    $(containers[index]).parent().find(".main").removeClass("hidden");
                }
            } else {
                $(containers[index]).hide();
                $(containers[index]).parent().find(".main").removeClass("hidden");
            }
        });
    }
    /*Start 0f Active Class*/
    $("#navbar nav ul a").click(function (ev) {
        testImgContainer();
        $("#navbar ul a").removeClass("active");
        $(this).addClass("active");
    });

    $(".brand-name a").click(function (ev) {
        ev.preventDefault();
        $(".brand-name a").removeClass("active");
        $(this).addClass("active");
        $(this).parent().find("input").click();
    });
    /*End 0f Active Class*/

    /*Start 0f Profile Section*/
    function slideDown(box, otherBoxs) {
        $(otherBoxs).hide();
        $(box).toggle();
    }

    $(".profile").click(function () {
        slideDown(".profile div, .profile .fa-caret-up", ".slide-down, .messages .fa-caret-up, .notification-icon .fa-caret-up");
    });

    $(".messages").click(function () {
        slideDown(".messages .slide-down, .messages .fa-caret-up", ".notification-icon .slide-down, .profile div, .notification-icon .fa-caret-up, .profile .fa-caret-up");
    });

    $(".notification-icon").click(function () {
        slideDown(".notification-icon .slide-down, .notification-icon .fa-caret-up", ".profile div, .profile .fa-caret-up, .messages .slide-down, .messages .fa-caret-up");
    });
    /*End 0f Profile Section*/

    //Start Upload Product

    // $(".uploadProduct").click(function(){

    // });

    // End 0f Upload Product

    function sameThing2() {
        $(".mainSection").hide();
        $(".addProduct").show();
    }

    $(".uploadProduct").click(function () {
        sameThing2();
    });

    $(".classification").change(function () {
        var situation = $(this).find(":selected").val();
        var sales = $(".salesPercentagePrice");
        var news = $(".news");
//        console.log(situation);
        function toggleElements(hide, show) {
            $(show).removeClass("hidden");
            $(hide).addClass("hidden");
        }

        switch (situation) {
            case "1":
                toggleElements(news, sales);
                break;
            case "2":
                toggleElements(sales, news);
                break;
            case "3":
                $(sales).removeClass("hidden");
                $(news).removeClass("hidden");
                break;
            default:
                $(sales).addClass("hidden");
                $(news).addClass("hidden");
                break;
        }
    });

    $(".generate .fa").click(function () {
        $(".generate").removeClass("generate").addClass("code").text(Math.floor(Math.random() * 5000001));
    });

    var clickedElements = [];

    $(".chooseColor, .sizes").on("click", "label", function () {
        $(this).toggleClass("checkboxStyle");
        $(this).next().prop("checked", !$(this).next().prop("checked"));
        clickedElements.push($(this).next()[0]);
    });

    function sameThing() {
        var children = $("#navbar nav ul").children();
        $(".mainSection").hide();
        $(children).each(function (index) {
            if ($(children[index]).hasClass("newOffers active")) {
                $(".newOffer").show();
            } else if ($(children[index]).hasClass("sales active")) {
                $(".sale").show();
            } else if ($(children[index]).hasClass("shopCats active")) {
                $(".shopCat").show();
                $(".newOffer").hide();
            }
        });
        $(".addProduct").hide();
    }

    $(".X i").click(function () {
        $(".addProduct").hide();
        $(".shopCat").show();
        for (var i = 0; i < clickedElements.length; i++) {
            if ($(clickedElements[i]).prop("checked")) {

                $(clickedElements[i]).prev().removeClass("checkboxStyle");
                $(clickedElements[i]).prop("checked", false);
            } else {
                $(clickedElements[i]).prev().addClass("checkboxStyle");
                $(clickedElements[i]).prop("checked", true);
            }
        }
        clickedElements = [];
    });

    $(".saveChanges").click(function (ev) {

        // if(!$(".productCode").hasClass("code")) {
        //     ev.preventDefault();
        //     errorMessage("خطأ", "يجب توليد كود للمنتج قبل الحفظ");
        // } else {
        //     const x = $(".chooseColor input, .sizes input");
        //     sameThing();
        // for(let i = 0; i < x.length; i++) {
        //     if($(x[i]).is(":checked")) {
        //         clickedElements = [];
        //         return false;
        //         }
        //     }
        // }

        // //****************************
        var productPrice = $("#productPrice").val().trim();
        var description = $("#description").val().trim();
        var productName = $("#productName").val().trim();
        var properties = $(".properties input[type='checkbox']");
        var img = $(".wholeSection .addProduct  .productPhotosUploader div").eq(0).find(".img").children().length;
        var flag = false;
        if (properties.length) {
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = properties[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var prop = _step.value;

                    if ($(prop).is(":checked")) {
                        clickedElements = [];
                        flag = 1;
//                        console.log(flag);
                        break;
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }

        if (productPrice && description && productName && flag) {} else {
            ev.preventDefault();

            if (!productPrice && !description && !productName && !flag) {
                errorMessage("يجب كتابه اسم وسعر ووصف المنتج واختيار خاصيه");
            } else if (!productPrice && !description && !flag) {
                errorMessage("يجب كتابة سعر ووصف المنتج واختيار خاصيه");
            } else if (!productPrice && !productName && !flag) {
                errorMessage("يجب كتابه سعر واسم المنتج واختيار خاصيه");
            } else if (!description && !productName && !flag) {
                errorMessage("يجب كتابه اسم ووصف المنتج واختيار خاصيه");
            } else if (!productPrice && !description) {
                errorMessage("يجب كتابه سعر ووصف المنتج");
            } else if (!productPrice && !productName) {
                errorMessage("يجب كتابه سعر واسم المنتج");
            } else if (!productName && !description) {
                errorMessage("يجب كتابه اسم ووصف المنتج");
            } else if (!productName && !flag) {
                errorMessage("يجب كتابه اسم المنتج واختيار خاصيه");
            } else if (!productPrice && !flag) {
                errorMessage("يجب كتابه سعر المنتج واختيار خاصيه");
            } else if (!description && !flag) {
                errorMessage("يجب كتابه وصف المنتج واختيار خاصيه");
            } else if (!description) {
                errorMessage("يجب كتابه وصف المنتج");
            } else if (!productPrice) {
                errorMessage("يجب كتابه سعر المنتج");
            } else if (!productName) {
                errorMessage("يجب كتابه اسم المنتج");
            } else if (!flag) {
                errorMessage("يجب اختيار خاصيه");
            }
        }
    });

    // Start 0f PoPups 0f Product
    // $(".options").on("click", "li", function() {
    //     const getClass = $(this).prop("class");

    //     switch(getClass) {
    //         case "addToNew": 
    //             swal({
    //                 title: "ادخل التاريخ",
    //                 html: 
    //                 `
    //                 <label>يوم الانتهاء من العروض الجديده</label>

    //                 <input type="date" id="newDate">`,
    //                 allowOutsideClick: false,
    //                 confirmButtonClass: "dateAll",
    //                 showCancelButton: true,
    //                 cancelButtonClass: "cancelAll",

    //             });

    //             on_off(true);

    //             $("#newDate").change(function() {
    //                 validate("#newDate");
    //             });

    //             $(".cancelAll").click(function() {
    //                 $("#saleDate, #newDate, #salePercent").val("");
    //             });
    //             break;
    //         case "addToSale":
    //             swal({
    //                 title: "ادخل التاريخ",
    //                 html: 
    //                 `
    //                 <label>يوم الانتهاء من التخفيضات</label>

    //                 <input type="date" id="saleDate">

    //                 <label>نسبه التخفيض</label>
    //                 <input type="number" id="salePercent" min="0" step="0.01">
    //                 `,
    //                 allowOutsideClick: false,
    //                 confirmButtonClass: "dateAll",
    //                 showCancelButton: true,
    //                 cancelButtonClass: "cancelAll",
    //             });

    //             on_off(true);

    //             $("#saleDate").change(function() {
    //                 validate("#saleDate", "#salePercent");
    //             });

    //             $("#salePercent").keyup(function() {
    //                 validate("#saleDate", "#salePercent");
    //             });

    //             $(".cancelAll").click(function() {
    //                 $("#saleDate", "#salePercent").val("");
    //             });

    //             break;
    //         case "addToAll":

    //             swal({
    //                 title: "ادخل التاريخ",
    //                 html: `
    //                 <label>يوم الانتهاء من التخفيضات</label>

    //                 <input type="date" id="saleDate">

    //                 <label>نسبه التخفيض</label>
    //                 <input type="number" id="salePercent" min="0" step="0.01">

    //                 <label>يوم الانتهاء من العروض الجديده</label>

    //                 <input type="date" id="newDate">`,
    //                 allowOutsideClick: false,
    //                 confirmButtonClass: "dateAll",
    //                 showCancelButton: true,
    //                 cancelButtonClass: "cancelAll",
    //             });

    //             on_off(true);

    //             $("#saleDate, #newDate").change(function() {
    //                 validate("#saleDate", "#newDate", "#salePercent");
    //             });

    //             $("#salePercent").keyup(function() {
    //                 validate("#saleDate", "#newDate", "#salePercent");
    //             });

    //             $(".cancelAll").click(function() {
    //                 $("#saleDate, #newDate, #salePercent").val("");
    //             });

    //             break;
    //         case "editProduct":
    //             sameThing2();
    //             break;

    //     }


    // });

    function on_off(state) {
        var button = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ".dateAll";

        $(button).prop("disabled", state);
    }

    function validate() {
        var z = 0;

        for (var _len = arguments.length, pars = Array(_len), _key = 0; _key < _len; _key++) {
            pars[_key] = arguments[_key];
        }

        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
            for (var _iterator2 = pars[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                par = _step2.value;

                if ($(par).val().trim()) {
                    ++z;
                }
                z == pars.length ? on_off(false) : on_off(true);
            }
        } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                    _iterator2.return();
                }
            } finally {
                if (_didIteratorError2) {
                    throw _iteratorError2;
                }
            }
        }
    }

    $(".messageBox textarea").focus(function () {
        $(".messageClicked .messageBox .fa").css("text-shadow", "-1px 0 #11998e, 1px 0 #11998e, 0 -1px #11998e");
    });

    $(".messageBox textarea").focusout(function () {
        $(".messageClicked .messageBox .fa").css("text-shadow", "-1px 0 #d3d3d3, 1px 0 #d3d3d3, 0 -1px #d3d3d3");
    });

    $(".message").click(function (ev) {
        ev.preventDefault();
        $(".messageContainer").show();
    });

    $(".exit").click(function (ev) {
        ev.preventDefault();
        $(".messageContainer").hide();
    });

    $("html").click(function () {
        $(".slide-down, .profile div, header nav .fa-caret-up").hide();
        $(".popup").addClass("hidden");
    });

    $(".messages, .profile, .notification-icon, .photo, .cover, .anProduct").click(function (event) {
        event.stopPropagation();
    });

    $(".contactus").click(function () {
        $("#navbar .slid-cates, #bigPopUp, .newOffer, .sale, .shopCat, .bigArrow .fa-caret-up, #uploadProduct").hide();
        $(".myMap").css({ "position": "relative", "left": "auto", "width": "auto" });
        $(".myMap").parent().removeClass("row");
        $(".myMap").parent().parent().removeClass("container");
        $(".myMap, #restInfo, #addAddress").show();
    });

    $("#navbar #shopCats").click(function () {
    	$(".slid-cates .owl-item").eq(0).find(".brand-name a").click();
        $(".myMap").parent().addClass("row");
        $(".myMap").parent().parent().addClass("container");
        $(".mainSection, .myMap, #restInfo, #addAddress, #uploadProduct").hide();
        $("#bigPopUp, #navbar .slid-cates, .shopCat, .fa-caret-up").show();
    });

    $(".newOffers, .sales, .stats").click(function () {
        $("#navbar .slid-cates, .mainSection, .myMap, #restInfo, #addAddress, .bigArrow .fa-caret-up, #uploadProduct").hide();
        if ($(this).hasClass("newOffers")) {
            $("#bigPopUp ,.newOffer").show();
        } else if ($(this).hasClass("sales")){
            $("#bigPopUp ,.sale").show();
        }
          else{
            $("#bigPopUp ,.statistics").show();
          }
    });
    
    

    // End 0f PoPups 0f Product
    // Start 0f Edit Place
    // $(".edit").click(function(ev) {
    //     ev.preventDefault();
    //     swal({
    //             html: '<textarea class="editPlace"></textarea>',
    //             allowOutsideClick: false,
    //             confirmButtonClass: "dateAll",
    //             showCancelButton: true,
    //             cancelButtonClass: "cancelAll",
    //         })
    //     on_off(true);
    //     const place = $(this).prev().find("span");
    //     $(".editPlace").val($(place).text());
    //     $(".editPlace").keyup(function() {
    //         validate(".editPlace");
    //     });
    //     $(".dateAll").click(function(ev) {
    //         ev.preventDefault();
    //         $(place).text($(".editPlace").val());
    //     });

    // });

    if ($(window).width() <= 891) {
        $("#addAddress").appendTo(".customizeHere");
    } else {
        $("#addAddress").appendTo(".myMap");
    }
    // End 0f Edit Place

    $("#infowindow-content a").click(function (ev) {
        ev.preventDefault();
    });

    // $(".addAnotherAddress div, .addAnotherPhone div").click(function(ev) {
    //     ev.preventDefault();
    //     if($(this).parent().hasClass("addAnotherAddress")) {
    //         swal({
    //             html: `<label>العنوان الرئيسى</label><textarea class="addPlace"></textarea>
    //             <label>علامات مميزة</label><textarea class="addMarker"></textarea>
    //             `,
    //             allowOutsideClick: false,
    //             confirmButtonClass: "dateAll",
    //             showCancelButton: true,
    //             cancelButtonClass: "cancel",
    //         })

    //         on_off(true);

    //         $(".addPlace, .addMarker").keyup(function() {
    //             validate(".addPlace", ".addMarker");
    //         });
    // /************************************************************************************ */
    //         //Start 0f Get Data 0f AddPlace and AddMarker Inputs
    //         $(".dateAll").click(function(ev) {
    //             ev.preventDefault(); 
    //         });
    //         //End 0f Get Data 0f AddPlace and AddMarker Inputs
    //     } else {
    //         swal({
    //             html: `<label>ادخل رقم التواصل مع المحل</label>
    //             <input type="text" name="phone2" class="addPhone">
    //             `,
    //             allowOutsideClick: false,
    //             confirmButtonClass: "dateAll",
    //             showCancelButton: true,
    //             cancelButtonClass: "cancel",
    //         })
    //         on_off(true);
    //         $(".addPhone").keyup(function() {
    //             validate(".addPhone");
    //         });
    //     }
    // });

    // $(".editTime").click(function(ev) {
    //     ev.preventDefault();
    //     swal({
    //         html: `<div class="choose-time">
    //                                 <div class="days">
    //                                     <span class="from">من يوم</span>
    //                                     <select name="from">
    //                                         <option value="السبت" selected="">السبت</option>
    //                                         <option value="الأحد">الأحد</option>
    //                                         <option value="الأثنين">الأثنين</option>
    //                                         <option value="الثلاثاء">الثلاثاء</option>
    //                                         <option value="الأربعاء">الأربعاء</option>
    //                                         <option value="الخميس">الخميس</option>
    //                                         <option value="الجمعه" >الجمعه</option>
    //                                     </select>
    //                                     <span>الى يوم</span>
    //                                     <select name="to">
    //                                         <option value="السبت">السبت</option>
    //                                         <option value="الأحد">الأحد</option>
    //                                         <option value="الأثنين">الأثنين</option>
    //                                         <option value="الثلاثاء">الثلاثاء</option>
    //                                         <option value="الأربعاء">الأربعاء</option>
    //                                         <option value="الخميس">الخميس</option>
    //                                         <option value="الجمعه" selected="">الجمعه</option>
    //                                     </select>
    //                                 </div>
    //                                 <br>
    //                                 <div class="openClose">
    //                                     <span class="from">من</span>
    //                                     <select name="h1" class="hoursOfDay">
    //                                         <!--Added By JS-->
    //                                     <option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select>
    //                                     <select name="modeOpening">
    //                                         <option value="صباحاً">صباحاً</option>
    //                                         <option value="مساءً">مساءً</option>
    //                                     </select>
    //                                     <br>
    //                                     <span>الى</span>
    //                                     <select name="h2" class="hoursOfDay">
    //                                         <!--Added By JS-->
    //                                     <option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select>
    //                                     <select name="modeClosing">
    //                                         <option value="صباحاً">صباحاً</option>
    //                                         <option value="مساءً">مساءً</option>
    //                                     </select>
    //                                 </div>
    //                             </div>`,
    //         customClass: "editDate",
    //         allowOutsideClick: false,
    //         confirmButtonClass: "editNow",
    //         showCancelButton: true,
    //         cancelButtonClass: "cancel",
    //     });
    //     $(".editNow").click(function() {
    //         const spans = $(".fromTo").find("span");
    //         $(spans).eq(0).text($(".days select").eq(0).val());
    //         $(spans).eq(1).text($(".days select").eq(1).val());
    //         $(spans).eq(2).text($(".openClose select").eq(0).val());
    //         $(spans).eq(3).text($(".openClose select").eq(1).val());
    //         $(spans).eq(4).text($(".openClose select").eq(2).val());
    //         $(spans).eq(5).text($(".openClose select").eq(3).val());


    //     });
    // });
    // START 0f MAP

    const userImg = $('.nameEmail .message-header .img-container img').attr('src');
    const userName = $('.nameEmail .message-header .client-title h4').html();
    

    $("#emoji").emojioneArea({
        standalone: false,
        search: false,
        events: {
            /**
         * @param {jQuery} editor EmojioneArea input
         * @param {Event} event jQuery Event object
         */
        keyup: function (editor, event) {
            if(event.which == 13 && !event.shiftKey) {
                $("#newmsgForum1 button").click();
                // `<p class="primary">${$(".emojionearea-editor").text()}</p>`
                /*
                '<div class="msg msg-right"><div class="img-container"><img src="/image/'+data[i].sender.photo+'" class="img-responsive"></div><div class="client-title"><h5>'+data[i].sender.user_name+'</h5></div><div class="clearfix"></div><div class="msgtxt"><p class="primary">'+data[i].message+'</p><p class="primary">'+data[i].created_at+'</p></div></div>'
                */
                var d = new Date();
                var date = d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDay()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
                $('<div class="msg msg-right"><div class="img-container"><img src="'+userImg+'" class="img-responsive"></div><div class="client-title"><h5>'+userName+'</h5></div><div class="clearfix"></div><div class="msgtxt"><p class="primary">'+$(".emojionearea-editor").text()+'</p><p class="primary">'+date+'</p></div></div>').appendTo(".textBox");
                $(".emojionearea-editor").text("");
//                console.log("clicked");
                let height = 0;
                $('.textBox').children().each(function(i, value){
                    height += parseInt($(this).height());
                });
                height += '';
                $('.textBox').animate({scrollTop: height});
            }
        }
    }        
    });
    $(".fa-thumbs-o-up").click(function () {
        $(".emojionearea-editor").append('<img alt="👍" class="emojioneemoji" src="https://cdn.jsdelivr.net/emojione/assets/3.1/png/32/1f44d.png">');
    });
    
    

    $(".fa-camera input[type='file']").change(function () {
        readIMG(this, 400, 400);
    });

    $(".profile .profile-settings, .more-messages").on("click", "a", function (ev) {
        if (!$(this).hasClass("logout")) {
            $("#uploader, #navbar, #bigPopUp, #restInfo").hide();
            $("#profile_settings").show();
            repeater(this);
        }
    });

    function repeater(self) {
        $("product");
        if ($(self).hasClass("reviewsPeople")) {
            $(".inbox, .allSettings, #third-step2").hide();
            $(".reviewsBox").show();

            $(".settings li a").removeClass("focus");
            $(".settings .reviewsPeople").addClass("focus");
        } else if ($(self).hasClass("boxMessages")) {
            $(".inbox").show();
            $(".reviewsBox, .allSettings, #third-step2").hide();
            $(".settings li a").removeClass("focus");
            $(".settings .boxMessages").addClass("focus");
        } else if ($(self).hasClass("settingProfile")) {
            $(".inbox, .reviewsBox, #third-step2").hide();
            $(".allSettings").show();

            $(".settings li a").removeClass("focus");
            $(".settings .settingProfile").addClass("focus");
        }
    }

    $($(".reviewsPeople").eq(1)).click(function (ev) {

        repeater(this);
    });

    $($(".boxMessages").eq(1)).click(function (ev) {

        repeater(this);
    });

    $($(".settingProfile").eq(1)).click(function (ev) {

        repeater(this);
    });
    
    //SHADY 2.5.18
    $(".boxMessages").click(function (ev) {

        repeater(this);
    });

    //$(".settings").on("click", "a", function (ev) {
       // $(".settings li a").removeClass("focus");
       // $(this).addClass("focus");
        // if(!$(this).hasClass("logout")) {
        //     ev.preventDefault();
        //     $(".settings li a").removeClass("focus");
        // }
    //});

    $("#mainPage").click(function (ev) {
        $("#profile_settings").hide();
        $("#uploader, #navbar, #bigPopUp").show();
    });

    // HERE
    var clickedElementss = [];
    $(".chooseCat").on("click", "label", function () {
        $(this).toggleClass("checkboxStyle");
        $(this).next().prop("checked", !$(this).next().prop("checked"));
        clickedElementss.push($(this).next()[0]);
    });
    $("#close2").click(function () {
        $(".allSettings").show();
        $("#third-step2").hide();
        for (var i = 0; i < clickedElementss.length; i++) {
            if ($(clickedElementss[i]).prop("checked")) {
                $(clickedElementss[i]).prev().removeClass("checkboxStyle");
                $(clickedElementss[i]).prop("checked", false);
            } else {
                $(clickedElementss[i]).prev().addClass("checkboxStyle");
                $(clickedElementss[i]).prop("checked", true);
            }
        }
        clickedElementss = [];
        $("#errorCates").text("");
    });

    $("#save2").click(function (ev) {
        ev.preventDefault();
        var x = $(".chooseCat input");
        var z = 0;
        $("#cates-list").text("");
        for (var i = 0; i < x.length; i++) {
            if ($(x[i]).is(":checked")) {
                z = 1;
            }
        }
        if (z) {
            $(".allSettings").show();
            $("#third-step2").hide();
            clickedElementss = [];
            $("#errorCates").text("");
        } else {
            $("#errorCates").text("اختر فئة واحده على الاقل");
        }
    });

    $("#cates").click(function () {
        $(".allSettings").hide();
        $("#third-step2").show();
    });

    $(".general").click(function () {
        $(".generalElments").show();
        $(".specificElments, .password-con").hide();
        $(this).css({ "background-color": "#11998e", "color": "#fff" });
        $(".specific, .pass-con").css({ "background-color": "#fff", "color": "#333" });
    });

    $(".specific").click(function () {
        $(".generalElments, .password-config").hide();
        $(".specificElments").show();
        $(this).css({ "background-color": "#11998e", "color": "#fff" });
        $(".general, .pass-con").css({ "background-color": "#fff", "color": "#333" });
    });
    
    $(".pass-con").click(function () {
        $(".generalElments, .specificElments").hide();
        $(".password-config").show();
        $(this).css({ "background-color": "#11998e", "color": "#fff" });
        $(".general, .specific").css({ "background-color": "#fff", "color": "#333" });
    });
    
    
    // HERE

    function tryMapAgain() {
        var time = setInterval(function () {
            if ($("#map").children().length) {
                clearInterval(time);
//                console.log("Map  Now Work! ;)");
            } else {
                initMap();
            }
        }, 5000);
    }

    tryMapAgain();

    $(".phoneBox").last().css("margin-bottom", "0");

    function showCoverDots() {
        var imgs = $("#uploader .img");
        imgs.each(function (index) {
            if ($(imgs[index]).children().length) {
                $("#uploader .owl-carousel").addClass("owl-theme");
                return false;
            }
        });
    }

    showCoverDots();

    $(".uploadMyCover").click(function () {
        var myInput = $(this).prev();
        $(myInput).click();
    });

    $(".slid-cates .owl-item").eq(0).find(".brand-name a").click();

    var hash = window.location.hash;
//    console.log(hash);
    switch (hash) {
        case "#shopCats":
            $("#shopCats").trigger("click");
            $(".slid-cates .owl-item").eq(0).find(".brand-name a").click();
            break;
        case "#newOffers":
            $("#newOffers").trigger("click");
            break;
        case "#sales":
            $("#sales").trigger("click");
            break;
        case "#contactus":
            $("#contactus").trigger("click");
            break;
        case "#reviewsPeople":
            $(".profile #reviewsPeople, .profile").trigger("click");
            break;
        case "#boxMessages":
            $(".profile #boxMessages, .profile").trigger("click");
            break;
        case "#settingProfile":
            $(".profile #settingProfile, .profile").trigger("click");

            break;
    }
    
    $(".inbox li").on("click", "a", function(ev) {
    	ev.preventDefault();
        $(".inbox li a").removeClass("focus");
        $(".inbox li").removeClass("focus");
        $(this).addClass("focus");
        $(this).parent().addClass("focus");
    });
    
    $("#block").click(function() {
          swal({
	  title: 'هل انت متأكد ؟',
	  text: "سيتم منع هذا الحساب من التواصل معك",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'نعم',
	  cancelButtonText: 'لا',
	}).then((result) => {
	  if (result.value) {
	    swal({
		 title:'تم',
		 
		 showConfirmButton: false,
		 timer: 1000,
	type:'success'
	    		})
	  	}
	})
	    });

	$("#report").click(function() {
          swal({
	  title: 'هل انت متأكد ؟',
	  text: "سيتم الابلاغ عن هذا الحساب",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'نعم',
	  cancelButtonText: 'لا',
	}).then((result) => {
	  if (result.value) {
	    swal({
		 title:'تم',
		 
		 showConfirmButton: false,
		 timer: 1000,
	type:'success'
	    		})
	  	}
	})
	    });
	    
	    
      $('.container').addClass('container-fluid').removeClass('container');
	    
});