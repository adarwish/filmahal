function readIMG(input, width, height) {
	
	if($(input).val()) {
            const extentions = [".jpg", ".jpeg", ".png", ".gif"];
            const x = $(input).val();
            let y = x.match(/(.png)|(.jpg)|(.jpeg)|(.gif)/ig);
            
            

            if(y) {
            	y = y.slice(-1);
            	for(const exten of extentions) {
                        if(exten == y) {
                        	const containerOfImage = $(input).parent().next();
            				const boxsToHide = $(input).parent().prev();
            				const checkParent = $(input).parent().parent();
                            const reader = new FileReader();
                            const theImage = new Image();

                            reader.onload = function(e) {
                            	$(theImage).attr('src', e.target.result);
                            	$(theImage).attr('class', "img-responsive");
                            	$(containerOfImage).find("img").remove();
                            	theImage.onload = function() {
                            		if($(checkParent).hasClass("photo")) {
	                            		if(this.width == width && this.height == height) {
	                            			processOfSuccess(boxsToHide, containerOfImage, theImage);
	                            			successMessage();
	                            			
	                            		}//End 0f *if*
	                            		else {
	                            			$(boxsToHide).show();
											errorMessage("Dimensions MUST be 400 X 400!");
											$(input).val("");
	                                    }//End 0f*else*
	                                }
	                                else if($(checkParent).hasClass("cover")) {
	                                    if(this.width >= width && this.height >= height) {
	                                    	processOfSuccess(boxsToHide, containerOfImage, theImage);
	                                    	successMessage();
	                                    	$(".owl-carousel").eq(0).addClass("owl-theme");
	                                  
	                                    }//End 0f *if*
	                                    else {
	                                    	$(boxsToHide).show();
	                                    	errorMessage("Dimensions should be equal or larger than 1349 X 471!");
	                                    	$(input).val("");
	                                    	}//End 0f *else*
	                                    } //End 0f *if*
	                                else if($("input").parent().hasClass("fa-camera")) {
	                                	$(theImage).css("width", "30px");
	                                	$(theImage).css("height", "30px");
	                                	$(theImage).css("display", "inline-block");
	                                	$(".emojionearea-editor").append(theImage);
	                                }
                            		}//End 0f *theImage.onload method*
                            	}//End 0f *reader.onload method*
                            	reader.readAsDataURL(input.files[0]);
                            	break;
                            }//*END 0f *if*    
            		}//End 0f *for loop*  
        		} //End 0f *if*
        		else {
                	swal(
	                      'Not Supported Format!',
	                      'The supported format is: .jpg, .jpeg, .png, .gif!',
	                      'error'
						);
					$(input).val("");
                }//End 0f *else*
			} //End 0f *if*
			else {
				swal(
	                  '!Cancelled',
	                  "!Don't worry we will keep the previous cover",
	                  'question'
	                );
			}
		}//End 0f Function

function successMessage() {
	swal(
	    'Supported Format!',
	    'You Uploaded an Image!',
	    'success'
	    ); //End 0f *success pop-up*
}

function errorMessage(error, why = "You Uploaded Image With unaccepted Dimensions!") {
	swal(
        `${error}`,
        `${why}`,
        'error'
        );
}

function processOfSuccess(boxsToHide, containerOfImage, theImage) {
	$(boxsToHide).hide();
	$(containerOfImage).append(theImage);
}