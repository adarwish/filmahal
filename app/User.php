<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Crypt;
//use App\EncryptableTrait;

class User extends Authenticatable
{
    use Notifiable;
   // use encryptable;

    public $table="users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];
    
   /* protected $encryptable = [
        'user_name','email','birth_day', 'phone','first_name', 'other_name','second_name', 'address', 'identity_type', 'identity'
    ];*/


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function noti()
    {
      return $this->hasMany('App\Noti');
    }
    public function block()
    {
      return $this->hasMany('App\Block');
    }
    public function follow()
    {
      return $this->hasMany('App\Follow');
    }
    public function review()
    {
      return $this->hasMany('App\Review');
    }
    public function room()
    {
      return $this->hasMany('App\Room');
    }
    public function market()
    {
      return $this->hasMany('App\Market');
    }
    
}
