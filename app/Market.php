<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Market extends Model
{
	protected $table = 'market';

    public function user()
    {
      return $this->belongsTo('App\User','user_id');
    }
    public function product()
    {
      return $this->hasMany('App\Product');
    }
    public function cinfo()
    {
      return $this->hasMany('App\Cinfo');
    }
    public function mplace()
    {
      return $this->hasMany('App\Mplace');
    }
    public function mcat()
    {
      return $this->hasMany('App\Mcat');
    }
    public function photo()
    {
      return $this->hasMany('App\Photo');
    }
    public function follow()
    {
      return $this->hasMany('App\Follow');
    }
    
}
