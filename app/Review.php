<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
	protected $table = 'review';

    public function market()
    {
      return $this->belongsTo('App\Market','market_id');
    }
    public function user()
    {
      return $this->belongsTo('App\User','user_id');
    }
    public function like()
    {
      return $this->hasMany('App\Like');
    }
}
