<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $table = 'product';
	
    public function market()
    {
      return $this->belongsTo('App\Market','market_id');
    }
    public function matt()
    {
      return $this->hasMany('App\Matt');
    }
    public function counters()
    {
      return $this->hasMany('App\Counters');
    }
}
