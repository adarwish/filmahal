<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aprop extends Model
{
    protected $table = 'sub_properties';

    public function properties()
    {
      return $this->belongsTo('App\Properties','att_properties_id');
    }
}