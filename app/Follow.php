<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
	protected $table = 'follow';
	
    public function market()
    {
      return $this->belongsTo('App\Market','market_id');
    }
}
