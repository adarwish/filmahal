<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $table = 'report_block';

    public function user()
    {
      return $this->belongsTo('App\User','user_id');
    }
}
