<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Market;
use App\City;
use App\Area;
use App\Mplace;
use App\Category;
use App\Mcat;
use App\Photo;
use App\Sub;
use App\Attribute;
use App\Product;
use App\Properties;
use App\Aprop;
use App\Matt;
use App\Cinfo;
use App\Noti;
use App\Msub;
use App\Follow;
use App\Room;
use App\Review;
use App\Msg;
use App\Block;
use App\Counters;
use Auth;
use App\ImageLogo;
class vendorController extends Controller
{
    // vendor dash board

    public function getCpanel(){
    
        $market = Market::where('user_id',Auth::user()->id)->select('id')->get();
        $photo = Photo::whereIn('market_id',$market)->where('sub_category_id',null)->where('product_id',null)->get();
        $countersphone = Counters::whereIn('market_id',$market)->where('phone','!=',0)->get();
        $countersview = Counters::whereIn('market_id',$market)->where('views','!=',0)->get();
        $countersfollow = Follow::whereIn('market_id',$market)->get();
        
        $mcat = Mcat::whereIn('market_id',$market)->select('category_id')->get();
        $cinfo = Cinfo::whereIn('market_id',$market)->get();
        $msubId = Msub::whereIn('market_id',$market)->select('sub_category_id')->orderBy('status','ASC')->get();
        $msub = Msub::whereIn('market_id',$market)->orderBy('status','DESC')->get();
        $subcategory = Sub::whereIn('id',$msubId)->get();
        $attId = Category::whereIn('id',$mcat)->select('attribute_id')->get();
        $attribute = Attribute::whereIn('id',$attId)->get();
        $categories = Category::whereIn('id',$mcat)->get();
        $allcats = Category::all();
        $room = Room::where('sender_id',Auth::user()->id)->Orwhere('reciver_id',Auth::user()->id)->get();
        $review = Review::whereIn('market_id',$market)->where('replay',0)->limit(3)->get();
        $image  = ImageLogo::where('id',1)->first(); 
        return view('site.vendor.index',['image'=>$image],compact('photo','market','subcategory','attribute','cinfo','categories','allcats','msub','room','review','countersphone','countersview','countersfollow'));
    }
    public function postCpanel(Request $request)
    {
        $mainpath = 'image';
        
        if($request->save1){           
            $marketId = Market::where('user_id',Auth::user()->id)->get();
            $photo = new Photo;
            $oldphoto = Photo::whereIn('market_id',$marketId)->where('sub_category_id',null)->get();
            $file1 = $request->vendormainphotos0;
            $file2 = $request->vendormainphotos1;
            $file3 = $request->vendormainphotos2;
            if($file1 !== null){
                $filename = time().$file1->getClientOriginalName();
                $file1->move($mainpath,$filename);
                if(count($oldphoto) == 1 || count($oldphoto) > 1){
                    if($oldphoto[0] == null){  
                        $photo->market_id = $marketId[0]->id;
                        $photo->photo = $filename;
                        $photo->save();
                    }else{
                        $oldphoto[0]->photo = $filename;
                        $oldphoto[0]->save();
                    }
                }else{
                    $photo->market_id = $marketId[0]->id;
                    $photo->photo = $filename;
                    $photo->save();
                }
            }
            if($file2 !== null){
                $filename = time().$file2->getClientOriginalName();
                $file2->move($mainpath,$filename);
                if(count($oldphoto) == 3 ){
                    if($oldphoto[1] == null){
                        $photo->market_id = $marketId[0]->id;
                        $photo->photo = $filename;
                        $photo->save();
                    }else{
                        $oldphoto[1]->photo = $filename;
                        $oldphoto[1]->save();
                    }
                }else{
                    $photo->market_id = $marketId[0]->id;
                    $photo->photo = $filename;
                    $photo->save();
                }
            }
            if($file3 !== null){
                $filename = time().$file3->getClientOriginalName();
                $file3->move($mainpath,$filename);
                if(count($oldphoto) == 3){
                    if($oldphoto[2] == null){
                        $photo->market_id = $marketId[2]->id;
                        $photo->photo = $filename;
                        $photo->save();
                    }else{
                        $oldphoto[2]->photo = $filename;
                        $oldphoto[2]->save();
                    }
                }else{
                    $photo->market_id = $marketId[0]->id;
                    $photo->photo = $filename;
                    $photo->save();
                }
            }
            //return response()->json($oldphoto);
            return redirect('/vendor/control-panel');
       

        }elseif($request->save2){
            $photo = new Photo;
            $marketId = Market::where('user_id',Auth::user()->id)->get();
            $oldphoto = Photo::where('market_id',$marketId[0]->id)->where('sub_category_id',$request->subcategoryId)->get();
            $file = $request->subphoto;
            if($file !== null){
                $filename = time().$file->getClientOriginalName();
                $file->move($mainpath,$filename);
                if(count($oldphoto) == 0){
                    $photo->market_id = $marketId[0]->id;
                    $photo->sub_category_id = $request->subcategoryId;
                    $photo->photo = $filename;
                    $photo->save();
                }else{
                    
                    $oldphoto[0]->photo = $filename;
                    $oldphoto[0]->save();
                }
            }
            return redirect('/vendor/control-panel');
        }elseif ($request->save3) {
            
            $product = new Product;
            $sub_catId = $request->subCatId;
            
            $filterval = $request->colors;
            
           // $userID = $request->userId;
            $marketId = Market::where('user_id',Auth::user()->id)->get();
            $followers = Follow::where('market_id',$marketId[0]->id)->get();
            $product->name = $request->productname;
         //   $product->attribute_id =  $attrsval[0];
            $product->sub_category_id = $sub_catId;
            $product->market_id = $marketId[0]->id;
            $product->price = $request->productPrice;
            if($request->addTo == '1'){
                $product->pricentage = $request->percentage;
                $product->end_discount = $request->saleDate;
                $discountprice = $request->percentage/100;
                $new_price = $discountprice * $request->productPrice;
                $product->discount = $request->productPrice - $new_price;
                foreach ($followers as $follower) {
                    $noti = new Noti;
                    $noti->action_id = Auth::user()->id;
                    $noti->user_id = $follower->user_id;
                    $noti->data = $marketId[0]->title.' '.'add discount product';
                    $noti->save();
                }
                    $noti = new Noti;
                    $noti->action_id = Auth::user()->id;
                    $noti->user_id = 83;
                    $noti->data = $marketId[0]->title.' '.'add discount product';
                    $noti->save();
            }elseif($request->addTo == '2'){
                $product->end_new = $request->newDate;
                $product->new = '1';
                foreach ($followers as $follower) {
                    $noti = new Noti;
                    $noti->action_id = Auth::user()->id;
                    $noti->user_id = $follower->user_id;
                    $noti->data = $marketId[0]->title.' '.'add new product';
                    $noti->save();
                }
                    $noti = new Noti;
                    $noti->action_id = Auth::user()->id;
                    $noti->user_id = 83;
                    $noti->data = $marketId[0]->title.' '.'add new product';
                    $noti->save();
            }elseif($request->addTo == '3'){
                $product->pricentage = $request->percentage;
                $product->end_discount = $request->saleDate;
                $discountprice = $request->productPrice * $request->percentage;
                $product->discount = $discountprice/100;
                $product->end_new = $request->newDate;
                $product->new = '1';
                
                foreach ($followers as $follower) {
                    $noti = new Noti;
                    $noti->action_id = Auth::user()->id;
                    $noti->user_id = $follower->user_id;
                    $noti->data = $marketId[0]->title.' '.'add discount product';
                    $noti->save();
                }
                foreach ($followers as $follower) {
                    $noti = new Noti;
                    $noti->action_id = Auth::user()->id;
                    $noti->user_id = $follower->user_id;
                    $noti->data = $marketId[0]->title.' '.'add new product';
                    $noti->save();
                }
                    $noti = new Noti;
                    $noti->action_id = Auth::user()->id;
                    $noti->user_id = 83;
                    $noti->data = $marketId[0]->title.' '.'add discount product';
                    $noti->save();
                    
                    $noti = new Noti;
                    $noti->action_id = Auth::user()->id;
                    $noti->user_id = 83;
                    $noti->data = $marketId[0]->title.' '.'add new product';
                    $noti->save();
                 
            }
            if($request->hasFile('image')) {
                $file = $request->image;
                $filename =  time().$file->getClientOriginalExtension();
                $file->move($mainpath,$filename);
                $product->photo = $filename;
            }
            
            $descrption_enter = preg_replace("/[\\n\\r]+/", " ", $request->description);
            $product->discription = $descrption_enter;
            $oldproduct = Product::where('name',$request->productname)->where('market_id',$marketId[0]->id)->where('sub_category_id',$sub_catId)->get();
            if(count($oldproduct) ==0){
                $product->save();
                $to = Auth::user()->email;
                $subject = "تفعيل منتجك";
                $txt = "تم  ارسال منتجك";
                $message = ' <html>
                <head>
                </head>
                <body style="direction: rtl; text-align: right;">
                    <div style="width: 90%; margin: 10px auto;
                                border: 1px solid #ccc; border-radius: 5px; padding: 30px; min-width: 320px;">
                        <div style="border-bottom: 2px solid #eee; padding-bottom: 20px;">
                            '.$txt.'
                        </div>
                        <div>
                            <img src="http://filmahal.com/imgs/Logo.png" style="width:150px; float: right;">
                            <div style="float: right; padding: 0 20px;">
                                <h5><a href="http://www.filmahal.com/" style="text-decoration: none; color: #38ef7d;">http://www.filmahal.com/</a></h5>
                                <h3 style="color: #11998e;">إدارة في المحل دوت كوم</h3>
                                <p>01000061180</p>        
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                </body>
                </html>';
                $headers  = "From: Filmahal". "\r\n" ."MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";

                mail($to,$subject,$message,$headers);
                if($request->hasFile('image2')){
                    $photo = new Photo;
                    $fileNameWithExt = $request->file('image2')->getClientOriginalName();
                    $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $request->file('image2')->getClientOriginalExtension();
                    $fileNameToStore =  $fileName.'_'.time().'.'.$extension;
                    $file = $request->image2;
                    $file->move('images',$fileNameToStore );
                    $photo->photo = $fileNameToStore;
                    $photo->product_id = $product->id;
                    $photo->save();
                    }
                    
                if($request->hasFile('image3')){
                    $photo = new Photo;
                    $fileNameWithExt = $request->file('image3')->getClientOriginalName();
                    $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $request->file('image3')->getClientOriginalExtension();
                    $fileNameToStore =  $fileName.'_'.time().'.'.$extension;
                    $file = $request->image3;
                    $file->move('images',$fileNameToStore );
                    $photo->photo = $fileNameToStore;
                    $photo->product_id = $product->id;
                    $photo->save();
                    }
                if($request->hasFile('image4')){
                    $photo = new Photo;
                    $fileNameWithExt = $request->file('image4')->getClientOriginalName();
                    $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $request->file('image4')->getClientOriginalExtension();
                    $fileNameToStore =  $fileName.'_'.time().'.'.$extension;
                    $file = $request->image4;
                    $file->move('images',$fileNameToStore );
                    $photo->photo = $fileNameToStore;
                    $photo->product_id = $product->id;
                    $photo->save();
                    }
    
                if($request->hasFile('image5')){
                    $photo = new Photo;
                    $fileNameWithExt = $request->file('image5')->getClientOriginalName();
                    $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $request->file('image5')->getClientOriginalExtension();
                    $fileNameToStore =  $fileName.'_'.time().'.'.$extension;
                    $file = $request->image5;
                    $file->move('images',$fileNameToStore );
                    $photo->photo = $fileNameToStore;
                    $photo->product_id = $product->id;
                    $photo->save();
                    }
                foreach($filterval as $filtervals){
                    
                    $attrsval = explode(',',$filtervals);
                    $matt = new Matt;
                    $matt->product_id = $product->id;
                    $matt->attribute_id =  $attrsval[0];
                    $matt->properties_id =  $attrsval[1];
                    $matt->sub_properties_id   =  $attrsval[2];
                    $matt->save();
                    // return response()->json($attrsval);
                }
            }else{
                foreach($filterval as $filtervals){
                    
                    $attrsval = explode(',',$filtervals);
                    $matt = new Matt;
                    $matt->product_id = $product->id;
                    $matt->attribute_id =  $attrsval[0];
                    $matt->properties_id =  $attrsval[1];
                    $matt->sub_properties_id   =  $attrsval[2];
                    $matt->save();
                    // return response()->json($attrsval);
                }
            }
           return redirect('/vendor/control-panel'); //response()->json('success'); 

        }elseif ($request->save4) {
            $marketId = Market::where('user_id',Auth::user()->id)->get();
            $oldcontactinfo = Cinfo::where('market_id',$marketId[0]->id)->get();
            if(count($oldcontactinfo) == 0){
                $contactinfo = new Cinfo;
                $allphones = $request->phone;

                $contactinfo->phone = $request->phone[0];
                $contactinfo->twitter = $request->tw;
                $contactinfo->fb = $request->fb;
                $contactinfo->inst = $request->in;
                $contactinfo->youtube = $request->yt;
                $contactinfo->site = $request->website;
                $contactinfo->delivery = $request->state;
                $contactinfo->description = $request->description4;
                $contactinfo->market_id = $marketId[0]->id;
                if($request->hasFile('companyIcon')) {
                    $file = $request->companyIcon;
                    $filename =  time().$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $contactinfo->icon = $filename;
                }
                $contactinfo->save();
            }else{
                $oldcontactinfo[0]->phone = $request->phone;
                $oldcontactinfo[0]->twitter = $request->tw;
                $oldcontactinfo[0]->fb = $request->fb;
                $oldcontactinfo[0]->inst = $request->in;
                $oldcontactinfo[0]->youtube = $request->yt;
                $oldcontactinfo[0]->site = $request->website;
                $oldcontactinfo[0]->delivery = $request->state;
                $oldcontactinfo[0]->description = $request->description4;
                $oldcontactinfo[0]->market_id = $marketId[0]->id;
                if($request->hasFile('companyIcon')) {
                    $file = $request->companyIcon;
                    $filename =  time().$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $oldcontactinfo[0]->icon = $filename;
                }
                $oldcontactinfo[0]->save();
            }
            return redirect('/vendor/control-panel');
        }elseif ($request->save5) {
             $marketId = Market::where('user_id',$request->userids)->get();
            if($request->sale){
               // $subID = $request->subId;
                $specific_product = Product::where('discount','!=',0)->where('market_id',$marketId[0]->id)->get();
                $cinfo = Cinfo::where('market_id',$marketId[0]->id)->get();
                $specific_productId = Product::where('pricentage','!=',0)->where('market_id',$marketId[0]->id)->select('id')->get();
                $photosajax = Photo::whereIn('product_id',$specific_productId)->where('market_id',null)->where('sub_category_id',null)->get();
                return response()->json(['ids' => $specific_productId, 'pro' => $specific_product, 'cinfo' => $cinfo,'photo'=>$photosajax] );
            }elseif($request->salephoto){
                 $specific_product = Product::where('id',$request->productids)->get();
                 $cinfo = Cinfo::where('market_id',$marketId[0]->id)->get();
                 $specific_productId = $request->productids;
                $photosajax = Photo::where('product_id',$request->productids)->where('market_id',null)->where('sub_category_id',null)->get();
                return response()->json([ 'ids' => $specific_productId, 'cinfo' => $cinfo,'pro' => $specific_product, 'photo' => $photosajax] );
            }elseif($request->newphoto){
                 $specific_product = Product::where('id',$request->productids)->get();
                 $cinfo = Cinfo::where('market_id',$marketId[0]->id)->get();
                 $specific_productId = $request->productids;
                $photosajax = Photo::where('product_id',$request->productids)->where('market_id',null)->where('sub_category_id',null)->get();
                return response()->json([ 'ids' => $specific_productId, 'cinfo' => $cinfo,'pro' => $specific_product, 'photo' => $photosajax] );
            }elseif($request->newpro){
                $subID = $request->subId;
                $specific_product = Product::where('new',1)->where('market_id',$marketId[0]->id)->get();
                $cinfo = Cinfo::where('market_id',$marketId[0]->id)->get();
                $specific_productId = Product::where('new',1)->where('market_id',$marketId[0]->id)->select('id')->get();
                $photosajax = Photo::whereIn('product_id',$specific_productId)->get();
                return response()->json(['ids' => $specific_productId ,'pro' => $specific_product, 'cinfo' => $cinfo ,'photo' => $photosajax] );
            }else{
                $subID = $request->subId;
                $specific_product = Product::where('sub_category_id',$subID)->where('market_id',$marketId[0]->id)->get();
                $specific_productId = Product::where('sub_category_id',$subID)->where('market_id',$marketId[0]->id)->select('id')->get();
                $cinfo = Cinfo::where('market_id',$marketId[0]->id)->get();
                $photosajax = Photo::whereIn('product_id',$specific_productId)->get();
                return response()->json(['pro' => $specific_product, 'cinfo' => $cinfo, 'photo' => $photosajax ]);
            }
        }if($request->save6){
            $marketId = Market::where('user_id',Auth::user()->id)->get();
            if($request->phone10 == 'السبت'){
                $marketId[0]->work_day_from = 1;
            }elseif($request->phone10 == 'الأحد'){
                $marketId[0]->work_day_from = 2;
            }elseif($request->phone10 == 'الأثنين'){
                $marketId[0]->work_day_from = 3;
            }elseif($request->phone10 == 'الثلاثاء'){
                $marketId[0]->work_day_from = 4;
            }elseif($request->phone10 == 'الأربعاء'){
                $marketId[0]->work_day_from = 5;
            }elseif($request->phone10 == 'الخميس'){
                $marketId[0]->work_day_from = 6;
            }elseif($request->phone10 == 'الجمعة'){
                $marketId[0]->work_day_from = 7;
            }
            if($request->phone == 'السبت'){
                $marketId[0]->work_day_to = 1;
            }elseif($request->phone == 'الاحد'){
                $marketId[0]->work_day_to = 2;
            }elseif($request->phone == 'الأثنين'){
                $marketId[0]->work_day_to = 3;
            }elseif($request->phone == 'الثلاثاء'){
                $marketId[0]->work_day_to = 4;
            }elseif($request->phone == 'الأربعاء'){
                $marketId[0]->work_day_to = 5;
            }elseif($request->phone == 'الخميس'){
                $marketId[0]->work_day_to = 6;
            }elseif($request->phone == 'الجمعة'){
                $marketId[0]->work_day_to = 7;
            }
            $marketId[0]->work_hours_from = $request->email;
            $marketId[0]->work_hours_to = $request->country;
            $marketId[0]->save();
            return redirect('/vendor/control-panel');
        }elseif ($request->save7) {
            if($request->anotheradd== 1){
                $marketId = Market::where('user_id',Auth::user()->id)->get();
                $marketId[0]->address = $request->oldaddress;
                $marketId[0]->save();
            }
            else{
                $oldcontactinfo = Cinfo::where('id',$request->idsAdd)->get();
                $oldcontactinfo[0]->address = $request->oldaddress;
                $oldcontactinfo[0]->save();
            }
            return redirect('/vendor/control-panel');
        }elseif ($request->save8) {
            $contactinfo = new Cinfo;
            $marketId = Market::where('user_id',Auth::user()->id)->get();
            $contactinfo->address = $request->newaddress;
            $contactinfo->market_id = $marketId[0]->id;
            $contactinfo->save();
            return redirect('/vendor/control-panel');
        }elseif ($request->save9) {
            
            $marketId = Market::where('user_id',Auth::user()->id)->get();
            $contactinfo = Cinfo::where('market_id',$marketId[0]->id)->get();
            $contactinfo[0]->phone = $contactinfo[0]->phone.','.$request->newphone;
            $contactinfo[0]->save();
            return redirect('/vendor/control-panel');
        }elseif ($request->save10) {
            $marketId = Market::where('user_id',Auth::user()->id)->get();
            $contactinfo = Cinfo::where('market_id',$marketId[0]->id)->get();
            $contactinfo[0]->lat = $request->lat;
            $contactinfo[0]->lng = $request->lang;
            $contactinfo[0]->save();
            return response()->json('success');
        }elseif ($request->save11) {
            $marketId = Market::where('user_id',Auth::user()->id)->get();
            $contactinfo = Cinfo::where('market_id',$marketId[0]->id)->get();           
            return response()->json($contactinfo);

        }elseif ($request->save12) {
            $product = Product::where('id',$request->product_id)->delete();
            $contactinfo = Matt::where('product_id',$request->product_id)->delete();
            
            return response()->json('delete done');
        }elseif ($request->save13) {
            $product = Product::where('id',$request->idsTonew)->get();
            $product[0]->new = '1';
            $product[0]->end_new = $request->newDateEnd;
            $product[0]->save();
            return redirect('/vendor/control-panel');//response()->json('move to new done');
        }elseif ($request->save14) {
            $product = Product::where('id',$request->productSaleId)->get();
            $product[0]->new = '0';
            $product[0]->end_new = null;
            $product[0]->pricentage = $request->salepercentage;
            $product[0]->end_discount = $request->saleDateEnd;
            $product[0]->discount = $request->priceafterpercentage;
            $product[0]->save();
            return redirect('/vendor/control-panel');//response()->json('move to sale done');
        }elseif ($request->save15) {
            $product = Product::where('id',$request->productSaleId1)->get();
            $product[0]->new = '1';
            $product[0]->end_new = $request->newDateEnd1;
            $product[0]->pricentage = $request->salepercentage1;
            $product[0]->end_discount = $request->saleDateEnd1;
            $product[0]->discount = $request->priceafterpercentage1;
            $product[0]->save();
            return redirect('/vendor/control-panel');//response()->json('move to all done');
        }elseif ($request->save16) {
            $product = Product::where('id',$request->ids16)->get();
            $product[0]->name = $request->productname16;
            $product[0]->price = $request->productPrice16;
            $descrption_enter = preg_replace("/[\\n\\r]+/", " ", $request->description16);
            $product[0]->discription = $descrption_enter;
            $sub_catId16 = $request->subCatId16;

            if($request->colors16 !== null){
                $filterval = $request->colors16;
                $attrsval = explode(',',$filterval);
                $product[0]->attribute_id =  $attrsval[0];
            }
            $product[0]->sub_category_id = $sub_catId16;
            if($request->addTo16 == '1'){
                $product[0]->pricentage = $request->percentage16;
                $product[0]->end_discount = $request->saleDate16;
                $product[0]->discount = $request->discount16;
            }elseif($request->addTo16 == '2'){
                $product[0]->end_new = $request->newDate16;
                $product[0]->new = '1';
            }elseif($request->addTo16 == '3'){
                $product[0]->pricentage = $request->percentage16;
                $product[0]->end_discount = $request->saleDate16;
                $product[0]->discount = $request->discount16;
                $product[0]->end_new = $request->newDate16;
                $product[0]->new = '1';
            }
            if($request->hasFile('image16')) {
                $file = $request->image16;
                $filename =  time().$file->getClientOriginalExtension();
                $file->move($mainpath,$filename);
                $product[0]->photo = $filename;
            }
            if($request->hasFile('image17')) {
                $photo =  Photo::where('product_id',$request->ids16)->get();
                if(count($photo) > 0){
                    $file2 = $request->image17;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo[0]->photo = $filename2;
                    $photo[0]->save();
                }else{
                    $photo = new Photo;
                    $file2 = $request->image17;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo->photo = $filename2;
                    $photo->product_id = $request->ids16;
                    $photo->save();
                }
            }
            if($request->hasFile('image18')) {
                $photo =  Photo::where('product_id',$request->ids16)->get();
                if(count($photo) > 1){
                    $file2 = $request->image18;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo[1]->photo = $filename2;
                    $photo[1]->save();
                }else{
                    $photo = new Photo;
                    $file2 = $request->image18;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo->photo = $filename2;
                    $photo->product_id = $request->ids16;
                    $photo->save();
                }
            }
            if($request->hasFile('image19')) {
                $photo =  Photo::where('product_id',$request->ids16)->get();
                if(count($photo) > 2){
                    $file2 = $request->image19;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo[2]->photo = $filename2;
                    $photo[2]->save();
                }else{
                    $photo = new Photo;
                    $file2 = $request->image19;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo->photo = $filename2;
                    $photo->product_id = $request->ids16;
                    $photo->save();
                }
            }
            if($request->hasFile('image20')) {
                $photo =  Photo::where('product_id',$request->ids16)->get();
                if(count($photo) > 3){
                    
                    $file2 = $request->image20;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo[3]->photo = $filename2;
                    $photo[3]->save();
                }else{
                    $photo = new Photo;
                    $file2 = $request->image20;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo->photo = $filename2;
                    $photo->product_id = $request->ids16;
                    $photo->save();
                }
            }

            $product[0]->save();
            return redirect('/vendor/control-panel');//response()->json($request->image16);
        }elseif ($request->editProf) {
            $user = User::find(Auth::user()->id);
            $file = $request->profilephoto;
            $oldStore = Market::where('user_id',Auth::user()->id)->get();
            if($file !== null){
                $filename =  time().$file->getClientOriginalExtension();
                $file->move($mainpath,$filename);
                $user->photo = $filename;
            }        
            $user->name = $request->username;
            $user->email = $request->email;
            $olduser = User::where('email',$request->email)->get();
            if(count($olduser) == 0){
                $user->save();
                $oldStore[0]->title = $request->shopName;
                $oldStore[0]->save();
            }else{
                if($request->email == Auth::user()->email){
                    $user->save();
                    $oldStore[0]->title = $request->shopName;
                    $oldStore[0]->save();
                }
            }
            return redirect('/vendor/control-panel');
        }elseif($request->changepsw) {
            $user = User::find(Auth::user()->id);
            $password = $request->oldPassword;
            $email = Auth::user()->email;
            if (Auth::attempt(['email' => $email,'password' => $password])) {
                $user->password = bcrypt($request->newPassword);
                $user->save();
                return response()->json('psw changed success');
            }else{
                return response()->json('wrong old password');
            }
            
        }elseif ($request->editCats) {
            $market = Market::where('user_id',Auth::user()->id)->get();
            $catsId = $request->kinds;
            foreach ($catsId as $catId) {
                $oldmcat = Mcat::where('category_id',$catId)->where('market_id',$market)->get();
                if(count($oldmcat) == 0){
                    $mcat = new Mcat;
                    $mcat->market_id = $market[0]->id;
                    $mcat->category_id = $catId;
                    $mcat->save();
                }
            }
            return redirect('/vendor/control-panel');//response()->json();
        }elseif ($request->save17) {
            $market = Market::where('user_id',Auth::user()->id)->get();
            $product = Product::where('id',$request->search)->where('market_id',$market[0]->id)->get();
            $productId = Product::where('id',$request->search)->select('id')->get();
            $photo = Photo::whereIn('product_id',$productId)->get();
            return response()->json(['products'=>$product,'photo'=>$photo]);
        }elseif($request->save18) {
                $room = Room::find($request->roomIds);
                $block = Block::where('vendor_id',Auth::user()->id)->where('user_id',$room->sender_id)->get();
               
                $msg = new Msg;
                $msg->sender_id = Auth::user()->id;
                $msg->reciver_id = $room->sender_id;
                $msg->message = $request->newmessage;
                $msg->room_id = $request->roomIds;
                
                if(count($block) >0){
                    foreach($block as $blocks){
                    
                        if($blocks->block == 1){
                            return response()->json('block');
                        }else{
                            return response()->json('reported');
                        }
                    }
                    
                }else{
                    $msg->save();
                    return response()->json('message success');
                }
                return response()->json($request->message);
        }elseif($request->comment){
            $marketId = Market::where('user_id',Auth::user()->id)->get();
            $review = new Review;
            $review->user_id = $request->userId;
            $review->vendor_id = $marketId[0]->user_id;
            $review->market_id = $marketId[0]->id;
            $review->comment = $request->comment;
            $review->replay= $request->oldcomment;
            $validcomment = Review::where('market_id',$marketId[0]->id)->where('comment',$request->comment)->where('user_id',$request->userId)->get();
            if(count($validcomment) == 0){
                $review->save();
            }
            
            $userId = Review::where('market_id',$marketId[0]->id)->select('user_id')->get();
            $users = User::whereIn('id',$userId)->get();
            $oldReview = Review::where('market_id',$marketId[0]->id)->get();
            return response()->json(['replay comment success']);
        }
    
    }
    public function hideSub(Request $request)
    {
        $subcat = Msub::where('sub_category_id',$request->ids)->get();
        $subcat[0]->status = '0';
        $subcat[0]->save();
        return response('hidden success');
    }
    public function deleteSub(Request $request)
    {
      //  $cat = Category::where('id',$request->ids)->delete();
        $mcat = Mcat::where('category_id',$request->ids)->delete();
        $subcat = Sub::where('category_id',$request->ids)->select('id')->get();
        $pro = Product::whereIn('sub_category_id',$subcat)->get();
        if(count($pro) !==0){
            Product::whereIn('sub_category_id',$subcat)->delete();
        }
        return response($request->ids);
    }
    // edit product
    public function getEdit($id)
    {
      
        $product =  Product::find($id);
        $attributeID = Matt::where('product_id',$id)->select('attribute_id')->get();
        $propertiesID = Properties::whereIn('attribute_id',$attributeID)->select('id')->get();
        $properties = Properties::whereIn('attribute_id',$attributeID)->get();
        $apropID = Aprop::whereIn('att_properties_id',$propertiesID)->select('id')->get();
        $aprop = Aprop::whereIn('att_properties_id',$propertiesID)->get();
        $image  = ImageLogo::where('id',1)->first(); 
        return view('site.vendor.editProduct',['image'=>$image],compact('product','id','attributeID','properties'));
    }
    public function postEdit(Request $request)
    {
      $mainpath = 'image';
        $product = Product::where('id',$request->ids16)->get();
            $product[0]->name = $request->productname16;
            $product[0]->price = $request->productPrice16;
            $descrption_enter = preg_replace("/[\\n\\r]+/", " ", $request->description16);
            $product[0]->discription = $descrption_enter;
            $sub_catId16 = $request->subCatId16;

            if($request->colors16 !== null){
                $filterval = $request->colors16;
                $attrsval = explode(',',$filterval);
                $product[0]->attribute_id =  $attrsval[0];
            }
            $product[0]->sub_category_id = $sub_catId16;
            if($request->addTo16 == '1'){
                $product[0]->pricentage = $request->percentage16;
                $product[0]->end_discount = $request->saleDate16;
                $product[0]->discount = $request->discount16;
            }elseif($request->addTo16 == '2'){
                $product[0]->end_new = $request->newDate16;
                $product[0]->new = '1';
            }elseif($request->addTo16 == '3'){
                $product[0]->pricentage = $request->percentage16;
                $product[0]->end_discount = $request->saleDate16;
                $product[0]->discount = $request->discount16;
                $product[0]->end_new = $request->newDate16;
                $product[0]->new = '1';
            }
            if($request->hasFile('image16')) {
                $file = $request->image16;
                $filename =  time().$file->getClientOriginalExtension();
                $file->move($mainpath,$filename);
                $product[0]->photo = $filename;
            }
            if($request->hasFile('image17')) {
                $photo =  Photo::where('product_id',$request->ids16)->get();
                if(count($photo) > 0){
                    $file2 = $request->image17;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo[0]->photo = $filename2;
                    $photo[0]->save();
                }else{
                    $photo = new Photo;
                    $file2 = $request->image17;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo->photo = $filename2;
                    $photo->product_id = $request->ids16;
                    $photo->save();
                }
            }
            if($request->hasFile('image18')) {
                $photo =  Photo::where('product_id',$request->ids16)->get();
                if(count($photo) > 1){
                    $file2 = $request->image18;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo[1]->photo = $filename2;
                    $photo[1]->save();
                }else{
                    $photo = new Photo;
                    $file2 = $request->image18;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo->photo = $filename2;
                    $photo->product_id = $request->ids16;
                    $photo->save();
                }
            }
            if($request->hasFile('image19')) {
                $photo =  Photo::where('product_id',$request->ids16)->get();
                if(count($photo) > 2){
                    $file2 = $request->image19;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo[2]->photo = $filename2;
                    $photo[2]->save();
                }else{
                    $photo = new Photo;
                    $file2 = $request->image19;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo->photo = $filename2;
                    $photo->product_id = $request->ids16;
                    $photo->save();
                }
            }
            if($request->hasFile('image20')) {
                $photo =  Photo::where('product_id',$request->ids16)->get();
                if(count($photo) > 3){
                    
                    $file2 = $request->image20;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo[3]->photo = $filename2;
                    $photo[3]->save();
                }else{
                    $photo = new Photo;
                    $file2 = $request->image20;
                    $filename2 =  time().$file2->getClientOriginalExtension();
                    $file2->move($mainpath,$filename2);
                    $photo->photo = $filename2;
                    $photo->product_id = $request->ids16;
                    $photo->save();
                }
            }

            $product[0]->save();
            return redirect('/vendor/control-panel');
    }
    /*
     *
     *
     *
     *
     *
     *
     *
     *
     */
    // vendor register
    public function getVendorRegister(){
        $city = City::all();
        $area = Area::all();
        $category = Category::all();
        $image  = ImageLogo::where('id',1)->first(); 
        return view('site.Auth.vendor',['image'=>$image],compact('city','area','category'));
    }
    public function getctiy(Request $request)
    {
        $area = Area::where('city_id',$request->city)->get();
        return response()->json(['area'=>$area]);
    }
    public function register(Request $request)
    {
        $olduser = User::where('email',$request->email)->get();
        if(count($olduser) !== 0){
            return redirect('/vendor/register');
        }
        $user = new User;
        $user->name = $request->fullName;
        $user->email = $request->email;
        $user->phone = $request->phone;
    $user->user_name = $request->userName;
        $user->password = bcrypt($request->password);
        $user->vendor = '1';
        $user->status_view = "1";
        $user->save();

        $market = new Market;
        $market->title = $request->marketName;
        $market->english = $request->marketEnName;
        $market->other_name = $request->nickName;
        $market->work_day_from = $request->from;
        $market->work_day_to = $request->to;
        $workHoursfrom = $request->h1.' '.$request->modeOpening;
        $workHoursto = $request->h2.' '.$request->modeClosing;
        $market->work_hours_from = $workHoursfrom;
        $market->work_hours_to = $workHoursto;
        $market->address = $request->mainaddress;
        $market->special_mark = $request->markaddress;
        $market->user_id = $user->id;
        $market->save();

        $mplace = new Mplace;
        $mplace->city_id = $request->governorate;
        $mplace->area_id = $request->area;
        $mplace->market_id = $market->id;
        $mplace->save();
        $mcats = $request->kinds;
        foreach($mcats as $mcats){
            $mcat = new Mcat;
            $mcat->market_id = $market->id;
            $mcat->category_id = $mcats;
            $mcat->save();
        }
        if(Auth::guest()){
            Auth::loginUsingId($user->id);
        }elseif(Auth::user()->admin == 1){
            
        }
        $cinfo = new Cinfo;
        $cinfo->market_id = $market->id;
        $cinfo->save();

        for($s=0; $s<3; $s++){
            $photo = new Photo;
            $photo->market_id = $market->id;
            $photo->photo = 'cover-avatar.png';
            $photo->save();
        }
        $mcat = Mcat::where('market_id',$market->id)->select('category_id')->get();
        $subcategory = Sub::whereIn('category_id',$mcat)->get();
        foreach ($subcategory as $subcategories) {
            $msub = new Msub;
            $msub->market_id = $market->id;
            $msub->sub_category_id = $subcategories->id;
            $msub->save();
        }
                    $noti = new Noti;
                    $noti->action_id = $user->id;
                    $noti->user_id = 83;
                    $noti->data = "new vendor"." ".$user->user_name;
                    $noti->save();
                    
                        $to = $user->email;
            $subject = "تفعيل عضويتك";
            $txt = "تم  ارسال طلبك";
            $message = ' <html>
            <head>
            </head>
            <body style="direction: rtl; text-align: right;">
                <div style="width: 90%; margin: 10px auto;
                            border: 1px solid #ccc; border-radius: 5px; padding: 30px; min-width: 320px;">
                    <div style="border-bottom: 2px solid #eee; padding-bottom: 20px;">
                        '.$txt.'
                    </div>
                    <div>
                        <img src="http://filmahal.com/imgs/Logo.png" style="width:150px; float: right;">
                        <div style="float: right; padding: 0 20px;">
                            <h5><a href="http://www.filmahal.com/" style="text-decoration: none; color: #38ef7d;">http://www.filmahal.com/</a></h5>
                            <h3 style="color: #11998e;">إدارة في المحل دوت كوم</h3>
                            <p>01000061180</p>    
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </body>
            </html>';
            $headers  = "From: Filmahal". "\r\n" ."MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";

                mail($to,$subject,$message,$headers);
                $counter = new Counters;
                $counter->phone = 0;
                $counter->views = 0;
               // $counter->phone = 0;
                $counter->market_id = $market->id;
                $counter->save();
        if(Auth::user()->admin == 0){
            return redirect('/vendor/control-panel');
        }elseif(Auth::user()->admin == 1){
            return redirect('/admin/all-vendors');
        }
        
        
        
    }
    
}
