<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Crypt;
use App\checkuser;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Socialite;

class RegisterLogin extends Controller
{
     public function redirectToProvider()
     {
        return Socialite::driver('facebook')->redirect();

     }
     public function handleProviderCallback(Request $request)
     {
        //return $optional;
        if (! $request->input('code')) {
            return redirect('/');
           }else{

               $user = Socialite::with('facebook')->user();

                   if($user->getEmail() == null || $user->getId() == null){

                    return redirect('/login');

            }else{
                    $uservalid = User::where('social_id',$user->getId())->get();
                    if(count($uservalid) > 0){
                        Auth::login($uservalid[0],true);
                        return redirect('/');
                    }else{
                       // $password = 123456;
                        $newuser = new User;
                        $newuser->social_id = $user->getId();
                        $newuser->user_name = $user->getName();
                        $newuser->email = $user->getEmail();
                    	$newuser->remember_token = $user->token;
                    	//$newuser->password = '123456';
                        $newuser->save();
                        Auth::login($newuser,true);
                        return redirect('/');
                    }
             }


     }
    }
    public function RLogin(Request $request)
	{	
		$users = User::all();
		$countusers = count($users);
		if($request->register){
			
			for ($s=0; $s<$countusers; $s++) {
				
				if($users[$s]->user_name == $request->cuserfield){
				 	return response()->json('exist'); 
				}
			}//01200500 //4445
			$months = $request->months;
        	$days = $request->days;
        	$years = $request->years;
			$user = new User;
			$user->first_name = encrypt($request->fnamefield);
			$user->phone = encrypt($request->phonefield);
			$user->second_name = encrypt($request->snamefield);
			$user->user_name = encrypt($request->cuserfield);
			$user->email = encrypt($request->emailfield);
			$user->other_name = encrypt($request->onamefield);
			$user->address = encrypt($request->addfield);
			$user->birth_day = encrypt($years.'-'.$months.'-'.$days);
			$user->identity =  encrypt($request->idnumfield);
	        $user->identity_type =  encrypt($request->idtypefield);
	        $user->pin =  bcrypt($request->pinnumfield);
	        $user->save();
	        $phone = $request->phonefield;
			$password = $request->pinnumfield;
	        Auth::login($user,true);
	        return response()->json('true');
		}else if($request->login){
			$email = $request->username;
			$password = $request->password;
				if (Auth::attempt(['email' => $email, 'password' => $password])) {
					$user = User::find(Auth::user()->id);
					$user->status_view = Auth::user()->status_view + 1;
					$user->save();

					//Auth::login($user,true);
					
				 	return response()->json(['admin'=>Auth::user()->admin,'vendor'=>Auth::user()->vendor]);
				}
				return response()->json('0');
			
		}
	}
}
