<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Sub;
use App\Target;
use App\Attribute;
use App\Properties;
use App\Aprop;
use App\City;
use App\Area;
use App\User;
use App\Market;
use App\Product;
use App\Ads;
use App\Blog;
use App\Icons;
use App\Photo;
use App\Settings;
use DB;
use Validator;
use App\Subscribe;
use App\Review;
use Auth;
use App\Cblog;
use App\BlogCategory;
use App\Contact_us;
use App\ImageLogo;
use \Carbon\Carbon;
class admin extends Controller
{
    public function getLogin()
    {
        
        return view('site.Auth.adminLogin');
    
    }
    public function postLogin()
    {
        
        if($request->login){
        $email = $request->username;
        $password = $request->password;
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = User::find(Auth::user()->id);
            $user->status_view = Auth::user()->status_view + 1;
            $user->save();
                    
            return response()->json(['admin'=>Auth::user()->admin,'vendor'=>Auth::user()->vendor]);
        }
        return response()->json('0');
    }
    
    }
    public function getlogo()
    {
        
        return view('dashBoard.logos.add');
    
    }
    public function getVideos()
    {
        return view('dashBoard.videos');
    }
    public function postVideos(Request $request)
    {
        $mainpath = 'video';
        
        
    if($request->hasFile('landing_video')){
        $setting = Settings::find(10);
        $file = $request->landing_video;
        
            $videoname = time().$file->getClientOriginalName();
         //     $extension = $file->getClientOriginalExtension();    
            $file->move($mainpath,$videoname);
            
            $setting->photo= $videoname;
            
            $setting->save();
    }
    if($request->hasFile('user_video')){
        $setting = Settings::find(11);
        $file = $request->user_video;
        
            $videoname = time().$file->getClientOriginalName();
         //     $extension = $file->getClientOriginalExtension();    
            $file->move($mainpath,$videoname);
            
            $setting->photo= $videoname;
            
            $setting->save();
    }
    // response()->json($videoname);
        return redirect('/admin.videos.mahalatmasr2018@fmax0*');
    
    }
    public function postlogo(Request $request)
    {
        $mainpath = 'image';
        $icon = new Icons;
        if($request->hasFile('logo')){
            $file = $request->logo;
            $photoname = time().$file->getClientOriginalName();     
            $file->move($mainpath,$photoname);
            
            $icon->icon= $photoname;
            
            $icon->save();
        }
        return redirect('/admin.add-logo.mahalatmasr2018@fmax0*');
    
    }
    public function getlogos()
    {
        $icon = Icons::all();
        return view('dashBoard.logos.all')->withIcon($icon);
    }
    public function deleteIcon($id)
    {
        $icon = Icons::find($id);
        $icon->delete();
        return redirect('/admin.Logos.mahalatmasr2018@fmax0*');
    }
    public function updatelogos(Request $request){
        $id = $request->input('iconsId');
        $icon=Icons::find($id);
        $mainpath = 'image';
        if($request->hasFile('logo')){
            $file = $request->logo;
            $photoname = time().$file->getClientOriginalName();     
            $file->move($mainpath,$photoname);
                $icon->icon= $photoname;
        
            $icon->save();
        }

        return  redirect('/admin/Logos');
    }
    public function blogView()
    {       
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $blog = Blog::all();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        $cats = Cblog::all();
        $image  = ImageLogo::where('id',1)->first(); 
            return view('site.blog.all')->withImage($image)->withBlog($blog)->withCity($city)->withArea($area)->withSavedArea($savedArea)->withSavedCity($savedCity)->withCity1($city1)->withArea1($area1)->withCats($cats);
    }
        
    public function eachPost($id)
    {
            $blog = Blog::find($id);
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        $cats = Cblog::all();
        $image  = ImageLogo::where('id',1)->first(); 

            return view('site.blog.blog')->withImage($image)->withBlog($blog)->withCity($city)->withArea($area)->withSavedArea($savedArea)->withSavedCity($savedCity)->withCity1($city1)->withArea1($area1)->withCats($cats);
    }
    public function getblogcat()
    {
        $cat = Cblog::all();
        return view('dashBoard.blog.blogCats')->withCat($cat);
    
    }
    public function postblogcat(Request $request)
    {
        if($request->save1){
            $id = $request->i;
            $cat = Cblog::find($id);
            $cat->name = $request->catName;
            $cat->save();
            return response()->json('success');
        }elseif($request->save2){
            $cat = Cblog::where('id',$request->id)->delete();
            return response()->json('deleted success');
        }else{
            $cat = new Cblog;
            $cat->name = $request->catname;
            $cat->save();
            return redirect('/admin.blog-category.mahalatmasr2018@fmax0*');
        }
    
    }
    public function getBlogFilter(Request $request) {
        
        if($request->catID == '00'){
            $blogs = Blog::all();
        }else{
            $catBlog = BlogCategory::where('cblog_id',$request->catID)->select('blog_id')->get();
            $blogs = Blog::whereIn('id',$catBlog)->get();
        }
        return response()->json($blogs);
    }
    public function getPosts()
    {
        $blog = Blog::all();
        return view('dashBoard.blog.all')->withBlog($blog);
    }
    public function getblog()
    {
        $cat = Cblog::all();
        return view('dashBoard.blog.blog',compact('cat'));
    
    }
    public function deleteBlog($id)
    {
        $blog=Blog::find($id);
        $blog->delete();
        return redirect('/admin.allPosts.mahalatmasr2018@fmax0*');
    }
    
    public function updatePost(Request $request){
        $id = $request->input('invisible');
        $blog=Blog::find($id);
        if($request->hasFile('background')){
            // Get file name with the extension
            $fileNameWithExt = $request->file('background')->getClientOriginalName();
            // Get just file name
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // Get just Ext
            $extension = $request->file('background')->getClientOriginalExtension();
            // File name to store
            $fileNameToStore =  $fileName.'_'.time().'.'.$extension;
            // upload image
        //    $path = $request->file('background-image')->storeAs('image', $fileNameToStore);
            
            $file = $request->background;
            $file->move('image',$fileNameToStore );
            $blog->photo = $fileNameToStore;
        }
        
        
        $blog->title = $request->input('title');
        $blog->paragraph = $request->aboutedit;
        
        $blog->save();
        return  redirect('/admin.allPosts.mahalatmasr2018@fmax0*');
    }
    public function postblog(Request $request)
    {
        $mainpath = 'image';
        $blog = new Blog;
        $blog->paragraph = $request->something;
        $blog->title = $request->title;
        
        
        if($request->hasFile('blogimage')){
            $file = $request->blogimage;
            $photoname = time().$file->getClientOriginalName();     
            $file->move($mainpath,$photoname);
            
            $blog->photo= $photoname;
    }
        
        $blog->save();
        
        foreach($request->categories as $category){
            $cat_blog = new BlogCategory;
            $cat_blog->blog_id = $blog->id;
            $cat_blog->cblog_id = $category;
            $cat_blog->save();
        }
        return redirect('/admin.allPosts.mahalatmasr2018@fmax0*');
    
    }
    public function getads()
    {
        $ads = Ads::get();
        return view('dashBoard.ads.ads',compact('ads'));
    }
    public function postads(Request $request)
    {
        $mainpath = 'image';
        if($request->save1){
            $ads = Ads::find(1);        
            if($request->hasFile('file')) {
                    $file = $request->file;
                    $filename =  time().'.'.$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $ads->files = $filename;
                    $ads->save();
            }
        }elseif($request->save2){
            $ads = Ads::find(2);        
            if($request->hasFile('file')) {
                    $file = $request->file;
                    $filename =  time().'.'.$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $ads->files = $filename;
                    $ads->save();
            }
        }elseif($request->save3){
            $ads = Ads::find(3);        
            if($request->hasFile('file')) {
                    $file = $request->file;
                    $filename =  time().'.'.$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $ads->files = $filename;
                    $ads->save();
            }
        }elseif($request->save4){
            $ads = Ads::find(4);        
            if($request->hasFile('file')) {
                    $file = $request->file;
                    $filename =  time().'.'.$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $ads->files = $filename;
                    $ads->save();
            }
        }elseif($request->save5){
            $ads = Ads::find(5);        
            if($request->hasFile('file')) {
                    $file = $request->file;
                    $filename =  time().'.'.$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $ads->files = $filename;
                    $ads->save();
            }
        }elseif($request->save6){
            $ads = Ads::find(6);        
            if($request->hasFile('file')) {
                    $file = $request->file;
                    $filename =  time().'.'.$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $ads->files = $filename;
                    $ads->save();
            }
        }elseif($request->save7){
            $ads = Ads::find(7);        
            if($request->hasFile('file')) {
                    $file = $request->file;
                    $filename =  time().'.'.$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $ads->files = $filename;
                    $ads->save();
            }
        }elseif($request->save8){
            $ads = Ads::find(8);        
            if($request->hasFile('file')) {
                    $file = $request->file;
                    $filename =  time().'.'.$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $ads->files = $filename;
                    $ads->save();
            }
        }elseif($request->save9){
            $ads = Ads::find(9);        
            if($request->hasFile('file')) {
                    $file = $request->file;
                    $filename =  time().'.'.$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $ads->files = $filename;
                    $ads->save();
            }
        }elseif($request->save10){
            $ads = Ads::find(10);       
            if($request->hasFile('file')) {
                    $file = $request->file;
                    $filename =  time().'.'.$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $ads->files = $filename;
                    $ads->save();
            }
        }
        return redirect('admin.all-Ads.mahalatmasr2018@fmax0*');
    }
    public function getuser()
    {
        $user = User::where('admin',0)->where('vendor',0)->get();
        return view('dashBoard.users.user',compact('user'));
    }
    public function getproduct()
    {
        $product = Product::all();
        return view('dashBoard.product.product',compact('product'));
    }
    public function active(Request $request)
    {
        if($request->vendor == 1){
            $user = User::find($request->id);
            $user->status = 1; // means active
            $user->save();
            $store = Market::where('user_id',$request->id)->get();
            if(count($store) > 0){
                $store[0]->status = 1;
                $store[0]->save();
            }
            $to = $user->email;
        $subject = "تفعيل عضويتك";
        $txt = "تم  تفعيل عضويتك";
           // $headers = "From: admin@filmahal.com";
        $message = "<html><head></head><body><h1>".$txt."</h1></br><img src='http://filmahal.com/imgs/Logo.png' style='width:150px;' alt='' /></br><h1><p>01000061180</p></h1></body></html>";
        $headers  = "From: Filmahal". "\r\n" ."MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
        mail($to,$subject,$message,$headers);
            return response()->json($user->email);
        }elseif($request->vendor == 0){
            $user = User::find($request->id);
            $user->status = 0; // means blocked
            $user->save();
            $store = Market::where('user_id',$request->id)->get();
            if(count($store) > 0){
                $store[0]->status = 0;
                $store[0]->save();
            }
            return response()->json('blocked');
        }elseif($request->vendor == 2){
            $user = User::find($request->id);
            $store = Market::where('user_id',$request->id)->delete();
            $user->delete();
            
            $store = Market::where('user_id',$request->id)->get();
            if(count($store) > 0){
                $store[0]->delete();
            }
            return response()->json('deleted');
        }elseif($request->vendor == 3){
            $user = User::find($request->id);
            $user->status = 2; // means rejected
            $user->reason = $request->rejectarea;
            $user->save();
            
            $store = Market::where('user_id',$request->id)->get();
            if(count($store) > 0){
                $store[0]->status = 2;
                $store[0]->save();
            }
            return response()->json($request->id);
        }elseif($request->vendor == 4){
            $user = User::find($request->id);
            if($request->name !==null){
                $user->user_name = $request->name;
            }
            if($request->email !==null){
                $user->email = $request->email;
            }
            if($request->password !==null){
                $user->password = bcrypt($request->psw);
            }
            $user->save();
            return response()->json('edit');
        }elseif($request->vendor == 5){
            $product = Product::find($request->id);
            $product->status = 1; // means active
            $product->save();
            $market = Market::find($product->market_id);
            $user = User::where('id',$market->user_id)->get();
            $to = $user[0]->email;
        $subject = "تفعيل منتجك";
        $txt = "تم  تفعيل منتجك";
            //$headers = "From: admin@filmahal.com";

        $message = '
                <html>
                    <head>
                    </head>
                    <body style="direction: rtl; text-align: right;">
                        <div style="width: 90%; margin: 10px auto;
                                    border: 1px solid #ccc; border-radius: 5px; padding: 30px; min-width: 320px;">
                            <p style="border-bottom: 2px solid #eee; padding-bottom: 20px;">
                                '.$txt.'
                            </p>
                            <div>
                                <img src="http://filmahal.com/imgs/Logo.png" style="width:150px; float: right;">
                                <div style="float: right; padding: 0 20px;">
                                    <h5><a href="http://www.filmahal.com/" style="text-decoration: none; color: #38ef7d;">http://www.filmahal.com/</a></h5>
                                    <h3 style="color: #11998e;">إدارة في المحل دوت كوم</h3>
                                    <p>01000061180</p>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                    </body>
                    </html>
        ';
        $headers  = "From: Filmahal". "\r\n" ."MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
        mail($to,$subject,$message,$headers);
            return response()->json($user);
        }elseif($request->vendor == 6){
            $product = Product::find($request->id);
            $product->status = 0; // means deactive
            $product->save();
            return response()->json('blocked pro');
        }elseif($request->vendor == 7){
            $product = Product::find($request->id);
            $product->status = 2; // means rejected
            $product->reason = $request->rejectarea;
            $product->save();
            return response()->json('rejected');
        }elseif($request->all == 'all'){
            $product = Product::all();
            return view('dashBoard.product.product',compact('product'));     
        }elseif($request->all == 'reject'){
            $product = Product::where('status',2)->get();
            return view('dashBoard.product.product',compact('product'));     
        }elseif($request->all == 'waiting'){
            $product = Product::where('status',0)->get();
            return view('dashBoard.product.product',compact('product'));     
        }elseif($request->all == 'today'){
            $product = Product::where('created_at', 'LIKE','%'.date('Y-m-d').'%')->get();
            return view('dashBoard.product.product',compact('product'));     
        }elseif($request->all == 'usersall'){
            $user = User::all();
            return view('dashBoard.users.user',compact('user'));     
        }elseif($request->all == 'usersreject'){
            $user = User::where('status',2)->get();
            return view('dashBoard.users.user',compact('user'));     
        }elseif($request->all == 'userswaiting'){
            $user = User::where('status',0)->get();
            return view('dashBoard.users.user',compact('user'));     
        }elseif($request->all == 'userstoday'){
            $user = User::where('created_at', 'LIKE','%'.date('Y-m-d').'%')->get();
            return view('dashBoard.users.user',compact('user'));     
        }elseif($request->all == 'vendall'){
            $user = User::where('vendor',1)->get();
            return view('dashBoard.vendor.vendor',compact('user'));     
        }elseif($request->all == 'vendreject'){
            $user = User::where('vendor',1)->where('status',2)->get();
            return view('dashBoard.vendor.vendor',compact('user'));     
        }elseif($request->all == 'vendwaiting'){
            $user = User::where('vendor',1)->where('status',0)->get();
            return view('dashBoard.vendor.vendor',compact('user'));     
        }elseif($request->all == 'vendtoday'){
            $user = User::where('vendor',1)->where('created_at', 'LIKE','%'.date('Y-m-d').'%')->get();
            return view('dashBoard.vendor.vendor',compact('user'));     
        }elseif($request->all == 'comall'){
            $review = Review::all();
            return view('dashBoard.review.all',compact('review'));     
        }elseif($request->all == 'comreject'){
            $review = Review::where('status',2)->get();
            return view('dashBoard.review.all',compact('review'));     
        }elseif($request->all == 'comwaiting'){
            $review = Review::where('status',0)->get();
            return view('dashBoard.review.all',compact('review'));     
        }elseif($request->all == 'comtoday'){
            $review = Review::where('created_at', 'LIKE','%'.date('Y-m-d').'%')->get();
            return view('dashBoard.review.all',compact('review'));     
        }   
    }
    public function getvendor()
    {
        $user = User::where('admin',0)->where('vendor',1)->get();
        return view('dashBoard.vendor.vendor',compact('user'));
    }
    public function geteditArea($id)
    {
        $city = Area::where('city_id',$id)->get();
        return view('dashBoard.cities.edit',compact('city','id'));
    }
    public function posteditArea(Request $request,$id)
    {
        $areaeditname = $request->editcityname;
        $areaeditId = $request->editcityId;
        if(!empty($areaeditId)){
            foreach($areaeditId as $key => $Ids){
                $oldarea = Area::where('id',$Ids)->get();
                $oldarea[0]->name = $areaeditname[$key];
                $oldarea[0]->save();
            }
        }
        return redirect('/admin.edit-areas/'.$id.'.mahalatmasr2018@fmax0*');
       // return response()->json($areaeditId);
        
    }
    public function deleteEditarea($areaId,$id)
    {
        $oldarea = Area::where('id',$areaId)->get();
        $oldcity = City::where('id',$id)->get();
        $areaarray = Area::where('city_id',$id)->get();
        if(count($areaarray) == 1){
            City::where('id',$id)->delete();
            Area::where('id',$areaId)->delete();
            return redirect('/admin/cities-areas');
        }else{
            Area::where('id',$areaId)->delete();
            return redirect('/admin/edit-areas/'.$id);
        }
        
        
    }
    public function getattvalues($id)
    {
        $prop = Properties::where('attribute_id',$id)->get();
        return view('dashBoard.category.edit',compact('prop','id'));
    }
    public function deleteAttribute($propID,$id)
    {
        $prop = Properties::where('attribute_id',$id)->get();
        if(count($prop) == 1){
            Attribute::where('id',$id)->delete();
            $subproperty = Aprop::where('att_properties_id',$prop[0])->delete();
            Properties::where('attribute_id',$id)->delete();
            return redirect('/admin.attributes.mahalatmasr2018@fmax0*#all');
        }else{
            Properties::where('id',$propID)->delete();
            $subproperty = Aprop::where('att_properties_id',$propID)->delete();
            return redirect('/admin.attributes-values/'.$id.'.mahalatmasr2018@fmax0*');
        }
    }
    public function deleteSelectedAtt(Request $reques)
    {
        $id = $request->Ids;
        Attribute::where('id',$id)->delete();
        $prop = Properties::where('attribute_id',$id)->get();
        foreach($prop as $proptodel){
            Aprop::where('att_properties_id',$proptodel->id)->delete();
        }
        Properties::where('attribute_id',$id)->delete();
        return response()->json('delete success');
    }
    public function deletesubproperty(Request $request)
    {
        $propID = $request->propID;
        $subproperty = Aprop::where('att_properties_id',$propID)->get();
        if(count($subproperty) == 1){
            Attribute::where('id',$request->attID)->delete();
            Aprop::where('id',$request->id)->delete();
            Properties::where('attribute_id',$propID)->delete();
            return response()->json('success1');
        }else{
            Aprop::where('id',$request->id)->delete();
            return response()->json('success2');
        }
        
    }
    public function postattvalues(Request $request,$id)
    {
        $editpropId = $request->editpropId;
        $editpropname = $request->editpropname;
        
        $editsubpropId = $request->editsubpropId;
        $editsubpropname = $request->editsubpropname;
        if($editsubpropId !== 'NULL' && $editsubpropId !=="" && $editsubpropId !==NULL){
            foreach($editpropId as $key => $editpropsId){
                    $property = Properties::where('id',$editpropsId)->get();
                
                    $property[0]->name = $editpropname[$key];
                    $property[0]->save();
                  /*  $subproperty = Aprop::where('att_properties_id',$editpropsId)->get();
                    $subproperty[0]->name = $editsubpropname[$key];
                    $subproperty[0]->save();*/
            }
            foreach($editsubpropId as $key => $editsubpropIds){
                    $subproperty = Aprop::where('id',$editsubpropIds)->get();
                    $subproperty[0]->name = $editsubpropname[$key];
                    $subproperty[0]->save();
            }
            
            
        }else{
            foreach($editpropId as $key => $editpropsId){
                $property = Properties::where('id',$editpropsId)->get();
                
                    $property[0]->name = $editpropname[$key];
                    $property[0]->save();
                    
            }
        }
        return redirect('/admin.attributes-values/'.$id.'.mahalatmasr2018@fmax0*');
         //   return redirect('/admin/attributes');
          //  return response()->json($subproperty);
    }
    public function getcity()
    {
        $city = City::all();
        return view('dashBoard.cities.city',compact('city'));
    }
    public function postcity(Request $request)
    {
        if($request->save1){
            $cityname = $request->governorates;
            $oldcity = City::where('name',$cityname)->get();
            if(count($oldcity) == 0){
                $city = new City;
                $city->name = $cityname;
                $city->save();
                
                $areaname = $request->area;
                foreach($areaname as $areas){
                    $oldarea = Area::where('name',$areaname)->where('city_id',$city->id)->get();
                    if(count($oldarea) == 0){
                        $area = new Area;
                        $area->name = $areas;
                        $area->city_id = $city->id;
                        $area->save();
                    }
                }
            }else{
                $areaname = $request->area;
                foreach($areaname as $areas){
                    $oldarea = Area::where('name',$areaname)->where('city_id',$oldcity[0]->id)->get();
                    if(count($oldarea) == 0){
                        $area = new Area;
                        $area->name = $areas;
                        $area->city_id = $oldcity[0]->id;
                        $area->save();
                    }
                }
            }
            return response()->json('success1');
        }
        
    }
    
    public function getAttr()
    {
        $att = Attribute::all();
        return view('dashBoard.category.attr',compact('att'));
    }
  /*  public function getsubatt(Request $request)
    {
        $att1 = Properties::where('attribute_id',$request->id)->get();
        $attId = Properties::where('attribute_id',$request->id)->select('id')->get();
        $prop1 = Aprop::whereIn('att_properties_id',$attId)->get();
        return response()->json(['properties'=>$att1,'propval'=>$prop1]);
    //    return response()->json($att1);
    }
    public function getsubattprop(Request $request)
    {
        $attprop = Aprop::where('att_properties_id',$request->id)->get();
  
        return response()->json($attprop);
    }*/
    public function postAttr(Request $request)
    {
        if($request->save1){
            if($request->collectionedit == '0'){
                $attname = $request->collectionName;
                $attribute = new Attribute;
                $validAtt = Attribute::where('name',$attname)->get();
                if(count($validAtt) == 0){
                    $attribute->name = $attname;
                    $attribute->save();
                    
                    $propertiesname = $request->newAttr;
                    $validProp = Properties::where('name',$propertiesname)->get();
                    if(count($validProp) ==0){
                            $property = new Properties;
                            $property->name = $propertiesname;
                            $property->attribute_id = $attribute->id;
                            $property->type = $request->choose;
                            $property->save();
                            $subpropname = $request->subprop;
                            if($request->choose == 3){
                                    $aprop = new Aprop;
                                    $aprop->name = $subpropname;
                                    $aprop->att_properties_id = $property->id;
                                    $aprop->save();
                            
                            }else{
                                foreach($subpropname as $subname){
                                    $aprop = new Aprop;
                                    $aprop->name = $subname;
                                    $aprop->att_properties_id = $property->id;
                                    $aprop->save();
                            }
                        }
                        
                     }else{
                            $subpropname = $request->subprop;
                            if($request->choose == 3){
                                    $aprop = new Aprop;
                                    $aprop->name = $subpropname;
                                    $aprop->att_properties_id = $validProp[0]->id;
                                    $aprop->save();
                            
                            }else{
                                foreach($subpropname as $subname){
                                    $aprop = new Aprop;
                                        $aprop->name = $subname;
                                        $aprop->att_properties_id = $validProp[0]->id;
                                        $aprop->save();
                                }
                            }
                    }                   
                    
                    return response()->json('added success');
                }else{
                   
                    $propertiesname = $request->newAttr;
                    $validProp = Properties::where('name',$propertiesname)->get();
                    if(count($validProp) ==0){
                        $property = new Properties;
                        $property->name = $propertiesname;
                        $property->attribute_id = $validAtt[0]->id;
                        $property->type = $request->choose;
                    
                        $property->save();
                        $subpropname = $request->subprop;
                        if($request->choose == 3){
                                    $aprop = new Aprop;
                                    $aprop->name = $subpropname;
                                    $aprop->att_properties_id = $property->id;
                                    $aprop->save();
                            
                 }else{
                            foreach($subpropname as $subname){
                                $aprop = new Aprop;
                                $aprop->name = $subname;
                                $aprop->att_properties_id = $property->id;
                                $aprop->save();
                        }
                     }
                
                    }else{
                        $subpropname = $request->subprop;
                       if($request->choose == 3){
                                    $aprop = new Aprop;
                                    $aprop->name = $subpropname;
                                    $aprop->att_properties_id = $validProp[0]->id;
                                    $aprop->save();
                            
                   }else{
                            foreach($subpropname as $subname){
                                $aprop = new Aprop;
                                $aprop->name = $subname;
                                $aprop->att_properties_id = $validProp[0]->id;
                                $aprop->save();
                            }
                       }
                    }
                    
                }
                
            }else{
                $Att = Attribute::where('id',$request->collectionedit)->get();
                $Att[0]->name = $request->collectionName;
                $Att[0]->save();
                
                $Prop = Properties::where('attribute_id',$request->collectionedit)->get();
                $Prop->name = $request->subprop;
                $Prop->save();
                
                $AProp = Aprop::where('att_properties_id',$Prop[0])->get();
            }
            return response()->json('success');
        }
        
        
        
    }
    public function getAdmin()
    {
        $category = Category::all();
        $attribute = Attribute::all();
        return view('dashBoard.index',compact('category','attribute'));
    }
    
    public function postAdmin(Request $request)
    {
        $addsome = $request->selectcates;
        $cates = $request->cates;
        if($request->save == '1'){
            if($addsome == 00){
                $cat = new Category;
                $cat->name = $cates;
                $cat->attribute_id = $request->selectatts;
                $cat->save();
            }else if($addsome[0] == 0){
                $subcat = new Sub;
                $subcat->name = $cates;
                $Ids = str_split($addsome);
                $catid = array_slice($Ids,1);
                $category_id = implode($catid);
                $subcat->category_id = $category_id;
                $subcat->save();
            }else if($addsome[0] == 1){
                $subsubcat = new Target;
                $subsubcat->name = $cates;
                $Ids = str_split($addsome);
                $catid = array_slice($Ids,1);
                $sub_category_id = implode($catid);
                $subsubcat->sub_id = $sub_category_id;
                $subsubcat->save();
            }
            $category = Category::all();
            $sub = Sub::all();
            $taregt = Target::all();
            return response()->json(['cat'=>$category,'sub'=>$sub,'subsub'=>$taregt]);
        }elseif($request->save2 == '2'){

            $name1 = $request->newcat;
            $newsub = $request->newsub;
            $newsubsub = $request->newsubsub;
            $textarea1 = $request->textarea1;
            $textarea2 = $request->textarea2;
            $textarea3 = $request->textarea3;

            $catIds = $request->catIds;
            $arrayCatIds = array($catIds);
            $subsIds = $request->subsIds;
            $arraySubIds = array($subsIds);
            $subsubsIds = $request->subsubsIds;
            $arraySubSubIds = array($subsubsIds);
            if(count($arrayCatIds) !==0){
                foreach($arrayCatIds as $key => $arrayCatIds) {
                    $updatecat = Category::where('id',$arrayCatIds)->get();
                    if(count($updatecat)!==0){
                        $updatecat[0]->name = $name1[$key];
                        $updatecat[0]->description = $textarea1[$key];
                        $updatecat[0]->save();
                    }
                    
                }
                                
            }
            if(count($arraySubIds) !==0){
                
                foreach($arraySubIds as $key => $arraySubIds) {
                    $updatesub = Sub::where('id',$arraySubIds)->get();
                    if(count($updatesub)!==0){
                        $updatesub[0]->name = $newsub[$key];
                        $updatesub[0]->description = $textarea2[$key];
                        $updatesub[0]->save();
                    }
                }
            }
            if(count($arraySubSubIds) !==0){
                
                foreach($arraySubSubIds as $key => $arraySubSubIds) {
                    $updatesubsub = Target::where('id',$arraySubSubIds)->get();
                    if(count($updatesubsub)!==0){
                        $updatesubsub[0]->name = $newsubsub[$key];
                        $updatesubsub[0]->description = $textarea3[$key];
                        $updatesubsub[0]->save();
                    }
                }
            }
            return response()->json('true');
        }elseif($request->delete == '1'){
            $Ids = $request->Ids;
            $newIds = str_split($Ids);
            $IdsToDelete = array_slice($newIds,1);
            $IdsAll = implode($IdsToDelete);
            if($Ids[0] == 'c'){
                Category::where('id',$IdsAll)->delete();
                $sub = Sub::where('category_id',$IdsAll)->get();
                Target::where('sub_id',$sub[0]->id)->delete();
                Sub::where('category_id',$IdsAll)->delete();
            }if($Ids[0] == 's'){
                Sub::where('id',$IdsAll)->delete();
                Target::where('sub_id',$IdsAll)->delete();
            }if($Ids[0] == 't'){
                Target::where('id',$IdsAll)->delete();
            }
            return response()->json('true1');
        }elseif($request->delete == '2'){
            $Ids = $request->Ids;
            Attribute::where('id',$Ids)->delete();
            $prop = Properties::where('attribute_id',$Ids)->get();
           
            if(count($prop)>0){
                foreach($prop as $proptodel){
                    $aprop1 = Aprop::where('att_properties_id',$proptodel->id)->get();
                    if(count($aprop1)>0){
                        Aprop::where('att_properties_id',$proptodel->id)->delete();
                    }
                }
                Properties::where('attribute_id',$id)->delete();
            }
            
            return response()->json('attr delete success');
        }elseif($request->delete == '3'){
            $Ids = $request->Ids;
            City::where('id',$Ids)->delete();
            Area::where('city_id',$Ids)->delete();
            return response()->json('true3');
        }elseif($request->delete == '4'){
            $user = User::where('id',$request->Ids)->delete();
            $store = Market::where('user_id',$request->Ids)->delete();
            $deleteReview = Review::where('market_id',$request->Ids)->delete();
            return response()->json('deleted');
        }elseif($request->delete == '5'){
            $pro = Product::where('id',$request->Ids)->delete();
            return response()->json('deleted');
        }elseif($request->delete == '55'){
            $pro = Review::where('id',$request->Ids)->delete();
                return response()->json('deleted');
        }
    }
    public function sendmail(Request $request){
        $mail = $request->email;
        //
        $to = "trans2c2017@gmail.com";
        $subject = "أرسل لى عندما يكون متاح";
        $txt = "أرسل لى عندما يكون متاح";
        $from= "From:".$mail;
    
        mail($to,$subject,$txt,$from);
        
        return response()->json('true');
    }
    
    
    // content functions

    public function getFAQ() {
        return view('dashBoard.contentPages.FAQ');
    }
    public function postFAQ(Request $request) {
        $terms = DB::table('setting')->where('id',1)->update(['paragraph'=>$request->something]);
            
            $mainpath = 'image';
        if ($request->hasFile('photo')) {
            $file = $request->photo;
                $photoname = time().$file->getClientOriginalName();     
                $file->move($mainpath,$photoname);
            
                $terms = DB::table('setting')->where('id',1)->update(['photo'=>$photoname]);
                
     }
        
        return redirect('/admin.FAQ.mahalatmasr2018@fmax0*');
    }
    public function getAbout() {
        return view('dashBoard.contentPages.who-are-we');
    }
    public function postAbout(Request $request) {
        $terms = DB::table('setting')->where('id',2)->update(['paragraph'=>$request->something]);
        $mainpath = 'image';
        if ($request->hasFile('photo')) {
            $file = $request->photo;
                $photoname = time().$file->getClientOriginalName();     
                $file->move($mainpath,$photoname);
            
                $terms = DB::table('setting')->where('id',2)->update(['photo'=>$photoname]);
    }
        
        return redirect('/admin.about.mahalatmasr2018@fmax0*');
    }
    public function getTerms() {
        return view('dashBoard.contentPages.vendor-terms');
    }
    public function postTerms(Request $request) {
        $terms = DB::table('setting')->where('id',3)->update(['paragraph'=>$request->something]);
        $mainpath = 'image';
        if ($request->hasFile('photo')) {
            $file = $request->photo;
                $photoname = time().$file->getClientOriginalName();     
                $file->move($mainpath,$photoname);
            
                $terms = DB::table('setting')->where('id',3)->update(['photo'=>$photoname]);
    }
        
        return redirect('/admin.terms.mahalatmasr2018@fmax0*');
    }
    public function getMobile() {
        return view('dashBoard.contentPages.mobile-app');
    }
    public function postMobile(Request $request) {
        $terms = DB::table('setting')->where('id',5)->update(['paragraph'=>$request->something]);
        $mainpath = 'image';
        if ($request->hasFile('photo')) {
            $file = $request->photo;
                $photoname = time().$file->getClientOriginalName();     
                $file->move($mainpath,$photoname);
            
                $terms = DB::table('setting')->where('id',5)->update(['photo'=>$photoname]);
    }
        
        return redirect('/admin.mobile.mahalatmasr2018@fmax0*');
    }
    public function getSupport() {
        return view('dashBoard.contentPages.support');
    }
    public function postSupport(Request $request) {
        $terms = DB::table('setting')->where('id',4)->update(['paragraph'=>$request->something]);
        $mainpath = 'image';
        if ($request->hasFile('photo')) {
            $file = $request->photo;
                $photoname = time().$file->getClientOriginalName();     
                $file->move($mainpath,$photoname);
            
                $terms = DB::table('setting')->where('id',4)->update(['photo'=>$photoname]);
    }
        
        return redirect('/admin.technical-support.mahalatmasr2018@fmax0*');
    }
    public function getPrivileges() {
        return view('dashBoard.contentPages.privileges');
    }
    public function postPrivileges(Request $request) {
        $terms = DB::table('setting')->where('id',7)->update(['paragraph'=>$request->something]);
        $mainpath = 'image';
        if ($request->hasFile('photo')) {
            $file = $request->photo;
                $photoname = time().$file->getClientOriginalName();     
                $file->move($mainpath,$photoname);
            
                $terms = DB::table('setting')->where('id',7)->update(['photo'=>$photoname]);
    }
        
        return redirect('/admin.privileges.mahalatmasr2018@fmax0*');
    }
    public function getProperty() {
        return view('dashBoard.contentPages.property');
    }
    public function postProperty(Request $request) {
        $terms = DB::table('setting')->where('id',6)->update(['paragraph'=>$request->something]);
        $mainpath = 'image';
        if ($request->hasFile('photo')) {
            $file = $request->photo;
                $photoname = time().$file->getClientOriginalName();     
                $file->move($mainpath,$photoname);
            
                $terms = DB::table('setting')->where('id',6)->update(['photo'=>$photoname]);
    }
        
        return redirect('/admin.intellectual-property.mahalatmasr2018@fmax0*');
    }
    public function getAdsUs() {
        return view('dashBoard.contentPages.add-ad');
    }
    public function postAdsUs(Request $request) {
        $terms = DB::table('setting')->where('id',8)->update(['paragraph'=>$request->something]);
        $mainpath = 'image';
        if ($request->hasFile('photo')) {
            $file = $request->photo;
                $photoname = time().$file->getClientOriginalName();     
                $file->move($mainpath,$photoname);
            
                $terms = DB::table('setting')->where('id',8)->update(['photo'=>$photoname]);
    }
        
        return redirect('/admin.ads-with-us.mahalatmasr2018@fmax0*');
    }
    
    
    public function getphotoCats() {
        $sub = Sub::all();
        
        return view('dashBoard.catimage',compact('sub'));
    }
    public function postphotoCats(Request $request) {
        $photo = Photo::where('sub_category_id',$request->subId)->get();
        if(count($photo) > 0){
            $mainpath = 'image';
        if ($request->hasFile('logo')) {
            $file = $request->logo;
                $photoname = time().$file->getClientOriginalName();     
                $file->move($mainpath,$photoname);
                
                $photo[0]->photo = $photoname;
                
                $photo[0]->save();
        }
            return redirect('/admin/sub-categories/photos');
        
        }else{
            $photo = new Photo;
            $mainpath = 'image';
        
        $file = $request->logo;
            $photoname = time().$file->getClientOriginalName();     
            $file->move($mainpath,$photoname);
            
            $photo->photo = $photoname;

        $photo->sub_category_id= $request->subId;
            
            $photo->save();
            return redirect('/admin/sub-categories/photos');
        
        }
   }
   public function getrules() {
        return view('dashBoard.contentPages.rules');
   }
   public function postrules(Request $request) {
        $rules = DB::table('setting')->where('id',13)->update(['paragraph'=>$request->something]);
        $mainpath = 'image';
        if ($request->hasFile('photo')) {
            $file = $request->photo;
                $photoname = time().$file->getClientOriginalName();     
                $file->move($mainpath,$photoname);
            
                $terms = DB::table('setting')->where('id',3)->update(['photo'=>$photoname]);
    }
        
        return redirect('/admin.rules.mahalatmasr2018@fmax0*');
   }
   public function getsendNews() {
        $subscribe = Subscribe::all();
        return view('dashBoard.newsletter.send',compact('subscribe'));
   }
   public function getsub() {
        $subscribe = Subscribe::all();
        return view('dashBoard.newsletter.subscribers',compact('subscribe'));
   }
   public function getprivacy() {
        return view('dashBoard.contentPages.privacy');
   }
   public function postprivacy(Request $request) {
        $privce = DB::table('setting')->where('id',12)->update(['paragraph'=>$request->something]);
        $mainpath = 'image';
        if ($request->hasFile('photo')) {
            $file = $request->photo;
                $photoname = time().$file->getClientOriginalName();     
                $file->move($mainpath,$photoname);
            
                $terms = DB::table('setting')->where('id',3)->update(['photo'=>$photoname]);
    }
        
        return redirect('/admin.privacy.mahalatmasr2018@fmax0*');
   }
   public function deletesub(Request $request) {
        $sub = Subscribe::where('id',$request->id)->delete();
        return response()->json('subscriber deleted');
   }
   public function postsendNews(Request $request) {
        $text = $request->something;
        $sub = Subscribe::where('id',$request->ids)->get();
        
        $to = $sub[0]->email;
        $subject = "thanks for your order";
        $message = ' <html>
        <head>
        </head>
        <body style="direction: rtl; text-align: right;">
            <div style="width: 90%; margin: 10px auto;
                        border: 1px solid #ccc; border-radius: 5px; padding: 30px; min-width: 320px;">
                <div style="border-bottom: 2px solid #eee; padding-bottom: 20px;">
                    '.$text.'
                </div>
                <div>
                    <img src="http://filmahal.com/imgs/Logo.png" style="width:150px; float: right;">
                    <div style="float: right; padding: 0 20px;">
                        <h5><a href="http://www.filmahal.com/" style="text-decoration: none; color: #38ef7d;">http://www.filmahal.com/</a></h5>
                        <h3 style="color: #11998e;">إدارة في المحل دوت كوم</h3>
                        <p>01000061180</p>
                        <a style="text-decoration: none;" href="http://filmahal.com/unsubscribe-us/"'.$to.'>الغاء المتابعه</a>

                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </body>
        </html>';
        $headers  = "From: Filmahal". "\r\n" ."MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
        
       mail($to,$subject,$message,$headers);
       
        return response()->json($sub[0]->email);
   }
   public function getcomments(Request $request) {
        $review = Review::all();
        if(Auth::user()->admin == 1){
            $image  = ImageLogo::where('id',1)->first(); 
            return view('dashBoard.review.all',['image'=>$image],compact('review'));
        }
   }
   public function comments(Request $request) {
    
        $review = Review::find($request->id);
        if($request->comment == '2'){
            $review->status = 2;
            $review->save();
        }elseif($request->comment == '1'){
            $review->status = 1;
            $review->save();
        }
        return response()->json($review);

   }
   public function getLinks() {
        $contact = Contact_us::all();
        $image  = ImageLogo::where('id',1)->first();    
        return view('dashBoard.contactLinks',compact('contact'),['image'=>$image]);
   }
    public function postLinks(Request $request) {

        $contact_us =  Contact_us::get();

        $contact_us[0]->data = $request->mobile1;
        $contact_us[1]->data = $request->mobile2;
        $contact_us[2]->data = $request->whatsapp;
        $contact_us[3]->data = $request->email;
        $contact_us[4]->data = $request->facebook;
        $contact_us[5]->data = $request->instagram;
        $contact_us[6]->data = $request->google;
        $contact_us[7]->data = $request->twitter;
        $contact_us[8]->data = $request->youtube;

        $contact_us[0]->save();
        $contact_us[1]->save();
        $contact_us[2]->save();
        $contact_us[3]->save();
        $contact_us[4]->save();
        $contact_us[5]->save();
        $contact_us[6]->save();
        $contact_us[7]->save();
        $contact_us[8]->save();

        return redirect('/admin.contact_links.mahalatmasr2018@fmax0*');
    }
   public function getdaynamic(Request $request) {
    
        if(Auth::user()->admin == 1){
        
            return view('dashBoard.mahalData');
        }
   }
    public function postdaynamic(Request $request) {
        $mainpath = 'image';
        
        $insert1 = DB::table('dynamic_home')->where('id',1)->update(['kind'=> 'title' , 'data' => $request->homeTitle]);
        $insert2 = DB::table('dynamic_home')->where('id',2)->update(['kind'=> 'mobile1' , 'data' => $request->mobileFooter1]);
        $insert3 = DB::table('dynamic_home')->where('id',3)->update(['kind'=> 'mobile2' , 'data' => $request->mobileFooter2]);
        $insert4 = DB::table('dynamic_home')->where('id',4)->update(['kind'=> 'facebook' , 'data' => $request->social1]);
        $insert5 = DB::table('dynamic_home')->where('id',5)->update(['kind'=> 'Youtube' , 'data' => $request->social2]);
        $insert6 = DB::table('dynamic_home')->where('id',6)->update(['kind'=> 'Instagram' , 'data' => $request->social3]);
        $insert7 = DB::table('dynamic_home')->where('id',7)->update(['kind'=> 'Twitter' , 'data' => $request->social4]);
        $insert8 = DB::table('dynamic_home')->where('id',8)->update(['kind'=> 'Google' , 'data' => $request->social5]);
        if($request->hasFile('filamahlLogo')){
        $file = $request->filamahlLogo;
        
            $filename = time().$file->getClientOriginalName();
            $file->move($mainpath,$filename);
            
            $insert9 = DB::table('dynamic_home')->where('id',9)->update(['kind'=> 'log' , 'data' => $filename]);
            
        }
        return redirect('/admin/dynamic-home');
    }
    public function imageLogo(Request $request)
    {
        if($request->hasFile('IMAGE')){
            $image  = ImageLogo::where('id',1)->first();        
            $fileNameWithExt = $request->file('IMAGE')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('IMAGE')->getClientOriginalExtension();
            $fileNameToStore =  $fileName.'_'.time().'.'.$extension;
            $file = $request->IMAGE;
            $file->move('image',$fileNameToStore );
            $image->value = $fileNameToStore;
            $image->save();
        }

        return response()->json([
            'status'=> 200,
            'success'=>true,
            'data'=>$image
        ]);
    }
    public function all()
    {
        $review = Review::all();
        return response()->json([
            'status'=> 200,
            'success'=>true,
            'data'=>$review
        ]);
    }
    public function reject()
    {
        $review = Review::where('status',2)->get();
        return response()->json([
            'status'=> 200,
            'success'=>true,
            'data'=>$review
        ]);
    }
    public function waiting()
    {
        $review = Review::where('status',0)->get();
        return response()->json([
            'status'=> 200,
            'success'=>true,
            'data'=>$review
        ]);
    }
    public function today()
    {
        $review = Review::whereDate('created_at', Carbon::today())->get();
        return response()->json([
            'status'=> 200,
            'success'=>true,
            'data'=>$review
        ]);
    }
}
