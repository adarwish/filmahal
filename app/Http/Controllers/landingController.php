<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Area;
use App\Market;
use App\Mplace;
use App\Mcat;
use App\Subscribe;
use App\Product;
use App\Attribute;
use App\Matt;
use App\Photo;
use App\Save;
use App\Counters;
use App\Category;
use App\Settings;
use App\ImageLogo;
class landingController extends Controller
{
    public function getlanding()
    {
        $city1 = City::where('id',8)->get();
        $city = City::where('id','!=',8)->get();
        $area = Area::where('city_id',8)->get();
        $videos = Settings::all();
        $image  = ImageLogo::where('id',1)->first(); 
        return view('site.index')->withImage($image)->withCity($city)->withCity1($city1)->withArea($area)->withVideos($videos);
    }
    public function getsearchresult(Request $request)
    {
        $city = City::all();
        
        if($request->searchbutton == 'عرض المحلات' || $request->search){
         //  return response()->json(1);
            $mahalname = $request->search;
            $areaname = $request->getarea;
            $cityId = Area::where('id',$areaname)->get();
            $mahalId = Mplace::where('area_id',$areaname)->select('market_id')->get();
            if(count($mahalId) > 0){
                $mahal = Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->where('status',1)->get();
                $mahalCatId = Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->where('status',1)->select('id')->get();
                $mahalId2 =  Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->where('status',1)->select('id')->get();
                if(count($mahal) == 0){
                    $productID = Product::where('name','LIKE','%'.$mahalname.'%')->select('market_id')->get();
                    $mahalProId = Mplace::where('area_id',$areaname)->whereIn('market_id',$productID)->select('market_id')->get();
                    $mahal = Market::whereIn('id',$mahalProId)->where('status',1)->get();
                    $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->where('status',1)->get();
                    if(count($mahal) == 0){
                            
                        $categoryStoreID = Category::where('name','LIKE','%'.$mahalname.'%')->select('id')->get();
                        $storeSaerchId = Mcat::whereIn('category_id',$categoryStoreID)->select('market_id')->get();
                        $mahalProId = Mplace::where('area_id',$areaname)->whereIn('market_id',$storeSaerchId)->select('market_id')->get();
                        $mahal = Market::whereIn('id',$mahalProId)->where('status',1)->get();
                        $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->where('status',1)->get();
                        
                    }
                }
                $savedCity = City::where('id',$cityId[0]->city_id)->get();
                $savedArea = Area::where('id',$areaname)->get();
                $area = Area::where('city_id',$cityId[0]->city_id)->where('id','!=',$areaname)->get();
            }else{
                if($areaname == "" || $areaname == 'null' || $areaname == 'NULL'){
                    $savedCity = City::where('id',8)->get();
                $savedArea = Area::where('id',$areaname)->get();
            //  $savedCity = Area::where('city_id',$savedArea[0]->city_id)->get();
                
                $area = Area::where('city_id',8)->where('id','!=',$areaname)->get();
                $areaID = Area::where('city_id',8)->where('id','!=',$areaname)->select('id')->get();
                $mahalId = Mplace::whereIn('area_id',$areaID)->select('market_id')->get();
                
                $mahal = Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->where('status',1)->get();
                $mahalCatId = Market::where('title','LIKE','%'.$mahalname.'%')->select('id')->where('status',1)->get();
                $mahalId2 =  Market::where('title','LIKE','%'.$mahalname.'%')->select('id')->where('status',1)->get();
                    
                    if(count($mahal) == 0){
                        $productID = Product::where('name','LIKE','%'.$mahalname.'%')->select('market_id')->get();
                        $mahalProId = Mplace::whereIn('area_id',$areaID)->whereIn('market_id',$productID)->select('market_id')->get();
                        $mahal = Market::whereIn('id',$mahalProId)->where('status',1)->get();
                        $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->where('status',1)->get();
                        $mahalId2 =  Market::whereIn('id',$mahalProId)->select('id')->where('status',1)->get();
                        
                        if(count($mahal) == 0){
                                
                            $categoryStoreID = Category::where('name','LIKE','%'.$mahalname.'%')->select('id')->get();
                            $storeSaerchId = Market::whereIn('id',$categoryStoreID)->where('status',1)->get();
                            $mahalProId = Mplace::where('area_id',$areaID)->whereIn('market_id',$storeSaerchId)->select('market_id')->get();
                            $mahal = Market::whereIn('id',$mahalProId)->where('status',1)->get();
                            $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->get();
                        }
                    }
            }else{
             
             
                //$savedCity = City::where('id',8)->get();
                $savedArea = Area::where('id',$areaname)->get();
                $savedCity = City::where('id',$savedArea[0]->city_id)->get();
                
                $area = Area::where('city_id',$savedArea[0]->city_id)->get();
                $areaID = Area::where('city_id',$savedArea[0]->city_id)->select('id')->get();
                $mahalId = Mplace::whereIn('area_id',$areaID)->select('market_id')->get();
                
                $mahal = Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->where('status',1)->get();
                $mahalCatId = Market::where('title','LIKE','%'.$mahalname.'%')->select('id')->where('status',1)->get();
                $mahalId2 =  Market::where('title','LIKE','%'.$mahalname.'%')->select('id')->where('status',1)->get();
            
                if(count($mahal) == 0){
                    $productID = Product::where('name','LIKE','%'.$mahalname.'%')->select('market_id')->get();
                    $mahalProId = Mplace::whereIn('area_id',$areaID)->whereIn('market_id',$productID)->select('market_id')->get();
                    $mahal = Market::whereIn('id',$mahalProId)->where('status',1)->get();
                    $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->where('status',1)->get();
                    $mahalId2 =  Market::whereIn('id',$mahalProId)->select('id')->where('status',1)->get();
                    
                    if(count($mahal) == 0){
                            
                        $categoryStoreID = Category::where('name','LIKE','%'.$mahalname.'%')->select('id')->get();
                        $storeSaerchId = Market::whereIn('id',$categoryStoreID)->where('status',1)->get();
                        $mahalProId = Mplace::where('area_id',$areaID)->whereIn('market_id',$storeSaerchId)->select('market_id')->get();
                        $mahal = Market::whereIn('id',$mahalProId)->where('status',1)->get();
                        $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->where('status',1)->get();
                    }
                }
             
            }
        }
    

        $categoryId = Mcat::whereIn('market_id',$mahalCatId)->select('category_id')->get();
        $samemahalId = Mcat::whereIn('category_id',$categoryId)->select('market_id')->get();
        $samemahal = Market::whereIn('id',$samemahalId)->whereNotIn('id',$mahalCatId)->where('status',1)->get();
       

        $areaId = Mplace::whereIn('market_id',$mahalCatId)->select('area_id')->get();

        $categoryId2 = Mcat::whereNotIn('market_id',$mahalCatId)->select('category_id')->get();

        $samemahalId2 = Mcat::whereIn('category_id',$categoryId2)->select('market_id')->get();
        $samemahal2 = Market::whereIn('id',$samemahalId2)->whereNotIn('id',$mahalCatId)->where('status',1)->get();
    
        
        if($request->getarea == NULL || $request->getarea == ""){
                $savedCity = City::where('id',8)->get();
                $savedArea = Area::where('id',$areaname)->get();
                $area = Area::where('city_id',8)->where('id','!=',$areaname)->get();
            //return response()->json($savedArea);
        }
    
        $searchval = $request->search;
        $image  = ImageLogo::where('id',1)->first();
        return view('site.search.index',['image'=>$image],compact('mahal','samemahal','city','area','samemahal2','savedCity','savedArea','mahalId2','searchval'));
            
        }elseif($request->sort){
            if($request->sort == 1){
                
                $stores2 = str_replace('[{',"",$request->sorting); 
                $stores3 = str_replace('&quot',"",$stores2); 
                $stores4 = str_replace('{',"",$stores3); 
                $stores5 = str_replace(';',"",$stores4);
                $stores6 = str_replace('id',"",$stores5);
                $stores7 = str_replace('}]',"",$stores6);
                $stores8 = str_replace('}',"",$stores7);
                $stores9 = str_replace(':',"",$stores8); 
                $stores = explode(",",$stores9); 
                
                $product = Product::whereIn('market_id',$stores)->orderBy('discount','ASC')->select('market_id')->get();
                $product2 = Product::whereIn('market_id',$stores)->orderBy('discount','ASC')->get();
                $mainphoto = Photo::whereIn('market_id',$stores)->where('sub_category_id',null)->where('product_id',null)->get();
                $mahal = Market::whereIn('id',$product)->where('status',1)->get();
                return response()->json(['store'=> $mahal,'products'=>$product2,'storephoto'=>$mainphoto]);
                
            }elseif($request->sort == 2){
                
                $stores2 = str_replace('[{',"",$request->sorting); 
                $stores3 = str_replace('&quot',"",$stores2); 
                $stores4 = str_replace('{',"",$stores3); 
                $stores5 = str_replace(';',"",$stores4);
                $stores6 = str_replace('id',"",$stores5);
                $stores7 = str_replace('}]',"",$stores6);
                $stores8 = str_replace('}',"",$stores7);
                $stores9 = str_replace(':',"",$stores8); 
                $stores = explode(",",$stores9); 
                
                $product = Product::whereIn('market_id',$stores)->where('new',1)->select('market_id')->get();
                $product2 = Product::whereIn('market_id',$stores)->where('new',1)->get();
                $mainphoto = Photo::whereIn('market_id',$stores)->where('sub_category_id',null)->where('product_id',null)->get();
                $mahal = Market::whereIn('id',$product)->where('status',1)->get();
                return response()->json(['store'=> $mahal,'products'=>$product2,'storephoto'=>$mainphoto]);
                
            }elseif($request->sort == 3){
                
                $stores2 = str_replace('[{',"",$request->sorting); 
                $stores3 = str_replace('&quot',"",$stores2); 
                $stores4 = str_replace('{',"",$stores3); 
                $stores5 = str_replace(';',"",$stores4);
                $stores6 = str_replace('id',"",$stores5);
                $stores7 = str_replace('}]',"",$stores6);
                $stores8 = str_replace('}',"",$stores7);
                $stores9 = str_replace(':',"",$stores8); 
                $stores = explode(",",$stores9); 
                
                $product = Product::whereIn('market_id',$stores)->orderBy('price','ASC')->select('market_id')->get();
                $product2 = Product::whereIn('market_id',$stores)->orderBy('price','ASC')->get();
                $mainphoto = Photo::whereIn('market_id',$stores)->where('sub_category_id',null)->where('product_id',null)->get();
                $mahal = Market::whereIn('id',$product)->where('status',1)->get();
                return response()->json(['store'=> $mahal,'products'=>$product2,'storephoto'=>$mainphoto]);
                
            }elseif($request->sort == 4){
                
                $stores2 = str_replace('[{',"",$request->sorting); 
                $stores3 = str_replace('&quot',"",$stores2); 
                $stores4 = str_replace('{',"",$stores3); 
                $stores5 = str_replace(';',"",$stores4);
                $stores6 = str_replace('id',"",$stores5);
                $stores7 = str_replace('}]',"",$stores6);
                $stores8 = str_replace('}',"",$stores7);
                $stores9 = str_replace(':',"",$stores8); 
                $stores = explode(",",$stores9); 
                
                $product = Product::whereIn('market_id',$stores)->orderBy('price','DESC')->select('market_id')->get();
                $product2 = Product::whereIn('market_id',$stores)->orderBy('price','DESC')->get();
                $mainphoto = Photo::whereIn('market_id',$stores)->where('sub_category_id',null)->where('product_id',null)->get();
                $mahal = Market::whereIn('id',$product)->where('status',1)->get();
                return response()->json(['store'=> $mahal,'products'=>$product2,'storephoto'=>$mainphoto]);
                
            }elseif($request->sort == 5){
                
                $stores2 = str_replace('[{',"",$request->sorting); 
                $stores3 = str_replace('&quot',"",$stores2); 
                $stores4 = str_replace('{',"",$stores3); 
                $stores5 = str_replace(';',"",$stores4);
                $stores6 = str_replace('id',"",$stores5);
                $stores7 = str_replace('}]',"",$stores6);
                $stores8 = str_replace('}',"",$stores7);
                $stores9 = str_replace(':',"",$stores8); 
                $stores = explode(",",$stores9); 
                
                $counters = Counters::whereIn('market_id',$stores)->orderBy('views','ASC')->select('market_id')->get();

                $product = Product::whereIn('market_id',$counters)->select('market_id')->get();
                $product2 = Product::whereIn('market_id',$counters)->get();
                $mainphoto = Photo::whereIn('market_id',$counters)->where('sub_category_id',null)->where('product_id',null)->get();
                $mahal = Market::whereIn('id',$counters)->where('status',1)->get();
                return response()->json(['store'=> $mahal,'products'=>$product2,'storephoto'=>$mainphoto]);
                
            }
            
        
        }elseif($request->storeID){
            
                
                $product2 = Product::where('market_id',$request->storeID)->get();
                $mainphoto = Photo::where('market_id',$request->storeID)->where('sub_category_id',NULL)->where('product_id',NULL)->get();
                $mahal = Market::where('id',$request->storeID)->where('status',1)->get();
                return response()->json(['store2'=> $mahal,'products2'=>$product2,'storephoto2'=>$mainphoto]);

        
        }elseif ($request->sub1 || $request->emailsub) {
            $subscribe = new Subscribe;
            $subscribe->email = $request->emailsub;
            $subscribe->save();
            return response()->json('true');
        }elseif ($request->sub2 || $request->emailsubmob) {
            $subscribe = new Subscribe;
            $subscribe->email = $request->emailsubmob;
            $subscribe->formob = '1';
            $subscribe->save();
            return response()->json('true');
        }elseif($request->saveFilter){
            $productResultfilter = str_replace('},{',"",$request->productResultArray);
            
            $productResultfilter4 = str_replace('[{',"",$productResultfilter);
            $productResultfilter5 = str_replace('"id"',"",$productResultfilter4);
            $productResultfilter6 = str_replace('}]',"",$productResultfilter5);
            $productResultfilter7 = explode(':',$productResultfilter6);

            $getfilter = $request->filter_attr;
            $getfilter2 = $request->filter_attr2;
            $getfilter3 = $request->filter_attr3;

            $length = count($productResultfilter7);
            $IDArray = ['id'=>$productResultfilter7];

            foreach ($IDArray as $IDs) {
                $mattResult = Matt::where('sub_properties_id',$getfilter)->whereIn('product_id',$IDs)->Orwhere('sub_properties_id',$getfilter2)->whereIn('product_id',$IDs)->Orwhere('sub_properties_id',$getfilter3)->whereIn('product_id',$IDs)->select('product_id')->get();
                $productResults = Product::whereIn('id',$mattResult)->get();

                return response()->json($productResults);
            }
            
        }
    }
    public function autocompleteResult(Request $request)
    {
            $mahalname = $request->searchVal;
         
            $mahal = Market::where('title','LIKE','%'.$mahalname.'%')->where('status',1)->get();
              
            return response()->json($mahal); 
            
        
    }
}
