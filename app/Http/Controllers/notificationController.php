<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Noti;
use App\Market;
use App\Cinfo;
use App\Cnoti;
use App\Block;
use App\Counters;
use App\Like;
use Auth;
use App\City;
use App\Area;
use App\ImageLogo;
class notificationController extends Controller
{
    public function getallNoti(Request $request)
    {
    		$city1 = City::where('id',8)->get();
    		$area1 = Area::where('city_id',8)->get();
    		$city = City::where('id','!=',$city1[0]->id)->get();
    		$area = Area::where('id','!=',$area1[0]->id)->get();
	    	$savedCity = City::where('id',$city1[0]->id)->get();
			$savedArea = Area::where('id',$area1[0]->id)->get();
			$image  = ImageLogo::where('id',1)->first(); 
    		$allnoti = Noti::where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
    		
	    	return view('site.allnoti',compact('city','area','savedCity','savedArea','city1','area1'))->withImage($image)->withAllnoti($allnoti);
    	
    }
    public function getallNoti2(Request $request)
    {
    		$city1 = City::where('id',8)->get();
    		$area1 = Area::where('city_id',8)->get();
    		$city = City::where('id','!=',$city1[0]->id)->get();
    		$area = Area::where('id','!=',$area1[0]->id)->get();
	    	$savedCity = City::where('id',$city1[0]->id)->get();
			$savedArea = Area::where('id',$area1[0]->id)->get();
    	
    		$allnoti = Noti::where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
    		$image  = ImageLogo::where('id',1)->first(); 
	    	return view('dashBoard.notifications.allnoti')->withImage($image)->withAllnoti($allnoti);
    	
    }
    public function usernotification(Request $request)
    {
    	if($request->noti == 1){
	    	$noti = Noti::where('user_id',$request->userId)->orderBy('id','DESC')->get();
	    	$oldNoti = Cnoti::where('user_id',$request->userId)->orderBy('id','DESC')->get();
	    	// $counter = count($oldNoti);
	    	$actionId = Noti::where('user_id',$request->userId)->select('action_id')->orderBy('id','DESC')->get();
	    	$marketnoti = Market::whereIn('user_id',$actionId)->get();
	    	$marketnotiId = Market::whereIn('user_id',$actionId)->select('id')->get();
	    	$contact_info = Cinfo::whereIn('market_id',$marketnotiId)->get();

	    	return response()->json(['notifications' => $noti, 'marketnoti' =>$marketnoti ,'contact_info' => $contact_info,'counter'=>$oldNoti]);
	    //	return response()->json(1);
    	}elseif($request->noti == 2){
    		$notifications = Noti::where('user_id',$request->userId)->orderBy('id','DESC')->get();
    		$counter = count($notifications);

	    	$noti = Noti::where('id',$request->notiId)->orderBy('id','DESC')->get();
	    	//$actionId = Noti::where('id',$request->notiId)->select('action_id')->get();
	    	$marketnoti = Market::where('user_id',$noti[0]->action_id)->get();
	    //	$marketnotiId = Market::where('user_id',$noti[0]->action_id)->select('id')->get();
	    	$contact_info = Cinfo::where('market_id',$marketnoti[0]->id)->get();

	    	$oldNoti = Cnoti::where('user_id',$request->userId)->get();
	    	$oldNoti[0]->counter = $counter;
	    	$oldNoti[0]->save();

	    	return response()->json(['newnotifications' => $noti, 'marketnoti' =>$marketnoti ,'contact_info' => $contact_info]);
    	}
    }
    public function usernotification2(Request $request)
    {
    	if($request->noti == 1){
	    	$noti = Noti::where('user_id',$request->userId)->orderBy('id','DESC')->orderBy('id','DESC')->get();
	    	$oldNoti = Cnoti::where('user_id',$request->userId)->orderBy('id','DESC')->get();
	    	// $counter = count($oldNoti);
	    	$actionId = Noti::where('user_id',$request->userId)->select('action_id')->orderBy('id','DESC')->get();
	    	$marketnoti = Market::whereIn('user_id',$actionId)->get();
	    	$marketnotiId = Market::whereIn('user_id',$actionId)->select('id')->get();
	    	$contact_info = Cinfo::whereIn('market_id',$marketnotiId)->get();

	    	return response()->json(['notifications' => $noti, 'marketnoti' =>$marketnoti ,'contact_info' => $contact_info,'counter'=>$oldNoti]);
	    //	return response()->json(1);
    	}elseif($request->noti == 2){
    		$notifications = Noti::where('user_id',$request->userId)->orderBy('id','DESC')->get();
    		$counter = count($notifications);

	    	$noti = Noti::where('id',$request->notiId)->orderBy('id','DESC')->get();
	    	//$actionId = Noti::where('id',$request->notiId)->select('action_id')->get();
	    	$marketnoti = User::where('id',$noti[0]->action_id)->get();
	    //	$marketnotiId = Market::where('user_id',$noti[0]->action_id)->select('id')->get();
	    	$contact_info = Cinfo::where('market_id',$marketnoti[0]->id)->get();

	    	$oldNoti = Cnoti::where('user_id',$request->userId)->get();
	    	$oldNoti[0]->counter = $counter;
	    	$oldNoti[0]->save();

	    	return response()->json(['newnotifications' => $noti, 'user' =>$marketnoti ,'contact_info' => $contact_info]);
    	}elseif($request->noti == 3){
    		$notifications = Noti::where('user_id',$request->userId)->orderBy('id','DESC')->get();
    		$counter = count($notifications);

	    	$noti = Noti::where('id',$request->notiId)->orderBy('id','DESC')->get();
	    	//$actionId = Noti::where('id',$request->notiId)->select('action_id')->get();
	    	$marketnoti = User::where('id',$noti[0]->action_id)->get();
	    //	$marketnotiId = Market::where('user_id',$noti[0]->action_id)->select('id')->get();
	    	$contact_info = Cinfo::where('market_id',$marketnoti[0]->id)->get();

	    	$oldNoti = Cnoti::where('user_id',$request->userId)->get();
	    	$oldNoti[0]->counter = $counter;
	    	$oldNoti[0]->save();

	    	return response()->json(['newnotifications' => $noti, 'user' =>$marketnoti ,'contact_info' => $contact_info]);
    	}
    }
    public function usernotification3(Request $request)
    {
    	if($request->noti == 3){
    		    $store = Market::find($request->storeId);
    		    $user = User::find($request->userId);
    		    $noti = new Noti;
	            $noti->action_id = $request->userId;
	            $noti->user_id = $store->user_id;
	            $noti->data = $user->user_name.' '.'click on your phone';
	            $noti->save();
	            
	            $counters = new Counters;
		    $counters->market_id = $request->storeId;
		    $counters->phone = 1;
		    $oldcounter = Counters::where('market_id',$request->storeId)->where('phone','!=',0)->where('product_id',NULL)->where('views',0)->get();
		    if(count($oldcounter) > 0){
		        $oldcounter[0]->phone = $oldcounter[0]->phone+1;
		        $oldcounter[0]->save();
		    }else{
		    	$counters->save();
		    }
	    	    return response()->json('counter noti phone success');
    	}
    }
    public function block(Request $request)
    {
    	$block = new Block;
    	$block->user_id = $request->user_id;
    	$block->vendor_id = $request->vendor_id;
    	$block->block = $request->block;

    	$oldBlock = Block::where('user_id',$request->user_id)->where('vendor_id',$request->vendor_id)->get();
    	if(count($oldBlock) > 0 ){
    		Block::where('user_id',$request->user_id)->where('vendor_id',$request->vendor_id)->delete();
    		return response()->json('unblock success');
    	}else{
    		$block->save();
    		return response()->json('block success');
    	}

		
    	
    }
    public function like(Request $request)
    {
    	if(Auth::guest()){
	    	
		
		return response()->json('guest');
	}else{
          	$like = new Like;
	    	$like->review_id = $request->commentId;
	    	$like->user_id = $request->userId;
	    	$like->save();
	
		return response()->json('like success');
        }
    	
    }
    public function forget(Request $request)
    {
        $code = rand(10,100);
        $emailforget = $request->forgetmail;
        $to = $emailforget ;
		$subject = "إستعادة كلمة المرور";
		$txt = $code;
		$from= "From: admin@filmahal.com";
		
		mail($to,$subject,$txt,$from);
	
		$userForget = User::where('email',$emailforget)->get();
	if(count($userForget)!==0){
		$userForget[0]->forgetCode = $code;
		$userForget[0]->save();
	}
	return redirect('/forgetpassword-step2');
        
    }
    public function forgetStep2(Request $request)
    {
        $code = $request->code;
        $newpass= $request->newpass;
    
	
	$userForget = User::where('forgetCode',$code)->get();
	if(count($userForget)!==0){
		$userForget[0]->password = bcrypt($newpass);
		$userForget[0]->forgetCode = null;
		$userForget[0]->save();
		return response()->json('true');
	}else{
		return response()->json('false');
	}
        
    }
}
