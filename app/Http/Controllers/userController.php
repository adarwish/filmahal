<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Follow;
use App\User;
use App\Product;
use App\Market;
use App\Photo;
use App\Msub;
use App\Sub;
use App\Cinfo;
use App\Matt;
use App\Attribute;
use App\Properties;
use App\Aprop;
use App\Mplace;
use App\Review;
use App\Msg;
use App\Room;
use App\Block;
use App\Counters;
use App\Noti;
use App\City;
use App\Area;
use App\Mcat;
use App\Category;
use App\Subscribe;
use Auth;
use App\ImageLogo;
class userController extends Controller
{
    public function getproductattr(Request $request)
    {
        $attribute1 = Matt::where('product_id',$request->proid)->select('attribute_id')->get();
        $attribute2 = Matt::where('product_id',$request->proid)->select('properties_id')->get();
        $attribute3 = Matt::where('product_id',$request->proid)->select('sub_properties_id')->get();

        $attr = Attribute::whereIn('id',$attribute1)->get();
        $attrId = Attribute::whereIn('id',$attribute2)->select('id')->get();
        $prop = Properties::whereIn('id',$attribute2)->get();
        $propId = Properties::whereIn('attribute_id',$attrId)->select('id')->get();
        $sub_prop = Aprop::whereIn('id',$attribute3)->get();
        
        $counters = new Counters;
        $counters->product_id = $request->proid;
        $counters->views = 1;
        $oldcounter = Counters::where('product_id',$request->proid)->where('views','!=',0)->get();
        if(count($oldcounter) > 0){
            $oldcounter[0]->views = $oldcounter[0]->views+1;
            $oldcounter[0]->save();
            return response()->json(['attr'=>$attr,'prop'=>$prop,'sub_prop'=>$sub_prop,'views'=>$oldcounter]);
        }else{
            $counters->save();
            return response()->json(['attr'=>$attr,'prop'=>$prop,'sub_prop'=>$sub_prop,'views'=>$oldcounter]);
        }
        
       // return response()->json($attribute2);
    }
    public function getspecificpro(Request $request)
    {
            $marketId = Market::where('id',$request->ids)->get();
            if($request->sale){
               // $subID = $request->subId;
                $specific_product = Product::where('pricentage','!=',0)->where('status',1)->where('market_id',$marketId[0]->id)->get();
                $cinfo = Cinfo::where('market_id',$marketId[0]->id)->get();
                $specific_productId = Product::where('pricentage','!=',0)->where('market_id',$marketId[0]->id)->select('id')->get();
                $photosajax = Photo::whereIn('product_id',$specific_productId)->get();
                return response()->json(['pro' => $specific_product, 'cinfo' => $cinfo ,'photo' => $photosajax] );
            }elseif($request->newpro){
                $subID = $request->subId;
                $specific_product = Product::where('new',1)->where('status',1)->where('market_id',$marketId[0]->id)->get();
                $cinfo = Cinfo::where('market_id',$marketId[0]->id)->get();
                $specific_productId = Product::where('new',1)->where('market_id',$marketId[0]->id)->select('id')->get();
                $photosajax = Photo::whereIn('product_id',$specific_productId)->get();
                return response()->json(['ids' => $specific_productId ,'pro' => $specific_product, 'cinfo' => $cinfo ,'photo' => $photosajax] );
            }else{
                $subID = $request->subId;
                $specific_product = Product::where('sub_category_id',$subID)->where('status',1)->where('market_id',$marketId[0]->id)->get();
                $specific_productId = Product::where('sub_category_id',$subID)->where('market_id',$marketId[0]->id)->select('id')->get();
                $cinfo = Cinfo::where('market_id',$marketId[0]->id)->get();
                $photosajax = Photo::whereIn('product_id',$specific_productId)->get();
                return response()->json(['pro' => $specific_product, 'cinfo' => $cinfo, 'photo' => $photosajax ]);
            }
    }
    public function getStore($id,$savedcityId,$savedareaId,$searchval)
    {
        $city1 = City::all();
        $area1 = Area::all();
        
        $store = Market::find($id);
        $photo = Photo::where('market_id',$id)->where('product_id',null)->where('sub_category_id',null)->get();

        $msubId = Msub::where('market_id',$id)->select('sub_category_id')->orderBy('status','ASC')->get();
        $msub = Msub::where('market_id',$id)->orderBy('status','DESC')->get();
        $subcategory = Sub::whereIn('id',$msubId)->get();
        $product = Product::where('status',1)->where('market_id',$id)->simplePaginate(9);
        $sale = Product::where('market_id',$id)->where('end_discount','!=',0)->where('status',1)->get();
        $new = Product::where('market_id',$id)->where('new',1)->where('status',1)->get();
        $productNum = Product::where('market_id',$id)->where('status',1)->get();
        $cinfo = Cinfo::where('market_id',$id)->get();
        $review = Review::where('market_id',$id)->where('replay',0)->where('user_replay',0)->where('status','1')->limit('3')->orderBy('id','DESC')->get();
        $areaId = Mplace::where('market_id',$id)->select('area_id')->get();
        $storesameareaId = Mplace::whereIn('area_id',$areaId)->select('market_id')->get();
        $samestore = Market::whereIn('id',$storesameareaId)->where('id','!=',$id)->limit(4)->get();
        
        $cityId = Area::where('id',$areaId[0]->area_id)->get();
      // return response()->json($areaId);
        $city = City::where('id','!=',$cityId[0]->city_id)->get();
        $area = Area::where('city_id',$cityId[0]->city_id)->where('id','!=',$areaId[0]->area_id)->get();
        
        $savedCity = City::where('id',$savedcityId)->get();
    $savedArea = Area::where('id',$savedareaId)->get();
      //  return response()->json($savedArea);
                $counters = new Counters;
            $counters->market_id = $id;
            $counters->views = 1;
            $oldcounter = Counters::where('market_id',$id)->where('phone','=',0)->where('product_id',NULL)->where('views','!=',0)->get();
            if(count($oldcounter) > 0){
                $oldcounter[0]->views = $oldcounter[0]->views+1;
                $oldcounter[0]->save();
            }else{
                $counters->save();
            }
        
        $countersall = Counters::where('market_id',$id)->get();
        $searchval = $searchval;
        $image  = ImageLogo::where('id',1)->first();
        return view('site.profiles.index',['image'=>$image],compact('store','photo','subcategory','msub','id','product','cinfo','mplace','samestore','productNum','sale','new','review','countersall','city','area','savedCity','savedArea','searchval','savedcityId','savedareaId'));
    }
    public function getreviewusers(Request $request)
    {
        $users = User::find($request->user_id);
        $oldReview = Review::where('id',$request->review_id)->get();
        return response()->json(['users'=>$users,'review'=>$oldReview]);
    }
    public function getlocation(Request $request)
    {
       $cinfo = Cinfo::where('market_id',$request->id)->get();
       return response()->json($cinfo);
    }
    public function addcomment(Request $request)
    {
        if($request->comment){
        if(Auth::user()){
            $review = new Review;
            $review->user_id = Auth::user()->id;
            $review->market_id = $request->storeId;
            $review->comment = $request->comment;
            $store = Market::find($request->storeId);
            $validcomment = Review::where('market_id',$request->storeId)->where('comment',$request->comment)->where('user_id',Auth::user()->id)->get();
            if(count($validcomment) == 0){
                    
                    
                    
                    if($request->replay){
                        $review = new Review;
                        $oldreview = Review::where('market_id',$request->storeId)->where('user_id',Auth::user()->id)->get();
                        $review->user_replay = $oldreview[0]->id;
                        $review->user_id = Auth::user()->id;
                $review->market_id = $request->storeId;
                    $review->comment = $request->comment;
                        $review->save();
                    }else{
                        $review->save();
                    }
                    $noti = new Noti;
                    $noti->action_id = Auth::user()->id;
                    $noti->user_id = $store->user_id;
                    $noti->data = Auth::user()->user_name.' '.'add new comment';
                    $noti->save();
            }
            
            $userId = Review::where('market_id',$request->storeId)->select('user_id')->get();
            $users = User::whereIn('id',$userId)->get();
            $oldReview = Review::where('market_id',$request->storeId)->get();
            return response()->json(['review'=>$oldReview]);
         }else{
            return response()->json('guest');
         }
        }elseif($request->save3) {
        if(Auth::user()){
            $store = Market::find($request->storeId);
            $reciver = User::find($store->user_id);
            $room = new Room;
            $oldroom = Room::where('sender_id',Auth::user()->id)->where('reciver_id',$reciver->id)->Orwhere('sender_id',$reciver->id)->where('reciver_id',Auth::user()->id)->get();
            $room->sender_id = Auth::user()->id;
            $room->reciver_id = $reciver->id;
            
             if(count($oldroom) > 0 ){
                $msg = new Msg;
                $msg->sender_id = Auth::user()->id;
                $msg->reciver_id = $reciver->id;
                $msg->message = $request->message;
                $msg->room_id = $oldroom[0]->id;
                $block = Block::where('user_id',Auth::user()->id)->where('vendor_id',$reciver->id)->get();
               if(count($block) >0){
                    foreach($block as $blocks){
                    
                        if($blocks->block == 1){
                            return response()->json('block');
                        }else{
                            $msg->save();
                            return response()->json('message success');
                        }
                    }
                    return response()->json('1');
                }else{
                    $msg->save();
                    return response()->json('message success');
                }
                                    return response()->json('message success');

            }else{
                $room->save();
                $msg = new Msg;
                $msg->sender_id = Auth::user()->id;
                $msg->reciver_id = $reciver->id;
                $msg->message = $request->message;
                $msg->room_id = $room->id;
                $block = Block::where('sender_id',Auth::user()->id)->where('vendor_id',$reciver->id)->get();
                if(count($block) >0){
                    foreach($block as $blocks){
                    
                        if($blocks->block == 1){
                            return response()->json('block');
                        }else{
                            $msg->save();
                            return response()->json('message success');
                        }
                    }
                }else{
                    $msg->save();
                    return response()->json('message success');
                }
                return response()->json(2);
                
            }
          }else{
            return response()->json('guest');
          }
            
        }elseif($request->searchbutton == 'عرض المحلات' || $request->search){
        $city = City::all();
        $mahalname = $request->search;
            $areaname = $request->getarea;
            $cityId = Area::where('id',$areaname)->get();
            $mahalId = Mplace::where('area_id',$areaname)->select('market_id')->get();
            if(count($mahalId) > 0){
                $mahal = Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->get();
                $mahalCatId = Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->select('id')->get();
                $mahalId2 =  Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->select('id')->get();
                if(count($mahal) == 0){
                    $productID = Product::where('name','LIKE','%'.$mahalname.'%')->select('market_id')->get();
                    $mahalProId = Mplace::where('area_id',$areaname)->whereIn('market_id',$productID)->select('market_id')->get();
                    $mahal = Market::whereIn('id',$mahalProId)->get();
                    $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->get();
                    if(count($mahal) == 0){
                            
                            $categoryStoreID = Category::where('name','LIKE','%'.$mahalname.'%')->select('id')->get();
                            $storeSaerchId = Mcat::whereIn('category_id',$categoryStoreID)->select('market_id')->get();
                        $mahalProId = Mplace::where('area_id',$areaname)->whereIn('market_id',$storeSaerchId)->select('market_id')->get();
                        $mahal = Market::whereIn('id',$mahalProId)->get();
                        $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->get();
                        
                    }
                }
                $savedCity = City::where('id',$cityId[0]->city_id)->get();
            $savedArea = Area::where('id',$areaname)->get();
            $area = Area::where('city_id',$cityId[0]->city_id)->where('id','!=',$areaname)->get();
            }else{
                $savedCity = City::where('id',8)->get();
            $savedArea = Area::where('id',$areaname)->get();
            $area = Area::where('city_id',8)->where('id','!=',$areaname)->get();
            $areaID = Area::where('city_id',8)->where('id','!=',$areaname)->select('id')->get();
                $mahal = Market::where('title','LIKE','%'.$mahalname.'%')->get();
                $mahalCatId = Market::where('title','LIKE','%'.$mahalname.'%')->select('id')->get();
                $mahalId2 =  Market::where('title','LIKE','%'.$mahalname.'%')->select('id')->get();
                
                if(count($mahal) == 0){
                    $productID = Product::where('name','LIKE','%'.$mahalname.'%')->select('market_id')->get();
                    $mahalProId = Mplace::whereIn('area_id',$areaID)->whereIn('market_id',$productID)->select('market_id')->get();
                    $mahal = Market::whereIn('id',$mahalProId)->get();
                    $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->get();
                    $mahalId2 =  Market::whereIn('id',$mahalProId)->select('id')->get();
                    
                    if(count($mahal) == 0){
                            
                            $categoryStoreID = Category::where('name','LIKE','%'.$mahalname.'%')->select('id')->get();
                            $storeSaerchId = Market::whereIn('id',$categoryStoreID)->get();
                        $mahalProId = Mplace::where('area_id',$areaID)->whereIn('market_id',$storeSaerchId)->select('market_id')->get();
                        $mahal = Market::whereIn('id',$mahalProId)->get();
                        $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->get();
                    }
                }
            }
            if($request->getarea == NULL || $request->getarea == ""){
                $mahal = Market::where('title','LIKE','%'.$mahalname.'%')->get();
                $mahalCatId = Market::where('title','LIKE','%'.$mahalname.'%')->select('id')->get();
                $mahalId2 =  Market::where('title','LIKE','%'.$mahalname.'%')->select('id')->get();
            }

            $categoryId = Mcat::whereIn('market_id',$mahalCatId)->select('category_id')->get();
            $samemahalId = Mcat::whereIn('category_id',$categoryId)->select('market_id')->get();
            $samemahal = Market::whereIn('id',$samemahalId)->whereNotIn('id',$mahalCatId)->get();


            $areaId = Mplace::whereIn('market_id',$mahalCatId)->select('area_id')->get();

            $categoryId2 = Mcat::whereNotIn('market_id',$mahalCatId)->select('category_id')->get();

            $samemahalId2 = Mcat::whereIn('category_id',$categoryId2)->select('market_id')->get();
            $samemahal2 = Market::whereIn('id',$samemahalId2)->whereNotIn('id',$mahalCatId)->get();
     /*   $save = new Save;
        $save->area_id = $areaname;
        $save->city_id = $cityId[0]->city_id;
        $save->save();*/
       
        
        if($request->getarea == NULL || $request->getarea == ""){
                $savedCity = City::where('id',8)->get();
            $savedArea = Area::where('id',$areaname)->get();
            $area = Area::where('city_id',8)->where('id','!=',$areaname)->get();
            //return response()->json($savedArea);
            }
      /*  $saveall = Save::all();
        foreach($saveall as $savealls){
        
            $savealls->delete();
        }*/
        $searchval = $request->search;
        $image  = ImageLogo::where('id',1)->first(); 
            return view('site.search.index',['image'=>$image],compact('mahal','samemahal','city','area','samemahal2','savedCity','savedArea','mahalId2','searchval'));
       }
    }
    public function postregister(Request $request)
    {
        $mainpath = 'image';
        $user = new User;
        $user->user_name = $request->username;
        $user->email = $request->email;
        $user->status = '1';
        $user->password = bcrypt($request->password);
        if($request->hasFile('photo')) {
                    $file = $request->photo;
                    $filename =  time().$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $user->photo = $filename;
              //      return response()->json($request->photo);
    }
        
        $olduser = User::where('user_name',$request->username)->orWhere('email',$request->email)->get();
        if(count($olduser) == 0){
            $user->status_view = "1";
            $user->save();
            
            Auth::loginUsingId($user->id);
            return redirect('/');
        }else{
            $msg = "مستخدم موجود بالفعل";
            return view('site.Auth.register')->withMsg($msg);
        }
        
    }
    public function getregister()
    {
        $msg = "";
        return view('site.Auth.register')->withMsg($msg);
    }
    public function getabout()
    {
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        $image  = ImageLogo::where('id',1)->first(); 

        return view('site.content.aboutUs',['image'=>$image],compact('city','area','savedCity','savedArea','city1','area1'));
    }
    public function getabout2()
    {
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        $image  = ImageLogo::where('id',1)->first(); 

        return view('site.content.terms',['image'=>$image],compact('city','area','savedCity','savedArea','city1','area1'));
    }
    public function getabout3()
    {
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        $image  = ImageLogo::where('id',1)->first();

        return view('site.content.fav',['image'=>$image],compact('city','area','savedCity','savedArea','city1','area1'));
    }
    public function getabout4()
    {
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        $image  = ImageLogo::where('id',1)->first(); 

        return view('site.content.custServ',['image'=>$image],compact('city','area','savedCity','savedArea','city1','area1'));
    }
    public function getabout5()
    {
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        $image  = ImageLogo::where('id',1)->first();

        return view('site.content.copyrights',['image'=>$image],compact('city','area','savedCity','savedArea','city1','area1'));
    }
    public function getabout6()
    {
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        $image  = ImageLogo::where('id',1)->first(); 

        
        return view('site.content.adWithUs',['image'=>$image],compact('city','area','savedCity','savedArea','city1','area1'));
    }
    public function getabout7()
    {
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        $image  = ImageLogo::where('id',1)->first(); 

        
        return view('site.content.faq',['image'=>$image],compact('city','area','savedCity','savedArea','city1','area1'));
    }
    public function getabout8()
    {
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        $image  = ImageLogo::where('id',1)->first();

        
        return view('site.content.mobApp',['image'=>$image],compact('city','area','savedCity','savedArea','city1','area1'));
    }
    public function getabout9()
    {
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        $image  = ImageLogo::where('id',1)->first();

        
        return view('site.content.rules',['image'=>$image],compact('city','area','savedCity','savedArea','city1','area1'));
    }
    public function getabout10()
    {
            $city1 = City::where('id',8)->get();
            $area1 = Area::where('city_id',8)->get();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            $savedCity = City::where('id',$city1[0]->id)->get();
        $savedArea = Area::where('id',$area1[0]->id)->get();
        
        $image  = ImageLogo::where('id',1)->first();

        return view('site.content.privacy',['image'=>$image],compact('city','area','savedCity','savedArea','city1','area1'));
    }
    public function show()
    {
        $city = City::all();
        $area = Area::where('city_id',8)->get();
        $savedCity = City::where('id',8)->get();
    //$savedArea = Area::where('id',$savedareaId)->get();
        $follow = Follow::where('user_id',Auth::user()->id)->get();
        $room = Room::where('sender_id',Auth::user()->id)->Orwhere('reciver_id',Auth::user()->id)->get();
        $image  = ImageLogo::where('id',1)->first();

        return view('site.user.index',['image'=>$image],compact('follow','room','city','area','savedCity','savedArea'));
    }
    public function getProDetails(Request $request,$storeID,$proID,$savedcityId,$savedareaId,$searchval)
    {
        
            //$city1 = City::where('id',8)->get();
            //$area1 = Area::where('city_id',8)->get();
            $city1 = City::all();
            $area1 = Area::all();
            $city = City::where('id','!=',$city1[0]->id)->get();
            $area = Area::where('id','!=',$area1[0]->id)->get();
            
        $store = Market::find($storeID);
        $product = Product::find($proID);
        $photo = Photo::where('product_id',$proID)->get();
        $cinfo = Cinfo::where('market_id',$storeID)->get();
    /*  $attribute1 = Matt::where('product_id',$proID)->select('attribute_id')->get();
            $attribute2 = Matt::where('product_id',$proID)->select('properties_id')->get();
            $attribute3 = Matt::where('product_id',$proID)->select('sub_properties_id')->get();
    
            $attr = Attribute::whereIn('id',$attribute1)->get();
            $prop = Properties::whereIn('id',$attribute2)->get();
            $sub_prop = Aprop::whereIn('id',$attribute3)->get();*/
            
            $attributeID = Matt::where('product_id',$proID)->select('attribute_id')->get();
            $propertiesID = Matt::where('product_id',$proID)->select('properties_id')->get();
            $properties = Properties::whereIn('id',$propertiesID)->get();
            
            $counters = new Counters;
            $counters->product_id = $proID;
            $counters->views = 1;
            $oldcounter = Counters::where('product_id',$proID)->where('views','!=',0)->get();
            
            $savedCity = City::where('id',$savedcityId)->get();
        $savedArea = Area::where('id',$savedareaId)->get();
        
        $searchval = $searchval;
        $image  = ImageLogo::where('id',1)->first();

        return view('site.profiles.details',['image'=>$image],compact('city','area','savedCity','savedArea','city1','area1','store','product','photo','cinfo','attributeID','properties','oldcounter','searchval','storeID'));
    }
    public function getmsgs(Request $request)
    {
        // $room = Room::find($request->roomId);
        // $sender = User::where('id',$room->sender_id)->get();
        // $reciver = User::find($room->reciver_id);
        $msg = Msg::where('room_id',$request->roomId)->with('sender')->with('reciver')->get();
        return response()->json($msg);
    }
    public function edit(Request $request)
    {
        $mainpath = 'image';
        if($request->save1){
            $user = User::find(Auth::user()->id);
            $user->user_name = $request->username;
            $user->email = $request->email;
            if($request->hasFile('photo')) {
                    $file = $request->photo;
                    $filename =  time().$file->getClientOriginalExtension();
                    $file->move($mainpath,$filename);
                    $user->photo = $filename;
            }
            $user->save();
            return redirect('/user/profile');
        }elseif($request->save2) {
            if(Auth::user()){
                    $follow = Follow::where('id',$request->followId)->delete();
           
                    return response()->json('remove follow success');
                }else{
                return response()->json('guest');
            }
        }elseif($request->save3) {
            if(Auth::user()->vendor == 1){
                $oldroom = Room::find($request->roomIds);
                $block = Block::where('user_id',Auth::user()->id)->where('vendor_id',$oldroom->reciver_id)->get();
                
                $msg = new Msg;
                $msg->sender_id = Auth::user()->id;
                $msg->reciver_id = $oldroom->reciver_id;
                $msg->message = $request->message;
                $msg->room_id = $request->roomIds;
                    if(count($block) >0){
                        foreach($block as $blocks){
                        
                            if($blocks->block == 1){
                                return response()->json('block');
                            }else{
                                $msg->save();
                                return response()->json('message success');
                            }
                        }
                        return response()->json('1');
                    }else{
                        $msg->save();
                        return response()->json('message success');
                    }
             }else{
                return response()->json('vendor');
             }
           
            return response()->json(1);
        }elseif($request->save4) {
            if(Auth::user()){
                if(Auth::user()->vendor == 0){
                    $follow = new Follow;
                    $follow->user_id = Auth::user()->id;
                    $follow->market_id = $request->storeId;
                
                    $oldfollow = Follow::where('user_id',Auth::user()->id)->where('market_id',$request->storeId)->get();
                    $store = Market::find($request->storeId);
                    
                    if(count($oldfollow) > 0){
                        $oldfollow[0]->delete();
                        return response()->json('remove follow success');
                    }else{
                            
                        $follow->save();
                        $noti = new Noti;
                        $noti->action_id = Auth::user()->id;
                        $noti->user_id = $store->user_id;
                        $noti->data = Auth::user()->user_name.' '.'add new comment';
                        $noti->save();
                        return response()->json('follow success');
                    }
                    
            }else{
                return response()->json('vendor');
            }
             }else{
                return response()->json('guest');
             }
            
        }elseif($request->changepsw) {
            $user = User::find(Auth::user()->id);
            $password = $request->oldPassword;
            $email = Auth::user()->email;
            if (Auth::attempt(['email' => $email,'password' => $password])) {
                $user->password = bcrypt($request->newPassword);
                $user->save();
                return response()->json('psw changed success');
            }else{
                return response()->json('wrong old password');
            }
            
        }elseif($request->searchbutton == 'عرض المحلات' || $request->search){
            $city = City::all();
        $mahalname = $request->search;
            $areaname = $request->getarea;
            $cityId = Area::where('id',$areaname)->get();
            $mahalId = Mplace::where('area_id',$areaname)->select('market_id')->get();
            if(count($mahalId) > 0){
                $mahal = Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->get();
                $mahalCatId = Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->select('id')->get();
                $mahalId2 =  Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->select('id')->get();
                if(count($mahal) == 0){
                    $productID = Product::where('name','LIKE','%'.$mahalname.'%')->select('market_id')->get();
                    $mahalProId = Mplace::where('area_id',$areaname)->whereIn('market_id',$productID)->select('market_id')->get();
                    $mahal = Market::whereIn('id',$mahalProId)->get();
                    $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->get();
                    if(count($mahal) == 0){
                            
                            $categoryStoreID = Category::where('name','LIKE','%'.$mahalname.'%')->select('id')->get();
                            $storeSaerchId = Mcat::whereIn('category_id',$categoryStoreID)->select('market_id')->get();
                        $mahalProId = Mplace::where('area_id',$areaname)->whereIn('market_id',$storeSaerchId)->select('market_id')->get();
                        $mahal = Market::whereIn('id',$mahalProId)->get();
                        $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->get();
                        
                    }
                }
                $savedCity = City::where('id',$cityId[0]->city_id)->get();
            $savedArea = Area::where('id',$areaname)->get();
            $area = Area::where('city_id',$cityId[0]->city_id)->where('id','!=',$areaname)->get();
            }else{
                $savedCity = City::where('id',8)->get();
            $savedArea = Area::where('id',$areaname)->get();
            $area = Area::where('city_id',8)->where('id','!=',$areaname)->get();
            $areaID = Area::where('city_id',8)->where('id','!=',$areaname)->select('id')->get();
            $mahalId = Mplace::whereIn('area_id',$areaID)->select('market_id')->get();
            
                $mahal = Market::where('title','LIKE','%'.$mahalname.'%')->whereIn('id',$mahalId)->get();
                $mahalCatId = Market::where('title','LIKE','%'.$mahalname.'%')->select('id')->get();
                $mahalId2 =  Market::where('title','LIKE','%'.$mahalname.'%')->select('id')->get();
                
                if(count($mahal) == 0){
                    $productID = Product::where('name','LIKE','%'.$mahalname.'%')->select('market_id')->get();
                    $mahalProId = Mplace::whereIn('area_id',$areaID)->whereIn('market_id',$productID)->select('market_id')->get();
                    $mahal = Market::whereIn('id',$mahalProId)->get();
                    $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->get();
                    $mahalId2 =  Market::whereIn('id',$mahalProId)->select('id')->get();
                    
                    if(count($mahal) == 0){
                            
                            $categoryStoreID = Category::where('name','LIKE','%'.$mahalname.'%')->select('id')->get();
                            $storeSaerchId = Market::whereIn('id',$categoryStoreID)->get();
                        $mahalProId = Mplace::where('area_id',$areaID)->whereIn('market_id',$storeSaerchId)->select('market_id')->get();
                        $mahal = Market::whereIn('id',$mahalProId)->get();
                        $mahalCatId = Market::whereIn('id',$mahalProId)->select('id')->get();
                    }
                }
            }
    

            $categoryId = Mcat::whereIn('market_id',$mahalCatId)->select('category_id')->get();
            $samemahalId = Mcat::whereIn('category_id',$categoryId)->select('market_id')->get();
            $samemahal = Market::whereIn('id',$samemahalId)->whereNotIn('id',$mahalCatId)->get();


            $areaId = Mplace::whereIn('market_id',$mahalCatId)->select('area_id')->get();

            $categoryId2 = Mcat::whereNotIn('market_id',$mahalCatId)->select('category_id')->get();

            $samemahalId2 = Mcat::whereIn('category_id',$categoryId2)->select('market_id')->get();
            $samemahal2 = Market::whereIn('id',$samemahalId2)->whereNotIn('id',$mahalCatId)->get();
    
        
        if($request->getarea == NULL || $request->getarea == ""){
                $savedCity = City::where('id',8)->get();
            $savedArea = Area::where('id',$areaname)->get();
            $area = Area::where('city_id',8)->where('id','!=',$areaname)->get();
            //return response()->json($savedArea);
            }
    
        $searchval = $request->search;
        $image  = ImageLogo::where('id',1)->first();

            return view('site.search.index',['image'=>$image],compact('mahal','samemahal','city','area','samemahal2','savedCity','savedArea','mahalId2','searchval'));
       }    
    }
    public function getallComment($id,$savedcityId,$savedareaId,$searchval)
    {
        $city1 = City::all();
        $area1 = Area::all();
        $savedCity = City::where('id',$savedcityId)->get();
        $savedArea = Area::where('id',$savedareaId)->get();
        $searchval = $searchval;
        $comment = Review::where('market_id',$id)->where('status','1')->orderBy('id','DESC')->get();
        
        $areaId = Mplace::where('market_id',$id)->select('area_id')->get();
        $cityId = Area::where('id',$areaId[0]->area_id)->get();
        $city = City::where('id','!=',$cityId[0]->city_id)->get();
        $area = Area::where('city_id',$cityId[0]->city_id)->where('id','!=',$areaId[0]->area_id)->get();
        $image  = ImageLogo::where('id',1)->first();

        return view('site.user.allComments',compact('comment','savedCity','savedArea','city1','area1','searchval','city','area'));
    }
    public function unsubscribe($email)
    {
        $subscribe =  Subscribe::where('email',$email)->delete();

        return redirect('/');
    }
}
