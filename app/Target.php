<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    protected $table = 'target';

    public function category()
    {
      return $this->belongsTo('App\Category','category_id');
    }
    public function sub()
    {
      return $this->belongsTo('App\Sub','sub_id');
    }
}
