<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'photo';

    public function product()
    {
      return $this->belongsTo('App\Product','product_id');
    }
    public function market()
    {
      return $this->belongsTo('App\Market','market_id');
    }
}
