<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matt extends Model
{
    protected $table = 'market_attribute';

    public function product()
    {
      return $this->belongsTo('App\Product','product_id');
    }
}
