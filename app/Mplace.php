<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mplace extends Model
{
    protected $table = 'market_place';

    public function city()
    {
      return $this->belongsTo('App\City','city_id');
    }
    public function area()
    {
      return $this->belongsTo('App\Area','area_id');
    }
    public function market()
    {
      return $this->belongsTo('App\Market','market_id');
    }
}
