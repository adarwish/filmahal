<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $table = 'filter';

    public function category()
    {
      return $this->belongsTo('App\Category','category_id');
    }
    public function sub_category()
    {
      return $this->belongsTo('App\Sub','sub_category_id');
    }
}
