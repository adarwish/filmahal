<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
	protected $table = 'room';

    public function sender()
    {
      return $this->belongsTo('App\User','sender_id');
    }
    public function reciver()
    {
      return $this->belongsTo('App\User','reciver_id');
    }
    public function message()
    {
      return $this->hasMany('App\Msg');
    }
}
