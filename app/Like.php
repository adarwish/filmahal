<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'like';
	
    public function review()
    {
      return $this->belongsTo('App\Review','review_id');
    }
}
