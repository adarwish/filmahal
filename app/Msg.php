<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Msg extends Model
{
	protected $table = 'message';

	public function sender()
    {
      return $this->belongsTo('App\User','sender_id');
    }
    public function reciver()
    {
      return $this->belongsTo('App\User','reciver_id');
    }
    public function room()
    {
      return $this->belongsTo('App\Room','room_id');
    }
}
