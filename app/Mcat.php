<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mcat extends Model
{
    protected $table = 'category';

    public function category()
    {
      return $this->belongsTo('App\Category','category_id');
    }
    public function market()
    {
      return $this->belongsTo('App\Market','market_id');
    }
}
