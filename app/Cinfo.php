<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cinfo extends Model
{
    protected $table = 'contact_info';

    public function market()
    {
      return $this->belongsTo('App\Market','market_id');
    }
}
