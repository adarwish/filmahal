<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table = 'attribute';

    public function properties()
    {
      return $this->hasMany('App\Properties');
    }
    public function category()
    {
      return $this->hasMany('App\Category');
    }
}
