<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Properties extends Model
{
    protected $table = 'att_properties';

    public function attributes()
    {
      return $this->belongsTo('App\Attributes','attribute_id');
    }
    public function aprop()
    {
      return $this->hasMany('App\Aprop');
    }
}
