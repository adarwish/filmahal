<?php

namespace App\GraphQL\Mutation;

use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use App\Apply;

class applied extends Mutation
{
    protected $attributes = [
        'name' => 'register',
        'description' => 'A mutation'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'user_id' => ['name' => 'user_id', 'type' => Type::int()],
            'post_id' => ['name' => 'post_id', 'type' => Type::int()],
            'comment' => ['name' => 'comment', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $apply = new Apply;
        $apply->user_id = $args['user_id'];
        $apply->post_id = $args['post_id'];
        $apply->comment = $args['comment'];
        $apply->save();     
        return Apply::where('user_id',$args['user_id'])->where('post_id',$args['post_id'])->get();

    }
}

//http://localhost:8000/graphql?query=mutation+{apply(user_id:1,post_id:1,comment:%22test%22){id}}
