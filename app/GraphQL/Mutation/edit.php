<?php

namespace App\GraphQL\Mutation;

use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use App\Wallet;
use App\Status;
use Illuminate\Support\Facades\Hash;

class edit extends Mutation
{
    protected $attributes = [
        'name' => 'edit',
        'description' => 'A mutation'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'api_token' => ['name' => 'api_token', 'type' => Type::string()],
            'first_name' => ['name' => 'first_name', 'type' => Type::string()],
            'second_name' => ['name' => 'second_name', 'type' => Type::string()],
            'birth_day' => ['name' => 'birth_day', 'type' => Type::string()],
            'identity' => ['name' => 'identity', 'type' => Type::string()],
            'identity_type' => ['name' => 'identity_type', 'type' => Type::string()],
            'address' => ['name' => 'address', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $user = User::where('api_token',$args['api_token'])->get();
        $user[0]->first_name = encrypt($args['first_name']);
        $user[0]->second_name = encrypt($args['second_name']);
        $user[0]->birth_day = encrypt($args['birth_day']);
        $user[0]->identity = encrypt($args['identity']);
        $user[0]->address = encrypt($args['address']);
        $user[0]->save();
        return Status::where('id',1)->get();
    }
}

//http://localhost:8000/graphql?query=mutation+{edit(api_token:"mIHy2GL3ef2kSdxHv6wDDBHNkBEJEHIXk7sw9iMN2GdaclaEKF7IRDHXY6JP",first_name:"khaled1",second_name:"adel",birth_day:"15/5/2016",address:"cairo",identity:"323555543"){response}}