<?php

namespace App\GraphQL\Mutation;

use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use App\Post;

class posts extends Mutation
{
    protected $attributes = [
        'name' => 'register',
        'description' => 'A mutation'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'user_id' => ['name' => 'user_id', 'type' => Type::int()],
            'photo' => ['name' => 'photo', 'type' => Type::string()],
            'video' => ['name' => 'video', 'type' => Type::string()],
            'paragraph' => ['name' => 'paragraph', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $post = new Post;
        $post->user_id = $args['user_id'];
        $post->photo = $args['photo'];
        $post->video = $args['video'];
        $post->paragraph = $args['paragraph'];

        $post->save();
        return Post::where('user_id',$args['user_id'])->get();

    }
}

//http://localhost:8000/graphql?query=mutation+{posts(user_id:1,photo:%222%22,video:%223%22,paragraph:%22test%22){id}}