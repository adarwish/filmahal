<?php

namespace App\GraphQL\Mutation;

use Illuminate\Http\response;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use App\Status;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\checkuser;

class register extends Mutation
{
    use AuthenticatesUsers;

    protected $attributes = [
        'name' => 'register',
        'description' => 'A mutation'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'user_name' => ['name' => 'user_name', 'type' => Type::string()],
            'email' => ['name' => 'email', 'type' => Type::string()],
            'first_name' => ['name' => 'first_name', 'type' => Type::string()],
            'second_name' => ['name' => 'second_name', 'type' => Type::string()],
            'other_name' => ['name' => 'other_name', 'type' => Type::string()],
            'phone' => ['name' => 'phone', 'type' => Type::string()],
            'birth_day' => ['name' => 'birth_day', 'type' => Type::string()],
            'identity' => ['name' => 'identity', 'type' => Type::string()],
            'identity_type' => ['name' => 'identity_type', 'type' => Type::string()],
            'address' => ['name' => 'address', 'type' => Type::string()],
            'PIN' => ['name' => 'PIN', 'type' => Type::string()],

        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $user = new User;
        $user->first_name =  encrypt($args['first_name']);
        $user->second_name =  encrypt($args['second_name']);
        $user->other_name =  encrypt($args['other_name']);
        $user->user_name =   encrypt($args['user_name']);
        $user->phone =  encrypt($args['phone']);
        $user->email =  encrypt($args['email']);
        $user->birth_day =  encrypt($args['birth_day']);
        $user->identity =  encrypt($args['identity']);
        $user->identity_type =  encrypt($args['identity_type']);
        $user->address =  encrypt($args['address']);
        $user->pin =  Hash::make($args['PIN']);
       

        $user_name = $args['user_name'];
        $pin = $args['PIN'];
        $email = $args['email'];
        $identity = $args['identity'];

        $users = checkuser::all();
        foreach ($users as $users) {
            $user_nameValid = ($users->user_name);
            $identityValid = ($users->identity);
            $emailValid = ($users->email);
         /*   if (Hash::check($args['PIN'],$users->pin) || Hash::check($args['user_name']) || Hash::check($args['email'],$users->email) || Hash::check($args['identity'],$users->identity)){
                return Status::where('id',2)->get();
            }*/
            if (Hash::check($args['PIN'],$users->pin) || $user_nameValid == $args['user_name'] || $identityValid == $identity || $emailValid == $email){
                return Status::where('id',2)->get();
            }
           
        }
        $user->save();
        return Status::where('id',1)->get();
    }
}

//http://localhost:8000/graphql?query=mutation+{register(user_name:"khaled",first_name:"mohamed",second_name:"adel",other_name:"mohamed3",phone:"01095607851",birth_day:"12/7/2017",identity:"0101212098066770",identity_type:"personal",address:"cairo",email:"khaledadel122@gmail.com",PIN:"0109"){response}}