<?php

namespace App\GraphQL\Mutation;

use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use App\ads;

class advs extends Mutation
{
    protected $attributes = [
        'name' => 'register',
        'description' => 'A mutation'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args()
    {
        return [
            'ads_id' => ['name' => 'ads_id', 'type' => Type::int()],
            'address' => ['name' => 'address', 'type' => Type::string()],
            'phone' => ['name' => 'phone', 'type' => Type::string()],
            'photo' => ['name' => 'photo', 'type' => Type::string()],
            'video' => ['name' => 'video', 'type' => Type::string()],
            'paragraph' => ['name' => 'paragraph', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $ads = new ads;
        $ads->address = $args['address'];
        $ads->phone = $args['phone'];
        $ads->photo = $args['photo'];
        $ads->video = $args['video'];
        $ads->paragraph = $args['paragraph'];

        $ads->save();
        return ads::where('address',$args['address'])->where('phone',$args['phone'])->where('paragraph',$args['paragraph'])->get();

    }
}

//http://localhost:8000/graphql?query=mutation+{ads(photo:%222%22,video:%223%22,paragraph:%22test%22,address:%22test%22,phone:%22010%22){id}}