<?php

namespace App\GraphQL\Mutation;

use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use App\Fav;
use App\Sub;
use App\Status;

class favorites extends Mutation
{
    protected $attributes = [
        'name' => 'register',
        'description' => 'A mutation'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'user_id' => ['name' => 'user_id', 'type' => Type::int()],
            'sub_service_id' => ['name' => 'sub_service_id', 'type' => Type::int()],
            'password' => ['name' => 'password', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $favorites = new Fav;
        $favorites->user_id = $args['user_id'];
     /*   if(isset($args['service_id'])){
            $follower->service_id = $args['service_id'];
        }*/
        if(isset($args['sub_service_id'])){
            $favorites->sub_service_id = $args['sub_service_id'];
            $sub = Sub::where('id',$args['sub_service_id'])->get();
            $favorites->service_id = $sub[0]->service->id;
        }
        


        $validation = Fav::where('sub_service_id',$args['sub_service_id'])->first();
        if(!$validation){
            $favorites->save();
            return Status::where('id',1)->get();
        }else{
            return Status::where('id',2)->get();
        }

    }
}

//http://localhost:8000/graphql?query=mutation+{favorites(user_id:29,sub_service_id:3){response}}
