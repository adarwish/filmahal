<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Sub;
use App\GraphQL\Type\Timestamp;

class service extends BaseType
{
    protected $attributes = [
        'name' => 'service',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
				'type' => Type::int(),
				'description' => 'The id of the user'
			],
            'service_id' => [
                'type' => Type::int(),
                'description' => 'The id of the user'
            ],
			'name' => [
				'type' => Type::string(),
				'description' => 'The id of the user'
			],
			'subservice' => [
                'args' => [
                    'service_id' => [
                        'type'        => Type::int(),
                        'description' => 'id of the  service',
                    ],
                ],
                'type' => Type::listOf(GraphQL::type('service')),
                'description' => 'The sub service of the service'
            ],
            
        ];
    }
    public function resolveSubserviceField($root, $args)
    {
        if(isset($args['service_id'])){
            return Sub::where('service_id',$args['service_id'])->get();
        }else{
            return Sub::all();
        }
    	
    }
}
