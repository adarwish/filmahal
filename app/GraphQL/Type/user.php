<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Service;
use Illuminate\Support\Facades\Crypt;

class user extends BaseType
{
    protected $attributes = [
        'name' => 'user',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            	'id' => [
    				'type' => Type::int(),
    				'description' => 'The id of the user'
				],
				'email' => [
					'type' => Type::string(),
					'description' => 'The email of user'
				],
                'service_id' => [
                    'type' => Type::string(),
                    'description' => 'The name of user'
                ],
                'status' => [
                    'type' => Type::string(),
                    'description' => 'The name of user'
                ],
                'pin' => [
                    'type' => Type::string(),
                    'description' => 'The password of user'
                ],
                'first_name' => [
                    'type' => Type::string(),
                    'description' => 'The name of user'
                ],
                'second_name' => [
                    'type' => Type::string(),
                    'description' => 'The name of user'
                ],
                'users' => [
                    'args' => [
                        'api_token' => [
                            'type'        => Type::string(),
                            'description' => 'id of the user',
                        ],
                    ],
                    'type' => Type::listOf(GraphQL::type('user')),
                    'description' => 'The id of the user'
                ],
                'other_name' => [
                    'type' => Type::string(),
                    'description' => 'The id of the user'
                ],
                'phone' => [
                    'type' => Type::int(),
                    'description' => 'The id of the user'
                ],
                'address' => [
                    'type' => Type::string(),
                    'description' => 'The id of the user'
                ],
                'birth_day' => [
                    'type' => Type::string(),
                    'description' => 'The id of the user'
                ],
                'sub_service_id' => [
                    'type' => Type::string(),
                    'description' => 'The id of the user'
                ],
                'video' => [
                    'type' => Type::string(),
                    'description' => 'The id of the user'
                ],
                'identity_type' => [
                    'type' => Type::string(),
                    'description' => 'The id of the user'
                ],
                'identity' => [
                    'type' => Type::string(),
                    'description' => 'The id of the user'
                ],
                'response' => [
                    'type' => Type::string(),
                    'description' => 'The id of the user',

                ],
                'user_name' => [
                    'type' => Type::string(),
                    'description' => 'The id of the user',
                    'default_value' => 'khaled',
                ],
                'api_token' => [
                    'type' => Type::string(),
                    'description' => 'The id of the user',
                    'default_value' => 'khaled',
                ],
                'services' => [
                    'args' => [
                        'service_id' => [
                            'type'        => Type::int(),
                            'description' => 'id of the service',
                        ],
                    ],
                    'type' => Type::listOf(GraphQL::type('service')),
                    'description' => 'The sub service of the service'
                    
                ],
             /*   'sub_services' => [
                    'args' => [
                        'service_id' => [
                            'type'        => Type::int(),
                            'description' => 'id of the  service',
                        ],
                    ],
                    'type' => Type::listOf(GraphQL::type('service')),
                    'description' => 'The sub service of the service'
                ],*/
        ];
    }
    public function resolveUsersField($root, $args)
    {
        if (isset($args['api_token'])) {
               return  $root->users->where('api_token', $args['api_token'])->get();
               
          //  return Crypt::decryptString($alluser[0]->user_name);

        }

        return $root->users;

    }
    public function resolveServicesField($root, $args)
    {
        return Service::where('id',$args['service_id'])->get();
    }
  
}
