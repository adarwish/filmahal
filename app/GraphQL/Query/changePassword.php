<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use App\Rate;
use App\Status;
use Illuminate\Support\Facades\Hash;
use App\checkuser;

class changePassword extends Query
{
    protected $attributes = [
        'name' => 'login',
        'description' => 'A query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'api_token' => ['name' => 'api_token', 'type' => Type::string()],
            'newpin' => ['name' => 'newpin', 'type' => Type::string()],
            'PIN' => ['name' => 'PIN', 'type' => Type::string()],

        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
     
        $user = checkuser::where('api_token',$args['api_token'])->get();
        if($user[0]->pin == hash::check($args['PIN'],$user[0]->pin)){
            $user[0]->pin = Hash::make($args['newpin']);
            $user[0]->save();
            return Status::where('id',1)->get();
        }else{
            return Status::where('id',2)->get();
        }
    }
}
//http://localhost:8000/graphql?query=query+{changePassword(api_token:"mIHy2GL3ef2kSdxHv6wDDBHNkBEJEHIXk7sw9iMN2GdaclaEKF7IRDHXY6JP",PIN:"0109",newpin:"1234"){response}}
