<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use App\Sub;

class sub_services extends Query
{
    protected $attributes = [
        'name' => 'register',
        'description' => 'A mutation'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('service'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'user_id' => ['name' => 'user_id', 'type' => Type::int()],
            'sub_service_id' => ['name' => 'sub_service_id', 'type' => Type::int()],
            'comment' => ['name' => 'comment', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {

        if(isset($args['sub_service_id'])){
            return Sub::where('id',$args['sub_service_id'])->get();
        }else{
            return Sub::all();
        }

    }
}

//http://localhost:8000/graphql?query=query+{sub_services(sub_service_id:1){id}}

//for all sub services http://localhost:8000/graphql?query=query+{sub_services{id}}
