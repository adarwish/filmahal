<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use App\Fav;
use App\Service;

class favorites extends Query
{
    protected $attributes = [
        'name' => 'favorites',
        'description' => 'A query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'user_id' => ['name' => 'user_id', 'type' => Type::int()],
            'service_id' => ['name' => 'service_id', 'type' => Type::int()],
            'api_token' => ['name' => 'api_token', 'type' => Type::string()],

        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {     
        $user = User::where('api_token',$args['api_token'])->get();
        $fav = Fav::where('user_id',$user[0]->id)->get();
        return $fav;
    }
}
//http://localhost:8000/graphql?query=query+{favorites(api_token:"mIHy2GL3ef2kSdxHv6wDDBHNkBEJEHIXk7sw9iMN2GdaclaEKF7IRDHXY6JP"){service_id,sub_service_id}}

// with user detail, http://localhost:8000/graphql?query=query+{favorites(api_token:"mIHy2GL3ef2kSdxHv6wDDBHNkBEJEHIXk7sw9iMN2GdaclaEKF7IRDHXY6JP"){id,service_id,sub_service_id,users(api_token:"mIHy2GL3ef2kSdxHv6wDDBHNkBEJEHIXk7sw9iMN2GdaclaEKF7IRDHXY6JP"){user_name}}}

// with services  details http://localhost:8000/graphql?query=query+{favorites(api_token:"mIHy2GL3ef2kSdxHv6wDDBHNkBEJEHIXk7sw9iMN2GdaclaEKF7IRDHXY6JP"){id,sub_service_id,users(api_token:"mIHy2GL3ef2kSdxHv6wDDBHNkBEJEHIXk7sw9iMN2GdaclaEKF7IRDHXY6JP"){user_name},services(service_id:1){id,name}}}

// with services and sub services details http://localhost:8000/graphql?query=query+{favorites(api_token:"mIHy2GL3ef2kSdxHv6wDDBHNkBEJEHIXk7sw9iMN2GdaclaEKF7IRDHXY6JP"){id,sub_service_id,users(api_token:"mIHy2GL3ef2kSdxHv6wDDBHNkBEJEHIXk7sw9iMN2GdaclaEKF7IRDHXY6JP"){user_name},services(service_id:1){id,name,subservice(service_id:1){id}}}}