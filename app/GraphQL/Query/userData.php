<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class userData extends Query
{
    protected $attributes = [
        'name' => 'login',
        'description' => 'A query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'api_token' => ['name' => 'api_token', 'type' => Type::string()],
            'follower_id' => ['name' => 'follower_id', 'type' => Type::int()],
            'password' => ['name' => 'password', 'type' => Type::string()],

        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        if(isset($args['api_token'])){
            $user = User::where('api_token',$args['api_token'])->get();
           // $decrypted = Crypt::decryptString($user[0]->user_name);

         //   $decrypted = decrypt($user);
            return $user;
        }else{
            return user::all();
        }

    }
}
//http://localhost:8000/graphql?query=query+{userData(api_token:"mIHy2GL3ef2kSdxHv6wDDBHNkBEJEHIXk7sw9iMN2GdaclaEKF7IRDHXY6JP"){user_name}}

// ofr all users http://localhost:8000/graphql?query=query+{userData{user_name}}

//http://smart-solution.000webhostapp.com/userAPIs(26-12-2017)/mySika/public/graphql?query=query+{userData(api_token:%22mIHy2GL3ef2kSdxHv6wDDBHNkBEJEHIXk7sw9iMN2GdaclaEKF7IRDHXY6JP%22){%20user_name,first_name,second_name,email,identity,other_name,identity_type,birth_day,address}}