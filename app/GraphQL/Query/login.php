<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Auth;
use App\checkuser;
use Illuminate\Contracts\Encryption\DecryptException;


class login extends Query
{
    protected $attributes = [
        'name' => 'login',
        'description' => 'A query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'user_name' => ['name' => 'user_name', 'type' => Type::string()],
            'email' => ['name' => 'email', 'type' => Type::string()],
            'PIN' => ['name' => 'PIN', 'type' => Type::string()],

        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $users = checkuser::all();

        foreach($users as $key => $user) {
          /* if(Auth::attempt(['pin'=> $args['PIN']])){
                return  User::all();
            }*/

            $user_name = decrypt($user->user_name);
            if($args['user_name'] == $user_name && Hash::check($args['PIN'],$user->pin)){
                $user->api_token = str_random(60);
                $user->save();
                return User::where('id',$user->id)->get();
            }
            
        }
        return null;

    }//
}
//http://localhost:8000/graphql?query=query+{login(user_name:"khaled",PIN:"1234"){api_token}}