<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\User;
use App\Service;

class services extends Query
{
    protected $attributes = [
        'name' => 'login',
        'description' => 'A query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('service'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'user_id' => ['name' => 'user_id', 'type' => Type::int()],
            'service_id' => ['name' => 'service_id', 'type' => Type::int()],
            'password' => ['name' => 'password', 'type' => Type::string()],

        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        if(isset($args['service_id'])){
     
            return Service::where('id',$args['service_id'])->get();
        }else{
            return Service::all();
        }

    }
}
// http://localhost:8000/graphql?query=query+{services(service_id:1){id}}
// for all   http://localhost:8000/graphql?query=query+{services{id}}

//with sub service http://localhost:8000/graphql?query=query+{services(service_id:2){id,subservice(service_id:2){id}}}
