<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noti extends Model
{
    protected $table = 'noti';

    public function user()
    {
      return $this->belongsTo('App\User','user_id');
    }
}
