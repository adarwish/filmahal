<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cnoti extends Model
{
    protected $table = 'noti_counter';
	
    public function user()
    {
      return $this->belongsTo('App\User','user_id');
    }
}
