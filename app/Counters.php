<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Counters extends Model
{
    protected $table = 'counters';

    public function product()
    {
      return $this->belongsTo('App\Product','product_id');
    }
}