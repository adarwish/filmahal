$(document).ready(function(){
  $('.samples-shops .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    autoplay: true,
    autoplayTimeout: 1500,
    autoplayHoverPause:true,
    dotsEach:true,
    responsive:{
        0:{
            items:2,
            nav:false,
            loop:true
        },
        
        320: {
            items: 1,
            nav:false,
            loop:true
        },
        600:{
            items:3,
            nav:false,
            loop:true
        },
        1000:{
            items:5,
            nav:false,
            loop:true
        }
    }
  });
    
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin: 42,
        stagePadding: 40,
        responsiveClass:true,
        autoplay: false,
        autoplayTimeout: 1500,
        dots: false,
        dotsEach: false,
        responsive:{
            0:{
                items:1,
                nav:true,
                loop:true
            },

            320: {
                items: 1,
                nav:true,
                loop:true
            },
            600:{
                items:2,
                nav:true,
                loop:true
            },
            1000:{
                items:3,
                nav:true,
                loop:true
            }
        }
    });
    
    $(".owl-prev").html('<i class="fa fa-angle-left" aria-hidden="true"></i>');
    
    $(".owl-next").html('<i class="fa fa-angle-right" aria-hidden="true"></i>');
    
    
     
});



$(".slide-header").click(function() {
    const parent = $(this).parent();
    $(this).find(".fa-chevron-left").toggleClass(function() {
        if($(this).hasClass("fa-chevron-left")) {
            return "fa-chevron-down";
        } else {
            return "fa-chevron-left";
        }
    });
    $(parent).find(".choose").toggle();
});

$(".navigate .fa-bars").click(function() {
    if($(".bg-white").css("display") == "none") {
        $(".bg-white").css({"right": 0, "display": "block"});
        $(this).css({"right": 217, "top": 47, "color": "#11998e"});
    } else {
        $(".bg-white").css({"right": -300, "display": "none"});
        $(this).css({"right": 0, "top": 37, "color": "#666"});
    }
});

if(window.innerWidth <= 1145) {
    $(".products").removeClass("col-md-9");
}

$('.anotherSides .img img').click(function () {
	const src = $(this).attr('src');
	$('.productPhotos .img img').attr('src', src);
});