$(document).ready(function(){
  $('.owl-carousel').eq(2).owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    autoplay: true,
    autoplayTimeout: 1500,
    autoplayHoverPause:true,
    dotsEach:true,
    responsive:{
        0:{
            items:2,
            nav:false,
            loop:true
        },
        
        320: {
            items: 1,
            nav:false,
            loop:true
        },
        600:{
            items:3,
            nav:false,
            loop:true
        },
        1000:{
            items:5,
            nav:false,
            loop:true
        }
    }
  });
    
    $('.owl-carousel:eq(0), .owl-carousel:eq(1)').owlCarousel({
        loop:true,
        margin: 42,
        stagePadding: 40,
        responsiveClass:true,
        autoplay: false,
        autoplayTimeout: 1500,
        dots: false,
        dotsEach: false,
        responsive:{
            0:{
                items:1,
                nav:true,
                loop:true
            },

            320: {
                items: 1,
                nav:true,
                loop:true
            },
            600:{
                items:2,
                nav:true,
                loop:true
            },
            1000:{
                items:3,
                nav:true,
                loop:true
            }
        }
    });
    
    $(".owl-prev").html('<i class="fa fa-angle-left" aria-hidden="true"></i>');
    
    $(".owl-next").html('<i class="fa fa-angle-right" aria-hidden="true"></i>');
    
    $('.notification-icon').on('click', function(){
    	$(this).find('.slide-down').toggleClass('hidden');
    });
    
    $('.profile').on('click', function(){
    	$(this).find('.profile-set-wrapper').toggleClass('hidden');
    });
    
    
     
});