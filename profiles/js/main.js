$(document).ready(function(){

    

    function print(x) {

        console.log(x);

    }
    
    
    $("#modal-contact").iziModal();
    

    $('.equal-companies .owl-carousel').owlCarousel({

        loop:true,

        margin: 42,

        stagePadding: 40,

        responsiveClass:true,

        autoplay: false,

        autoplayTimeout: 1500,

        dots: false,

        dotsEach: false,

        responsive:{

            0:{

                items:1,

                nav:true,

                loop:true

            },



            320: {

                items: 1,

                nav:true,

                loop:true

            },

            600:{

                items:2,

                nav:true,

                loop:true

            },

            1000:{

                items:3,

                nav:true,

                loop:true

            }

        }

    });

    

    $(".equal-companies .owl-prev").html('<i class="fa fa-angle-left" aria-hidden="true"></i>');

    

    $(".equal-companies .owl-next").html('<i class="fa fa-angle-right" aria-hidden="true"></i>');

    

/*Start Img Uploader*/

    // function readURL(input, width, height) {

        

    //     let flag;

    //     let thisImg;

    //     const thisDiv = $(input).parent().parent().find("div");

    //     const target = $(input).parents();

    //     const targetID = $(target[9]).attr("id");

        

    //     let main = thisDiv[2];

    //     const img = thisDiv[3];

        

    //     print(main);

        

    //     if($(input).val()) {

    //         const extentions = [".jpg", ".jpeg", ".png", ".gif"];

    //         const x = $(input).val();

    //         let y = x.match(/(.png)|(.jpg)|(.jpeg)|(.gif)/ig);

            

            

    //         if(y) {

    //             y = y.slice(-1);

    //                 for(const exten of extentions) {

    //                     if(exten == y) {

    //                         console.log("founded");

    //                         var reader = new FileReader();

    //                         reader.onload = function(e) {

                                

    //                         thisImg = new Image();

    //                         $(thisImg).attr('src', e.target.result);

    //                         $(thisImg).attr('class', "img-responsive");

    //                         $(img).find("img").remove();

                            

    //                         thisImg.onload = function() {

    //                             if(targetID == "navbar") {

    //                                 if(this.width == width && this.height == height) {

    //                                     $(main).css("display", "none");

                                        

    //                                     $(img).append(thisImg);

    //                                     swal(

    //                                         'Supported Format!',

    //                                         'You Uploaded an Image!',

    //                                         'success'

    //                                         );



    //                                 } else {

    //                                     $(main).css("display", "block");

    //                                     swal(

    //                                         'Dimensions MUST be 400 X 400!',

    //                                         'You Uploaded Image With unaccepted Dimensions!',

    //                                         'error'

    //                                         );

    //                                       }

    //                             } else {

    //                                 main = $(target[1]).find(".main");

    //                                 $(target[1]).css("padding", 0);

                                    

    //                                 if(this.width >= 1349 && this.height >= 471) {

    //                                     $(main).css("display", "none");

    //                                     $(img).append(thisImg);

    //                                     swal(

    //                                         'Supported Format!',

    //                                         'You Uploaded an Image!',

    //                                         'success'

    //                                         );

    //                                     $(".owl-carousel").eq(0).addClass("owl-theme");

    //                                 } else {

    //                                     $(main).css("display", "block");

                                        

    //                                     swal(

    //                                         'Dimensions Small!',

    //                                         'You Uploaded an Image is SMALL!',

    //                                         'error'

    //                                         );

    //                                       }

    //                             }

    //                                 }

    //                             }

    //                         reader.readAsDataURL(input.files[0]);

    //                         break;

    //                 }    

    //             }

    //         }

    //         else {

    //             flag = 0;

    //             console.log("Not Valid");

    //             $(thisImg).attr("src", "");

    //             $(main).css("display", "block");

    //             swal(

    //                   'Not Supported Format!',

    //                   '.jpg, .jpeg, .png, .gif!',

    //                   'error'

    //                 );

    //             }

                

    //     } else {

    //         console.log("Enter SomeThing");

    //         swal(

    //               '!Canceled',

    //               'Still on Previous Cover',

    //               'question'

    //             );

    //         }

    // }



    $("#uploader input[type='file']").change(function() {

        readIMG(this, 1349, 471);

    });

    

    $("#navbar input[type='file']").change(function() {

        readIMG(this, 400, 400);

    });



    $("#uploadProduct input[type='file']").change(function() {

        readIMG(this, 400, 400);

    });



    $("#restInfo input[type='file']").change(function() {

        readIMG(this, 400, 400);

    });

/*End Img Uploader*/

    

    

    /*Start Of 1st Carousel */

    $('.owl-carousel').eq(0).owlCarousel({

        loop:false,

        margin:0,

        responsiveClass:true,

        autoplay: true,

        autoplayTimeout: 2000,

        autoplayHoverPause:true,

        responsive:{

            0:{

                items:1,

                nav:false,

                loop: false

            }

        }

    });

    /*End Of 1st Carousel*/

    

    /*Start Of 2nd Carousel */

    $('.owl-carousel').eq(1).owlCarousel({
        navText: ['<img src="/image/SVG/arrow.svg" class="img-responsive">', '<img src="/image/SVG/arrow.svg" class="img-responsive">'],
        loop: false,
        margin: 30,
        stagePadding: 15,
        responsiveClass: true,
        autoplay: false,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        startPosition: -1,
        responsive: {
            0: {
                items: 1,
                nav: false,
                loop: false
            },

            480: {
                items: 2,
                nav: false,
                loop: false
            },

            600: {
                items: 3,
                nav: false,
                loop: false
            },

            850: {
                items: 4,
                nav: false,
                loop: false
            },

            1000: {
                items: 5,
                nav: false,
                loop: false
            },

            1270: {
                items: 5,
                nav: true,
                loop: false
            }
        }
    });
    $('.slid-cates .owl-carousel .owl-stage').children().each(function (index) {
        $(this).attr('data-position', index);
        console.log($(this).attr("data-position"));
    });

   /* $(".slid-cates .brand-name").click(function () {
    	
        var position = $(this).parent().parent();
        $(position).prependTo(".slid-cates .owl-carousel .owl-stage");
        $('.slid-cates .owl-carousel .owl-stage').trigger('to.owl.carousel', -1);
    }); */
    
    $(".slid-cates .brand-name a").click(function (e){
        e.preventDefault();
    });

    /*End Of 2nd Carousel*/

    

    

    /*Start Of Hide Element*/

    $(".item .fa-ellipsis-v, .anProduct .fa-ellipsis-v, .val .fa-ellipsis-v").click(function() {

        const firstParent = $(this).parent();

        const popUp = $(firstParent).find(".popup");

        $(".popup").addClass("hidden");

        setTimeout(function() {

            $(popUp).toggleClass("hidden");

        }, 0.1);

    });

    

    $("#navbar .contain p").click(function() {

        const parents = $(this).parents();

        const popUp = parents[1];

        const targetParent = parents[3];

        console.log(targetParent);

        $(popUp).toggleClass("hidden");

        

        $(targetParent).appendTo(".owl-stage:eq(1)");

    });



    $(".attrsVal .contain p").click(function() {

        const parent = $(this).parent().parent().parent();

        $(parent).remove();

    });

    

    /*End Of Hide Element*/

    

    /*Start 0f Active Class*/

    $("#navbar nav ul a").click(function(ev) {

        $("#navbar ul a").removeClass("active");

        $(this).addClass("active");

        ev.preventDefault();

    });

    $(".brand-name a").click(function (ev) {
        // ev.preventDefault();
        $(".brand-name a").removeClass("active");
        $(this).addClass("active");
        $(this).parent().find("input").click();
    });
    
    $('.owl-item .item').on('click', function(){
      $('.brand-name a').removeClass('active');
      $('.owl-item .item').removeClass('selected');
      $(this).find('.brand-name a').addClass('active');
      $(this).addClass('selected');
    });

    /*End 0f Active Class*/

    

    /*Start 0f Profile Section*/

    function slideDown(box, otherBoxs) {

        $(otherBoxs).hide();

        $(box).toggle();

    }





    $(".messages").click(function() {

        slideDown(".messages .slide-down, .messages .fa-caret-up", 

            ".notification-icon .slide-down, .profile div, .notification-icon .fa-caret-up, .profile .fa-caret-up");

    });

    

    $(".notification-icon").click(function() {

        slideDown(".notification-icon .slide-down, .notification-icon .fa-caret-up",

            ".profile div, .profile .fa-caret-up, .messages .slide-down, .messages .fa-caret-up");

    });
    
    $(".profile").click(function() {

        slideDown(".profile .slide-down, .profile .fa-caret-up",
        ".messages .slide-down, .messages .fa-caret-up, .notification-icon .slide-down, .notification-icon .fa-caret-up"
        );

    });
    
    

    /*End 0f Profile Section*/

    

    //Start Upload Product



    $(".uploadProduct").click(function(){



    });



    // End 0f Upload Product

    

    function sameThing2() {

        $(".mainSection").hide();

        $(".addProduct").show();

    }

    

    $(".uploadProduct").click(function() {

        sameThing2();

    });

    

   

    

    $(".classification").change(function() {

        const situation = $(this).find(":selected").val();

        const saleDateExpire = $(".inputForm").eq(2);

        const newDateExpire = $(".inputForm").eq(3);

        if(situation == 1) {

            $(saleDateExpire).removeClass("hidden");

        } else {

            $(saleDateExpire).addClass("hidden");

        }

        if(situation == 2) {

            $(newDateExpire).removeClass("hidden");

        } else {

            $(newDateExpire).addClass("hidden");

        }

        if(situation == 3) {

            $(newDateExpire).removeClass("hidden");

            $(saleDateExpire).removeClass("hidden");

        }

    });



    $(".generate .fa").click(function() {

        $(".generate").removeClass("generate").addClass("code").text(Math.floor(Math.random() * 5000001));

    });

    

    let clickedElements = [];

    

    

    $(".chooseColor, .sizes").on("click", "label", function() {

        $(this).toggleClass("checkboxStyle");

        $(this).next().prop("checked", !($(this).next().prop("checked")));

        clickedElements.push($(this).next()[0]);

    });

    

    function sameThing() {

        const  children = $("#navbar nav ul").children();

        $(".mainSection").hide();

        $(children).each(function(index){ 

            if($(children[index]).hasClass("newOffers active")) {

                $(".newOffer").show();

            } else if($(children[index]).hasClass("sales active")) {

                $(".sale").show();

            } else if($(children[index]).hasClass("shopCats active")) {

                $(".shopCat").show();

                $(".newOffer").hide();

            }

        });

        $(".addProduct").hide();

    }

    

//    $(".X").click(function(){

//        sameThing();

//       for(let i = 0; i < clickedElements.length; i++) {

//       if($(clickedElements[i]).prop("checked")) {

//                

//                $(clickedElements[i]).prev().removeClass("checkboxStyle");

//                $(clickedElements[i]).prop("checked", false);

//        } else {

//                $(clickedElements[i]).prev().addClass("checkboxStyle");

//                $(clickedElements[i]).prop("checked", true);

//            }

//        }

//        clickedElements = [];

//    });

    

    $(".X").click(function() {

        const ele = $("#navbar li .active");

        const classes = $(ele).prop("class");

    

        switch(classes.split(' ')[0]) {

            case "shopCats": $(".shopCats").trigger("click");
            
                break;

            case "sales": $(".sales").trigger("click");

                break;

            case "newOffers": $(".newOffers").trigger("click");

                break;

        }

    });

    

    $(".saveChanges").click(function(ev) {

       

        if(!$(".productCode").hasClass("code")) {

            ev.preventDefault();

            errorMessage("خطأ", "يجب توليد كود للمنتج قبل الحفظ");

        } else {

            const x = $(".chooseColor input, .sizes input");

            sameThing();

        for(let i = 0; i < x.length; i++) {

            if($(x[i]).is(":checked")) {

                clickedElements = [];

                return false;

                }

            }

        }

    });



    // Start 0f PoPups 0f Product

    $(".options").on("click", "li", function() {

        const getClass = $(this).prop("class");



        switch(getClass) {

            case "addToNew": 

                swal({

                    title: "ادخل التاريخ",

                    html: 

                    `

                    <label>يوم الانتهاء من العروض الجديده</label>

                    

                    <input type="date" id="newDate">`,

                    allowOutsideClick: false,

                    confirmButtonClass: "dateAll",

                    showCancelButton: true,

                    cancelButtonClass: "cancelAll",

                });



                on_off(true);



                $("#newDate").change(function() {

                    validate("#newDate");

                });



                $(".cancelAll").click(function() {

                    $("#saleDate, #newDate, #salePercent").val("");

                });

                break;

            case "addToSale":

                swal({

                    title: "ادخل التاريخ",

                    html: 

                    `

                    <label>يوم الانتهاء من التخفيضات</label>

                    

                    <input type="date" id="saleDate">

                    

                    <label>نسبه التخفيض</label>

                    <input type="number" id="salePercent" min="0" step="0.01">

                    `,

                    allowOutsideClick: false,

                    confirmButtonClass: "dateAll",

                    showCancelButton: true,

                    cancelButtonClass: "cancelAll",

                });



                on_off(true);

                

                $("#saleDate").change(function() {

                    validate("#saleDate", "#salePercent");

                });



                $("#salePercent").keyup(function() {

                    validate("#saleDate", "#salePercent");

                });



                $(".cancelAll").click(function() {

                    $("#saleDate", "#salePercent").val("");

                });



                break;

            case "addToAll":

              

                swal({

                    title: "ادخل التاريخ",

                    html: `

                    <label>يوم الانتهاء من التخفيضات</label>

                    

                    <input type="date" id="saleDate">

                    

                    <label>نسبه التخفيض</label>

                    <input type="number" id="salePercent" min="0" step="0.01">

                    

                    <label>يوم الانتهاء من العروض الجديده</label>

                    

                    <input type="date" id="newDate">`,

                    allowOutsideClick: false,

                    confirmButtonClass: "dateAll",

                    showCancelButton: true,

                    cancelButtonClass: "cancelAll",

                });



                on_off(true);



                $("#saleDate, #newDate").change(function() {

                    validate("#saleDate", "#newDate", "#salePercent");

                });



                $("#salePercent").keyup(function() {

                    validate("#saleDate", "#newDate", "#salePercent");

                });



                $(".cancelAll").click(function() {

                    $("#saleDate, #newDate, #salePercent").val("");

                });

                

                break;

            case "editProduct":

                sameThing2();

                break;

            case "deleteProduct":

                swal({

                  title: 'Are you sure?',

                  text: "You won't be able to revert this!",

                  type: 'warning',

                  showCancelButton: true,

                  confirmButtonColor: '#3085d6',

                  cancelButtonColor: '#d33',

                  confirmButtonText: 'Yes, delete it!'

                }).then((result) => {

                  if (result.value) {

                    swal(

                      'Deleted!',

                      'Your file has been deleted.',

                      'success'

                    )

                   $(this).parents(".anProduct").remove();

                    }



                    });



                break;

        }



       

    });



 function on_off(state, button = ".dateAll") {

     $(button).prop("disabled", state);

 }    



 function validate(...pars) {

            let z = 0;

            for(par of pars) {

                if($(par).val().trim()) {

                    ++z;

                }

                z == pars.length ? on_off(false) : on_off(true);

            }  

        }



    $(".messageBox textarea").focus(function() {

        $(".messageClicked .messageBox .fa").css("text-shadow", "-1px 0 #11998e, 1px 0 #11998e, 0 -1px #11998e");

    });



     $(".messageBox textarea").focusout(function() {

        $(".messageClicked .messageBox .fa").css("text-shadow", "-1px 0 #d3d3d3, 1px 0 #d3d3d3, 0 -1px #d3d3d3");

    });



     $(".message").click(function(ev) {

        ev.preventDefault();

        $("#uploader .messageContainer").show();

     });



     $(".exit").click(function(ev) {

        ev.preventDefault();

        $(this).parents(".messageContainer").hide();

     });



    

    $("html").click(function() {

        $(".slide-down, .profile div, .fa-caret-up").hide();

        $(".popup").addClass("hidden");

    });



    $(".messages, .profile, .notification-icon, .photo, .cover, .anProduct").click(function(event){

        event.stopPropagation();

    });



    $(".contactus").click(function() {

        $(".myMap, #restInfo").show();

        $("#navbar .slid-cates, #bigPopUp, .newOffer, .sale").hide();

        $(".myMap").css("position", "relative");

        $(".myMap").css("left", "auto");

        $(".myMap").css("width", "auto");

        $(".myMap").parent().removeClass("row");

        $(".myMap").parent().parent().removeClass("container");

        $("#addAddress").show();

    });



    $(".shopCats").click(function() {

        $(".myMap, .newOffe, .sale").hide();
        $(".slid-cates .owl-item").eq(0).find(".brand-name a").click();
        $("#navbar .slid-cates, #bigPopUp, .mainSection:eq(0), .wholeSection").show();

        $(".myMap").parent().addClass("row");

        $(".myMap").parent().parent().addClass("container");

        $("#addAddress, #restInfo, #uploadProduct").hide();

    });



    $(".newOffers, .sales").click(function() {

        $(".slid-cates, .mainSection, #restInfo, .myMap, .wholeSection").hide();

        if($(this).hasClass("newOffers")) {

            $("#bigPopUp, .newOffer").show();

            customizeReview(".newOffer .reviewExample");

        } else {

            $("#bigPopUp, .sale").show();    

            customizeReview(".sale .reviewExample");

        }

    });



    function customizeReview(selected) {

        $(selected).last().css({"border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px", "margin-bottom": "15px", "border-bottom": "none"});

    }

    

    // End 0f PoPups 0f Product

    // Start 0f Edit Place

    $(".edit").click(function(ev) {

        ev.preventDefault();

        swal({

                html: '<textarea class="editPlace"></textarea>',

                allowOutsideClick: false,

                confirmButtonClass: "dateAll",

                showCancelButton: true,

                cancelButtonClass: "cancelAll",

            })

        on_off(true);

        const place = $(this).prev().find("span");

        $(".editPlace").val($(place).text());

        $(".editPlace").keyup(function() {

            validate(".editPlace");

        });

        $(".dateAll").click(function(ev) {

            ev.preventDefault();

            $(place).text($(".editPlace").val());

        });

        

    });



    if($(window).width() <= 891) {

        $("#addAddress").appendTo(".customizeHere");

    } else {

        $("#addAddress").appendTo(".myMap");

    }

    // End 0f Edit Place



    $("#infowindow-content a").click(function(ev) {

        ev.preventDefault();

        

    });



    //  $(".addAnotherAddress div, .addAnotherPhone div").click(function(ev) {

    //     ev.preventDefault();

    //     if($(this).parent().hasClass("addAnotherAddress")) {

    //         swal({

    //             html: `<label>العنوان الرئيسى</label><textarea class="addPlace"></textarea>

    //             <label>علامات مميزة</label><textarea class="addMarker"></textarea>

    //             `,

    //             allowOutsideClick: false,

    //             confirmButtonClass: "dateAll",

    //             showCancelButton: true,

    //             cancelButtonClass: "cancel",

    //         })



    //         on_off(true);



    //         $(".addPlace, .addMarker").keyup(function() {

    //             validate(".addPlace", ".addMarker");

    //         });

    // /************************************************************************************ */

    //         //Start 0f Get Data 0f AddPlace and AddMarker Inputs

    //         $(".dateAll").click(function(ev) {

    //             ev.preventDefault(); 

    //         });

    //         //End 0f Get Data 0f AddPlace and AddMarker Inputs

    //     } else {

    //         swal({

    //             html: `<label>ادخل رقم التواصل مع المحل</label>

    //             <input type="text" name="phone" class="addPhone">

    //             `,

    //             allowOutsideClick: false,

    //             confirmButtonClass: "dateAll",

    //             showCancelButton: true,

    //             cancelButtonClass: "cancel",

    //         })

    //         on_off(true);

    //         $(".addPhone").keyup(function() {

    //             validate(".addPhone");

    //         });

    //     }

    // });



    $(".editTime").click(function(ev) {

        ev.preventDefault();

        swal({

            html: `<div class="choose-time">

                                    <div class="days">

                                        <span class="from">من يوم</span>

                                        <select name="from">

                                            <option value="السبت" selected="">السبت</option>

                                            <option value="الأحد">الأحد</option>

                                            <option value="الأثنين">الأثنين</option>

                                            <option value="الثلاثاء">الثلاثاء</option>

                                            <option value="الأربعاء">الأربعاء</option>

                                            <option value="الخميس">الخميس</option>

                                            <option value="الجمعه" >الجمعه</option>

                                        </select>

                                        <span>الى يوم</span>

                                        <select name="to">

                                            <option value="السبت">السبت</option>

                                            <option value="الأحد">الأحد</option>

                                            <option value="الأثنين">الأثنين</option>

                                            <option value="الثلاثاء">الثلاثاء</option>

                                            <option value="الأربعاء">الأربعاء</option>

                                            <option value="الخميس">الخميس</option>

                                            <option value="الجمعه" selected="">الجمعه</option>

                                        </select>

                                    </div>

                                    <br>

                                    <div class="openClose">

                                        <span class="from">من</span>

                                        <select name="h1" class="hoursOfDay">

                                            <!--Added By JS-->

                                        <option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select>

                                        <select name="modeOpening">

                                            <option value="صباحاً">صباحاً</option>

                                            <option value="مساءً">مساءً</option>

                                        </select>

                                        <br>

                                        <span>الى</span>

                                        <select name="h2" class="hoursOfDay">

                                            <!--Added By JS-->

                                        <option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select>

                                        <select name="modeClosing">

                                            <option value="صباحاً">صباحاً</option>

                                            <option value="مساءً">مساءً</option>

                                        </select>

                                    </div>

                                </div>`,

            customClass: "editDate",

            allowOutsideClick: false,

            confirmButtonClass: "editNow",

            showCancelButton: true,

            cancelButtonClass: "cancel",

        });

        $(".editNow").click(function() {

            const spans = $(".fromTo").find("span");

            $(spans).eq(0).text($(".days select").eq(0).val());

            $(spans).eq(1).text($(".days select").eq(1).val());

            $(spans).eq(2).text($(".openClose select").eq(0).val());

            $(spans).eq(3).text($(".openClose select").eq(1).val());

            $(spans).eq(4).text($(".openClose select").eq(2).val());

            $(spans).eq(5).text($(".openClose select").eq(3).val());

        });

    });

    // START 0f MAP

    

// END 0f MAP

    

    $("#emoji").emojioneArea();

    $(".fa-thumbs-o-up").click(function() {

        $(".emojionearea-editor").append('<img alt="👍" class="emojioneemoji" src="https://cdn.jsdelivr.net/emojione/assets/3.1/png/32/1f44d.png">');

    });



    $(".fa-camera").click(function() {

        $(".fa-camera input[type='file']").change(function() {

        readIMG(this, 400, 400);

        });

    });



    /* $(".profile .profile-settings").on("click", "a", function(ev) {

        ev.preventDefault();

        $("#uploader, #navbar, #bigPopUp, #restInfo").hide();

        $("#profile_settings").show();

        repeater(this);

    }); */



    // function repeater(self) {

    //     if($(self).hasClass("reviewsPeople")) {

    //         $(".inbox, .allSettings").hide();

    //         $(".reviewsBox").show();

            

    //         $(".settings li a").removeClass("focus");

    //         $(".settings .reviewsPeople").addClass("focus");

            

    //     } else if($(self).hasClass("boxMessages")) {

    //         $(".inbox").show();

    //         $(".reviewsBox, .allSettings").hide();

    //         $(".settings li a").removeClass("focus");

    //         $(".settings .boxMessages").addClass("focus");

            

    //     } else if($(self).hasClass("settingProfile")) {

    //         $(".inbox, .reviewsBox").hide();

    //         $(".allSettings").show();

    //         $(".general").trigger("click");

    //         $(".settings li a").removeClass("focus");

    //         $(".settings .settingProfile").addClass("focus");

    //     }

    // }

    

    // $($(".reviewsPeople").eq(1)).click(function(ev) {

    //     ev.preventDefault();

    //     repeater(this);

    // });

    

    // $($(".boxMessages").eq(1)).click(function(ev) {

    //     ev.preventDefault();

    //     repeater(this);

    // });

    

    // $($(".settingProfile").eq(1)).click(function(ev) {

    //     ev.preventDefault();

    //     repeater(this);

    // });





    // $(".settings li").on("click", "a", function(ev) {

    //     ev.preventDefault();

    //     $(".settings li a").removeClass("focus");

    //     $(this).addClass("focus");

    // }); 



    $("#mainPage").click(function(ev) {

        $("#profile_settings").hide();

        $("#uploader, #navbar, #bigPopUp").show();

    });





    // HERE

    var clickedElementss = [];

    $(".chooseCat").on("click", "label", function() {

        $(this).toggleClass("checkboxStyle");

        $(this).next().prop("checked", !($(this).next().prop("checked")));

        clickedElementss.push($(this).next()[0]);

    });

     $("#close2").click(function() {

        $(".allSettings").show();

        $("#third-step2").hide();

        for(var i = 0; i < clickedElementss.length; i++) {

        if($(clickedElementss[i]).prop("checked")) {

                    $(clickedElementss[i]).prev().removeClass("checkboxStyle");

                    $(clickedElementss[i]).prop("checked", false);

            } else {

                    $(clickedElementss[i]).prev().addClass("checkboxStyle");

                    $(clickedElementss[i]).prop("checked", true);

                }

            }

        clickedElementss = [];

        $("#errorCates").text("");

        });



     $("#save2").click(function(ev) {

        ev.preventDefault();

        var x = $(".chooseCat input");

        var z = 0;

        $("#cates-list").text("");

        for(var i = 0; i < x.length; i++) {

            if($(x[i]).is(":checked")) {

                z = 1;

            }

        }

        if(z) {

            $(".allSettings").show();

        $("#third-step2").hide();

            clickedElementss = [];

            $("#errorCates").text("");

        } else {

            $("#errorCates").text("اختر فئة واحده على الاقل");

        }

    });



    $("#cates").click(function() {

        $(".allSettings").hide();

        $("#third-step2").show();

    })



    $(".general").click(function() {

        $(".generalElments").show();

        $(".specificElments").hide();

        $(this).css({"background-color": "#11998e", "color": "#fff"});

        $(".specific").css({"background-color": "#fff", "color": "#333"});

    });



    $(".specific").click(function() {

        $(".generalElments").hide();

        $(".specificElments").show();

        $(this).css({"background-color": "#11998e", "color": "#fff"});

        $(".general").css({"background-color": "#fff", "color": "#333"});

    });

    // HER





    $(".fa-bars").click(function() {

        const parent = $(this).parent().parent();

        const asideContainer = $(parent).find(".asideContainer");



        if($(asideContainer).css("display") == "none") {

            $(asideContainer).css({"right": 0, "display": "block"});

            $(asideContainer).removeClass("col-xs-4 col-md-3");

            $(asideContainer).addClass("col-xs-12");

            $(this).css("color", "#11998e");

        } else {

            $(asideContainer).css({"right": -300, "display": "none"});

            $(this).css("color", "#666");

        }

    });

    

    if(window.innerWidth <= 1145) {

        $(".products").removeClass("col-md-9");

    }



    $(".getPhone").click(function() {

        $(this).children('span:first-of-type').hide();
        $(this).children('span:last-of-type').show();

    });



    $(".sendMessage").click(function() {

        $("#restInfo .messageContainer").show();

    });

    

    $(".moreInfo").click(function(ev) {

        ev.preventDefault();

        $("#uploadProduct").show();

        $(".shopCat, .sale, .newOffer, .wholeSection").hide();

    });

    $(".slid-cates .owl-next, .slid-cates .owl-prev").hover(function () {
        if ($(this).hasClass("disabled")) {
            $(this).find("i").css("color", "#11998e");
            $(this).css("cursor", "no-drop");
        } else {
            $(this).find("i").css("color", "#30baaf");
            $(this).css("cursor", "pointer");
        }
    }, function () {
        $(this).find("i").css("color", "#11998e");
        if ($(this).hasClass("disabled")) {
            $(this).css("cursor", "no-drop");
        } else {
            $(this).css("cursor", "pointer");
        }
    });


    $(".slid-cates .owl-item").eq(0).find(".brand-name a").click();

$(".buttonsActions li").eq(2).click(function() {
    $(this).toggleClass("redborder");
    $(this).find("i").toggleClass("redheart"); 
});

$(".buttonsActions li").eq(1).click(function() {
    $("#navbar .messageContainer").eq(1).show();
});

$(".buttonsActions li").last().click(function() {
    
    $("#navbar .messageContainer").eq(0).show();
    
});

$(".sale .textBar .search input").keyup(function(ev) {
    if(ev.which == 13 && !ev.shiftkey) {
        const newReview = $(".newreviewExample").last().clone(true);
        const profilePic = $(".sale .textBar .search img").prop("src");
        $(newReview).find(".img-container img").prop("src", `${profilePic}`);
        $(newReview).find("h5").text($(".client-title h4").text());
        $(newReview).find(".counterLikes").text("0");
        $(newReview).find(".textReview").text($(this).val());
        
        $(this).val("");
    }
}); 

$(document).mouseup(function(e) 
{
    var container = $(".messageclicked");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        $('.exit').trigger('click')
    }
});

});

    // AH Remove Discount Number onClick
    $('#nav-discount').click(function () {
	   $('#nav-discount .numNotify').remove();
    });





    $('.anotherSides .img img').click(function () {
	   const src = $(this).attr('src');
        $('.productPhotos .img img').attr('src', src);
    });

    $('.likeBtn').click(function () {
        const counter = $(this).parent().find('.counterLikes');
        counter.html(parseInt(counter.html(), 10)+1);
    });



    $(document).ready(function() {
        var $header = $("#mainFixedNav"),
            $clone = $header.before($header.clone().addClass("clone"));
        
        $(window).on("scroll", function() {
            var fromTop = $(window).scrollTop();
            $("body").toggleClass("down", (fromTop > 400));
        });
 
        
    });
    

   
