$(function() {
    var valid = "تم التطابق";
    var inValid = "خطأ";
    
    function validStyle(ele) {
        ele = ele.next();
        ele.text(valid);
        ele.css({"color":"#11998e", "font-weight":"bold"});
    };
    
    function inValidStyle(ele, text=inValid) {
        ele = ele.next();
        ele.text(text);
        ele.css({"color":"red", "font-weight":"bold"});
    };
    
    
    
    $("#submit").click(function(ev) {
        ev.preventDefault();
        var username = $("#username");
        var password = $("#password");
        var email = $("#email");
        
        var fullNameRegex = /^[A-z ء-ي]{4,}$/g;
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var phoneRegex = /^[\d۰-۹]{6,11}$/g;
        var pwRegex = /^[A-z\d]+[^\^!@#$%&)(\][:;'|"\`\~\/\\\*\-\+\=\<>?\؟]{7,16}$/g;


        var usernameValue = username.val().trim();
        var pwValue = password.val().trim();
        var emailValue = email.val().trim();
        
        var fullNameTest = fullNameRegex.test(usernameValue);
        var userPhoneTest = phoneRegex.test(usernameValue);
        var emailTest = emailRegex.test(emailValue);
        var pwTest = pwRegex.test(pwValue);

        fullNameTest ? validStyle(username) : userPhoneTest ? validStyle(username) : inValidStyle(username, "ادخل اسم المستخدم ٤ حروف على الأقل أو رقم التليفون");

        emailTest ? validStyle(email) : inValidStyle(email, "الأميل الالكترونى غير صحيح");
        
        pwTest ? validStyle(password) : inValidStyle(password, "ادخل ارقام وحروف فقط على الأقل ۸");
        
        fullNameTest && emailTest && pwTest ? $("#registerForum").submit() : console.log('no');
        
    });
    
        $('.container').addClass('container-fluid').removeClass('container')
    
});

document.getElementById("userPhoto").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("user-image-auth").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};

$('.videoVideo').parent().click(function () {
  if($(this).children(".videoVideo").get(0).paused){
          $(this).children(".videoVideo").get(0).play();
          $(this).children(".playpause").fadeOut();
    	  } else{
    	  	$(this).children(".videoVideo").get(0).pause();
  		$(this).children(".playpause").fadeIn();
    	  }
});
$('#exampleModal').click(function () {
	setTimeout(function () {
		if (!$('#exampleModal').hasClass('in')) {
			$('.videoVideo').parent().children(".videoVideo").get(0).pause();
  			$('.videoVideo').parent().children(".playpause").fadeIn();
		} else {
			console.log('');
		}}, 500);
});