$(function() {
    var valid = "تم التطابق";
    var inValid = "خطأ";
    
    function validStyle(ele) {
        ele = ele.next();
        ele.text(valid);
        ele.css({"color":"#11998e", "font-weight":"bold"});
    };
    
    function inValidStyle(ele, text=inValid) {
        ele = ele.next();
        ele.text(text);
        ele.css({"color":"red", "font-weight":"bold"});
    };
    
    
    
    $("#submit").click(function(ev) {
        ev.preventDefault();
        var username = $("#username");
        var password = $("#password");
        var fullNameRegex = /^[A-z ء-ي]{3,}$/g;
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var phoneRegex = /^[\d۰-۹]{6,11}$/g;
        var pwRegex = /^[A-z\d]+[^\^!@#$%&)(\][:;'|"\`\~\/\\\*\-\+\=\<>?\؟]{7,16}$/g;


        var usernameValue = username.val().trim();

        var fullNameTest = fullNameRegex.test(usernameValue);
        var emailTest = emailRegex.test(usernameValue);
        var phoneTest = phoneRegex.test(usernameValue);
        var pwTest = pwRegex.test(password);
        
        if(fullNameTest || emailTest || phoneTest) {
            validStyle(username);
        } else {
            inValidStyle(username);
        }
        
        pwTest ? validStyle(password) : inValidStyle(password, "ادخل فقط ارقام وحروف");
        
    });
    
    $("#pass img").click(function() {
        const input = $("#pass input");
        if($(input).attr("type") == "password") {
            $("#pass input").attr("type", "text");
            $(this).css("top", "-15px");
        } else {
            $("#pass input").attr("type", "password");
            $(this).css("top", "auto");
        }
    });
    
    $("#forgotSend").click(function(ev) {
        ev.preventDefault();
        var email = $("#forgotEmail");
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


        var emailValue = email.val().trim();

        var emailTest = emailRegex.test(emailValue);
        
        emailTest ? validStyle(email) : inValidStyle(email, "ادخل ايميل صحيح");
        
        emailTest ? $("#forgotFormOne").submit() : console.log(emailTest);
        
    });
    
});