$(function() {
    
    for(var i = 1; i <= 12; i++)
    {
        $(".hoursOfDay").append('<option value="' + i + '">' + i + '</option>');
    }
    
$("#create-account").click(function (ev) {
    ev.preventDefault();

    $("#howItWorks, #second-step, #third-step").css("display", "none");
    $(".step .num:eq(0)").css({
        "background-color": "#11998e",
        "color": "#fff"
    });
    $("#step1, #first_step").css("display", "block");
});
    
    var valid = "تم التطابق";
    var inValid = "خطأ";
    var Original = "20 حرف";
    var emptyMsg = "يجب ادخال هذا الحقل";
    var fullNameMsg = "برجاء ادخال الإسم الكامل";
    var pwLengthMsg = "الرقم السري يجب ألا يقل عن 8 حروف أو أرقام";
    var clickedElements = [];
    function validStyle(ele) {
        ele = ele.next();
        ele.text(valid);
        ele.css({"color":"#11998e", "font-weight":"bold"});
    };
    
    function inValidStyle(ele, text = inValid) {
        ele = ele.next();
        ele.text(text);
        ele.css({"color":"red", "font-weight":"bold"});
    };
    
    function emptyField(ele, text = emptyMsg) {
        ele = ele.next();
        ele.text(text);
        ele.css({"color":"red", "font-weight":"bold"});
    };
    
    function fullNameMsgError(ele, text = fullNameMsg) {
        ele = ele.next();
        ele.text(text);
        ele.css({"color":"red", "font-weight":"bold"});
    };
    
    function pwLengthMsgError(ele, text = pwLengthMsg) {
        ele = ele.next();
        ele.text(text);
        ele.css({"color":"red", "font-weight":"bold"});
    };
    
    
    function originalStyle(ele, original = Original) {
        ele = ele.next();
        ele.text(original);
        ele.css({"color":"#666", "font-weight":"normal"});
    }


$("#next-step2").click(function (ev) {
    ev.preventDefault();
    
    var fullNameRegex = /^[A-z ء-ي]{3,}$/g;
    var userNameRegex = /^[A-zء-ي]{3,}$/g;
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;                          
    var phoneRegex = /^[\+]?[\d۰-۹]{6,11}$/g;
    var pwRegex = /^[A-z\d]+[^\^!@#$%&)(\][:;'|"\`\~\/\\\*\-\+\=\<>?\؟]{7,16}$/g;
    
    var fullName = $("#fullName");
    var email = $("#email");
    var phone = $("#phone");
    var userName = $("#userName");
    var pw = $("#password");
    
    var allSpan = $("#first_step input + span");
    
    var fullNameTest = fullNameRegex.test(fullName.val().trim());
    fullNameRegex.lastIndex = 0;
    var emailTest = emailRegex.test(email.val().trim());
    var phoneTest = phoneRegex.test(phone.val().trim());
    var pwTest = pwRegex.test(pw.val().trim());
    var userNameTest = userNameRegex.test(userName.val().trim());
    var isEmailEmpty = email.val();
    var isPwEmpty = pw.val();
    var isNameEmpty = fullName.val();
    var isPhoneEmpty = phone.val();
    var isUserEmpty = userName.val();
    
    if(fullNameTest && emailTest && phoneTest && pwTest && userNameTest) {
        
        allSpan.text(valid);
        
        $("#first_step").css("display", "none");
        $(".step .num:eq(1)").css({"background-color": "#11998e","color": "#fff"});
        $(".step .num:eq(0)").css({"background-color": "#fff","color": "#11998e"});
        $("#second-step").css("display", "block");
    }
    
    isNameEmpty ? 
isNameEmpty.length >= 4 ? 
fullNameTest ? validStyle(fullName) : inValidStyle(fullName, "الاسم يجب ان يكون حروف فقط بدون ارقام او رموز") : 
        fullNameMsgError(fullName) : 
        emptyField(fullName);  
          
    isEmailEmpty ? emailTest ? validStyle(email) : inValidStyle(email, "الأميل يجب ان يحتوى على @ و .") : emptyField(email);
    isPhoneEmpty ? phoneTest ? validStyle(phone) : inValidStyle(phone, "يجب ان يكون ارقام فقط من 6 الى 11 رقم") : emptyField(phone);
    isUserEmpty ? userNameTest ? validStyle(userName) : inValidStyle(userName, "يجب ان يكون كلمه واحده فقط واكثر من حرفين") : emptyField(userName);
    
    isPwEmpty ? 
    isPwEmpty.length >= 8 ?
    pwTest ? validStyle(pw) : inValidStyle(pw, "كلمه السر يجب الا تحتوى على رموز غير '_' ")
    : pwLengthMsgError(pw)
    : emptyField(pw);
    
});

$("#next-step0").click(function(ev) {
    ev.preventDefault();
   $("#first_step").hide();
   $("#howItWorks").show();
    $(".step .num:eq(0), .step .num:eq(1)").css({
        "background-color": "#fff",
        "color": "#11998e"
    });
});


$("#prev-step").click(function (ev) {
    ev.preventDefault();
    
    $("#step1, #first_step").css("display", "block");
    $("#second-step").css("display", "none");
    $(".step .num:eq(0)").css({
        "background-color": "#11998e",
        "color": "#fff"
    });
    $(".step .num:eq(1)").css({
        "background-color": "#fff",
        "color": "#11998e"
    });

    
});

$("#next-step3").click(function (ev) {
   ev.preventDefault();
   
   
   $(".step .num:eq(1)").css({
        "background-color": "#fff",
        "color": "#11998e"
    });
    $(".step .num:eq(2)").css({
        "background-color": "#11998e",
        "color": "#fff"
    });
    
  });

$("#next-step3").click(function (ev) {
   ev.preventDefault();
      
    var nameRegex = /^[\d\w\W]+$/g;
    var addressRegex =/^[\d\w\W]+$/g;
    var nameEnRegex = /^[\dA-z-_!@#$%^&*)(-+,.<>?\/\\)]+$/g;
    var shopName = $("#shopName");
    var shopNameEN = $("#shopNameEN");
    var famousName = $("#famousName");
    var mainAddress = $("#mainAddress");
    var specialMarks = $("#specialMarks");
    
    var shopNameTest = nameRegex.test(shopName.val().trim());
    nameRegex.lastIndex = 0;
    var shopNameENTest = nameEnRegex.test(shopNameEN.val().trim());
    nameEnRegex.lastIndex = 0;
    var famousNameTest = nameRegex.test(famousName.val().trim());
    nameRegex.lastIndex = 0;
    var mainAddressTest = addressRegex.test(mainAddress.val().trim());
    addressRegex.lastIndex = 0;
    var specialMarksTest = addressRegex.test(specialMarks.val().trim());
    addressRegex.lastIndex = 0;
    
    if(shopNameENTest) {
        validStyle(shopNameEN);
    } else if(shopNameEN.val() == "") {
        {originalStyle(shopNameEN);}
    } else {
        inValidStyle(shopNameEN, "يجب ادخال اسم المحل باللغه الأنجليزيه");
    }
    
    if(famousNameTest) {
        validStyle(famousName);
    } else if(famousName.val() == "") {
        {originalStyle(famousName);}
    } else {
        inValidStyle(famousName);
    }
    
    if(shopNameTest && mainAddressTest && specialMarksTest) {
        validStyle(shopName);
        validStyle(mainAddress);
        validStyle(specialMarks);
        
         $("#first_step, #second-step").css("display", "none");
        $("#third-step").css("display", "block");
        
    } else {
    shopNameTest ? validStyle(shopName) : inValidStyle(shopName, "يجب ادخال اسم المحل");
    mainAddressTest ? validStyle(mainAddress) : inValidStyle(mainAddress, "يجب ادخال العنوان الرئيسى للمحل");
    specialMarksTest ? validStyle(specialMarks) : inValidStyle(specialMarks, "يجب ادخال علامات مميزه للمحل");

    }
    
   
    
    
});
    
    $("#prev-to-secondStep").click(function(ev) {
        $("#third-step").css("display", "none");
        $("#second-step").css("display", "block");
    });

     $("#thanks").click(function(ev) {
        var cons = $("#cons");
        var sub = $("#subscribe");
        var cats = $("#cates");
        var nums =  $("#cates-list").children().length;
        
        if(cons.is(":checked") && sub.is(":checked") && nums) {
                $(".thanks").removeClass("hidden");
                $(".thanks").css("margin-top", "0");
                $("body").css("overflow", "hidden");
        } else {
            ev.preventDefault();
            cons.is(":checked") ? originalStyle(cons, "") : inValidStyle(cons, "يجب الموافقة على شروط الموقع");
            // sub.is(":checked") ? originalStyle(sub, "") : inValidStyle(sub, "يجب الاشتراك فى نشرة الموقع لمعرفة كل جديد");
            nums ? originalStyle(cats, "") : inValidStyle(cats, "يجب اختيار الفئات");
        }
    });
    
    $("#kinds").click(function() {
        $("#third-step").css("display", "none");
        $("#third-step1").css("display", "block");

    });
    
    
    $("#cates").click(function() {
       $("#third-step, #third-step1").css("display", "none");
        $("#third-step2").css("display", "block");
    });
    
    function sameThing() {
        $("#third-step").css("display", "block");
        $("#third-step1").css("display", "none");
    }
    
    $("#chooseNow, .chooseCat").on("click", "label", function() {
        $(this).toggleClass("checkboxStyle");
        $(this).next().prop("checked", !($(this).next().prop("checked")));
        clickedElements.push($(this).next()[0]);
    });
    
    $("#close").click(function() {
        sameThing();
        for(var i = 0; i < clickedElements.length; i++) {
       if($(clickedElements[i]).prop("checked")) {
                console.log($(clickedElements[i]));
                $(clickedElements[i]).prev().removeClass("checkboxStyle");
                $(clickedElements[i]).prop("checked", false);
        } else {
                $(clickedElements[i]).prev().addClass("checkboxStyle");
                $(clickedElements[i]).prop("checked", true);
            }
        }
        clickedElements = [];
        $("#errorKinds").text("");
    });

    function getInfo(info, ele) {
        $(ele).append("<li>" + $(info).prev().text() + " </li>")
    }
    
    $("#save").click(function(ev) {
        ev.preventDefault();
        
        var x = $("#chooseNow input");
        var z = 0;
        $("#kinds-list").text("");
        for(var i = 0; i < x.length; i++) {
            if($(x[i]).is(":checked")) {
               z = 1;
               getInfo(x[i], "#kinds-list");
            }
        }
        if(z) {
            sameThing();
            clickedElements = [];
            $("#errorKinds").text("");
        } else {
            $("#errorKinds").text("اختر فئة واحده على الاقل");
        }
    });
    
     function sameThing2() {
        $("#third-step1, #third-step2").css("display", "none");
        $("#third-step").css("display", "block");
    }
    
    $("#close2").click(function() {
       sameThing2(); 
         for(var i = 0; i < clickedElements.length; i++) {
       if($(clickedElements[i]).prop("checked")) {
                $(clickedElements[i]).prev().removeClass("checkboxStyle");
                $(clickedElements[i]).prop("checked", false);
        } else {
                $(clickedElements[i]).prev().addClass("checkboxStyle");
                $(clickedElements[i]).prop("checked", true);
            }
        }
       clickedElements = [];
        $("#errorCates").text("");
    });
    
    $("#save2").click(function(ev) {
        ev.preventDefault();
        var x = $(".chooseCat input");
        var z = 0;
        $("#cates-list").text("");
        for(var i = 0; i < x.length; i++) {
            if($(x[i]).is(":checked")) {
                z = 1;
                getInfo(x[i], "#cates-list");
            }
        }
        if(z) {
            sameThing2();
            clickedElements = [];
            $("#errorCates").text("");
        } else {
            $("#errorCates").text("اختر فئة واحده على الاقل");
        }
    });
    
    $(".thanks button").click(function() {
        $("#submit-shop").trigger("click");
    });
    
    $("#submit-shop").css("display", "none");
    
    $("#pass img").click(function() {
        const input = $("#pass input");
        if($(input).attr("type") == "password") {
            $("#pass input").attr("type", "text");
            $(this).css("top", "-16px");
        } else {
            $("#pass input").attr("type", "password");
            $(this).css("top", "auto");
        }
    });

    const alphaEn = "abcdefghijklmnopqrstuvwxyzAB";
    const alphaAr = "ابتثجحخدذرزسشصضطظعغفقكلمنهوى";
    let chars = [];
    const catLabels = $(".chooseCat .text-center label");
    function appendToChooseCat(char, parent, Class) {
        if(Class == false) {
            var index = alphaAr.search(char);
            Class = alphaEn[index];
        }
        if($(`.${Class}`).children().length) {
            $(`.${Class}`).append(parent);
        } else {
            $(".chooseCat").append(`<div data-alpha="${Class}" class="${Class} col-xs-12 col-sm-4 text-center"><h3 class="text-bold text-right primary">${char}</h3></div>`);
            $(`.${Class}`).append(parent);
        } 
    }
    catLabels.each(function() {
        const firstChar = $(this).text().charAt(0);
        const parent = $(this).parent().detach().clone(true);
        switch(firstChar) {
            case "ء":
            case "ا":
            case "أ":
            case "آ":
            case "إ":
            case "ئ":
                appendToChooseCat(firstChar, parent, "a");
                break;
            case "ت":
            case "ة":
                appendToChooseCat(firstChar, parent, "b");
                break;
            default: 
                appendToChooseCat(firstChar, parent, false);
        }

        if(!chars.includes(firstChar)) {
            chars.push(firstChar);
        }
    });

    chars.sort();
    for(char of chars) {
        $(`.${alphaEn[alphaAr.search(char)]}`).detach().clone(true).appendTo(".chooseCat");
    }

    
});