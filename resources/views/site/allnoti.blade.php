@extends('site.master')
@section('content')

    <div class="navigation">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p class="navigationText text-bold">
                        <a href="#" id="mainPage">الرئيسيه</a> >> اعدادات
                    </p>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 nameEmail notiEmailName">
            <div class="message-header">
                <div class="img-container">
                    <img src="/image/{{Auth::user()->photo}}" class="img-responsive">
                </div>              
                <div class="client-title">
                    <h4>{{Auth::user()->user_name}}</h4>
                    <p>{{Auth::user()->email}}</p>
                </div>
            </div>
        </div>
    </div>
    
    <ul class="messages-list noti noti-other">
    @if(count($allnoti) == 0)
    <li class="message">
                                       لا يوجد لديك اي اشعارات
                                    </li>
                        @endif
    
    @foreach($allnoti as $allnotis)
                                 <li class="message message-other">
                                        <a href="#">
                                            <div class="client">
                                                <div class="message-header">
                                                    <div class="img-container-noti">
                                                        <img src="/image/400x400.png" class="img-responsive">
                                                    </div>              
                                                    <div class="client-title">
                                                        <h5>{{$allnotis->user->user_name}}</h5>
                                                        <p>{{$allnotis->data}}</p>
                                                        <p class="time">
                                                            <i class="fa fa-phone"></i>
                                                           {{$allnotis->created_at}}
                                                        </p>
                                                    </div>
                                            
                                                </div>
                                                <div>
                                                    
                                                </div>
                                            
                                            </div>
                                        </a>
                                    </li>
                                 @endforeach                                       
                                 
                                </ul>
@endsection