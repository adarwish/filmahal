@extends('site.profiles.master')

@section('content')
<?php $ads = DB::table('ads')->get(); ?>
<section class="navigate">
    <div class="container">
        <div class="row">
            <div class="navigation text-bold">
                <p><a href="/">الرئيسية</a> &gt; &gt; بحث</p>
            </div>
        </div>
    </div>
</section>


<section class="companies">
    <div class="container">
        <div class="row">
            <div class="order">
                <form>
                <script>
                function sorting(){

			$.ajax({
		            type:'POST',
		            url:'/',
		            data:{sort:$('#sortselect').val(), sorting:"{{$mahalId2}}" },
		            dataType:'json',
		            success: function(data){
		                console.log(data);
		               	$('.storeresult').remove();
		              	for(i=0; i< data.store.length; i++){
			    		$.ajax({
				            type:'POST',
				            url:'/',
				            data:{storeID: data.store[i].id},
				            dataType:'json',
				            success: function(data){
				                console.log(data);
				                console.log(data.products2.length);
				                
						dom = '<div class="company storeresult"><div class="brand col-xs-12 col-md-3"><div class="img-responsive"><img src="/image/'+data.storephoto2[0].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="/store/profile/'+data.store2[0].id+'">'+data.store2[0].title+'</a></div></div><div class="brand-info col-xs-12 col-md-5"><ul><li><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i><span>'+data.store2[0].address+'</span></li><li><i class="fa fa-clock-o fa-lg" aria-hidden="true"></i><span>اوقات العمل من '+data.store2[0].work_hours_from+'  حتى الـ '+data.store2[0].work_hours_to+' </span></li><li id="opinions"><span class="right"><i class="fa fa-users fa-lg" aria-hidden="true"></i><span class="users">اراء الناس</span></span><span class="people-opinion"><span class="people-pics first"><img src="/search/imgs/1.jpg" class="img-responsive"></span><span class="people-pics second"><img src="/search/imgs/1.jpg" class="img-responsive"></span><span class="people-pics third"><img src="/search/imgs/1.jpg" class="img-responsive"></span><span class="opinions-number text-bold"></span><span class="clear"></span></span></li></ul></div><div class="brand-gallery col-xs-12 col-md-4"><div class="f-row col-xs-12"><div class="product col-xs-4"><a href="#" target="_blank"><img src="/image/'+data.products2[0].photo+'" class="img-responsive"></a></div></div></div></div>';
					                
					                
						$('.companyResult').append(dom); 
						
						
					    }
					});
				};  
				
				
			    },
		            error: function(){
		                console.log('error');
		            }
		          });
		       
		}
		</script>
		
                    <select onchange="sorting()" id="sortselect" class="text-bold">
                        <option disabled selected >رتب على حسب</option>
                        <option value="1">محلات التخفيضات</option>
                        <option value="2">محلات العروض الجديده</option>
                        <option value="3"> المحلات الاقل سعرا</option>
                        <option value="4">المحلات الاعلي سعرا</option>
                        <option value="5">المحلات الاكثر مشاهده</option>
                        <option value="6">المحلات الاقرب جغرافيا</option>
                        <option value="7">محلات صاحبه علامه تجاريه</option>
                    </select>
                </form>
            </div>
            <div class="companyResult" >
                @foreach($mahal as  $mahalat)
                <?php 
                    $products = DB::table('product')->where('market_id',$mahalat->id)->limit('6')->get();
                    $reviewnum =count(DB::table('review')->where('market_id',$mahalat->id)->get());
                ?>
                
                <div class="company storeresult" >
                    <div class="brand col-xs-12 col-md-3">
                        <div class="img-responsive">

                            <?php $mainphoto = DB::table('photo')->where('market_id',$mahalat->id)->where('sub_category_id',null)->where('product_id',null)->get(); ?>
                            @if(count($mainphoto) !== 0)
                            <img src="/image/{{$mainphoto[0]->photo}}" class="img-responsive">
                            @endif
                        </div>
                        <div class="brand-name text-center text-bold">
                        @if(count($savedArea) > 0)
                        @if($searchval == "" || $searchval == NULL)
                        <a href="/store/profile/{{$mahalat->id}}/{{$savedCity[0]->id}}/{{$savedArea[0]->id}}/search">{{$mahalat->title}}</a>
                        @else
                        <a href="/store/profile/{{$mahalat->id}}/{{$savedCity[0]->id}}/{{$savedArea[0]->id}}/{{$searchval}}">{{$mahalat->title}}</a>
                        @endif
                            
                        @else
                            @if($searchval == "" || $searchval == NULL)
                            <a href="/store/profile/{{$mahalat->id}}/{{$savedCity[0]->id}}/1/search">{{$mahalat->title}}</a>
                            @else
                            <a href="/store/profile/{{$mahalat->id}}/{{$savedCity[0]->id}}/1/{{$searchval}}">{{$mahalat->title}}</a>
                            @endif
                        @endif
                            
                            
                        </div>
                    </div>
                    
                    <div class="brand-info col-xs-12 col-md-5">
                        <ul>
                            <li>
                                <i class="fa fa-align-right fa-lg" aria-hidden="true"></i>
                                <span>{{$mahalat->description}}</span>
                            </li>
                            
                            <li>
                                <i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>
                                <span>{{$mahalat->address}}</span>
                            </li>
                            
                            <li>
                                <i class="fa fa-clock-o fa-lg" aria-hidden="true"></i>
                                <span>اوقات العمل من {{$mahalat->work_hours_from}}  حتى الـ {{$mahalat->work_hours_to}} </span>
                            </li>
                            
                            
                            
                            <li id="opinions">
                                <span class="right">
                                    <i class="fa fa-users fa-lg" aria-hidden="true"></i>
                                    <span class="users">اراء الناس</span>
                                </span>
                                
                                <span class="people-opinion">
                                    <span class="people-pics first">
                                        <img src="{{asset('search/imgs/people.jpg')}}" class="img-responsive">
                                    </span>
                                    <span class="people-pics second">
                                        <img src="{{asset('search/imgs/1.jpg')}}" class="img-responsive">
                                    </span>
                                    <span class="people-pics third">
                                        <img src="{{asset('search/imgs/people.jpg')}}" class="img-responsive">
                                    </span>
                                    <span class="opinions-number text-bold">{{$reviewnum}}</span>
                                    <span class="clear"></span>
                                </span>
                                
                                
                            </li>
                            
                        </ul>
                    </div>
                    
                    <div class="brand-gallery col-xs-12 col-md-4">
                        <div class="f-row col-xs-12">
                        @foreach($products as $product)

                            <?php $photos = DB::table('photo')->where('product_id',$product->id)->get(); ?>
                            @if(count($photos) !==0)
                            <div class="product col-xs-4" style="padding-left: 3px; padding-right: 3px; width: 26%!important; margin-bottom: 5px;">
                                <a href="#" target="_blank">
                                    <img src="/image/{{$photos[0]->photo}}" class="img-responsive">
                                </a>
                            </div>
                            @endif
                        @endforeach 
                            
                        
                        
                        </div>
                        
                        
                    
                    </div>
                </div>
             @endforeach

            </div>
            
            
        </div>
    </div>
</section>

<section class="equal-companies">
    <div class="container">
        <div class="row">
            <div class="headerWord">
                <h2 class="text-right text-bold">
                    محلات مشابهة
                </h2>
                <div class="hr-right">
                    <hr>
                </div>
            </div>
            
            
            <div class="owl-carousel owl-theme">
                @foreach($samemahal as $key => $samemahalat)            
                <div class="item">
                    <div class="company">
                        <div class="brand col-xs-12">
                            <div class="img-responsive">
                                <?php $mainphoto2 = DB::table('photo')->where('market_id',$samemahalat->id)->where('sub_category_id',null)->where('sub_category_id',null)->where('product_id',null)->get();
                                 $cinfo =  DB::table('contact_info')->where('market_id',$samemahalat->id)->get(); 
                                 $reviewnum = count(DB::table('review')->where('market_id',$samemahalat->id)->get());
                                 ?>
                                @if(count($mainphoto2) !==0)
                                    <img src="/image/{{$mainphoto2[0]->photo}}" class="img-responsive">
                                @endif
                            </div>
                            
                            <div class="brand-name text-center text-bold">
                            
                               @if(count($savedArea) > 0)
	                       @if($searchval == "" || $searchval == NULL)
	                       <a href="/store/profile/{{$samemahalat->id}}/{{$savedCity[0]->id}}/{{$savedArea[0]->id}}/search">{{$samemahalat->title}}</a>
	                       @else
	                       <a href="/store/profile/{{$samemahalat->id}}/{{$savedCity[0]->id}}/{{$savedArea[0]->id}}/{{$searchval}}">{{$samemahalat->title}}</a>
	                       @endif
                            
                               @endif
                            
                              
                            </div>
                        </div>
                        
                        <div class="brand-info col-xs-12">
                            <ul>
                                <li>
                                    <i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>
                                    <span>{{$samemahalat->address}}</span>
                                </li>

                                <li>
                                    <i class="fa fa-clock-o fa-lg" aria-hidden="true"></i>
                                    <span>اوقات العمل من {{$samemahalat->work_hours_from}} حتى الـ {{$samemahalat->work_hours_to}}</span>
                                </li>

                                 <li id="opinions">
                                    <span class="right">
                                        <i class="fa fa-users fa-lg" aria-hidden="true"></i>
                                        <span class="users">اراء الناس</span>
                                    </span>

                                    <span class="people-opinion">

                                        <span class="people-pics first">
                                             <img src="{{asset('/search/imgs/people.jpg')}}" class="img-responsive">
                                        </span>
                                        <span class="people-pics second">
                                             <img src="{{asset('/search/imgs/1.jpg')}}" class="img-responsive">
                                        </span>
                                        <span class="people-pics third">
                                             <img src="{{asset('/search/imgs/people.jpg')}}" class="img-responsive">
                                        </span>
                                        <span class="opinions-number text-bold">{{$reviewnum}}</span>
                                        <span class="clear"></span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
              
            </div>
            
        </div>

    </div>
</section>

<section class="ads">
    <img src="/image/{{$ads[6]->files}}" alt="تشكيله الشتاء" class="img-responsive">
</section>

<section class="equal-companies">
    <div class="container">
        <div class="row">
            <div class="headerWord">
                <h2 class="text-right text-bold">
                    محلات اخرى قد تنال اعجابك
                </h2>
                <div class="hr-right">
                    <hr>
                </div>
            </div>
            
            
            <div class="owl-carousel owl-theme">
                @foreach($samemahal2 as $key => $samemahalat2)                
                <div class="item">
                    <div class="company">
                        <div class="brand col-xs-12">
                            <div class="img-responsive">
                                <?php $mainphoto3 = DB::table('photo')->where('market_id',$samemahalat2->id)->where('sub_category_id',null)->where('sub_category_id',null)->where('product_id',null)->get();
                                $cinfo =  DB::table('contact_info')->where('market_id',$samemahalat2->id)->get();
                                $reviewnum = count(DB::table('review')->where('market_id',$samemahalat2->id)->get()); ?>
                               
                                @if(count($mainphoto3) !==0)
                                    <img src="/image/{{$mainphoto3[0]->photo}}" class="img-responsive">
                                @endif
                            </div>
                            <div class="brand-name text-center text-bold">
                               @if(count($savedArea) > 0)
	                       @if($searchval == "" || $searchval == NULL)
	                       <a href="/store/profile/{{$samemahalat2->id}}/{{$savedCity[0]->id}}/{{$savedArea[0]->id}}/search">{{$samemahalat2->title}}</a>
	                       @else
	                       <a href="/store/profile/{{$samemahalat2->id}}/{{$savedCity[0]->id}}/{{$savedArea[0]->id}}/{{$searchval}}">{{$samemahalat2->title}}</a>
	                       @endif
	                       @endif
                            </div>
                        </div>
                        
                        <div class="brand-info col-xs-12">
                            <ul>
                                <li>
                                    <i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>
                                    <span>{{$samemahalat2->address}}</span>
                                </li>

                                <li>
                                    <i class="fa fa-clock-o fa-lg" aria-hidden="true"></i>
                                    <span>اوقات العمل من {{$samemahalat2->work_hours_from}} حتى الـ {{$samemahalat2->work_hours_to}}</span>
                                </li>

                                <li>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true"></i>
                                    @if(count($cinfo) > 0)
                                    <span>{{$cinfo[0]->phone}}</span>
                                    @endif
                                </li>

                                 <li id="opinions">
                                    <span class="right">
                                        <i class="fa fa-users fa-lg" aria-hidden="true"></i>
                                        <span class="users">اراء الناس</span>
                                    </span>

                                    <span class="people-opinion">

                                        <span class="people-pics first">
                                             <img src="{{asset('/search/imgs/people.jpg')}}" class="img-responsive">
                                        </span>
                                        <span class="people-pics second">
                                             <img src="{{asset('/search/imgs/1.jpg')}}" class="img-responsive">
                                        </span>
                                        <span class="people-pics third">
                                             <img src="{{asset('/search/imgs/people.jpg')}}" class="img-responsive">
                                        </span>
                                        <span class="opinions-number text-bold">{{$reviewnum}}</span>
                                        <span class="clear"></span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            
        </div>

    </div>
</section>


@endsection

