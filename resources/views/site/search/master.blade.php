<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('search/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('search/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('search/css/font-awesome.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('search/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('search/owlcarousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('search/css/main.css')}}">
    
    <script defer src="{{asset('search/js/jquery-3.2.1.min.js')}}"></script>
    <script defer src="{{asset('search/js/bootstrap.min.js')}}"></script>
    <script defer src="{{asset('search/owlcarousel/owl.carousel.min.js')}}"></script>
    <script defer src="{{asset('search/js/main.js')}}"></script>
    
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Of dHeader Search-->
    <header>
        <nav class="container">
            <div class="row"> 
                <ul class="left-side col-xs-12 col-lg-6">   
                    <li class="drop-down">
			
                        <select onchange="getarea()" id="governorate">
                            <option value="{{$savedCity[0]->id}}">{{$savedCity[0]->name}}</option>
                            @foreach($city as $cities)
                            <option value="{{$cities->id}}">{{$cities->name}}</option>
                            @endforeach
                        </select>
                    </li>
                    <li class="button-shop">
			@if(Auth::guest())
                        <a href="#" class="add-shop">
                            أضف محلك
                            <i class="fa fa-plus"></i>
                        </a>
                        @endif
                    </li>
                </ul>

                <ul class="right-side col-xs-12 col-lg-6">

                    <li class="notification-icon">
                                <img src="/image/SVG/bell.svg" class="img-responsive">
                            <div class="slide-down hidden">
                                   @if(Auth::user())
                                <?php $notifications = DB::table('noti')->where('user_id',Auth::user()->id)->get();
                                $notifications2 = DB::table('noti')->where('user_id',Auth::user()->id)->limit(3)->orderBy('id','DESC')->get();
                                $counter = count($notifications);
                                
                                $old_noti = DB::table('noti_counter')->where('user_id',Auth::user()->id)->get();
                                $counter_noti = count($old_noti);
                                if($counter_noti >0){
                                    DB::table('noti_counter')->where('user_id',Auth::user()->id)->update(['counter'=>$counter]);
                                }else{
                                    DB::table('noti_counter')->insert(['counter'=>$counter,'user_id'=>Auth::user()->id]);
                                }

                                 ?>

                                <ul class="messages-list noti nav-drop-alter">
                                    @if($counter !==0)
                                    @foreach($notifications2 as $noti)
                                    <?php $marketnoti = DB::table('market')->where('user_id',$noti->action_id)->get();
                                    $contact_info = DB::table('contact_info')->where('market_id',$marketnoti[0]->id)->get(); ?>
                                    <li class="message">
                                        <a href="#">
                                            <div class="client">
                                                <div class="message-header">
                                                    <div class="img-container">
                                                        <img src="/image/{{$contact_info[0]->icon}}" class="img-responsive">
                                                    </div>              
                                                    <div class="client-title">
                                                        <h5>{{$marketnoti[0]->title}}</h5>
                                                        <p>{{$noti->data}}</p>
                                                        <p class="time">
                                                            <i class="fa fa-phone"></i>
                                                            {{$noti->created_at}}
                                                        </p>
                                                    </div>
                                            
                                                </div>
                                                <div>
                                                    
                                                </div>
                                            
                                            </div>
                                        </a>
                                    </li>
                                    @endforeach
                                    @endif                          
                                    <li class="more-messages">
                                        <p><a href="/all-notification">شاهد جميع الاشعارات</a></p>
                                        <p><a href="/all-notification"><font id="countnoti">{{$counter}} اشعار اخر</font></a></p>
                                        <div class="clear"></div>
                                    </li>
                                </ul>
                                 @endif 
                           </div>
                        </li>
                     
                     
                    <li class="profile">
                            <i class="fa fa-angle-down fa-lg" aria-hidden="true">
                                <i class="fa fa-caret-up" aria-hidden="true" style="display: none;"></i>
                            </i>
                            <img src="/image/SVG/profile.svg" class="img-responsive">
                            
                            <div class="hidden profile-set-wrapper">
                                
                                <ul class="profile-settings">
                                    <li>
                                        <a href="/user/profile#favoriteShops" class="reviewsPeople" id="favoriteShops">
                                            <i class="ion ion-heart" aria-hidden="true"></i>
                                            محلات اعجبت بها                                       
                                        </a>
                                    </li>

                                    <li>
                                        <a href="/user/profile#boxMessages" class="boxMessages" id="boxMessages">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            صندوق الرسائل
                                        </a>
                                    </li>

                                    <li>
                                        <a href="/user/profile#settingProfile" class="settingProfile" id="settingProfile">
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                            اعدادات حسابى
                                        </a>
                                    </li>

                                    <li>
                                        <a href="/logout">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            تسجيل الخروج
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                </ul>
            </div>
        </nav>
        <div class="clear"></div>
        
        <div class="container">
            <div class="row text-center">
                <div class="img-container">
                   <a href="/"> <img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo"> </a>
                </div>
            
                <form  method="POST">
                    {{csrf_field()}}
                    <div class="form-container">
                        <div class="search">
                     
                            <input type="search" name="search" placeholder="أبحث باسم المحل">
                        
                            <button type="submit" name="searchbutton" value="بحث">
                                <i class="fa fa-search fa-2x"></i>
                            </button>
                        </div>
                        <select id="area" name="getarea">
                            <option value="{{$savedArea[0]->id}}">{{$savedArea[0]->name}}</option>
                            @foreach($area as $areas)
                            <option value="{{$areas->id}}">{{$areas->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    
                </form>           
            </div>
        </div>       
    </header>
<!--End Of Header Search-->

@yield('content')
<section class="samples-shops">
    <div class="container">
        <div class="row">
             <div class="owl-carousel owl-theme last-carousel">
                <div class="item">
                    <img src="{{asset('search/imgs/shops-logos/adidas.png')}}" class="img-responsive"> 
                </div>
                <div class="item">
                    <img src="{{asset('search/imgs/shops-logos/Nike-Logo-PNG-Picture.png')}}" class="img-responsive"> 
                </div>
                 <div class="item">
                    <img src="{{asset('search/imgs/shops-logos/PHLIPSE.png')}}" class="img-responsive"> 
                </div>
                 <div class="item">
                    <img src="{{asset('search/imgs/shops-logos/wordpress-logo-simplified-rgb.png')}}" class="img-responsive"> 
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Of Samples-Shops Section-->
    
<!--Start Of Footer Section-->
<footer>
    <div class="container">
        <div class="row">
            <div class="left-section col-xs-12 col-md-6 col-lg-4">
                <div class="headerWord">
                    <h3 class="text-bold">الدعم الفنى وخدمه العملاء</h3>
                </div>
                <div class="contact-us">
                    <div class="phones">

                        <i class="fa fa-phone"></i>

                        <div class="text-color-grey numbers text-bold">
                            <span>
                                010 879 987 23
                            </span>
                            <span>
                                010 879 987 23
                            </span>
                        </div>

                    </div>

                    <div class="email">
                        <span class="text-bold">
                            <i class="fa fa-envelope"></i> 
                            <span class="text-color-grey">
                                info @ filmahal.com
                            </span>
                        </span>
                    </div>

                    <div class="whatsApp">
                        <div class="text-bold">
                            <span class="whatsApp-icon"></span> 
                            <span class="text-color-grey">
                                010 879 987 23
                            </span>
                        </div>
                    </div>
                </div>
                
                <div class="follow-us">
                    <h2>
                        تابعنا
                    </h2>
                    <div class="social-icons">
                        <ul>
                            <a href="#" target="_blank" class="facebook">
			        <li>
			            <i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
			        </li>
			    </a>
			    
			    <a href="#" target="_blank" class="instagram">
			        <li>
			            <i class="fa fa-instagram fa-2x" aria-hidden="true"></i>
			        </li>
			    </a>
			    
			 <a href="#" target="_blank" class="googlePlus">
			    <li>
			        <i class="fa fa-google-plus fa-2x"></i>
			    </li>
			    </a>
			    
			    
			    <a href="#" target="_blank" class="twitter">
			        <li>
			            <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
			        </li>
			    </a>
			    
			    <a href="#" target="_blank" class="youtube">
			        <li>
			            <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
			        </li>
			    </a>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="center-section col-xs-12 col-md-6 col-lg-4">
                <div class="headerWord">
                    <h3 class="text-bold"><font id="msg">اشترك ليصلك كل جديد</font></h3>
                </div>
                
                <div class="send-message">
                    <form method="POST" id="subsForum">
                        {{csrf_field()}}
                        <input type="hidden" name="sub1" value="sub1">
                        <input type="email" name="emailsub" placeholder="البريد الالكترونى">
                        <button class="text-bold" onclick="newsubuser()" type="submit">أرسل</button>
                    </form>
                </div>
                
                <div class="categories col-xs-6" style="padding: 0;">
                        <h3 class="text-bold">لأصحاب المحلات</h3>
                        <ul>
                            <li><a class="text-color-grey text-bold" href="/vendor/register" target="_blank">اضف محلك</a></li>
                        </ul>
                    </div>
                    <div class="categories col-xs-6" style="padding-left: 30px; padding-right: 0;">
                        <h3 class="text-bold">أقسام الموقع</h3>
                        <ul>
                            <li><a class="text-color-grey text-bold" href="/mobile" target="_blank">تطبيق الموبيل</a></li>
                            <li><a class="text-color-grey text-bold" href="/technical-support" target="_blank">الدعم الفنى</a></li>
                            <li><a class="text-color-grey text-bold" href="/FAQ" target="_blank">الأسئلة الأكثر شيوعاً</a></li>
                            <li><a class="text-color-grey text-bold" href="/ads-with-us" target="_blank">أعلن معنا</a></li>
                            <li><a class="text-color-grey text-bold" href="/copyright" target="_blank">الملكية الفكرية</a></li>
                            <li><a class="text-color-grey text-bold" href="/blog" target="_blank">المدونة</a></li>
                        </ul>
                    </div>
                
            </div>
            
            <div class="right-section col-xs-12 col-lg-4">
                <div class="img-container">
                <a href="/"><img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo"></a>
                </div>
                <h3 class="text-bold">
                    من نحن ؟ ولماذا تنضم الينا ؟
                </h3>
                <ul>
                    <li><a class="text-color-grey text-bold" href="/about-us" target="_blank"><i class="fa fa-check fa-lg"></i>من نحن ؟</a></li>
                        <li><a class="text-color-grey text-bold" href="/terms-condition" target="_blank"><i class="fa fa-check fa-lg"></i>شروط وجود محلك فى الموقع ؟</a></li>
                        <li><a class="text-color-grey text-bold" href="/privileges" target="_blank"><i class="fa fa-check fa-lg"></i>مميزات وجوده معنا ؟</a></li>
                </ul>
            </div>
        </div> 
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="footer-bottom text-center">
                <div class="copy-right col-xs-12 col-md-9">
                    <p>Developed By <a style="color: #777; margin-right: 20px;" href="http://www.wasiladev.com" target="_blank">WasilaDev</a></p>
                    <p>جميع الحقوق محفوظه لشركه فى المحل دوت كوم</p>
                </div>

                <div class="conditions col-xs-12 col-md-3">
                    <a href="/privacy">سياسة الخصوصية</a>
                    <a href="/rules">احكام وشروط</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<script>
    function newsubuser(){
        $('#subsForum').submit(function(e){
          e.preventDefault();
          var form = $('#subsForum');
          $.ajax({
            type:'POST',
            url:'/',
            data:form.serialize(),
            dataType:'json',
            success: function(data){
                console.log(data);
                $('#msg').html('تم الأشتراك بنجاح');
                $("#msg").fadeIn(3000);
                setTimeout(function() {
                   $('#msg').fadeOut(2000);
                }, 10000);
            }
          });
        });
    };
    function getarea(){
        cities = document.getElementById('governorate').value;
        console.log(cities);
          $.ajax({
            type:'GET',
            url:'/vendor/cities',
            data:{city:cities},
            success: function(data){
                console.log(data);
                $('#area > option').remove();
                for(var i = 0; i < data.area.length; i++){
                    addoption = '<option value='+data.area[i].id+' class="level-0">'+data.area[i].name+'</option>';
                    $('#area').append(addoption);
                }
                addoption="";
            },
            error: function(){
                console.log('error');
            }
          });
       // console.log('error');
    };
</script>
</body>
</html>