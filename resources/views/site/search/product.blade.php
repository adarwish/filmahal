@extends('site.search.masterprod')

@section('content')
<!--End Of Header Search-->
    
    <section class="navigate">
        <div class="container-fluid">
            <div class="row">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <div class="order col-xs-12">
                    <form>
                        <select class="text-bold">
                            <option disabled="" selected="">رتب على حسب</option>
                            <option>A-Z</option>
                            <option>Z-A</option>
                        </select>
                    </form>
                    <div class="clear"></div>
                </div>
                
                <div class="col-xs-4 col-md-3 bg-white">
                    <div class="row">
                        <aside>

                            <h4 class="text-bold">اختر تحديداً ماذا تريد</h4>
                        <form method="POST" id="filterForum">
                            {{csrf_field()}}
                            <input type="hidden" name="saveFilter" value="saveFilter">
                            <input type="hidden" name="searchtype" value="2">
                            <input type="hidden" name="productResultArray" value="{{$productId}}">
                            @foreach($attributeResult as $attributes)
                            <div class="side-menu">
                                <div class="slide-header">
                                    <p class="text-bold">{{$attributes->name}}</p>
                                    <i class="fa fa-chevron-left" aria-hidden="true"></i>
                                </div>
                                <?php $sub_att = DB::table('att_properties')->where('attribute_id',$attributes->id)->get(); ?>

                                @foreach($sub_att as $sub_attributes)

                                <?php $sub_prop = DB::table('sub_properties')->where('att_properties_id',$sub_attributes->id)->get(); ?>

                                <ul class="choose">
                                    <label>{{$sub_attributes->name}}</label>
                                    @foreach($sub_prop as $sub_properties)
                                    <li>
                                        @if($sub_attributes->type == "1")
                                        <input type="checkbox" id="" name="filter_attr" value="{{$sub_properties->id}}">
                                        <label for="">{{$sub_properties->name}}</label>
                                        @elseif($sub_attributes->type == "2")
                                        <input type="radio" id="" name="filter_attr2" value="{{$sub_properties->id}}">
                                        <label for="">{{$sub_attributes->name}}</label>
                                        @elseif($sub_attributes->type == "3")
                                        <input type="checkbox" id="" name="filter_attr3" value="{{$sub_properties->id}}">
                                        <label for="">{{$sub_properties->name}}</label>
                                        @endif
                                    </li>
                                    @endforeach
                                    
                                </ul>
                                @endforeach
                            </div>
                            @endforeach
                           <button href="#" onclick="filters()" >بحث</button>
                       </form>
                        </aside>
                        <div class="ads">
                                <a href="#" target="_blank">
                                    <img src="/search/product/imgs/ADDs.png" alt="تشكيله الشتاء" class="img-responsive">
                                </a>
                                <a href="#" target="_blank">
                                    <img src="/search/product/imgs/ADDs.png" alt="تشكيله الشتاء" class="img-responsive">
                                </a>
                                <a href="#" target="_blank">
                                    <img src="/search/product/imgs/ADDs.png" alt="تشكيله الشتاء" class="img-responsive">
                                </a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-9 products" id="products">
                    @foreach($productResult as $products)
                    <?php echo $productResult?>
                    <div class="anProduct">
                        <div class="productPhoto">
                            <img src="/image/{{$products->photo}}" class="img-responsive">
                        </div>
                        <div class="brand-name text-center text-bold">
                            <a href="" class="shopName">
                                <span>{{$products->name}}</span>
                                <div class="img-container">
                                    <img src="/image/{{$products->photo}}" class="img-responsive">
                                </div>
                            </a>
                        </div>
                        
                        <div class="productInfo">
                            <p class="text-bold text-center nameOfProduct">{{$products->discription}}</p>
                            <div class="prices">
                                <p class="text-bold priceOfProduct">{{$products->price}} جنيه مصرى</p>
                                @if($products->discount !=="0")
                                <p class="text-bold deleted">{{$products->discount}} جنيه مصرى</p>
                                @endif
                            </div>
                            <div class="moreInfo">
                                    <a href="#">تفاصيل اكثر</a>
                            </div>
                        </div>

                        

                    </div>
                    @endforeach
                    
                   
                
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pagination">
                        <button><i class="fa fa-angle-right" aria-hidden="true"></i>السابق</button>
                        <div class="counter text-bold">
                            <span class="num">200</span>
                            <span class="fasl">|</span>
                            <span class="primary num">9</span>
                            <span class="primary">محل</span>
                        </div>
                        <button>التالى<i class="fa fa-angle-left" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ads">
        <a href="">
            <img src="/search/product/imgs/ADDs.png" alt="تشكيله الشتاء" class="img-responsive">
        </a>
    </section>
    

<!--End Of Samples-Shops Section-->
@endsection
<script>
    function filters(){
        $('.anProduct').remove();
        $('#filterForum').submit(function(e){
          e.preventDefault();
          var form = $('#filterForum');
          $.ajax({
            type:'POST',
            url:'/',
            data:form.serialize(),
            dataType:'json',
            success: function(data){
                console.log(data.length);
                for(var i = 0; i<data.length; i++){
                    if(data[i].discount == 0){
                        filtergets = '<div class="anProduct"><div class="productPhoto"><img src="/image/'+data[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data[i].name+'</span><div class="img-container"><img src="/image/'+data[i].photo+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data[i].discription+'</p><div class="prices"><p class="text-bold priceOfProduct">'+data[i].price+' جنيه مصرى</p></div><div class="moreInfo"><a href="#">تفاصيل اكثر</a></div></div>';
                        $('.products').append(filtergets);
                    }else{
                        filtergets = '<div class="anProduct"><div class="productPhoto"><img src="/image/'+data[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data[i].name+'</span><div class="img-container"><img src="/image/'+data[i].photo+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data[i].discription+'</p><div class="prices"><p class="text-bold priceOfProduct">'+data[i].price+' جنيه مصرى</p><p class="text-bold deleted">'+data[i].discount+' جنيه مصرى</p></div><div class="moreInfo"><a href="#">تفاصيل اكثر</a></div></div></div>';
                        $('.products').append(filtergets);
                        
                    }
                }
                filtergets = "";
                
            }
          });
        });
    };
</script>