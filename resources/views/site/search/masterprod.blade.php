<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('search/product/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('search/product/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('search/product/css/font-awesome.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('search/product/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('search/product/owlcarousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('search/product/css/main.css')}}">
    
    <script defer src="{{asset('search/product/js/jquery-3.2.1.min.js')}}"></script>
    <script defer src="{{asset('search/product/js/bootstrap.min.js')}}"></script>
    <script defer src="{{asset('search/product/owlcarousel/owl.carousel.min.js')}}"></script>
    <script defer src="{{asset('search/product/js/main.js')}}"></script>
    
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Of dHeader Search-->
    <header>
        <nav class="container">
            <div class="row"> 
                <ul class="left-side col-xs-12 col-lg-6">   
                    <li class="drop-down">

                        <select onchange="getarea()" id="governorate">
                            @foreach($city as $cities)
                            <option value="{{$cities->id}}">{{$cities->name}}</option>
                            @endforeach
                        </select>
                    </li>
                    <li class="button-shop">

                        <a href="#" class="add-shop">
                            أضف محلك
                            <i class="fa fa-plus"></i>
                        </a>
                    </li>
                </ul>

                <ul class="right-side col-xs-12 col-lg-6">

                    <li class="notification-icon">
                        <i class="fa fa-bell-o fa-2x" aria-hidden="true"></i>
                    </li>

                    <li class="profile">
                        <i class="fa fa-angle-down fa-lg" aria-hidden="true"></i>
                        <i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="clear"></div>
        
        <div class="container">
            <div class="row text-center">
                <div class="img-container">
                    <a href="/"><img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo"></a>
                </div>
            
                <form  method="POST" action="/">
                    {{csrf_field()}}
                    <div class="form-container">
                        <div class="search">
                     
                            <input type="search" name="search" placeholder="أبحث باسم المحل">
                        
                            <button type="submit" name="searchbutton" value="بحث">
                                <i class="fa fa-search fa-2x"></i>
                            </button>
                        </div>
                        
                        <div>
                            <select id="area" name="getarea">
                                @foreach($area as $areas)
                                <option value="{{$areas->id}}">{{$areas->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div>
                        <label>محلات</label>
                        <input type="radio" name="searchtype" id="searchtype" checked value="1">
                        <label>منتجات</label>
                        <input type="radio" name="searchtype" id="searchtype"  value="2">
                        <label>فئات</label>
                        <input type="radio" name="searchtype" id="searchtype"  value="3">
                    </div>
                </form>           
            </div>
        </div>       
    </header>
<!--End Of Header Search-->

@yield('content')
<section class="samples-shops">
    <div class="container">
        <div class="row">
             <div class="owl-carousel owl-theme last-carousel">
                <div class="item">
                    <img src="{{asset('search/product/imgs/shops-logos/adidas.png')}}" class="img-responsive"> 
                </div>
                <div class="item">
                    <img src="{{asset('search/product/imgs/shops-logos/Nike-Logo-PNG-Picture.png')}}" class="img-responsive"> 
                </div>
                 <div class="item">
                    <img src="{{asset('search/product/imgs/shops-logos/PHLIPSE.png')}}" class="img-responsive"> 
                </div>
                 <div class="item">
                    <img src="{{asset('search/product/imgs/shops-logos/wordpress-logo-simplified-rgb.png')}}" class="img-responsive"> 
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Of Samples-Shops Section-->
    
<!--Start Of Footer Section-->
<footer>
    <div class="container">
        <div class="row">
            <div class="left-section col-xs-12 col-md-6 col-lg-4">
                <div class="headerWord">
                    <h3 class="text-bold">الدعم الفنى وخدمه العملاء</h3>
                </div>
                <div class="contact-us">
                    <div class="phones">

                        <i class="fa fa-phone"></i>

                        <div class="text-color-grey numbers text-bold">
                            <span>
                                010 879 987 23
                            </span>
                            <span>
                                010 879 987 23
                            </span>
                        </div>

                    </div>

                    <div class="email">
                        <span class="text-bold">
                            <i class="fa fa-envelope"></i> 
                            <span class="text-color-grey">
                                info @ filmahal.com
                            </span>
                        </span>
                    </div>

                    <div class="whatsApp">
                        <div class="text-bold">
                            <span class="whatsApp-icon"></span> 
                            <span class="text-color-grey">
                                010 879 987 23
                            </span>
                        </div>
                    </div>
                </div>
                
                <div class="follow-us">
                    <h2>
                        تابعنا
                    </h2>
                    <div class="social-icons">
                        <ul>
                            <a href="#" target="_blank" class="facebook">
        <li>
            <i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
        </li>
    </a>
    
    <a href="#" target="_blank" class="instagram">
        <li>
            <i class="fa fa-instagram fa-2x" aria-hidden="true"></i>
        </li>
    </a>
    
 <a href="#" target="_blank" class="googlePlus">
    <li>
        <i class="fa fa-google-plus fa-2x"></i>
    </li>
    </a>
    
    
    <a href="#" target="_blank" class="twitter">
        <li>
            <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
        </li>
    </a>
    
    <a href="#" target="_blank" class="youtube">
        <li>
            <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
        </li>
    </a>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="center-section col-xs-12 col-md-6 col-lg-4">
                <div class="headerWord">
                    <h3 class="text-bold"><font id="msg">اشترك ليصلك كل جديد</font></h3>
                </div>
                
                <div class="send-message">
                    <form method="POST" id="subsForum">
                        {{csrf_field()}}
                        <input type="hidden" name="sub1" value="sub1">
                        <input type="email" name="emailsub" placeholder="البريد الالكترونى">
                        <button class="text-bold" onclick="newsubuser()" type="submit">أرسل</button>
                    </form>
                </div>
                
                <div class="categories col-xs-6" style="padding: 0;">
                        <h3 class="text-bold">لأصحاب المحلات</h3>
                        <ul>
                            <li><a class="text-color-grey text-bold" href="#" target="_blank">اضف محلك</a></li>
                        </ul>
                    </div>
                    <div class="categories col-xs-6" style="padding-left: 30px; padding-right: 0;">
                        <h3 class="text-bold">أقسام الموقع</h3>
                        <ul>
                            <li><a class="text-color-grey text-bold" href="#" target="_blank">تطبيق الموبيل</a></li>
                            <li><a class="text-color-grey text-bold" href="#" target="_blank">الدعم الفنى</a></li>
                            <li><a class="text-color-grey text-bold" href="#" target="_blank">الأسئلة الأكثر شيوعاً</a></li>
                            <li><a class="text-color-grey text-bold" href="#" target="_blank">أعلن معنا</a></li>
                            <li><a class="text-color-grey text-bold" href="#" target="_blank">الملكية الفكرية</a></li>
                            <li><a class="text-color-grey text-bold" href="#" target="_blank">المدونة</a></li>
                        </ul>
                    </div>
                
            </div>
            
            <div class="right-section col-xs-12 col-lg-4">
                <div class="img-container">
                <a href="/"><img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo"></a>
                </div>
                <h3 class="text-bold">
                    من نحن ؟ ولماذا تنضم الينا ؟
                </h3>
                <ul>
                    <li><a class="text-color-grey text-bold" href="#" target="_blank"><i class="fa fa-check fa-lg"></i>من نحن ؟</a></li>
                    <li><a class="text-color-grey text-bold" href="#" target="_blank"><i class="fa fa-check fa-lg"></i>شروط وجود محلك فى الموقع ؟</a></li>
                    <li><a class="text-color-grey text-bold" href="#" target="_blank"><i class="fa fa-check fa-lg"></i>مميزات وجوده معنا ؟</a></li>
                </ul>
            </div>
        </div> 
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="footer-bottom text-center">
                <div class="copy-right col-xs-12 col-md-9">
                    <p>Developed By <a style="color: #777; margin-right: 20px;" href="http://www.wasiladev.com" target="_blank">WasilaDev</a></p>
                    <p>جميع الحقوق محفوظه لشركه فى المحل دوت كوم</p>
                </div>

                <div class="conditions col-xs-12 col-md-3">
                    <a href="/privacy">سياسة الخصوصية</a>
                    <a href="/rules">احكام وشروط</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<script>
    function newsubuser(){
        $('#subsForum').submit(function(e){
          e.preventDefault();
          var form = $('#subsForum');
          $.ajax({
            type:'POST',
            url:'/',
            data:form.serialize(),
            dataType:'json',
            success: function(data){
                console.log(data);
                $('#msg').html('تم الأشتراك بنجاح');
                $("#msg").fadeIn(3000);
                setTimeout(function() {
                   $('#msg').fadeOut(2000);
                }, 10000);
            }
          });
        });
    };
    function getarea(){
        cities = document.getElementById('governorate').value;
        console.log(cities);
          $.ajax({
            type:'GET',
            url:'/vendor/cities',
            data:{city:cities},
            success: function(data){
                console.log(data);
                $('#area > option').remove();
                for(var i = 0; i < data.area.length; i++){
                    addoption = '<option value='+data.area[i].id+' class="level-0">'+data.area[i].name+'</option>';
                    $('#area').append(addoption);
                }
                addoption="";
            },
            error: function(){
                console.log('error');
            }
          });
       // console.log('error');
    };
</script>
</body>
</html>