@extends('site.master')
@section('content')
	<?php $setting = DB::table('setting')->get(); ?>
	<!-- Main Content - start -->
<main id="post">
<script defer>
	function header() {
		let h2 = document.createElement('h2');
		h2.append('الدعم الفني');
		h2.classList.add('content-header');
		document.getElementById("contentPagesHeader").append(h2);
	}
	window.header();
</script>
   <div class="container">
    <div class="row">
      <div class="col-xs-12" style="margin-bottom: 3rem; ,argin-top: 3rem">
      <div class="img-wrapper" style="background-image: url('/image/{{$setting[3]->photo}}'); background-size: cover; height: 350px">
      <img class="img-responsive" src="" alt="" srcset="">
      </div>
      
        <p style="margin-top: 1.5rem">         
          <?php echo $setting[3]->paragraph; ?>
        </p>
      </div>
      </div>
    </div>
</main>
<!-- Main Content - end -->
	
@endsection