@extends('site.master') @section('content')



<!-- Blog - Start -->
<section class="navigate">
  <div class="container" style="margin-right: 0 !important; margin-left: 0 !important">
    <div class="row">
      <div class="col-xs-12">
        <div class="navigation text-bold">
          <p>
            <a href="/">الرئيسية</a> &gt; &gt; المدونة</p>
        </div>
      </div>

    </div>

  </div>
</section>
<section id="blog-filters">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul class="cat-filters">
            <li>
                <a href="#" onclick="filters('00')">
                    كل المدونات
                </a>
            </li>
        @foreach($cats as $cat)
          <li>
            <a href="#" onclick="filters('{{$cat->id}}')">
              {{$cat->name}}
            </a>
          </li>
        @endforeach
        </ul>
      </div>
    </div>
  </div>

</section>

<section id="blog">
  <div class="container">
    <div class="row">

      @if(count($blog)>0) 
      <div class="filterBlog">
      @foreach($blog as $blogs)

      <div class="col-md-3 col-sm-6" id="filterBlog{{$blogs->id}}">
        <a href="/blog/post/{{$blogs->id}}">
          <div class="thumbnail">
            <img src="/image/{{$blogs->photo}}" alt="" srcset="">
            <div class="card-det">
              <h3>{!!$blogs->title!!}</h3>
              <p>
                <?php echo $blogs->paragraph; ?>
              </p>
            </div>
          </div>
        </a>
      </div>
      
      @endforeach 
      </div>
      @endif
    </div>
  </div>
</section>


<!-- Header - end -->


@endsection

<script>
function filters(Data){
  $.ajax({
      type:'POST',
      url:'/api/filterBlog',
      data:{catID:Data},
      success: function(data){
          console.log(data);
          $('.filterBlog > div').remove();
          
          for(var i = 0; i < data.length; i++){
              addoption = '<div class="col-md-3 col-sm-6" id="filterBlog'+data[i].id+'"><a href="/blog/post/'+data[i].id+'"><div class="thumbnail"><img src="/image/'+data[i].photo+'" alt="" srcset=""><div class="card-det"><h3>'+data[i].title+'</h3><p>'+data[i].paragraph+'</p></div></div></a></div>';
              $('.filterBlog').append(addoption);
          }
          addoption="";
      },
      error: function(){
          console.log('error');
      }
  });

}
</script>