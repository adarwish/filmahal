@extends('site.master')
@section('content')
	
	<!-- Main Content - start -->
<main id="post">
    <section class="navigate">
    <div class="container" style="margin-right: 0 !important; margin-left: 0 !important">
        <div class="row">
            <div class="col-xs-12">
                <div class="navigation text-bold">
                    <p><a href="/">المدونة</a> &gt; &gt; </p>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
      <div class="col-xs-12" style="margin-bottom: 3rem; ,argin-top: 3rem">
      <div class="img-wrapper" style="background-image: url('/image/{{$blog->photo}}'); background-size: cover; height: 350px">
      <img class="img-responsive" src="" alt="" srcset="">
      </div>
      
        <p style="margin-top: 1.5rem">         
          <?php echo $blog->paragraph; ?>
        </p>
      </div>
      </div>
    </div>
</main>
<!-- Main Content - end -->
	
@endsection