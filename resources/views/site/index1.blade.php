<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>فى المحل . كوم</title>
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('imgs/favIcon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('imgs/favIcon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('imgs/favIcon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('imgs/favIcon/manifest.json')}}">
    <link rel="mask-icon" href="{{asset('imgs/favIcon/safari-pinned-tab.svg')}}" color="#38ef7d">
    <meta name="theme-color" content="#ffffff">
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('csssoon/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('csssoon/font-awesome.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('csssoon/main.css')}}">
    
    <script defer src="{{asset('jssoon/jquery-3.2.1.min.js')}}"></script>
    <script defer src="{{asset('jssoon/bootstrap.min.js')}}"></script>
    <script defer src="{{asset('jssoon/typed.min.js')}}"></script>
    <script defer src="{{asset('jssoon/main.js')}}"></script>
    
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <main>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="img-container">
                        <img class="img-responsive" src="{{asset('imgs/logo.png')}}">
                    </div>

                    <div class="virtual-search">
                        <p class="text-bold opening-soon"></p>
                        <i class="fa fa-search fa-2x"></i>
                        <div class="clear"></div>
                    </div>
                </div>
                
                <div class="shop-box col-xs-12">
                    <div class="shop">
                        <div class="tanda">
                            <img class="img-responsive" src="{{asset('imgs/tanda.svg')}}">
                    </div>
                        <div class="timer-bg">
                            <img src="imgs/Group%20832.svg" class="img-responsive">
                            <div class="counter text-bold">
                                <div class="days">
                                    00
                                    <div class="word text-center">
                                        يوم
                                    </div>
                                </div>
                                <div class="hours">
                                    00
                                    <div class="word text-center">
                                        ساعة
                                    </div>
                                </div>
                                <div class="minuts">
                                    00
                                    <div class="word text-center">
                                        دقيقة
                                    </div>
                                </div>
                                <div class="seconds">
                                    00
                                    <div class="word text-center">
                                        ثانية
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="send-message">
                            <h4 class="text-bold">أرسل لى عندما يكون مُتاح</h4>
                            <form method="POST" id="contactForum">
                                {{csrf_field()}}
                                <input type="email" name="email" id="email" required placeholder="البريد الالكترونى">
                                
                                <button type="submit" class="text-bold" onclick="sendmail()">ارسال</button>
                                <h3><font id="msg" color="green"></font></h3>
                            </form>
                        </div>
                    </div>
                </div>
                
                <div class="social-icons col-xs-12">
                            <ul>
                                <a href="#" target="_blank" class="facebook">
                                    <li>
                                        <i class="fa fa-facebook fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="#" target="_blank" class="instagram">
                                    <li>
                                        <i class="fa fa-instagram fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="#" target="_blank" class="googlePlus">
                                    <li>
                                        <i class="fa fa-google-plus fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="#" target="_blank" class="twitter">
                                    <li>
                                        <i class="fa fa-twitter fa-2x"></i>
                                    </li>
                                </a>
                            </ul>
                        </div>
            </div>
        </div>
    </main>
    <script>
        function sendmail(){
            $('#contactForum').submit(function(e){
              e.preventDefault();
              var form = $('#contactForum');
              $.ajax({
                type:'POST',
                url:'/',
                data:form.serialize(),
                success: function(data){
                    console.log(data);
                    $('#msg').html('تم إرسال بريدك بنجاح ، شكراً لك');
                    $("#msg").fadeIn(3000);
                    setTimeout(function() {
                       $('#msg').fadeOut(2000);
                    }, 10000);                     
                },
                error: function(){
                    console.log('error');
                    
                }
              });
            });
        }
    </script>
    
</body>
</html>
