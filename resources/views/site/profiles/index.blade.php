@extends('site.profiles.master') 
@section('content')

<?php $ads = DB::table('ads')->get();

 ?>
 
 @if(Auth::user())
 <?php 
 $checkfollow = DB::table('follow')->where('user_id',Auth::user()->id)->where('market_id',$id)->get();
	$checkedFollow = count($checkfollow);
 ?>
 @endif

<!--Start 0f Pofile-settings-->
<section id="profile_settings">
  <div class="navigation">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <p class="navigationText text-bold">
            <a href="/" id="mainPage">الرئيسيه</a> >> اعدادات
          </p>
        </div>
      </div>
    </div>
  </div>

  <div class="container vendorProfile">
    <div class="row">
      <div class="col-xs-12 nameEmail">
        <div class="message-header">
          @if(Auth::user())
          <div class="img-container">
            <img src="/image/{{Auth::user()->photo}}" class="img-responsive">
          </div>
          <div class="client-title">
            <h4>{{Auth::user()->user_name}}</h4>
            <p>{{Auth::user()->email}}</p>
          </div>


          @endif
        </div>
      </div>
    </div>
    <div class="row settings">

      <div class="col-xs-12 col-md-9 inbox">
        <div class="reviews row">
          <h4 class="primary text-bold">صندوق الرسائل</h4>
        </div>
        <div class="row">
          <div class="col-xs-7">
            <div class="row col-reverse">
              <div class="col-xs-12 textBox">
                <p class="primary">كنت كلمنت حضرتك بخصوص البنطلون الرمادى</p>
                <p class="secondary text-left">هنبعته بكره يا حبيبى</p>
              </div>
              <div class="col-xs-12 msg">
                <div class="row">

                  <div class="col-xs-12">
                    <div class="row box">
                      <i class="fa fa-camera fa-lg" aria-hidden="true">
                        <input type="file" class="myInput" accept="image/gif, image/jpeg, image/png, image/jpg">
                      </i>
                      <textarea id="emoji"></textarea>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>

          <div class="col-xs-5 messages-box">
            <div class="row">
              <ul class="messages-list">
                <li>
                  <a href="#">
                    <div class="client">
                      <div class="message-header">
                        <div class="img-container">
                          <img src="imgs/hosny.jpg" class="img-responsive">
                        </div>
                        <div class="client-title">
                          <h5>حسنى مبارك</h5>
                          <p>هو ينفع تبعتلى الاوردر على القصر الرئاسى</p>
                          <p class="time">07:03 PM</p>
                        </div>

                      </div>
                      <div>

                      </div>

                    </div>
                  </a>
                </li>

                <li>
                  <a href="#">
                    <div class="client">
                      <div class="message-header">
                        <div class="img-container">
                          <img src="imgs/hosny.jpg" class="img-responsive">
                        </div>
                        <div class="client-title">
                          <h5>حسنى مبارك</h5>
                          <p>هو ينفع تبعتلى الاوردر على القصر الرئاسى</p>
                          <p class="time">07:03 PM</p>
                        </div>

                      </div>
                      <div>

                      </div>

                    </div>
                  </a>
                </li>

                <li>
                  <a href="#">
                    <div class="client">
                      <div class="message-header">
                        <div class="img-container">
                          <img src="imgs/hosny.jpg" class="img-responsive">
                        </div>
                        <div class="client-title">
                          <h5>حسنى مبارك</h5>
                          <p>هو ينفع تبعتلى الاوردر على القصر الرئاسى</p>
                          <p class="time">07:03 PM</p>
                        </div>

                      </div>
                      <div>

                      </div>

                    </div>
                  </a>
                </li>

                <li>
                  <a href="#">
                    <div class="client">
                      <div class="message-header">
                        <div class="img-container">
                          <img src="imgs/hosny.jpg" class="img-responsive">
                        </div>
                        <div class="client-title">
                          <h5>حسنى مبارك</h5>
                          <p>هو ينفع تبعتلى الاوردر على القصر الرئاسى</p>
                          <p class="time">07:03 PM</p>
                        </div>

                      </div>
                      <div>

                      </div>

                    </div>
                  </a>
                </li>

                <li>
                  <a href="#">
                    <div class="client">
                      <div class="message-header">
                        <div class="img-container">
                          <img src="imgs/hosny.jpg" class="img-responsive">
                        </div>
                        <div class="client-title">
                          <h5>حسنى مبارك</h5>
                          <p>هو ينفع تبعتلى الاوردر على القصر الرئاسى</p>
                          <p class="time">07:03 PM</p>
                        </div>

                      </div>
                      <div>

                      </div>

                    </div>
                  </a>
                </li>
              </ul>
            </div>
          </div>


        </div>
      </div>

      <div class="col-xs-12 col-md-9 reviewsBox">
        <div class="reviews">
          <h4 class="primary text-bold">تقييمات المحل</h4>
        </div>
        <div class="person-review">
          <div class="message-header">
            <div class="img-container">
              <img src="imgs/hosny.jpg" class="img-responsive">
            </div>
            <div class="client-title">
              <h4>حسنى مبارك</h4>
              <p class="timeEn">2:30 PM</p>
            </div>
          </div>
          <div class="reviewText">
            <p>اشتريت تيشيرت من المحل ده وباظ</p>
          </div>
        </div>

        <div class="person-review">
          <div class="message-header">
            <div class="img-container">
              <img src="imgs/hosny.jpg" class="img-responsive">
            </div>
            <div class="client-title">
              <h4>حسنى مبارك</h4>
              <p class="timeEn">2:30 PM</p>
            </div>
          </div>
          <div class="reviewText">
            <p>اشتريت تيشيرت من المحل ده وباظ</p>
          </div>
        </div>

        <div class="person-review">
          <div class="message-header">
            <div class="img-container">
              <img src="imgs/hosny.jpg" class="img-responsive">
            </div>
            <div class="client-title">
              <h4>حسنى مبارك</h4>
              <p class="timeEn">2:30 PM</p>
            </div>
          </div>
          <div class="reviewText">
            <p>اشتريت تيشيرت من المحل ده وباظ</p>
          </div>
        </div>

        <div class="textBar">
          <form>
            <div class="search">
              <input type="search" placeholder="اكتب ردك">
              <div class="img-container">
                <img src="imgs/hosny.jpg" class="img-responsive">
              </div>
            </div>
          </form>
        </div>
      </div>

      <div class="col-xs-12 col-md-9 allSettings">
        <div class="settingsNav row">
          <div class="col-xs-4 text-center general">
            <h4>اعدادات عامه</h4>
          </div>
          <div class="col-xs-4 text-center specific">
            <h4>اعدادات الملف الشخصى</h4>
          </div>
        </div>
        <div class="generalElments">
          <div class="
                    rs">
            <h4 class="primary text-bold 
                        rsNow">فئات المحل الحاليه</h4>
          </div>
          <div class="attrsVal">
            <div class="val col-xs-2 col-md-3 text-center">
              <i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"></i>
              <label>ادوات كهربائيه</label>

              <div class="popup hidden">
                <div class="contain">
                  <p class="text-bold">اخفاء هذه الفئة</p>
                  <i class="fa fa-caret-right" aria-hidden="true"></i>
                </div>
              </div>
            </div>

            <div class="val col-xs-2 col-md-3 text-center">
              <i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"></i>
              <label>ادوات كهربائيه</label>

              <div class="popup hidden">
                <div class="contain">
                  <p class="text-bold">اخفاء هذه الفئة</p>
                  <i class="fa fa-caret-right" aria-hidden="true"></i>
                </div>
              </div>
            </div>

            <div class="val col-xs-2 col-md-3 text-center">
              <i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"></i>
              <label>ادوات كهربائيه</label>

              <div class="popup hidden">
                <div class="contain">
                  <p class="text-bold">اخفاء هذه الفئة</p>
                  <i class="fa fa-caret-right" aria-hidden="true"></i>
                </div>
              </div>
            </div>
          </div>

          <div class="clear"></div>

          <div>
            <div class="attrs">
              <h4 class="primary text-bold attrsNow">اضافة فئات اخرى</h4>
            </div>
            <div class="add-cates">
              <p>قم باضافة كل الفئات الموجوده فى محل
                <i id="cates" class="fa fa-plus-square primary fa-lg" aria-hidden="true"></i>
              </p>

            </div>

          </div>
        </div>

        <div class="specificElments" id="restInfo">
          <form>
            <div class="photo img-uploader">
              <div class="main">
                <div class="text-center cloud">
                  <i class="fa fa-cloud-upload fa-lg" aria-hidden="true"></i>
                  <p>صوره الشخصيه</p>
                </div>
              </div>
              <div class="absolute">
                <input type="file" class="myInput" accept="image/gif, image/jpeg, image/png, image/jpg">
              </div>
              <div class="img"></div>
            </div>
            <div class="infoProfile">
              <div class="box">
                <label for="username" class="text-right">اسم المستخدم</label>
                <input type="text" name="username">
                <span class="text-bold text-left"></span>
              </div>

              <div class="box">
                <label for="email" class="text-right">الاميل</label>
                <input type="email" name="email">
                <span class="text-bold text-left"></span>
              </div>

              <div class="box">
                <label for="password" class="text-right">كلمة السر</label>
                <input type="password" name="password">
                <span class="text-bold text-left"></span>
              </div>
            </div>

            <button class="editProfileSettings">حفظ</button>
          </form>
        </div>
        
        
        
      </div>

      <div id="third-step2">
        <div class="col-xs-9 bg-white">
          <div class="choose-kinds">
            <h3 class="text-bold">فئات المحل</h3>
            <div class="clear"></div>
            <p>اختر جميع الفئات الموجوده فى محل</p>
          </div>
          <div class="chooseCat">
            <div class="col-xs-2 col-md-3 text-center">
              <label>للرجال</label>
              <input type="checkbox" name="kinds" value="mens">
            </div>
            <div class="col-xs-2 col-md-3 text-center">
              <label>احذية</label>
              <input type="checkbox" name="kinds" value="mens">
            </div>
            <div class="col-xs-2 col-md-3 text-center">
              <label>للرجال</label>
              <input type="checkbox" name="kinds" value="mens">
            </div>
            <div class="col-xs-2 col-md-3 text-center">
              <label>للرجال</label>
              <input type="checkbox" name="kinds" value="mens">
            </div>
          </div>
          <div>
            <p class="text-center error text-bold" id="errorCates"></p>
          </div>
          <div class="clear"></div>
          <div class="next-step step3">
            <a href="#" id="save2" class="text-center">حفظ</a>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-md-3">
        <ul class="profile-settings profile-settingss">
          <li>
            <a href="#" class="reviewsPeople">
              <i class="ion ion-edit" aria-hidden="true"></i>
              اراء الناس فى محلك
            </a>
          </li>

          <li>
            <a href="#" class="boxMessages">
              <i class="fa fa-envelope" aria-hidden="true"></i>
              صندوق الرسائل
            </a>
          </li>

          <li>
            <a href="#" class="settingProfile">
              <i class="fa fa-cog" aria-hidden="true"></i>
              اعدادات حسابى
            </a>
          </li>

          <li>
            <a href="#">
              <i class="fa fa-user" aria-hidden="true"></i>
              تسجيل الخروج
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!--End 0f Pofile-settings-->






<!--Start Of Photo Uploader-->
<section id="uploader">

  <div class="navigate">
    <div class="container-fluid">
      <div class="navigation text-bold">
        <p>
          <a href="/">الرئيسية</a>
          &gt; &gt;
          <a href="#">محلات ملابس</a>
          &gt; &gt;
          <span>{{$store->title}}</span>



        </p>
      </div>
    </div>
  </div>
  <div class="owl-carousel owl-loaded owl-drag owl-theme">
    @foreach($photo as $photos)
    <div class="item cover">

      <div class="img">
        <img src="/image/{{$photos->photo}}" class="img-responsive">
      </div>

    </div>
    @endforeach
  </div>
</section>
<!--End Of Photo Uploader-->
<section id="navbar">

  <div class="row messageContainer">
    <div class="messageClicked client col-xs-12">
      <div class="newMessage text-color">
        <h3 class="text-bold">
          <i class="fa fa-envelope fa-lg"></i>رساله جديده</h3>
      </div>
      <hr>
      <div class="message-header">
        <div class="img-container">
          @if(Auth::user())
          <img src="/image/{{Auth::user()->photo}}" class="img-responsive"> @endif
        </div>
        <div class="client-title">
          @if(Auth::user())
          <h5>{{Auth::user()->user_name}}</h5>
          @endif
          <p>سوف يتواصل معك قريبا من خلال رقم التليفون</p>
        </div>
      </div>
      <form method="POST" id="newmsgForum2">
        {{csrf_field()}}
        <input type="hidden" name="save3" value="save3">
        <input type="hidden" name="storeId" value="{{$id}}">
        <div class="messageBox">
          <i class="fa fa-caret-up"></i>
          <textarea name="message" id="message" placeholder="اكتب رسالتك هنا"></textarea>
        </div>
        <div class="buttons">
          <h3>
            <font color="green" id="msgsuccess2"></font>
          </h3>
          <button class="exit">الغاء</button>
          <button onclick="newmsg2()">ارسال</button>
        </div>
      </form>
    </div>
  </div>
  <div class="row messageContainer">


    <div class="messageClicked client col-xs-12">
      <div class="newMessage text-color">
        <h3 class="text-bold">
          <i class="ion ion-edit primary"></i>تقييم جديد</h3>
      </div>
      <hr>
      <div class="message-header">
        <div class="img-container">
          @if(Auth::user())
          <img src="/image/{{Auth::user()->photo}}" class="img-responsive"> @endif
        </div>
        <div class="client-title">
          @if(Auth::user())
          <h5>{{Auth::user()->user_name}}</h5>
          @endif
          <p>اكتب تقييم للمحل</p>
        </div>
      </div>
      <form method="POST" id="newcommentForum2">
        {{csrf_field()}}
        <input type="hidden" name="save1" value="save1">
        <input type="hidden" name="storeId" value="{{$id}}">
        <div class="messageBox">
          <i class="fa fa-caret-up"></i>
          <textarea name="comment" id="message" placeholder="اكتب تقييم هنا"></textarea>
        </div>
        <div class="buttons">
          <h3>
            <font id="newcomment2" color="green"></font>
          </h3>
          <button class="exit">الغاء</button>
          <button onclick="addcomment2()">ارسال</button>
        </div>
      </form>
    </div>
  </div>

  <div class="border">
    <div class="container">
      <div class="row">
        <nav>
          <ul>
            <li>
              <a class="newOffers" href="">عروض جديدة</a>
              <span class="primary coz">(</span>
              <span class="num">
                <?php  $num = DB::table('product')->where('market_id',$id)->where('status',1)->get();?>
                <?php echo count($num); ?>
              </span>
              <span class="num">منتج</span>
              <span class="primary coz">)</span>
            </li>
            <li>
              <a class="sales" href="" id="nav-discount">
                تخفيضات @if(count($sale) >0)
                <span class="numNotify">
                  <?php echo count($sale); ?>
                </span> @endif
              </a>
            </li>
            <li>
              <a class="active shopCats" href="">اقسام المحل</a>
            </li>
            <li>
              <a class="contactus" href="">تواصل معنا</a>
            </li>
          </ul>
          <ul class="buttonsActions">
            <li>
              <i class="fa fa-share-alt a2a_dd primary" aria-hidden="true"></i>
            </li>
            <li>
              <i class="ion ion-edit primary" aria-hidden="true"></i>
            </li>
            @if(Auth::user())
            @if($checkedFollow > 0)
 		<li class="favorMe redborder">
              <i onclick="followstore('{{$id}}')" class="fa fa-heart-o primary" aria-hidden="true"></i>
            </li>
 	    @else
 		<li class="favorMe">
              <i onclick="followstore('{{$id}}')" class="fa fa-heart-o primary" aria-hidden="true"></i>
            </li>
	     @endif
	     @else
	     <li class="favorMe">
              <i onclick="followstore('{{$id}}')" class="fa fa-heart-o primary" aria-hidden="true"></i>
            </li>
	     @endif
            
            <li>
              <i class="fa fa-envelope primary" aria-hidden="true"></i>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="slid-cates">
        <div class="owl-carousel">
          <?php  $counter = count($subcategory); ?> 
          @foreach($msub as $key => $msubs)

          <?php 
                    $sub = DB::table('sub_category')->where('id',$msubs->sub_category_id)->get();
                    $photo2 = DB::table('photo')->where('sub_category_id',$sub[0]->id)->where('product_id',NULL)->get(); 

                    ?>
          <div class="item photo">

            @if(count($photo2) > 0)
            <div class="img">
              <img src="/image/{{$photo2[0]->photo}}" class="img-responsive cat-image">
            </div>
            @else
            <div class="main">

              <div class="inputUploader text-center">
                <img src="/image/400x400.png" class="img-responsive">

              </div>

            </div>
            @endif

            <div class="brand-name text-center text-bold">
              <a href="#">{{$sub[0]->name}}</a>
              @if($key == $counter-1)
              <input type="radio" name="gender" onchange="specificProducttwo('{{$sub[0]->id}}')" id="genders1" value="{{$sub[0]->id}}">
              
               @else
              <input type="radio" name="gender" onchange="specificProducttwo('{{$sub[0]->id}}')" id="genders" value="{{$sub[0]->id}}">
               @endif
            </div>
          </div>
          @endforeach


        </div>
      </div>
      <div class="myMap">
        <div id="map"></div>
        <div id="addAddress">
          <div class="title">
            <h3 class="text-center">سعداء بزيارتك لينا</h3>
          </div>
          <div class="addresses">
            <?php 
                                $another_Address = DB::table('contact_info')->where('market_id',$store->id)->where('address','!=',null)->get(); ?>
            <ul>
              <li>
                <span class="places">
                  <i class="fa fa-map-marker marker" aria-hidden="true"></i>
                  <span class="text-bold">{{$store->address}}</span>
                </span>
                <div class="clear"></div>
              </li>

              <li>
                <span class="places">
                  <i class="fa fa-map-signs" aria-hidden="true"></i>
                  <span class="text-bold">{{$store->special_mark}}</span>
                </span>
                <div class="clear"></div>
              </li>
            </ul>

            <div class="addAnotherAddress">
              <div class="customize">
                <a href="#" class="secondary">
                  <?php if(count($another_Address) > 0){ echo count($another_Address); } else{echo '';} ?> فروع اخرى</a>
                <div class="clear"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="customizeHere col-xs-12">
            <!-- For Places NEVER DELETE IT -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--End 0f Nav -->

<!-- Start 0f Main -->
<section id="bigPopUp">
  <div class="container-fluid">
    <div class="row">

      <div class="container">

      </div>

    </div>
  </div>

  <div class="container-fluid">
    <div class="row">

      <div class="container">
        <div class="row messageContainer">

        </div>
      </div>

    </div>
  </div>







  <!-- Modal structure -->
  <div id="modal">

    <div class="addProduct mainSection col-xs-12" id="uploadProduct">
      <div class="row">
        <div class="reverseCols">
          <div class="col-xs-12 col-md-6 productForm">

            <div class="close">
              <a data-izimodal-close="">×</a>
            </div>


            <div class="name">
              <h4 class="text-bold primary">اسم المنتج</h4>
              <p class="secondary text-bold">
                <input type="text" id="productNm" value="0" disabled>
              </p>
            </div>

            <div class="price">
              <h4 class="text-bold primary">سعر المنتج</h4>
              <p class="secondary text-bold">
                <input type="text" id="productPr" value="0" disabled>
                <span class="egp-span"> جنيه مصرى </span>
              </p>
            </div>
            
             <div class="description">
              <h4 class="text-bold primary">وصف المُنتج</h4>
              <p class="secondary text-bold">
                <input type="text" id="productDes" value="0" disabled>
              </p>
            </div>




            <div class="attribute-container">


                <div class="attribute-type">
                
                    <div class="attribute-title">
                    
                        

                    </div>

                    <div class="attribute-props">
                    
                        <ul>
                        
                            <li>
                         
	                        <ul class="attribute-second">
	                        
	                            
	                        
	                        </ul>
	                   </li>
                           
                        
                        </ul>
                    
                    </div>
                    
                
                </div>


               <!-- <div class="attribute-type">
                
                    <div class="attribute-title">
                    
                        <h4 class="primary">الألوان</h4>

                    </div>

                    <div class="attribute-props">
                    
                        <ul>
                        
                            <li>اسود</li>
                            <li>اخضر</li>
                            <li>احمر</li>
                            <li>ازرق</li>
                        
                        </ul>
                    
                    </div>
                
                </div> -->
            
            
            </div>





            <div class="view-prouduct">

              <a href="#" id="proDetails" class="view-prouduct-button">تفاصيل المنتج</a>

            </div>




          </div>

          <div class="col-xs-12 col-md-6 productPhotosUploader">
            <div class="productPhotos photo">
              <div class="img">
                <img id="img1" src="imgs/1.jpg" class="img-responsive">
              </div>
            </div>
           
          </div>
        </div>
      </div>
    </div>


  </div>







  <div class="container">
    <div class="row">
      <div class="wholeSection col-xs-12">
        <div class="row">
          <div class="bigArrow">
            <i class="fa fa-caret-up" aria-hidden="true"></i>
          </div>

          <div class="mainSection shopCat col-xs-12">


            <div class="row">



              <div id="catproducts" class="products">


                @foreach($product as $products)
                
               
                <?php $photo3 = DB::table('photo')->where('market_id',null)->where('product_id',$products->id)->where('sub_category_id',null)->get();
                            $attribute = DB::table('market_attribute')->where('product_id',$products->id)->get(); ?>

                <div class="anProduct">
                
                  <div class="productPhoto" style="position: relative">

                    <img src="/image/{{$products->photo}}" class="img-responsive">

                  </div>
                  <div class="brand-name text-center text-bold">
                    <a href="" class="shopName">
                      <span>{{$products->name}}</span>
                      <div class="img-container">
                        @if(count($cinfo) !==0)
                        <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive"> @endif
                      </div>
                    </a>
                  </div>

                  <div class="productInfo">
                    <p class="text-bold text-center nameOfProduct">{{$products->discription}}</p>
                    <div class="prices">
                      @if($products->discount ==0)
                      <p class="text-bold priceOfProduct">{{$products->price}} جنيه مصرى</p>
                      @else
                      <p class="text-bold priceOfProduct">{{$products->discount}} جنيه مصرى</p>
                      <p class="text-bold deleted">{{$products->price}} جنيه مصرى</p>
                      @endif
                    </div>




                    <div class="">
                      @if(count($photo3) == 1)
                      <button class="trigger" onclick="replace('{{$products->id}}','{{$products->discription}}','{{$products->price}}','{{$products->name}}','{{$products->created_at}}','{{$products->photo}}','{{$photo3[0]->photo}}',null,null,null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @elseif(count($photo3) == 2)
                      <button class="trigger" onclick="replace('{{$products->id}}','{{$products->discription}}','{{$products->price}}','{{$products->name}}','{{$products->created_at}}','{{$products->photo}}','{{$photo3[0]->photo}}','{{$photo3[1]->photo}}',null,null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @elseif(count($photo3) == 3)
                      <button class="trigger" onclick="replace('{{$products->id}}','{{$products->discription}}','{{$products->price}}','{{$products->name}}','{{$products->created_at}}','{{$products->photo}}','{{$photo3[0]->photo}}','{{$photo3[1]->photo}}','{{$photo3[2]->photo}}',null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @elseif(count($photo3) == 4)
                      <button class="trigger" onclick="replace('{{$products->id}}','{{$products->discription}}','{{$products->price}}','{{$products->name}}','{{$products->created_at}}','{{$products->photo}}','{{$photo3[0]->photo}}','{{$photo3[1]->photo}}','{{$photo3[2]->photo}}','{{$photo3[3]->photo}}')">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @else
                      <button class="trigger" onclick="replace('{{$products->id}}','{{$products->discription}}','{{$products->price}}','{{$products->name}}','{{$products->created_at}}','{{$products->photo}}',null,null,null,null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>

                      @endif
                    </div>
                  </div>
                </div>
               
                @endforeach
              </div>


            </div>

            <div class="pagination">
              <a href="{{$product->previousPageUrl()}}">
                <i class="fa fa-angle-right" aria-hidden="true"></i>السابق</a>
              <div class="counter text-bold">

                <span class="num">

                  <?php echo count($productNum); ?>
                </span>
                <span class="fasl">|</span>
                <span class="primary num">
                  <?php echo count($product); ?>
                </span>
                <span class="primary">محل</span>
              </div>
              <a href="{{$product->nextPageUrl()}}">التالى
                <i class="fa fa-angle-left" aria-hidden="true"></i>
              </a>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="mainSection newOffer col-xs-12">
        <div class="row">
          <i class="fa fa-bars" aria-hidden="true"></i>
          <div class="col-xs-4 col-md-3 asideContainer">

            <aside>
              <div class="reviewsClients col-xs-12">
                <h4 class="text-bold primary">تقييمات المحل</h4>
                <a href="/store/{{$id}}/{{$savedcityId}}/{{$savedareaId}}/{{$searchval}}/all-comments" class="primary">مشاهدة الكل
                  <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                </a>
              </div>
              @if(count($review) >0) 
              @foreach($review as $reviews)

              <?php $vendorReview = DB::table('review')->where('replay',$reviews->id)->get(); ?>
              <div class="newreviewExample">
                <div class="reviewExample col-xs-12">
                  <div class="client">
                    <div class="message-header">
                      <div class="img-container">
                        @if($reviews->replay > 0)
                        <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive"> 
                        @else
                        <img src="/image/{{$reviews->user->photo}}" class="img-responsive"> 
                        @endif
                      </div>
                      <div class="client-title">
                        @if($reviews->replay == 0)

                        <h5>{{$reviews->user->user_name}}</h5>
                        @else
                        <h5>{{$store->title}}</h5>

                        @endif
                        <p class="time">منذ
                          <?php
                              $date1=date_create($reviews->created_at);
                              $date2=date_create(date("Y-m-d"));
                              $diff=date_diff($date1,$date2);
                              if($diff->format("%R%a days") == '+0 days' || $diff->format("%R%a days") == '-0 days'){
	                                                              
	                              $date1=date_create($reviews->created_at);
	                              $date2=date_create(date("Y-m-d H:i:s"));
	                              $diff=date_diff($date1,$date2);
	                              
                                                       echo       $diff->format("%h ساعات");
                              }else{
                                echo $diff->format("%a ايام");
                              }
                               ?>
                        </p>
                      </div>
                    </div>
                  </div>
                  <p class="textReview">{{$reviews->comment}}</p>
                  <p class="likes secondary text-bold">
                    <a class="likeBtn" onclick="likecomment('{{$reviews->id}}')">
                      <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                    </a>
                    <span class="approveCounter">(
                      <span class="counterLikes">
                        <?php echo count($reviews->like); ?>
                      </span>)</span>
                    <span class="approve">يؤيد هذا التقييم</span>
                  </p>

                </div>
              </div>
              @foreach($vendorReview as $vendorReviews)
              <?php $like = DB::table('like')->where('review_id',$vendorReviews->id)->get(); ?>
              <div class="newreviewExample">
                <div class="reviewExample col-xs-12">
                  <div class="client">
                    <div class="message-header">
                      <div class="img-container">
                        @if($vendorReviews->replay > 0 || $vendorReviews->vendor_id > 0)
                        <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive"> 
                        @else
                        <img src="/image/{{$vendorReviews->user->photo}}" class="img-responsive"> 
                        @endif
                      </div>
                      <div class="client-title">
                        @if($vendorReviews->replay == 0 || $vendorReviews->vendor_id == 0)

                        <h5>{{$vendorReviews->user->user_name}}</h5>
                        @else
                        <h5>{{$store->title}}</h5>

                        @endif
                        <p class="time">منذ
                          <?php
                              $date1=date_create($reviews->created_at);
                              $date2=date_create(date("Y-m-d"));
                              $diff=date_diff($date1,$date2);
                              if($diff->format("%R%a days") == '+0 days' || $diff->format("%R%a days") == '-0 days'){
	                                                              
	                              $date1=date_create($reviews->created_at);
	                              $date2=date_create(date("Y-m-d H:i:s"));
	                              $diff=date_diff($date1,$date2);
	                              
                                                       echo       $diff->format("%h ساعات");
                              }else{
                                echo $diff->format("%a ايام");
                              }
                               ?>
                        </p>
                      </div>
                    </div>
                  </div>
                  <p class="textReview">{{$vendorReviews->comment}}</p>
                  <p class="likes secondary text-bold">
                    <a class="likeBtn" onclick="likecomment('{{$reviews->id}}')">
                      <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                    </a>
                    <span class="approveCounter">(
                      <span class="counterLikes">
                        <?php echo count($like); ?>
                      </span>)</span>
                    <span class="approve">يؤيد هذا التقييم</span>
                  </p>

                </div>
              </div>
              @endforeach @if(Auth::user()) @if($reviews->user->id == Auth::user()->id)
              <div class="textBar">
                <form method="POST" id="newcommentForum1">
                  {{csrf_field()}}
                  <input type="hidden" name="save1" value="save1">
                  <input type="hidden" name="replay" value="replay">
                  <input type="hidden" name="storeId" value="{{$id}}">
                  <div class="search">
                    <input onclick="addcomment1()" type="text" name="comment" placeholder="رد"> @if(Auth::user()))
                    <div class="img-container">
                      <img src="/image/{{Auth::user()->photo}}" class="img-responsive">
                    </div>
                    @endif
                  </div>
                </form>
              </div>
              @endif @endif @endforeach @endif

              <div class="textBar">
                <form method="POST" id="newcommentForum1">
                  {{csrf_field()}}
                  <input type="hidden" name="save1" value="save1">
                  <input type="hidden" name="storeId" value="{{$id}}">
                  <div class="search">
                    <input onclick="addcomment1()" type="text" name="comment" placeholder="شارك برأيك"> @if(Auth::user()))
                    <div class="img-container">
                      <img src="/image/{{Auth::user()->photo}}" class="img-responsive">
                    </div>
                    @endif
                  </div>
                </form>
              </div>


              <div class="ads">
                <a href="#" target="_blank">
                  <img src="/image/{{$ads[1]->files}}" alt="تشكيله الشتاء" class="img-responsive">
                </a>
                <a href="#" target="_blank">
                  <img src="/image/{{$ads[2]->files}}" alt="تشكيله الشتاء" class="img-responsive">
                </a>
                <a href="#" target="_blank">
                  <img src="/image/{{$ads[3]->files}}" alt="تشكيله الشتاء" class="img-responsive">
                </a>
              </div>
            </aside>
          </div>
          <div class="col-xs-12 col-md-9 products">

            @foreach($new as $news)
            
            <?php $photo5 = DB::table('photo')->where('market_id',null)->where('product_id',$news->id)->where('sub_category_id',null)->get(); ?>
            <div id="newproducts">
              <div class="newProductItem">
                <div class="productPhoto">
                  <img src="/image/{{$news->photo}}" class="img-responsive">
                </div>
                <div class="brand-name text-center text-bold">
                  <a href="" class="shopName">
                    <span>{{$store->title}}</span>
                    <div class="img-container">
                      @if(count($cinfo) !==0)
                      <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive"> @endif
                    </div>
                  </a>
                </div>

                <div class="productInfo">
                  <p class="text-bold text-center nameOfProduct">{{$news->name}}</p>
                  <div class="prices">
                    @if($news->discount > 0)
                    <p class="text-bold priceOfProduct">{{$news->discount}} جنيه مصرى</p>
                    <p class="text-bold deleted">{{$news->price}} جنيه مصرى</p>
                    @else
                    <p class="text-bold priceOfProduct">{{$news->price}} جنيه مصرى</p>
                    @endif
                  </div>
                  <div class="">
                    <?php
                                    $newdisc = preg_replace('/\s+/', '+', $news->discription);
                                    $newname = preg_replace('/\s+/', '+', $news->name);
                                     ?>
                      @if(count($photo5) == 1)
                      <button class="trigger" onclick="replace('{{$news->id}}','{{$newdisc}}','{{$news->price}}','{{$newname}}',null,'{{$news->photo}}','{{$photo5[0]->photo}}',null,null,null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @elseif(count($photo5) == 2)
                      <button class="trigger" onclick="replace('{{$news->id}}','{{$newdisc}}','{{$news->price}}','{{$newname}}',null,'{{$news->photo}}','{{$photo5[0]->photo}}','{{$photo5[1]->photo}}',null,null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @elseif(count($photo5) == 3)
                      <button class="trigger" onclick="replace('{{$news->id}}','{{$newdisc}}','{{$news->price}}','{{$newname}}',null,'{{$news->photo}}','{{$photo5[0]->photo}}','{{$photo5[1]->photo}}','{{$photo5[2]->photo}}',null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @elseif(count($photo5) == 4)
                      <button class="trigger" onclick="replace('{{$news->id}}','{{$newdisc}}','{{$news->price}}','{{$newname}}',null,'{{$news->photo}}','{{$photo5[0]->photo}}','{{$photo5[1]->photo}}','{{$photo5[2]->photo}}','{{$photo5[3]->photo}}')">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @else
                      <button class="trigger" onclick="replace('{{$news->id}}','{{$newdisc}}','{{$news->price}}','{{$newname}}',null,'{{$news->photo}}',null,null,null,null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @endif
                  </div>
                </div>
              </div>
            </div>
            
            @endforeach
          </div>
        </div>
      </div>
      <div class="mainSection sale col-xs-12">
        <div class="row">
          <i class="fa fa-bars" aria-hidden="true"></i>
          <div class="col-xs-4 col-md-3 asideContainer">

            <aside>
              <div class="reviewsClients col-xs-12">
                <h4 class="text-bold primary">تقييمات المحل</h4>
                <a href="/store/{{$id}}/{{$savedcityId}}/{{$savedareaId}}/{{$searchval}}/all-comments" class="primary">مشاهدة الكل
                  <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                </a>
              </div>
              @if(count($review) >0) @foreach($review as $reviews)
              <?php $vendorReview = DB::table('review')->where('replay',$reviews->id)->get(); ?>
              <div class="newreviewExample">
                <div class="reviewExample col-xs-12">
                  <div class="client">
                    <div class="message-header">
                      <div class="img-container">
                        @if($reviews->replay > 0)
                        <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive"> @else
                        <img src="/image/{{$reviews->user->photo}}" class="img-responsive"> @endif
                      </div>
                      <div class="client-title">
                        @if($reviews->replay == 0)

                        <h5>{{$reviews->user->user_name}}</h5>
                        @else
                        <h5>{{$store->title}}</h5>

                        @endif
                        <p class="time">منذ
                          <?php
                              $date1=date_create($reviews->created_at);
                              $date2=date_create(date("Y-m-d"));
                              $diff=date_diff($date1,$date2);
                              if($diff->format("%R%a days") == '+0 days' || $diff->format("%R%a days") == '-0 days'){
	                                                              
	                              $date1=date_create($reviews->created_at);
	                              $date2=date_create(date("Y-m-d H:i:s"));
	                              $diff=date_diff($date1,$date2);
	                              
                                                       echo       $diff->format("%h ساعات");
                              }else{
                                echo $diff->format("%a ايام");
                              }
                               ?>
                        </p>
                      </div>
                    </div>
                  </div>
                  <p class="textReview">{{$reviews->comment}}</p>
                  <p class="likes secondary text-bold">
                    <a class="likeBtn" onclick="likecomment('{{$reviews->id}}')">
                      <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                    </a>
                    <span class="approveCounter">(
                      <span class="counterLikes">
                        <?php echo count($reviews->like); ?>
                      </span>)</span>
                    <span class="approve">يؤيد هذا التقييم</span>
                  </p>

                </div>
              </div>
              @foreach($vendorReview as $vendorReviews)
              <?php $like = DB::table('like')->where('review_id',$vendorReviews->id)->get(); ?>
              <div class="newreviewExample">
                <div class="reviewExample col-xs-12">
                  <div class="client">
                    <div class="message-header">
                      <div class="img-container">
                        @if($vendorReviews->replay > 0)
                        <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive"> @else
                        <img src="/image/{{$vendorReviews->user->photo}}" class="img-responsive"> @endif
                      </div>
                      <div class="client-title">
                        @if($vendorReviews->replay == 0)

                        <h5>{{$vendorReviews->user->user_name}}</h5>
                        @else
                        <h5>{{$store->title}}</h5>

                        @endif
                        <p class="time">منذ
                          <?php
                              $date1=date_create($reviews->created_at);
                              $date2=date_create(date("Y-m-d"));
                              $diff=date_diff($date1,$date2);
                              if($diff->format("%R%a days") == '+0 days' || $diff->format("%R%a days") == '-0 days'){
	                                                              
	                              $date1=date_create($reviews->created_at);
	                              $date2=date_create(date("Y-m-d H:i:s"));
	                              $diff=date_diff($date1,$date2);
	                              
                                                       echo       $diff->format("%h ساعات");
                              }else{
                                echo $diff->format("%a ايام");
                              }
                               ?>
                        </p>
                      </div>
                    </div>
                  </div>
                  <p class="textReview">{{$vendorReviews->comment}}</p>
                  <p class="likes secondary text-bold">
                    <a class="likeBtn" onclick="likecomment('{{$reviews->id}}')">
                      <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                    </a>
                    <span class="approveCounter">(
                      <span class="counterLikes">
                        <?php echo count($like); ?>
                      </span>)</span>
                    <span class="approve">يؤيد هذا التقييم</span>
                  </p>

                </div>
              </div>
              @endforeach @endforeach @endif
              <div class="textBar">
                <form method="POST" id="newcommentForum">
                  {{csrf_field()}}
                  <input type="hidden" name="save1" value="save1">
                  <input type="hidden" name="storeId" value="{{$id}}">
                  <div class="search">
                    <input onclick="addcomment()" type="text" name="comment" placeholder="شارك برأيك">

                    <div class="img-container">
                      @if(Auth::user())
                      <img src="/image/{{Auth::user()->photo}}" class="img-responsive"> @endif
                    </div>

                  </div>
                </form>
              </div>


              <div class="ads">
                <a href="#" target="_blank">
                  <img src="/image/{{$ads[1]->files}}" alt="تشكيله الشتاء" class="img-responsive">
                </a>
                <a href="#" target="_blank">
                  <img src="/image/{{$ads[2]->files}}" alt="تشكيله الشتاء" class="img-responsive">
                </a>
                <a href="#" target="_blank">
                  <img src="/image/{{$ads[3]->files}}" alt="تشكيله الشتاء" class="img-responsive">
                </a>
              </div>
            </aside>
          </div>
          <div class="col-xs-12 col-md-9 products">
            @foreach($sale as $sales)
            <?php echo $photo4 = DB::table('photo')->where('market_id',null)->where('product_id',$sales->id)->where('sub_category_id',null)->get(); ?>
            <div id="productsale">
              <div>
                <div class="productPhoto">

                  <img src="/image/{{$sales->photo}}" class="img-responsive">

                  <p class="salePercentage primary text-bold">{{$sales->pricentage}} خصم</p>
                </div>
                <div class="brand-name text-center text-bold">
                  <a href="" class="shopName">
                    <span>{{$store->title}}</span>
                    <div class="img-container">
                      @if(count($cinfo) !==0)
                      <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive"> @endif
                    </div>
                  </a>
                </div>


                <div class="productInfo">
                  <p class="text-bold text-center nameOfProduct">{{$sales->name}}</p>
                  <div class="prices">
                    <p class="text-bold priceOfProduct">{{$sales->discount}} جنيه مصرى</p>
                    <p class="text-bold deleted">{{$sales->price}} جنيه مصرى</p>
                  </div>

                  <?php
                                    $newdisc = preg_replace('/\s+/', '+', $sales->discription);
                                    $newname = preg_replace('/\s+/', '+', $sales->name);
                                     ?>
                    <div class="">
                      @if(count($photo4) == 1)
                      <button class="trigger" onclick="replace('{{$sales->id}}','{{$newdisc}}','{{$sales->price}}','{{$newname}}',null,'{{$sales->photo}}','{{$photo4[0]->photo}}',null,null,null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @elseif(count($photo4) == 2)
                      <button class="trigger" onclick="replace('{{$sales->id}}','{{$newdisc}}','{{$sales->price}}','{{$newname}}',null,'{{$sales->photo}}','{{$photo4[0]->photo}}','{{$photo4[1]->photo}}',null,null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @elseif(count($photo4) == 3)
                      <button class="trigger" onclick="replace('{{$sales->id}}','{{$newdisc}}','{{$sales->price}}','{{$newname}}',null,'{{$sales->photo}}','{{$photo4[0]->photo}}','{{$photo4[1]->photo}}','{{$photo4[2]->photo}}',null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @elseif(count($photo4) == 4)
                      <button class="trigger" onclick="replace('{{$sales->id}}','{{$newdisc}}','{{$sales->price}}','{{$newname}}',null,'{{$sales->photo}}','{{$photo4[0]->photo}}','{{$photo4[1]->photo}}','{{$photo4[2]->photo}}','{{$photo4[3]->photo}}')">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @else
                      <button class="trigger" onclick="replace('{{$sales->id}}','{{$sales->discription}}','{{$sales->price}}','{{$sales->name}}',null,'{{$sales->photo}}',null,null,null,null)">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button>
                      @endif
                    </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="restInfo">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-12 col-md-4 addons">
        <div class="boxInput primary">
          <a onclick="newuserphone()">
            <p class="lead getMe text-center text-bold getPhone">
            
            <span>رقم التليفون</span>
            <span>0100000000</span>
            </p>
          </a>
          <p class="lead getMe text-center text-bold"><a href="" data-izimodal-open="#modal-contact" data-izimodal-transitionin="fadeInDown">ارسل لنا رساله</a></p>
          
          
          <!-- Modal structure -->
<div id="modal-contact">
<div class="">
        <div class="contact-us-modal client col-xs-12">
          <div class="newMessage text-color">
            <h3 class="text-bold">
              <i class="fa fa-envelope fa-lg"></i>رساله جديده</h3>
          </div>
          <hr>
          <div class="message-header">
            @if(Auth::user())
            <div class="img-container">

              <img src="/image/{{Auth::user()->photo}}" class="img-responsive">

            </div>
            <div class="client-title">
              <h5>{{Auth::user()->user_name}}</h5>
              <p>سوف يتواصل معك قريبا من خلال رقم التليفون</p>
            </div>
            @endif
          </div>
          <form method="POST" id="newmsgForum">
            {{csrf_field()}}
            <input type="hidden" name="save3" value="save3">
            <input type="hidden" name="storeId" value="{{$id}}">
            <div class="messageBox">
              <i class="fa fa-caret-up"></i>
              <textarea name="message" id="message" placeholder="اكتب رسالتك هنا"></textarea>
            </div>
            <div class="buttons">
              <h3>
                <font color="green" id="msgsuccess"></font>
              </h3>
              <button class="contact-us-exit" data-izimodal-close="#modal-contact">الغاء</button>
              <button onclick="newmsg()">ارسال</button>
            </div>
          </form>
        </div>
      </div>
</div>
          
          
        </div>

        <div class="social-icons">
          <ul>
            @if(count($cinfo)>0) @if($cinfo[0]->fb !== null)
            <a href="{{$cinfo[0]->facebook}}" target="_blank" class="facebook">
              <li>
                <i class="fa fa-facebook fa-2x"></i>
              </li>
            </a>
            @endif @if($cinfo[0]->inst !== null)
            <a href="#" target="_blank" class="instagram">
              <li>
                <i class="fa fa-instagram fa-2x"></i>
              </li>
            </a>
            @endif @if($cinfo[0]->facebook !== null)
            <a href="#" target="_blank" class="googlePlus">
              <li>
                <i class="fa fa-google-plus fa-2x"></i>
              </li>
            </a>
            @endif @if($cinfo[0]->twitter !== null)
            <a href="#" target="_blank" class="twitter">
              <li>
                <i class="fa fa-twitter fa-2x"></i>
              </li>
            </a>
            @endif @endif
          </ul>
        </div>
        @if(count($cinfo)>0)
        <p class="lead text-center text-bold">
          <a href="#">{{$cinfo[0]->site}}</a>
        </p>
        @endif

      </div>
      <div class="col-xs-12 col-md-8">
        <div class="row">
          <div class="companyBrand col-xs-12 col-md-2">
            <div class="photo img-uploader">
              <div class="img-container">
                @if(count($cinfo)>0)
                <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive"> @endif
              </div>
            </div>
          </div>
          @if(count($cinfo)>0)
          <div class="companyDesc col-xs-12 col-md-10">
            <p class="lead"> {{$cinfo[0]->description}}</p>
          </div>
          @endif
          <div class="clear"></div>


          <div class="dt col-xs-12">
            <div class="row">
              <div class="delivery col-xs-12 col-md-6">
                <h4 class="workTime text-bold">
                  @if(count($cinfo)>0)
                  <i class="fa fa-truck primary" aria-hidden="true"></i>
                  توصيل الطلبات @if($cinfo[0]->delivery == 1)
                  <span class="secondary text-bold">متاح</span>
                  @else
                  <span class="secondary text-bold">غير متاح</span>
                  @endif @endif
                </h4>
              </div>
              <div class="col-xs-12 col-md-3">
                <h4 class="workTime text-bold">
                  <i class="fa fa-clock-o primary fa-lg" aria-hidden="true"></i>
                  مواعيد العمل
                </h4>
                <p class="fromTo secondary text-bold">
                  من يوم
                  <span>

                    من يوم
                    <span id="editDfrom">
                      @switch($store->work_day_from) @case(1) السبت @break @case(2) الاحد @break @case(3) الاتنين @break @case(4) الثلاثاء @break
                      @case(5) الاربع @break @case(6) الخميس @break @case(7) الجمعة @break @endswitch
                    </span>
                    الى يوم
                    <span id="editDto">
                      @switch($store->work_day_to) @case(1) السبت @break @case(2) الاحد @break @case(3) الاتنين @break @case(4) الثلاثاء @break
                      @case(5) الاربع @break @case(6) الخميس @break @case(7) الجمعة @break @endswitch
                    </span>


                    <br> من &nbsp;
                    <span>{{$store->work_hours_from}}</span>&nbsp;
                    <span></span>
                    الى &nbsp;
                    <span>{{$store->work_hours_to}}</span>&nbsp;
                    <span></span>
                </p>
                <p class="lead text-bold holiday">
                  <?php $holiday = $store->work_day_to - $store->work_day_from; ?> @if($store->work_day_to == 7) @if($holiday == 6) يوم الاجازه لايوجد @elseif($holiday == 5) يوم الاجازه
                  يوم السبت @elseif($holiday == 4) يوم الاجازه يوم السبت و الاحد @elseif($holiday == 3) يوم الاجازه يوم السبت
                  و الاحد و الاتنين @elseif($holiday == 2) يوم الاجازه يوم السبت و الاحد و الاتنين و الثلاثاء @elseif($holiday
                  == 1) يوم الاجازه يوم السبت و الاحد و الاتنين و الثلاثاء و الاربع @elseif($holiday == 0) يوم الاجازه لايوجد
                  @endif @elseif($store->work_day_to == 6) @if($holiday == 5) يوم الاجازه يوم الجمعة @elseif($holiday ==
                  4) يوم الاجازه يوم الجمعة و السبت @elseif($holiday == 3) يوم الاجازه يوم الجمعة و السبت و الاحد @elseif($holiday
                  == 2) يوم الاجازه يوم الجمعة و السبت و الاحد و الاتنين @elseif($holiday == 1) يوم الاجازه يوم الجمعة و
                  السبت و الاحد و الاتنين و الثلاثاء @elseif($holiday == 0) يوم الاجازه لايوجد @endif @elseif($store->work_day_to
                  == 5) @if($holiday == 4) يوم الاجازه يوم الخميس و الجمعة @elseif($holiday == 3) يوم الاجازه يوم الخميس
                  و الجمعة و السبت @elseif($holiday == 2) يوم الاجازه يوم الخميس و الجمعة و السبت و الاحد @elseif($holiday
                  == 1) يوم الاجازه يوم الخميس و الجمعة و السبت و الاحد و الاتنين @elseif($holiday == 0) يوم الاجازه لايوجد
                  @endif @elseif($store->work_day_to == 4) @if($holiday == 3) يوم الاجازه يوم الاربع و الخميس و الجمعة @elseif($holiday
                  == 2) يوم الاجازه يوم الاربع و الخميس و الجمعة و السبت @elseif($holiday == 1) يوم الاجازه يوم الاربع و
                  الخميس و الجمعة و السبت و الاحد @elseif($holiday == 0) يوم الاجازه لايوجد @endif @elseif($store->work_day_to
                  == 3) @if($holiday == 2) يوم الاجازه يوم الثلاثاء و الاربع و الخميس و الجمعة و السبت @elseif($holiday ==
                  1) يوم الاجازه يوم الثلاثاء و الاربع و الخميس و الجمعة و السبت و الاحد @elseif($holiday == 0) يوم الاجازه
                  لايوجد @endif @elseif($store->work_day_to == 2) @if($holiday == 1) يوم الاجازه يوم الاتنين و الثلاثاء و
                  الاربع و الخميس و الجمعة و السبت و الاحد @elseif($holiday == 0) يوم الاجازه لايوجد @endif @elseif($store->work_day_to
                  == 1) يوم الاجازه يوم لايوجد @endif

                </p>
              </div>
            </div>
            <div class="col-md-3"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="ads">
  <a href="">
    <img src="/image/{{$ads[4]->files}}" alt="تشكيله الشتاء" class="img-responsive">
  </a>
</section>
<section class="equal-companies">
  <div class="container">
    <div class="row">
      <div class="headerWord">
        <h2 class="text-right text-bold">
          محلات فى نفس المنطقة
        </h2>
        <div class="hr-right">
          <hr>
        </div>
      </div>


      <div class="owl-carousel owl-theme">
        @foreach($samestore as $samestores)
        <?php $photo12 = DB::table('photo')->where('market_id',$samestores->id)->where('product_id',null)->where('sub_category_id',null)->get();

                    $cinfo12 = DB::table('contact_info')->where('market_id',$samestores->id)->get();
                     ?>
        <div class="item">
          <div class="company">
            <div class="brand col-xs-12">
              <div class="img-responsive">
                @if(count($photo12) !==0)
                <img src="/image/{{$photo12[0]->photo}}" class="img-responsive"> @endif
              </div>
              <div class="brand-name text-center text-bold">
                <a href="/user/store/profile/{{$samestores->id}}">{{$samestores->title}}</a>
              </div>
            </div>

            <div class="brand-info col-xs-12">
              <ul>
                <li>
                  <i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>
                  <span>{{$samestores->address}}</span>
                </li>

                <li>
                  <i class="fa fa-clock-o fa-lg" aria-hidden="true"></i>
                  <span>اوقات العمل من {{$samestores->work_hours_from}} حتى الـ {{$samestores->work_hours_to}}
                  </span>
                </li>

              

                <li id="opinions">
                  <span class="right">
                    <i class="fa fa-users fa-lg" aria-hidden="true"></i>
                    <span class="users">اراء الناس</span>
                  </span>

                  <span class="people-opinion">

                    <span class="people-pics first">
                      <img src="/profiles/imgs/people.jpg" class="img-responsive">
                    </span>
                    <span class="people-pics second">
                      <img src="/profiles/imgs/1.jpg" class="img-responsive">
                    </span>
                    <span class="people-pics third">
                      <img src="/profiles/imgs/people.jpg" class="img-responsive">
                    </span>
                    <span class="opinions-number text-bold">200</span>
                    <span class="clear"></span>
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        @endforeach

      </div>

    </div>
  </div>

</section>


<?php $userId = $store->user->id; ?>
<input type="hidden" name="userid" value="{{$userId}}" id="userIDS">
@if(Auth::user())
<input type="hidden" name="userid" value="{{Auth::user()->id}}" id="userLikeIDS">
@else
<input type="hidden" name="userid" value="0" id="userLikeIDS">
@endif
<input type="hidden" name="ids" value="{{$id}}" id="ids"> @endsection
<script src="{{asset('js/js.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPZjhRY2jUckGQNh0f_tc7jo2dw18GRz4&libraries=places"></script>
<script>
  function initMap() {
    store_id = "{{$id}}"; //document.getElementById('ids').value;
    $.ajax({
      type: 'GET',
      url: '/api/get/location',
      data: {
        id: store_id
      },
      success: function (data) {
        console.log(data);
        if (data[0].lat == null) {
          var map = new google.maps.Map(document.getElementById('map'), {

            center: {
              lat: 30.0594838,
              lng: 31.2934839
            },
            zoom: 13,
            styles: [{
                "elementType": "geometry",
                "stylers": [{
                  "color": "#f5f5f5"
                }]
              },
              {
                "elementType": "labels.icon",
                "stylers": [{
                  "visibility": "off"
                }]
              },
              {
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#616161"
                }]
              },
              {
                "elementType": "labels.text.stroke",
                "stylers": [{
                  "color": "#f5f5f5"
                }]
              },
              {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#bdbdbd"
                }]
              },
              {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#eeeeee"
                }]
              },
              {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#757575"
                }]
              },
              {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#e5e5e5"
                }]
              },
              {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#9e9e9e"
                }]
              },
              {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#ffffff"
                }]
              },
              {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#757575"
                }]
              },
              {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#dadada"
                }]
              },
              {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#616161"
                }]
              },
              {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#9e9e9e"
                }]
              },
              {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#e5e5e5"
                }]
              },
              {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#eeeeee"
                }]
              },
              {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#c9c9c9"
                }]
              },
              {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#9e9e9e"
                }]
              }
            ]
          });
        } else {
          var map = new google.maps.Map(document.getElementById('map'), {

            center: {
              lat: data[0].lat,
              lng: data[0].lng
            },
            zoom: 13,
            styles: [{
                "elementType": "geometry",
                "stylers": [{
                  "color": "#f5f5f5"
                }]
              },
              {
                "elementType": "labels.icon",
                "stylers": [{
                  "visibility": "off"
                }]
              },
              {
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#616161"
                }]
              },
              {
                "elementType": "labels.text.stroke",
                "stylers": [{
                  "color": "#f5f5f5"
                }]
              },
              {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#bdbdbd"
                }]
              },
              {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#eeeeee"
                }]
              },
              {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#757575"
                }]
              },
              {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#e5e5e5"
                }]
              },
              {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#9e9e9e"
                }]
              },
              {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#ffffff"
                }]
              },
              {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#757575"
                }]
              },
              {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#dadada"
                }]
              },
              {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#616161"
                }]
              },
              {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#9e9e9e"
                }]
              },
              {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#e5e5e5"
                }]
              },
              {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#eeeeee"
                }]
              },
              {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                  "color": "#c9c9c9"
                }]
              },
              {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [{
                  "color": "#9e9e9e"
                }]
              }
            ]
          });
        }
        if(data[0].lat == null){
            	    var marker = new google.maps.Marker({
	                position: {lat: 30.0594838, lng: 31.2934839},
	          	map: map,
	            });
            }else{
	            var marker = new google.maps.Marker({
	                position: {lat: data[0].lat, lng: data[0].lng},
	          	map: map,
	            });
            }
      },
      error: function () {
        console.log('error');
      }
    });
  }
  // END 0f MAP
  setTimeout(function () {
    initMap();
  }, 5000);

  function replace(Data, desc, price, name, create, image1, image2, image3, image4, image5) {
    $('.attribute-container > div').remove();
    //$('.attribute-type > attribute-props').remove();
   // $('.att-container-child > h3').remove();
    //  proviews = document.getElementById('productViews');
    $.ajax({
      type: 'POST',
      url: '/api/product/attributes',
      data: {
        proid: Data
      },
      success: function (data) {
        console.log(data);
	//for (var i = 0; i < data.attr.length; i++) {
       //   inputs = '<div class="attribute-type"><div class="attribute-title"><h4>' + data.attr[i].name + '</h4></div></div>';
       //   $('.attribute-container').append(inputs);
          for (var m = 0; m < data.prop.length; m++) {
            //	if(data.attr[i].id == data.prop[m].attribute_id){
	              inputs = '<div class="attribute-title"><div class="attribute-props"><Ul><li>'+ data.prop[m].name + '</li></Ul></div></div>';
	              $('.attribute-container').append(inputs);
	              for (var k = 0; k < data.sub_prop.length; k++) {
	                if (data.sub_prop[k].att_properties_id == data.prop[m].id) {
	                  	inputs = '<div class="attribute-type"><div class="attribute-sub-props"><Ul><li>' + data.sub_prop[k].name + '</li></Ul></div></div>';
	              		$('.attribute-container').append(inputs);
	                }
                      }
	    //    }
	        
	 }
		
     //   }

        
   /*     for (var i = 0; i < data.attr.length; i++) {
          inputs = '<h4>' + data.attr[i].name + '</h4>';
          $('.attribute-title').append(inputs);
          for (var m = 0; m < data.prop.length; m++) {
            if (data.attr[i].id == data.prop[m].attribute_id) {
              inputs = '<h4 class="text-bold primary">' + data.prop[i].name + '</h4>';
              $('.att-container').append(inputs);
              for (var k = 0; k < data.sub_prop.length; k++) {
                if (data.sub_prop[k].att_properties_id == data.prop[m].id) {
                  inputs = '<h3>' + data.sub_prop[k].name + '</h3>';
                  $('.att-container-child').append(inputs);
                }
              }
            }

          }

        }*/
        

        /*  for (var i = 0; i < data.prop.length; i++) {
              inputs = '<span>' + data.prop[i].name + '</span>';
              $('.att-container').append(inputs);
          }
          for (var i = 0; i < data.sub_prop.length; i++) {
              inputs = '<span>' + data.sub_prop[i].name + '</span>';
              $('.att-container').append(inputs);
          }*/


        /*  if (data.views.length > 0) {
              proviews.value = data.views[0].views;
          } else {
              proviews.value = 1;
          }*/

      },
      error: function () {
        console.log('error');
      }



    });
  //  ids = document.getElementById('productId');
  //  ids.value = Data;
    description = document.getElementById('productDes');
    newdiscription = desc.replace(/\+/g, ' ');

    description.value = newdiscription;
    prices = document.getElementById('productPr');
    prices.value = price;
    names = document.getElementById('productNm');
    newname = name.replace(/\+/g, ' ');
    names.value = newname;
    URL = document.getElementById('proDetails');

    URL.href = '/{{$id}}/product/' + Data + '/{{$savedcityId}}/{{$savedareaId}}/{{$searchval}}';

    imags1 = document.getElementById('img1');
    imags1.src = '/image/' + image1;
   /* imags2 = document.getElementById('img2');
    imags2.src = '/image/' + image2;
    imags3 = document.getElementById('img3');
    imags3.src = '/image/' + image3;
    imags4 = document.getElementById('img4');
    imags4.src = '/image/' + image4;
    imags5 = document.getElementById('img5');
    imags5.src = '/image/' + image5;*/




  }

  function newuserphone() {
    user_id = document.getElementById('userIDS').value;
    store_id = document.getElementById('ids').value;
    $.ajax({
      type: 'post',
      url: '/api/noti/phone',
      data: {
        userId: user_id,
        noti: 3,
        storeId: store_id
      },
      success: function (data) {
        console.log(data);
      },
      error: function () {
        console.log('error');
      }
    });
  }

  function likecomment(Data) {
    user_id = document.getElementById('userLikeIDS').value;
    $.ajax({
      type: 'POST',
      url: '/like/comment',
      data: {
        commentId: Data,
        userId: user_id
      },
      success: function (data) {
        console.log(data);
        console.log("data");
        if (data == 'guest') {
          swal();
            swal({
              title: "يرجي تسجيل الدخول",
              text: " "
            });
        }

      },
      error: function () {
        console.log('error');
        
      }
    });
  }

  function specificProducttwo(Data) {
    subData = document.getElementsByName('gender');
    user_id = document.getElementById('userIDS').value;
    store_id = document.getElementById('ids').value;
    for (var i = 0; i < subData.length; i++) {
      if (subData[i].checked) {
        $("#subCatId").val(subData[i].value);
        // console.log(user_id);
      }
    }
    $('#catproducts > div').remove();
    $.ajax({
      type: 'post',
      url: '/api/store/specific',
      data: {
        save5: 'save5',
        subId: Data,
        userids: user_id,
        ids: store_id
      },
      success: function (data) {
        console.log(data);
        for (var i = 0; i < data.pro.length; i++) {
          newdiscription = data.pro[i].discription.replace(/ /g, '+');
          newname = data.pro[i].name.replace(/ /g, '+');
          //	alert(newname);
          if (data.pro[i].discount == '0') {


            if (data.photo.length == 1) {
              allspecificprodtwo = '<div class="anProduct"><div class="productPhoto"><img src="/image/' + data.pro[i].photo +'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name + '</span><div class="img-container"><img src="/image/' + data.cinfo[0].icon +'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">' +data.pro[i].discription + '</p><p class="text-bold priceOfProduct">' + data.pro[i].price +'جنيه مصرى</p><div><button class="trigger" onclick=replace("' + data.pro[i].id + '","' +newdiscription + '","' + data.pro[i].price + '","' + newname + '",null,"' + data.pro[i].photo +'","' + data.photo[0].photo +'",null,null,null)><i class="fa fa-eye" aria-hidden="true"></i></button></div></div></div>';
              $('#catproducts').append(allspecificprodtwo);
            } else if (data.photo.length == 2) {
              allspecificprodtwo = '<div class="anProduct"><div class="productPhoto"><img src="/image/' + data.pro[i].photo +'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>' +data.pro[i].name + '</span><div class="img-container"><img src="/image/' + data.cinfo[0].icon +'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">' +data.pro[i].discription + '</p><p class="text-bold priceOfProduct">' + data.pro[i].price +'جنيه مصرى</p><div><button class="trigger" onclick=replace("' + data.pro[i].id + '","' +newdiscription + '","' + data.pro[i].price + '","' + newname + '",null,"' + data.pro[i].photo +'","' + data.photo[0].photo + '","' + data.photo[1].photo +'",null,null)><i class="fa fa-eye" aria-hidden="true"></i></button></div></div></div>';
              $('#catproducts').append(allspecificprodtwo);
            } else if (data.photo.length == 3) {
              allspecificprodtwo = '<div class="anProduct"><div class="productPhoto"><img src="/image/' + data.pro[i].photo +'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>' +data.pro[i].name + '</span><div class="img-container"><img src="/image/' + data.cinfo[0].icon +'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">' +data.pro[i].discription + '</p><p class="text-bold priceOfProduct">' + data.pro[i].price +' جنيه مصرى</p><div><button class="trigger" onclick=replace("' + data.pro[i].id + '","' +newdiscription + '","' + data.pro[i].price + '","' + newname + '",null,"' + data.pro[i].photo +'","' + data.photo[0].photo + '","' + data.photo[1].photo + '","' + data.photo[2].photo +'",null)><i class="fa fa-eye" aria-hidden="true"></i></button></div></div></div>';
              $('#catproducts').append(allspecificprodtwo);
            } else if (data.photo.length == 4) {
              allspecificprodtwo = '<div class="anProduct"><div class="productPhoto"><img src="/image/' + data.pro[i].photo +'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>' +data.pro[i].name + '</span><div class="img-container"><img src="/image/' + data.cinfo[0].icon +'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">' +data.pro[i].discription + '</p><p class="text-bold priceOfProduct">' + data.pro[i].price +' جنيه مصرى</p><div><button class="trigger" onclick=replace("' + data.pro[i].id + '","' +newdiscription + '","' + data.pro[i].price + '","' + newname + '",null,"' + data.pro[i].photo +'","' + data.photo[0].photo + '","' + data.photo[0].photo + '","' + data.photo[1].photo + '","' +data.photo[2].photo + '","' + data.photo[3].photo +'")><i class="fa fa-eye" aria-hidden="true"></i></button></div></div></div>';
              $('#catproducts').append(allspecificprodtwo);
            } else {
              allspecificprodtwo =
                '<div class="anProduct"><div class="productPhoto"><img src="/image/' + data.pro[i].photo +'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name+'</span><div class="img-container"><img src="/image/' + data.cinfo[0].icon +'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[i].discription + '</p><p class="text-bold priceOfProduct">' +data.pro[i].price +'جنيه مصرى</p><div><button class="trigger" onclick=replace("' + data.pro[i].id +'","'+newdiscription +'","'+ data.pro[i].price +'","'+ newname+'",null,"'+ data.pro[i].photo+'",null,null,null,null)><i class="fa fa-eye" aria-hidden="true"></i></button></div></div></div>';
              $('#catproducts').append(allspecificprodtwo);
            }


          } else {
            if (data.photo.length == 1) {
              allspecificprodtwo = '<div class="anProduct"><div class="productPhoto"><img src="/image/' + data.pro[i].photo +'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>' +data.pro[i].name + '</span><div class="img-container"><img src="/image/' + data.cinfo[0].icon +'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">' +data.pro[i].discription + '</p><p class="text-bold priceOfProduct">' + data.pro[i].discount +'جنيه مصرى</p><p class="text-bold deleted">' + data.pro[i].price +' جنيه مصرى</p><div class=""><button class="trigger" onclick=replace("' + data.pro[i].id + '","' +newdiscription + '","' + data.pro[i].price + '","' + newname + '",null,"' + data.pro[i].photo +'","' + data.photo[0].photo +'",null,null,null)><i class="fa fa-eye" aria-hidden="true"></i></button></div></div></div>';
              $('#catproducts').append(allspecificprodtwo);
            } else if (data.photo.length == 2) {
              allspecificprodtwo = '<div class="anProduct"><div class="productPhoto"><img src="/image/' + data.pro[i].photo +'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>' +data.pro[i].name + '</span><div class="img-container"><img src="/image/' + data.cinfo[0].icon +'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">' +data.pro[i].discription + '</p><p class="text-bold priceOfProduct">' + data.pro[i].discount +' جنيه مصرى</p><p class="text-bold deleted">' + data.pro[i].price +' جنيه مصرى</p><div class=""><button class="trigger" onclick=replace("' + data.pro[i].id + '","' +newdiscription + '","' + data.pro[i].price + '","' + newname + '",null,"' + data.pro[i].photo +'","' + data.photo[0].photo + '","' + data.photo[1].photo +'",null,null)><i class="fa fa-eye" aria-hidden="true"></i></button></div></div></div>';
              $('#catproducts').append(allspecificprodtwo);
            } else if (data.photo.length == 3) {
              allspecificprodtwo = '<div class="anProduc"><div class="productPhoto"><img src="/image/' + data.pro[i].photo +'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>' +data.pro[i].name + '</span><div class="img-container"><img src="/image/' + data.cinfo[0].icon +'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">' +data.pro[i].discription + '</p><p class="text-bold priceOfProduct">' + data.pro[i].discount +'جنيه مصرى</p><p class="text-bold deleted">' + data.pro[i].price +'جنيه مصرى</p><div class=""><button class="trigger" onclick=replace("' + data.pro[i].id + '","' +newdiscription + '","' + data.pro[i].price + '","' + newname + '",null,"' + data.pro[i].photo +'","' + data.photo[0].photo + '","' + data.photo[1].photo + '","' + data.photo[2].photo +'",null)><i class="fa fa-eye" aria-hidden="true"></i></button></div></div></div>';
              $('#catproducts').append(allspecificprodtwo);
            } else if (data.photo.length == 4) {
              allspecificprodtwo = '<div class="prouduct-container"><div class="productPhoto"><img src="/image/' +data.pro[i].photo +'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>' +data.pro[i].name + '</span><div class="img-container"><img src="/image/' + data.cinfo[0].icon +'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">' +data.pro[i].discription + '</p><p class="text-bold priceOfProduct">' + data.pro[i].discount +' جنيه مصرى</p><p class="text-bold deleted">' + data.pro[i].price +' جنيه مصرى</p><div class=""><button class="trigger" onclick=replace("' + data.pro[i].id + '","' +newdiscription + '","' + data.pro[i].price + '","' + newname + '",null,"' + data.pro[i].photo +'","' + data.photo[0].photo + '","' + data.photo[0].photo + '","' + data.photo[1].photo + '","' +data.photo[2].photo + '","' + data.photo[3].photo +'")><i class="fa fa-eye" aria-hidden="true"></i></button></div></div></div>';
              $('#catproducts').append(allspecificprodtwo);
            } else {
              allspecificprodtwo = '<div class="anProduct"><div class="productPhoto"><img src="/image/' + data.pro[i].photo +'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>' +data.pro[i].name + '</span><div class="img-container"><img src="/image/' + data.cinfo[0].icon +'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">' +data.pro[i].discription + '</p><p class="text-bold priceOfProduct">' + data.pro[i].discount +' جنيه مصرى</p><p class="text-bold deleted">' + data.pro[i].price +' جنيه مصرى</p><div class=""><button class="trigger" onclick=replace("' + data.pro[i].id + '","' +newdiscription + '","' + data.pro[i].price + '","' + newname + '",null,"' + data.pro[i].photo +'",null,null,null,null)><i class="fa fa-eye" aria-hidden="true"></i></button></div></div></div>';
              $('#catproducts').append(allspecificprodtwo);
            }
          }

          allspecificprodtwo = "";

        }

      },
      error: function () {
        console.log('error');
      }
    });
  }

  function addcomment() {
    $('#newcommentForum').submit(function (e) {
      e.preventDefault();
      var form = $('#newcommentForum');
      $.ajax({
        type: 'POST',
        url: '/store/profile/{{$id}}/{{$city}}/{{$area}}/{{$searchval}}',
        data: form.serialize(),
        dataType: 'json',

        success: function (data) {
          console.log(data);
          if (data == 'guest') {
            swal();
            swal({
              title: "يرجي تسجيل الدخول",
              text: " "
            });
          } else {
           /* var newReview = $(".sale .newreviewExample").last().clone(true);
            var profilePic = $(".sale .textBar .search img").prop("src");
            $(newReview).find(".img-container img").prop("src", profilePic);
            $(newReview).find("h5").text($(".nameEmail .client-title h4").text());
            $(newReview).find(".counterLikes").text("0");
            $(newReview).find(".textReview").text($(".sale .search input").val());
            $(newReview).insertBefore(".sale .textBar");
            $(".sale .search input").val("");*/
            swal();
            swal({
              title: "تم ارسال تقييمك",
              text: " "
            });
          }
        },
        error: function () {
          console.log('error');
        }
      });
    });

  }

  function addcomment1() {
    $('#newcommentForum1').submit(function (e) {
      e.preventDefault();
      var form = $('#newcommentForum1');
      $.ajax({
        type: 'POST',
        url: '/store/profile/{{$id}}/{{$city}}/{{$area}}/{{$searchval}}',
        data: form.serialize(),
        dataType: 'json',

        success: function (data) {
          console.log(data);
          if (data == 'guest') {
            swal();
            swal({
              title: "يرجي تسجيل الدخول",
              text: " "
            });
          } else {
          /*  var newReview = $(".newOffer .newreviewExample").last().clone(true);
            var profilePic = $(".newOffer .textBar .search img").prop("src");
            $(newReview).find(".img-container img").prop("src", profilePic);
            $(newReview).find("h5").text($(".nameEmail .client-title h4").text());
            $(newReview).find(".counterLikes").text("0");
            $(newReview).find(".textReview").text($(".newOffer .search input").val());
            $(newReview).insertBefore(".newOffer .textBar");
            $(".newOffer .search input").val("");*/
            swal();
            swal({
              title: "تم ارسال تقييمك",
              text: " "
            });
          }
        },
        error: function () {
          console.log('error');
        }
      });
    });

  }

  function addcomment2() {
    $('#newcommentForum2').submit(function (e) {
      e.preventDefault();
      var form = $('#newcommentForum2');
      $.ajax({
        type: 'POST',
        url: '/store/profile/{{$id}}/{{$city}}/{{$area}}/{{$searchval}}',
        data: form.serialize(),
        dataType: 'json',

        success: function (data) {
          console.log(data);
          if (data == 'guest') {
            swal();
            swal({
              title: "يرجي تسجيل الدخول",
              text: " "
            });
          } else {
            var newReview = $(".newOffer .newreviewExample").last().clone(true);
            var profilePic = $(".newOffer .textBar .search img").prop("src");
            $(newReview).find(".img-container img").prop("src", profilePic);
            $(newReview).find("h5").text($(".nameEmail .client-title h4").text());
            $(newReview).find(".counterLikes").text("0");
            $(newReview).find(".textReview").text($(".newOffer .search input").val());
            $(newReview).insertBefore(".newOffer .textBar");
            $(".newOffer .search input").val("");
            $('.messageContainer').hide();
            swal({
            title: "شكرًا لتفاعلك معنا!"
            });
          }
        },
        error: function () {
          console.log('error');
        }
      });
    });

  }

  function newmsg() {
    $('#newmsgForum').submit(function (e) {
      e.preventDefault();
      var form = $('#newmsgForum');
      $.ajax({
        type: 'POST',
        url: '/store/profile/{{$id}}/{{$city}}/{{$area}}/{{$searchval}}',
        data: form.serialize(),
        dataType: 'json',

        success: function (data) {
          console.log(data);
          if (data == 'block') {
            swal();
            swal({
              title: "قام هذا التاجر بحظرك",
              text: " "
            });

          } else if (data == 'guest') {
            swal();
            swal({
              title: "يرجي تسجيل الدخول",
              text: " "
            });
          } else {
             $('.messageContainer').hide();
            swal({
            title: "سيتم الرد عليكم في أقرب وقت"
            });
            $('#message').val('');
          }
          $('#message').val('');
        },
        error: function () {
          console.log('error');
        }
      });
    });

  }

  function newmsg2() {
    $('#newmsgForum2').submit(function (e) {
      e.preventDefault();
      var form = $('#newmsgForum2');
      $.ajax({
        type: 'POST',
        url: '/store/profile/{{$id}}/{{$city}}/{{$area}}/{{$searchval}}',
        data: form.serialize(),
        dataType: 'json',

        success: function (data) {
          console.log(data);
          if (data == 'block') {
            swal();
            swal({
              title: "قام هذا التاجر بحظرك",
              text: " "
            });

          } else if (data == 'guest') {
            swal();
            swal({
              title: "يرجي تسجيل الدخول",
              text: " "
            });
          } else {
            $('.messageContainer').hide();
            swal({
            title: "سيتم الرد عليكم في أقرب وقت"
            });
            $('#message').val('');
          }
           $('#message').val('');
          
        },
        error: function () {
          console.log('error');
        }
      });
    });

  }
  
  function followstore(Data) {
    $.ajax({
      type: 'POST',
      url: '/user/profile',
      data: {
        storeId: Data,
        save4: 'save4'
      },

      success: function (data) {
        console.log(data);
        if (data == 'guest') {
          $('.favorMe').removeClass('redborder');
          $('.fa-heart-o').removeClass('redheart');

          swal();
          swal({
            title: "يرجي تسجيل الدخول",
            text: " "
          });

          
        }
      },
      error: function () {
        console.log('error');
      }
    });
  }
</script>
