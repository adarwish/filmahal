@extends('site.profiles.master')

@section('content')

<section class="navigate">
    <div class="container">
        <div class="row">
            <div class="navigation text-bold">
                <p><a href="/">المحل</a> &gt; &gt; المحل</p>
            </div>
        </div>
    </div>
</section>

<div class="product-info-page addProduct mainSection col-xs-12" id="uploadProduct">
    <div class="row">
        <div class="reverseCols">
        
        

        
        
        
            <div class="col-xs-12 col-md-4">
                <div class="publisher">
                    <h4 class="text-bold primary">المحل الموزع للمنتج</h4>
                    <p class="who">
                        <i class="fa fa-shopping-bag primary fa-lg" aria-hidden="true"></i>
                        <span class="secondary text-bold">{{$store->title}}</span>
                    </p>

                    <p class="who">
                        <i class="fa fa-motorcycle primary fa-lg" aria-hidden="true"></i>
                        @if(count($cinfo)>0) @if($cinfo[0]->delivery == 1)
                        <span class="secondary text-bold">خدمة التوصيل متاحة</span>
                        @else
                        <span class="secondary text-bold">خدمة التوصيل غير متاحة</span>
                        @endif @endif
                    </p>

                    <p class="who">
                        <i class="fa fa-map-marker primary fa-lg" aria-hidden="true"></i>
                        <span class="secondary text-bold">{{$store->address}}</span>
                    </p>

                </div>
                <div id="map">
                </div>
		<?php 
		$contactinfo = DB::table('contact_info')->where('market_id',$store->id)->get();
                
                $phones = explode(',',$contactinfo[0]->phone); ?>
                <div class="lead getMe text-center text-bold getPhone">
                    <span>رقم التليفون</span>
                    <span>{{$phones[0]}}</span>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 productForm">

                <div class="name">
                    <h4 class="text-bold primary">اسم المنتج</h4>
                    <p class="secondary text-bold">
                        <input type="text" id="productNm" value="{{$product->name}}" disabled>
                    </p>
                </div>

                <div class="price">
                    <h4 class="text-bold primary">سعر المنتج</h4>
                    <p class="secondary text-bold">
                        <input type="text" id="productPr" value="{{$product->price}}" disabled>
                        <span> جنيه مصرى </span>
                    </p>
                </div>
               

            <div class="attribute-container">


                <?php 
                                        $attribute = DB::Table('attribute')->whereIn('id',$attributeID)->get();
                                         ?> 
                                        @foreach($attribute as $attributes)
                                        <div>
                                            <h4>{{$attributes->name}}</h4>
					<?php 
					$allIds = DB::table('market_attribute')->where('attribute_id',$attributes->id)->get();
					
                                         ?>
                                        
                                        @endforeach
                                          @foreach($properties as $property)
                                          <?php 
                                          $apropId =DB::table('market_attribute')->where('product_id',$product->id)->where('properties_id',$property->id)->get();
        				  ?>
                                            <h5 class="sub-att">{{$property->name}}</h5>
                                            @foreach($apropId as $apropIds)
                                            <?php
                                            $aprops = DB::table('sub_properties')->where('id',$apropIds->sub_properties_id)->get(); ?>
                                            <div class="no-margin">
                                                <label class="sub-sub-att">{{$aprops[0]->name}}</label>
                                            </div>
                                           
                                            @endforeach
                                          @endforeach
            
            </div>



                <div class="description">
                    <h4 class="text-bold primary">وصف المُنتج</h4>
                    <p class="secondary text-bold">

                        
                        <textarea cols="30" id="productDes" disabled>
{{$product->discription}}
</textarea>
                    </p>
                </div>

                <div class="seens">
                    <h4 class="text-bold primary">عدد المشاهدات المنتج</h4>
                    <p class="secondary text-bold">
                        <input type="text" id="productViews" value="{{$oldcounter[0]->views}}" disabled> شاهدوا هذا المُنتج</p>
                </div>

                <div class="publishDate">
                    <h4 class="text-bold primary">تاريخ نشر المنتج</h4>
                    <p class="secondary text-bold">
                        <input type="text" id="productCreate" value="{{$product->created_at}}" disabled>
                    </p>
                </div>

            </div>
            
            
            

            


        </div>
        
                                 <div class="col-xs-12 col-md-4 productPhotosUploader">
                <div class="productPhotos photo">
                    <div class="img">
                        <img id="img1" src="/image/{{$product->photo}}" class="img-responsive">
                    </div>
                </div>
                <p class="text-center text-bold productCode generate">
                    <span class="primary">رقم المنتج</span>
                    <span class="productNum">
                        <input type="text" id="productId" value="{{$product->id}}" disabled>
                    </span>
                </p>

                <div class="centerMe">
                @foreach($photo as $photos)
                    <div class="anotherSides photo">
                        <div class="img" style="cursor: pointer;">
                            <img src="/images/{{$photos->photo}}"  class="img-responsive">
                        </div>
                    </div>
		        @endforeach
                    
                </div>
            </div>
        
    </div>
</div>
@endsection

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPZjhRY2jUckGQNh0f_tc7jo2dw18GRz4&libraries=places"></script>
<script>

function initMap() {
        store_id = "{{$storeID}}";//document.getElementById('ids').value;
        $.ajax({
            type: 'GET',
            url: '/api/get/location',
            data: { id: store_id },
            success: function (data) {
                console.log(data);
                if (data[0].lat == null) {
                    var map = new google.maps.Map(document.getElementById('map'), {

                        center: { lat: 30.0594838, lng: 31.2934839 },
                        zoom: 13,
                        styles:
                            [
                                {
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#f5f5f5"
                                        }
                                    ]
                                },
                                {
                                    "elementType": "labels.icon",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#616161"
                                        }
                                    ]
                                },
                                {
                                    "elementType": "labels.text.stroke",
                                    "stylers": [
                                        {
                                            "color": "#f5f5f5"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative.land_parcel",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#bdbdbd"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#eeeeee"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#757575"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.park",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#e5e5e5"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.park",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#9e9e9e"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#757575"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#dadada"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#616161"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.local",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#9e9e9e"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "transit.line",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#e5e5e5"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "transit.station",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#eeeeee"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#c9c9c9"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#9e9e9e"
                                        }
                                    ]
                                }
                            ]
                    });
                } else {
                    var map = new google.maps.Map(document.getElementById('map'), {

                        center: { lat: data[0].lat, lng: data[0].lng},
                        zoom: 13,
                        styles: [
                            {
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#f5f5f5"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#616161"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "color": "#f5f5f5"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.land_parcel",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#bdbdbd"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#eeeeee"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#e5e5e5"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#ffffff"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#dadada"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#616161"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.line",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#e5e5e5"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.station",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#eeeeee"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#c9c9c9"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            }
                        ]
                    });
                }
            },
            error: function () {
                console.log('error');
            }
        });
    }
initMap();

</script>

@endsection