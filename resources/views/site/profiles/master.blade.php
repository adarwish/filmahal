<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title></title>
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" sizes="32x32" href="{{asset('imgs/favIcon/favicon-32x32.png')}}">
    <link rel="shortcut icon" type="image/png" sizes="16x16" href="{{asset('imgs/favIcon/favicon-16x16.png')}}">

    <link rel="stylesheet" href="{{asset('profiles/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('profiles/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('profiles/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('profiles/css/emojionearea.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('css/pace-theme-flash.css')}}">

    <link rel="stylesheet" href="{{asset('profiles/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('profiles/owlcarousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('profiles/css/sweetalert2.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('profiles/css/main1.css')}}">
    <link rel="stylesheet" href="{{asset('profiles/css/main0.css')}}">
    
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/css/iziModal.css" />

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php $dynamic_data = DB::table('dynamic_home')->get(); ?>
<!--Start Of Header Section-->
    <header class="prod-header">
        <nav>
            <div class="container" style="width: 98%;">
                <div class="row"> 
                    <ul class="left-side col-xs-12 col-lg-6">   
                        @if(Auth::guest())
                        <li><a href="/user/register" class="sign-up">انشاء حساب</a></li>
                        <li><a href="/login" class="sign-in">تسجيل الدخول</a></li>
                        @else
                              <li class="notification-icon">
                              <div class="fa fa-bell-o fa-2x" aria-hidden="true">
                                      <img src="/image/SVG/bell.svg" class="img-responsive">
                                      <i class="fa fa-caret-up" aria-hidden="true" style="display: none;"></i>
                                  </div>
                                  <div class="slide-down">
                                  @if(Auth::user())
                                    @if(Auth::user()->vendor == '1')
                                    <?php $notifications = DB::table('noti')->where('user_id',Auth::user()->id)->get();
                                      $notifications2 = DB::table('noti')->where('user_id',Auth::user()->id)->limit(3)->orderBy('id','DESC')->get();
                                      $counter = count($notifications);
                                      
                                      $old_noti = DB::table('noti_counter')->where('user_id',Auth::user()->id)->get();
                                      $counter_noti = count($old_noti);
                                      if($counter_noti >0){
                                          DB::table('noti_counter')->where('user_id',Auth::user()->id)->update(['counter'=>$counter]);
                                      }else{
                                          DB::table('noti_counter')->insert(['counter'=>$counter,'user_id'=>Auth::user()->id]);
                                      }
      
                                       ?>
      
                                      <ul class="messages-list noti nav-drop-alter">
                                          @if($counter !==0)
                                          @foreach($notifications2 as $noti)
                                          <?php $marketnoti = DB::table('users')->where('id',$noti->action_id)->get();
                                           ?>
                                           @if(count($marketnoti) > 0)
                                          <li class="message">
                                              <a href="#">
                                                  <div class="client">
                                                      <div class="message-header">
                                                          <div class="img-container">
                                                              <img src="/image/{{$marketnoti[0]->photo}}" class="img-responsive">
                                                          </div>              
                                                          <div class="client-title">
                                                              <h5>{{$marketnoti[0]->user_name}}</h5>
                                                              <p>{{$noti->data}}</p>
                                                              <p class="time">
                                                                  <i class="fa fa-phone"></i>
                                                                  {{$noti->created_at}}
                                                              </p>
                                                          </div>
                                                  
                                                      </div>
                                                      <div>
                                                          
                                                      </div>
                                                  
                                                  </div>
                                              </a>
                                          </li>
                                          @endif
                                          @endforeach
                                          @endif
      
                                                                  
                                          <li class="more-messages">
                                              <p><a href="/all-notification">شاهد جميع الاشعارات</a></p>
                                              <p><a href="/all-notification"><font id="countnoti">{{$counter}} اشعار اخر</font></a></p>
                                              <div class="clear"></div>
                                          </li>
                                      </ul>
                                    @else
                                      <?php 
                                      $notifications = DB::table('noti')->where('user_id',Auth::user()->id)->get();
                                      $notifications2 = DB::table('noti')->where('user_id',Auth::user()->id)->limit(3)->orderBy('id','DESC')->get();
                                      $counter = count($notifications);
                                      
                                      $old_noti = DB::table('noti_counter')->where('user_id',Auth::user()->id)->get();
                                      $counter_noti = count($old_noti);
                                      if($counter_noti >0){
                                          DB::table('noti_counter')->where('user_id',Auth::user()->id)->update(['counter'=>$counter]);
                                      }else{
                                          DB::table('noti_counter')->insert(['counter'=>$counter,'user_id'=>Auth::user()->id]);
                                      }
      
                                       ?>
      
                                      <ul class="messages-list noti nav-drop-alter">
                                          @if($counter > 0)
                                          @foreach($notifications2 as $noti)
                                          <?php $marketnoti = DB::table('market')->where('user_id',$noti->action_id)->get();
                                          if(count($marketnoti) > 0){
                                            $contact_info = DB::table('contact_info')->where('market_id',$marketnoti[0]->id)->get();
                                          }
                                          ?>
                                          <li class="message">
                                              <a href="#">
                                                  <div class="client">
                                                      <div class="message-header">
                                                          <div class="img-container">
                                                              <img src="/image/{{$contact_info[0]->icon}}" class="img-responsive">
                                                          </div>              
                                                          <div class="client-title">
                                                              @if(count($marketnoti) > 0)
                                                              <h5>{{$marketnoti[0]->title}}</h5>
                                                              @endif
                                                              <p>{{$noti->data}}</p>
                                                              <p class="time">
                                                                  <i class="fa fa-phone"></i>
                                                                  {{$noti->created_at}}
                                                              </p>
                                                          </div>
                                                  
                                                      </div>
                                                      <div>
                                                          
                                                      </div>
                                                  
                                                  </div>
                                              </a>
                                          </li>
                                          @endforeach
                                          @endif                          
                                          <li class="more-messages">
                                              <p><a href="/all-notification">شاهد جميع الاشعارات</a></p>
                                              <p><a href="/all-notification"><font id="countnoti">{{$counter}} اشعار اخر</font></a></p>
                                              <div class="clear"></div>
                                          </li>
                                          
                                      </ul>
                                      @endif
                                      @endif
                                       
                                 </div>
                              </li>
                              
                              <li class="messages">
                                  <div class="fa fa-envelope-o fa-2x" aria-hidden="true">
                                      <img src="/image/SVG/envelope.svg" class="img-responsive">
                                      <i class="fa fa-caret-up" aria-hidden="true" style="display: none;"></i>
                                  </div>
                                  
                                  <div class="slide-down" style="display: none;">
                                      
                                                                      
                                       @if(Auth::user())
                                      <?php $room = DB::table('room')->where('sender_id',Auth::user()->id)->Orwhere('reciver_id',Auth::user()->id)->limit(3)->get();
                                    
                                      $counter = count($room);
                                      
                                     
                                       ?>
      
                                      <ul class="messages-list nav-drop-alter">
                                          @if($counter !==0)
                                          @foreach($room as $rooms)
                                          <?php 
                                          $message = DB::table('message')->where('room_id',$rooms->id)->orderBy('id','DESC')->get();
                                          $sender = DB::table('users')->where('id',$message[0]->sender_id)->get();
                                          $reciver = DB::table('users')->where('id',$message[0]->reciver_id)->get();
                                          ?>
                                          <li class="message">
                                              <a href="#">
                                                  <div class="client">
                                                      <div class="message-header">
                                                          <div class="img-container">
                                                              <img src="/image/{{$sender[0]->photo}}" class="img-responsive">
                                                          </div>              
                                                          <div class="client-title">
                                                              <h5>{{$sender[0]->user_name}}</h5>
                                                              <p>{{$message[0]->message}}</p>
                                                              <p class="time">
                                                                  <i class="fa fa-phone"></i>
                                                                  <?php
      
                                                          $timestamp = $rooms->created_at;
                                                          $datetime = explode(" ",$timestamp);
                                                          $date = $datetime[0];
                                                          $time = $datetime[1];
                                                          echo $time;?>
                                                                 
                                                              </p>
                                                          </div>
                                                  
                                                      </div>
                                                      <div>
                                                          
                                                      </div>
                                                  
                                                  </div>
                                              </a>
                                          </li>
                                          @endforeach
                                          @endif
                                          <li class="more-messages">
                                              <p><a href="/user/profile#boxMessages">شاهد جميع الرسائل</a></p>
                                              <p><a href="/user/profile#boxMessages"><font id="countnoti">{{$counter}} رسالة إخرى</font></a></p>
                                              <div class="clear"></div>
                                          </li>
                                          
                                      </ul>
                                      @endif
                                                                  </div>
                              </li>
      
                              <li class="profile">
                                  <i class="fa fa-angle-down fa-lg" aria-hidden="true">
                                      <i class="fa fa-caret-up" aria-hidden="true" style="display: none;"></i>
                                  </i>
                                  <img src="/image/SVG/profile.svg" class="img-responsive">
                                  
                                  <div class="slide-down">
                                      @if(Auth::user()->vendor == 1)
                                      <ul class="profile-settings">
                                          <li>
                                        <a href="/vendor/control-panel#reviewsPeople" class="reviewsPeople" id="reviewsPeople">
                                            <i class="ion ion-edit" aria-hidden="true"></i>
                                            اراء الناس فى محلك
                                        </a>
                                    </li>
      
                                          <li>
                                              <a href="/vendor/control-panel#boxMessages" class="boxMessages" id="boxMessages">
                                                  <i class="fa fa-envelope" aria-hidden="true"></i>
                                                  صندوق الرسائل
                                              </a>
                                          </li>
      
                                          <li>
                                              <a href="/vendor/control-panel#settingProfile" class="settingProfile" id="settingProfile">
                                                  <i class="fa fa-cog" aria-hidden="true"></i>
                                                  اعدادات حسابى
                                              </a>
                                          </li>
      
                                          <li>
                                              <a href="/logout">
                                                  <i class="fa fa-user" aria-hidden="true"></i>
                                                  تسجيل الخروج
                                              </a>
                                          </li>
                                      </ul>
                                      @elseif(Auth::user()->admin == 1)
                                      <ul class="profile-settings">
                                          <li>
                                              <a href="/admin.home.mahalatmasr2018@fmax0*" class="reviewsPeople" id="favoriteShops">
                                                  <i class="ion ion-heart" aria-hidden="true"></i>
                                               لوحة التحكم                                     
                                              </a>
                                          </li>
      
                                         
                                          <li>
                                              <a href="/logout">
                                                  <i class="fa fa-user" aria-hidden="true"></i>
                                                  تسجيل الخروج
                                              </a>
                                          </li>
                                      </ul>
                                      @else
                                      <ul class="profile-settings">
                                          <li>
                                              <a href="/user/profile#favoriteShops" class="reviewsPeople" id="favoriteShops">
                                                  <i class="ion ion-heart" aria-hidden="true"></i>
                                                  محلات اعجبت بها                                       
                                              </a>
                                          </li>
      
                                          <li>
                                              <a href="/user/profile#boxMessages" class="boxMessages" id="boxMessages">
                                                  <i class="fa fa-envelope" aria-hidden="true"></i>
                                                  صندوق الرسائل
                                              </a>
                                          </li>
      
                                          <li>
                                              <a href="/user/profile#settingProfile" class="settingProfile" id="settingProfile">
                                                  <i class="fa fa-cog" aria-hidden="true"></i>
                                                  اعدادات حسابى
                                              </a>
                                          </li>
      
                                          <li>
                                              <a href="/logout">
                                                  <i class="fa fa-user" aria-hidden="true"></i>
                                                  تسجيل الخروج
                                              </a>
                                          </li>
                                      </ul>
                                      @endif
                                  </div>
                              </li>
                              @endif
                    </ul>
                    <ul class="right-side col-xs-12 col-md-6">

                        <li class="drop-down">
                            <select id="governorate" onchange="getarea()">
                              <option value="{{$savedCity[0]->id}}">{{$savedCity[0]->name}}</option>
                                @foreach($city as $cities)
                          <option value="{{$cities->id}}">{{$cities->name}}</option>
                          @endforeach
                            </select>
                            
                        </li>
                        <li class="button-shop">

                            @if(Auth::guest())
                      <a href="/vendor/register" class="add-shop">
                          أضف محلك
                          <i class="fa fa-plus"></i>
                      </a>
                      @endif

                        </li>


                    
                        <div class="img-container">
                         <a href="/">    <img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo"> </a>
                        </div>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
        </nav>
        
        
         <div class="container">
            <div class="row text-center">
                <form method="POST" style="margin-bottom: 0;">
                 {{csrf_field()}}
                <div class="form-container">
                        <div class="search">
                        @if($searchval == "search")
                        <input class="search-profile" type="search" name="search"  id="searching" placeholder="أبحث باسم المحل">
                        @else
                            <input class="search-profile" type="search" name="search" id="searching" placeholder="أبحث باسم المحل">
                          @endif
                              <button type="submit" name="searchbutton" value="عرض المحلات">
                                <i class="fa fa-search fa-2x"></i>
                            </button>
                        </div>
                       <select id="area" name="getarea" class="text-bold towns">
                       @if(count($savedArea) > 0)
                            <option value="{{$savedArea[0]->id}}">{{$savedArea[0]->name}}</option>
                       @else
                            <option value="">اختار مدينه</option>
                       @endif
                            @foreach($area as $areas)
                            <option value="{{$areas->id}}">{{$areas->name}}</option>
                            @endforeach
                            </select>
                    </div>
                </form>            
            </div>
        </div>        
    </header>
<!--End Of Header Section-->



<!--Start Fixed Nav Section-->


<div id="mainFixedNav">

  <div class="container">
  
    <div class="row">
    

      
      <div class="col-md-12">
      
      
        <div class="mainFixedNav-container">
        
          <div class="mainFixedNav-logo">
                              <a href="/">    <img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo"> </a>
                            </div>
                            
                            <div>
                            
                              <ul class="mainFixedNav-regster">
                                @if(Auth::guest())
                                <li><a href="/user/register" class="mainFixedNav-sign-up">انشاء حساب</a></li>
                          <li><a href="/login" class="mainFixedNav-sign-in">تسجيل الدخول</a></li>
                          @else

                          <li><a href="/vendor/control-panel#settingProfile" class="mainFixedNav-sign-up">حسابي</a></li>
                          <li><a href="/logout" class="mainFixedNav-sign-in">تسجيل الخروج</a></li>
                          
                          @endif
                              
                              </ul>
                            
                            </div>
                            
                            
                            <div class="mainFixedNav-search">
                            
                                            <form method="POST">
                            {{csrf_field()}}
                          <div class="form-container">
                                  <div class="search">
                               
                                      <input type="search" name="search" placeholder="أبحث باسم المحل">
                                  
                                  
                                    @if(Auth::guest())
                                    
                                    <button type="submit" name="searchbutton" value="عرض المحلات">
                                          <i class="fa fa-search fa-2x"></i>
                                      </button>
                                    
                                    
                                    @else
                                    
                                    
<button class="user-search" type="submit" name="searchbutton" value="عرض المحلات">
                                          <i class="fa fa-search fa-2x"></i>
                                      </button>
                                    
                                    
                                    @endif
                                  

                                  </div>
                                 <select id="area2" name="getarea" class="text-bold towns">

                                         @if(count($savedArea) > 0)
                                        <option value="{{$savedArea[0]->id}}">{{$savedArea[0]->name}}</option>
                                   @else
                                        <option value="">اختار مدينه</option>
                                   @endif
                                   @foreach($area as $areas)
                                        <option value="{{$areas->id}}">{{$areas->name}}</option>
                                   @endforeach


                                 </select>
                              </div>
                          </form>  
                            
                            
                            </div>
                            
                            
                            <div class="mainFixedNav-end">
                            
                              <ul class="">
                              
                                <li class="drop-down">
                                    <select id="governorate2" onchange="getarea2()">
                                      <option value="{{$savedCity[0]->id}}">{{$savedCity[0]->name}}</option>
                                        @foreach($city as $cities)
                                  <option value="{{$cities->id}}">{{$cities->name}}</option>
                                  @endforeach
                                    </select>
                                    
                                </li>
                                <li class="button-shop">
        
                                    @if(Auth::guest())
                              <a href="/vendor/register" class="add-shop">
                                  أضف محلك
                                  <i class="fa fa-plus"></i>
                              </a>
                              @endif
                              
        
                                </li>
                              
                              </ul>
                            
                            </div>
                            
  
                          </div>
                          
                          
                          
                          
                          
                          
      
      </div>
    

    
    
    </div>
  
  </div>

</div>



<!--End Fixed Nav Section-->










    @yield('content')
<!--Start Of Footer Section-->
<?php $contact_us = DB::table('contact_us')->get(); ?>
    <footer>
        <div class="container">
            <div class="row">
                <div class="left-section col-xs-12 col-md-6 col-lg-4">
                    <div class="headerWord">
                        <h3 class="text-bold">الدعم الفنى وخدمه العملاء</h3>
                    </div>
                    <div class="contact-us">
                        <div class="phones">

                            <i class="fa fa-phone"></i>

                            <div class="text-color-grey numbers text-bold">
                                <span>
                                    {{$contact_us[0]->data}}
                                </span>
                                <span>
                                    {{$contact_us[1]->data}}
                                </span>
                            </div>

                        </div>

                        <div class="email">
                            <span class="text-bold">
                                <i class="fa fa-envelope"></i> 
                                <span class="text-color-grey">
                                    {{$contact_us[3]->data}}
                                </span>
                            </span>
                        </div>

                        <div class="whatsApp">
                            <div class="text-bold">
                                <span class="whatsApp-icon"></span> 
                                <span class="text-color-grey">
                                    {{$contact_us[2]->data}}
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="follow-us">
                        <h2>
                            تابعنا
                        </h2>
                        <div class="social-icons">
                            <ul>
                                <a href="{{$contact_us[4]->data}}" target="_blank" class="facebook">
                                    <li>
                                        <i class="fa fa-facebook fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$contact_us[5]->data}}" target="_blank" class="instagram">
                                    <li>
                                        <i class="fa fa-instagram fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$contact_us[6]->data}}" target="_blank" class="googlePlus">
                                    <li>
                                        <i class="fa fa-google-plus fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$contact_us[7]->data}}" target="_blank" class="twitter">
                                    <li>
                                        <i class="fa fa-twitter fa-2x"></i>
                                    </li>
                                </a>
                                <a href="{{$contact_us[8]->data}}" target="_blank" class="youtube">
                                    <li>
                                        <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
                                    </li>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="center-section col-xs-12 col-md-6 col-lg-4">
                    <div class="headerWord">
                        <h3 class="text-bold">اشترك ليصلك كل جديد</h3>
                    </div>
                    <h2 class="text-bold">
                           <font id="msg"> أرسل لى رساله عندما يكون مُتاح </font>
                    </h2>
                    <div class="send-message">
                        <form method="POST" id="subsForum">
                            {{csrf_field()}}
                            <input type="hidden" name="sub1" value="sub1">
                            <input type="email" name="emailsub" placeholder="البريد الالكترونى">
                            <button class="text-bold subscribeBtn" onclick="newsubuser()" type="submit">أرسل</button>
                        </form>
                    </div>
                    
                    <div class="categories col-xs-6" style="padding: 0;">
                        <h3 class="text-bold">لأصحاب المحلات</h3>
                        <ul>
                            <li><a class="text-color-grey text-bold" href="/vendor/register" target="_blank">اضف محلك</a></li>
                        </ul>
                    </div>
                    <div class="categories col-xs-6" style="padding-left: 30px; padding-right: 0;">
                        <h3 class="text-bold">أقسام الموقع</h3>
                        <ul>
                            <li><a class="text-color-grey text-bold" href="/mobile" target="_blank">تطبيق الموبيل</a></li>
                            <li><a class="text-color-grey text-bold" href="/technical-support" target="_blank">الدعم الفنى</a></li>
                            <li><a class="text-color-grey text-bold" href="/FAQ" target="_blank">الأسئلة الأكثر شيوعاً</a></li>
                            <li><a class="text-color-grey text-bold" href="/ads-with-us" target="_blank">أعلن معنا</a></li>
                            <li><a class="text-color-grey text-bold" href="/copyright" target="_blank">الملكية الفكرية</a></li>
                            <li><a class="text-color-grey text-bold" href="/blog" target="_blank">المدونة</a></li>
                        </ul>
                    </div>
                    
                </div>
                
                <div class="right-section col-xs-12 col-lg-4">
                    <div class="img-container">
                 <a href="/">   <img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo"> </a>
                    </div>
                    <h3 class="text-bold">
                        من نحن ؟ ولماذا تنضم الينا ؟
                    </h3>
                    <ul>
                        <li><a class="text-color-grey text-bold" href="/about-us" target="_blank"><i class="fa fa-check fa-lg"></i>من نحن ؟</a></li>
                        <li><a class="text-color-grey text-bold" href="/terms-condition" target="_blank"><i class="fa fa-check fa-lg"></i>شروط وجود محلك فى الموقع ؟</a></li>
                        <li><a class="text-color-grey text-bold" href="/privileges" target="_blank"><i class="fa fa-check fa-lg"></i>مميزات وجوده معنا ؟</a></li>
                    </ul>
                </div>
            </div> 
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="footer-bottom text-center">
                    <div class="copy-right col-xs-12 col-md-9">
                        <p>Developed By <a style="color: #777; margin-right: 20px;" href="http://www.wasiladev.com" target="_blank">WasilaDev</a></p>
                        <p>جميع الحقوق محفوظه لشركه فى المحل دوت كوم</p>
                    </div>

                    <div class="conditions col-xs-12 col-md-3">
                        <a href="/privacy">سياسة الخصوصية</a>
                        <a href="/rules">احكام وشروط</a>
                    </div>
                </div>
            </div>
        </div>
        </footer>
        
        
    <div id="preloader">
        <div class="text">

            <img src="{{asset("image/".$image->value)}}" alt="Filmahal Logo">

        </div>
    </div>



        
        
        
<!--End Of Footer Section-->
<script src="/pagination/datatables.min.js"></script>
    <script src="/pagination/main.js"></script>
    <script src="{{asset('profiles/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('profiles/js/bootstrap.min.js')}}')}}"></script>
    <script src="{{asset('profiles/owlcarousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('profiles/js/sweetalert2.min.js')}}"></script>
    <script src="{{asset('profiles/js/watermark.min.js')}}"></script>
    <script src="{{asset('profiles/js/readIMG.js')}}"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.min.js"></script>
    
   <script src="{{asset('js/pace.min.js')}}"></script>
    <script src="{{asset('profiles/js/emojionearea.min.js')}}"></script>
    
    <script src="{{asset('profiles/js/typeahead.bundle.min.js')}}"></script>
    
    <script src="https://static.addtoany.com/menu/page.js"></script>
    <script src="{{asset('profiles/js/main.js')}}"></script>
    <script src="{{asset('js/js.js')}}"></script>
  
@yield('script')

@if(Auth::user())
    <input type="hidden" name="user_id" id="user_id" value="{{Auth::user()->id}}">
    <script defer>
        setInterval(function(){
            setTimeout(function getnoti(){
                user_ID = "{{Auth::user()->id}}";
                oldnotificationlength = "{{$counter}}";
                
                $.ajax({
                    type:'POST',
                    url:'/api/get/notification',
                    data:{noti:1,userId:user_ID},
                    success: function(data){
                        console.log(data);

                        if(data.notifications.length > data.counter[0].counter){
        $('.notification-icon').append('<span class="label label-warning" style="height:25px;">'+data.notifications.length+'</span>');
                            if(data.notifications.length > 3){

                                for(i=0; i<3; i++){
                                    $('.message').remove();

                                //    $('.more-messages').remove();
                                   
                                    $.ajax({
                                        type:'POST',
                                        url:'/api/get/notification',
                                        data:{noti:2,userId:user_ID,notiId:data.notifications[i].id},
                                        success: function(data){
                                              
                                           notis = '<li class="message"><a href="#"><div class="client"><div class="message-header"><div class="img-container"><img src="/image/'+data.contact_info[0].icon+'" class="img-responsive"></div><div class="client-title"><h5>'+data.marketnoti[0].title+'</h5><p>'+data.newnotifications[0].data+'</p><p class="time"><i class="fa fa-phone"></i>'+data.newnotifications[0].created_at+'</p></div></div><div></div></div></a></li>';
                                        
                                            $('.noti').append(notis);

                                        }
                                        
                                    });

                                    notis = "";
                                }

                                
                            }else{
                            $('.notification-icon').append('<span class="label label-warning" style="height:25px;">'+data.notifications.length+'</span>');
                                for(i=0; i<data.notifications.length; i++){
                                    $('.message').remove();

                               //     $('.more-messages').remove();                                    
                                    $.ajax({
                                        type:'POST',
                                        url:'/api/get/notification',
                                        data:{noti:2,userId:user_ID,notiId:data.notifications[i].id},
                                        success: function(data){
                                          
                                          notis = '<li class="message"><a href="#"><div class="client"><div class="message-header"><div class="img-container"><img src="/image/'+data.contact_info[0].icon+'" class="img-responsive"></div><div class="client-title"><h5>'+data.marketnoti[0].title+'</h5><p>'+data.newnotifications[0].data+'</p><p class="time"><i class="fa fa-phone"></i>'+data.newnotifications[0].created_at+'</p></div></div><div></div></div></a></li>';
                                          $('.noti').append(notis);
           
                                        }

                                    });
                                    notis = "";
                                }
                            }
                           /* notis2 = '<li class="more-messages"><p><a href="#">شاهد جميع الاشعارات</a></p><p><a href="#"><font id="countnoti">'+oldnotificationlength+' اشعار اخر</font></a></p><div class="clear"></div></li>';
                            $('.noti').append(notis2);*/
                        }
                    },
                    error: function(){
                        console.log('error');
                    }
                  });
                
            }, 1000);
        }, 5000);
        
        
        <!-- AddToAny BEGIN -->
    
    var a2a_config = a2a_config || {};
    a2a_config.onclick = 1; 

<!-- AddToAny END -->

            
    </script>
    @endif
    <script>
    function getarea(){
            cities = document.getElementById('governorate').value;
            console.log(cities);
              $.ajax({
                type:'GET',
                url:'/vendor/cities',
                data:{city:cities},
                success: function(data){
                    console.log(data);
                    $('#area > option').remove();
                    for(var i = 0; i < data.area.length; i++){
                        addoption = '<option value='+data.area[i].id+' class="level-0">'+data.area[i].name+'</option>';
                        $('#area').append(addoption);
                    }
                    addoption="";
                },
                error: function(){
                    console.log('error');
                }
              });
           // console.log('error');
        };
    function getarea2(){
            cities = document.getElementById('governorate2').value;
            console.log(cities);
              $.ajax({
                type:'GET',
                url:'/vendor/cities',
                data:{city:cities},
                success: function(data){
                    console.log(data);
                    $('#area2 > option').remove();
                    for(var i = 0; i < data.area.length; i++){
                        addoption = '<option value='+data.area[i].id+' class="level-0">'+data.area[i].name+'</option>';
                        $('#area2').append(addoption);
                    }
                    addoption="";
                },
                error: function(){
                    console.log('error');
                }
              });
           // console.log('error');
        };
  
        function newsubuser(){
            $('#subsForum').submit(function(e){
              e.preventDefault();
              var form = $('#subsForum');
              $.ajax({
                type:'POST',
                url:'/',
                data:form.serialize(),
                dataType:'json',
                success: function(data){
                    console.log(data);
                    $('#msg').html('تم الأشتراك بنجاح');
                    $("#msg").fadeIn(3000);
                    setTimeout(function() {
                       $('#msg').fadeOut(2000);
                    }, 10000);
                }
              });
            });
        };
      
$(function() {

  
    $("#modal").iziModal({

        bodyOverflow: true,
        openFullscreen: false,
        closeOnEscape: true,
        closeButton: true,
        fullscreen: false,
        transitionIn: 'fadeIn',
        transitionOut: 'fadeOut'


    });


    $(document).on('click', '.trigger, .moreInfo a', function (event) {
        event.preventDefault();
        $('#modal').iziModal('open');
    });

      

});
      
  
   </script>
   
   
       <script>
    
    
    paceOptions = {

    ajax: true,
    document: true,
    eventLag: false

};

Pace.on('done', function () {

    'use strict';

    $("body, html").css("overflow", "visible");

    $('#preloader').delay(500).fadeOut(800, function () {

        $(this).remove();

    });
});
    
    
    </script>
    
    
    <script>
        
        $(document).ready(function() {
            cities = document.getElementById('searching').value;
            console.log(cities);
              $.ajax({
                type:'POST',
                url:'/autocomplete',
                data:{searchVal:cities},
                success: function(data){
                        var a = [data[0].title];
                        for(var i = 0; i< data.length; i++){
                          a.push(data[i].title);
                        }
                        console.log(data);
                        var arabicPhrases = new Bloodhound({
                            datumTokenizer: Bloodhound.tokenizers.whitespace,
                            queryTokenizer: Bloodhound.tokenizers.whitespace,
                            local: a

                        });
                        

                        $('.search-profile').typeahead({
                        hint: false
                        },
                        {
                        name: 'search',
                        source: arabicPhrases,
                        remote: 'city.php?query=%QUERY',
                        templates: {
                            empty: [
                            '<div class="empty-message">',
                                'لا توجد نتائج',
                            '</div>'
                            ].join('\n'),
                        }
                        });
                },
                error:function(){
                  console.log('error');
                }
              });

        });
        
        // $(document).ready(function() {
        
        
        // var arabicPhrases = new Bloodhound({
        //     datumTokenizer: Bloodhound.tokenizers.whitespace,
        //     queryTokenizer: Bloodhound.tokenizers.whitespace,
        //     local: [
        //         "الإنجليزية",
        //         "نعم",
        //         "لا",
        //         "مرحبااا",
        //         "مرحباااااا",
        //         "مرحبانننن",
        //         "مرحبا",
        //         "أهلا"
        //     ]
        //     });


        //     $('.search-profile').typeahead({
        //     hint: false
        //     },
        //     {
        //     name: 'search',
        //     source: arabicPhrases,
        //     remote: 'city.php?query=%QUERY',
        //     templates: {
        //         empty: [
        //         '<div class="empty-message">',
        //             'لا توجد نتائج',
        //         '</div>'
        //         ].join('\n'),
        //     }
        //     });

        // })




        // $(document).ready(function () {

        // $('.search-profile').typeahead({
        //         source: function (query, result) {

        //             $.ajax({

        //                 url: "/autocomplete",
        //                 method: 'POST',
        //                 data: {
        //                     query: query
        //                 },
        //                 dataType: "json",
        //                 success: function (data) {

        //                     result($.map(data, function (item) {

        //                         return item;

        //                     }));

        //                 }

        //             })

        //     }
        // });
        
        
    </script>
   


   
  
   
</body>
</html>
