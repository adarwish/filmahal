<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"> <!--This is added by me-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Vendor Dashboard</title>
     <link rel="apple-touch-icon" sizes="180x180" href="http://filmahal.com/imgs/favIcon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="http://filmahal.com/imgs/favIcon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="http://filmahal.com/imgs/favIcon/favicon-16x16.png">
    <link rel="manifest" href="http://filmahal.com/imgs/favIcon/manifest.json">
    <link rel="mask-icon" href="http://filmahal.com/imgs/favIcon/safari-pinned-tab.svg" color="#38ef7d">
    <meta name="theme-color" content="#ffffff">
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" sizes="32x32" href="{{asset('imgs/favIcon/favicon-32x32.png')}}">
    <link rel="shortcut icon" type="image/png" sizes="16x16" href="{{asset('imgs/favIcon/favicon-16x16.png')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/css/emojionearea.min.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/owlcarousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/sweetalert2.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('vendor/css/main1.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/main0.css')}}">

    
    
    <script src="{{asset('js/js.js')}}"></script>
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div>
        <h3 class="title">تعديل بيانات المنتج</h3>
    </div>
    <div class="addProduct" style="display: block;">
            <div class="row">

               <div class="col-md-12">
                   <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save16" id="save16" value="save16">
                    <input type="hidden" name="ids16" value="{{$id}}" id="ids16">
                        <div class="row">
                            <div class="reverseCols">
                                <div class="col-xs-12 col-md-7 productForm">
                           		
                                        <input type="hidden" name="subCatId16 value="{{$product->sub_category_id}}""  id="subCatId">
                                        <div class="inputForm">
                                            <div class="haveLength">
                                                <label>اسم المنتج</label>
                                                <input maxlength="50" type="text" id="productName16" name="productname16" value="{{$product->name}}"  placeholder="لا يتعدى الاسم عن 4 كلمات">
                                                <span>50 حرف</span>

                                            </div>
                                        </div>

                                        <div class="inputForm">
                                            <label class="addTo">اضافة الى</label>
                                            <select name="addTo16" id="addTo" class="classification">
                                                <option value="0">اختر التصنيف</option>
                                                <option value="1">تخفيضات</option>
                                                <option value="2">عروض جديدة</option>
                                                <option value="3">كلاهما</option>
                                            </select>
                                            <span class="optional">اختيارى</span>
                                        </div>
                                        
                                        <div class="inputForm">
                                            <div class="haveLength">
                                                <label>سعر المنتج</label>
                                                <input type="text" id="productPrice16" name="productPrice16" value="{{$product->price}}" placeholder="السعر الاصلى للمنتج">
                                            </div>
                                        </div>
                                        
                                        <div class="salesPercentagePrice hidden">
                                            <div class="inputForm">
                                                <label>نسبة التخفيض</label>
                                                <input type="text" name="percentage16" id="percentage16" value="{{$product->percentage}}">
                                            </div>    
    
                                            <div class="inputForm">
                                                <div class="haveLength">
                                                    <label>سعر المنتج بعد التخفيض</label>
                                                    <input type="text" id="discount16" name="discount16" value="{{$product->discount}}" placeholder="السعر الجديد">
                                                </div>
                                            </div>
   
                                            <div class="inputForm">
                                                <div class="haveLength">
                                                    <label class="productStatus">تاريخ الانتهاء من التخفيضات</label>
                                                    <input type="date" id="saleDeadLine16" name="saleDate16" value="{{$product->end_discount}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="inputForm news hidden">
                                            <div class="haveLength">
                                                <label class="productStatus">تاريخ الانتهاء من العروض الجديده</label>

                                                <input type="date" id="myDate16" name="newDate16" value="{{$product->end_new}}"> 
                                            </div>
                                        </div>

                                        

                                        <div class="inputForm">
                                            <div class="haveLength">

                                                <label>وصف المنتج</label>
                                                <textarea maxlength="200" name="description16" id="description16"  placeholder="اكتب وصف عن المنتج به معلومات قد تفيد عملائك">{{$product->discription}}</textarea>
                                                <span class="textArea">200 حرف</span>
                                            </div>
                                        </div>
                                        <input type="hidden" name="userIds" id="userIDS" value="{{Auth::user()->id}}">
                                        
                                       
                                        <?php 
                                        $attribute = DB::Table('attribute')->whereIn('id',$attributeID)->get();
                                         ?> 
                                        @foreach($attribute as $attributes)
                                        <div class="properties chooseColor">
                                            <h4>{{$attributes->name}}</h4>
					<?php 
					$allIds = DB::table('market_attribute')->where('attribute_id',$attributes->id)->get();
					
                                         ?>
                                        
                                        @endforeach
                                          @foreach($properties as $property)
                                          <?php 
        				  $aprop =DB::table('sub_properties')->where('att_properties_id',$property->id)->get(); ?>
                                            <h5 class="colors">{{$property->name}}</h5>
                                            <div class="clear"></div>
                                            @foreach($aprop as $aprops)
                                            <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                                                <label>{{$aprops->name}}</label>
                                                <input type="checkbox" id="colorId" name="colors16" value="{{$attributes->id}},{{$property->id}},{{$aprops->id}}">
                                            </div>
                                           
                                            @endforeach
                                          @endforeach
                                        
                                            
                                        </div>
                                        
                                       
                                        
                                        <div class="clear"></div>
                                    
                                </div>
                                
                                <div class="col-xs-12 col-md-5 productPhotosUploader">
                                    <div class="productPhotos photo">
                                        <div class="main">
                                            <div class="inputUploader text-center">
                                            <?php $photo = DB::table('photo')->where('product_id',$id)->get(); ?>
                                           @if(count($product->photo) > 0)
                                            <img src="/image/{{$product->photo}}" class="img-responsive">
                                            @else
                    				<img src="/image/SVG/uploadWhite.svg" class="img-responsive photo-img">
                    			  
                                                <p class="lead">اضف صورة للمنتج بجوده عاليه</p>
                                                <p class="lead">يحب ان يكون حجم الصوره 400x400</p>
                                             @endif
                                            </div>
                                        </div>
                                        <div>
                                            <input type="file" name="image16" accept="image/gif, image/jpeg, image/png, image/jpg">
                                        </div>
                                        <div class="img">
                                        
                    				<!--<img class="img-responsive" id="image16" src="0"></div>-->
                    			   
                    			    
                                    </div>
                                    <p class="text-center text-bold lead">كود المنتج: <span class="productCode"></span></p>
                                    <div class="centerMe">
                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image17"  accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>    
                                            
                                            
                                            <div class="img">
                                            @if(count($photo) > 0)
                                            <img id="image17" class="img-responsive" src="/image/{{$photo[0]->photo}}">
                                            @else
                                            <img id="image17" class="img-responsive" src="">
                                            @endif</div>
                                        </div>

                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image18"  accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>
                                            
                                            <div class="img">
                                            @if(count($photo) > 1)
                                            <img id="image17" class="img-responsive" src="/image/{{$photo[1]->photo}}">
                                            @else
                                            <img id="image17" class="img-responsive" src="">
                                            @endif
                                            </div>
                                        </div>

                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                            
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image19"   accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>
                                            
                                            <div class="img">
                                            @if(count($photo) > 2)
                                            <img id="image17" class="img-responsive" src="/image/{{$photo[2]->photo}}">
                                            @else
                                            <img id="image17" class="img-responsive" src="">
                                            @endif
                                            </div>
                                        </div>

                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                            	
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image20"  accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>
                                            
                                            <div class="img">
                                            
                                            @if(count($photo) > 3)
                                            <img id="image17" class="img-responsive" src="/image/{{$photo[3]->photo}}">
                                            @else
                                            <img id="image17" class="img-responsive" src="">
                                            @endif</div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                      
                    

                       <button class="editBtn"><strong>حفظ</strong></button>
                     </form>
                   </div>
               </div>
            </div>
         </div>
         <script src="{{asset('vendor/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('vendor/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendor/owlcarousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('vendor/js/sweetalert2.min.js')}}"></script>
    <script src="{{asset('vendor/js/watermark.min.js')}}"></script>
    <script src="{{asset('vendor/js/readIMG.js')}}"></script>
    

    <script src="{{asset('vendor/js/emojionearea.min.js')}}"></script>

    <script src="{{asset('vendor/js/main.js')}}"></script>
	 <script defer>
 
    function editPro(data,nameData,phoneData,emailData,countryData,saleData,disData,percentageData,imgData1,imgData2,imgData3,imgData4,imgData5) {
               
                id =  document.getElementById('ids16');
                id.value  = data;
                
      		newdiscription1 = emailData.replace(/\+/g,' ');
 		newname1 = nameData.replace(/\+/g,' ');
			    
			    
                olddayname = document.getElementById('productName16');
                olddayname.value = newname1;

                oldprice = document.getElementById('productPrice16');
                oldprice.value = phoneData;
                
                olddesc = document.getElementById('description16');
                olddesc.value = newdiscription1;

                oldnewDate = document.getElementById('myDate16');
                oldnewDate.value =   countryData;

                oldsaleDate = document.getElementById('saleDeadLine16');
                oldsaleDate.value =   saleData;

                olddis = document.getElementById('discount16');
                olddis.value =   newdiscription1;

                oldpercentage = document.getElementById('percentage16');
                oldpercentage.value =   percentageData; 

                if(imgData1 !== null){
                    oldimage1 = document.getElementById('image16');
                    oldimage1.src =   '/image/'+imgData1;
                    $(oldimage1).parent().parent().find(".main").addClass("hidden");
                    
                }
                if(imgData2 !== null){
                    oldimage2 = document.getElementById('image17');
                    oldimage2.src =   '/image/'+imgData2; 
                    $(oldimage2).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData3 !== null){
                    oldimage3 = document.getElementById('image18');
                    oldimage3.src =   '/image/'+imgData3; 
                    $(oldimage3).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData4 !== null){
                    oldimage4 = document.getElementById('image19');
                    oldimage4.src =   '/image/'+imgData4;
                    $(oldimage4).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData5 !== null){
                    oldimage5 = document.getElementById('image20');
                    oldimage5.src =   '/image/'+imgData5;
                    $(oldimage5).parent().parent().find(".main").addClass("hidden");
                }
                $(".productCode").text($("#ids16").val());
                testImgContainer();
               // window.alert(data);
    }
    function editTime(data,nameData,phoneData,emailData,countryData) {

                id =  document.getElementById('ids');
                id.value  = data;
                olddayfrom = document.getElementById('fromeditday');
                olddayfrom.value = nameData;
                olddayto = document.getElementById('toeditday');
                olddayto.value = phoneData;
                user_name = document.getElementById('phone10');
                    if(nameData == '1'){
                        user_name.value = 'السبت';
                    }else if(nameData == '2'){
                        user_name.value = 'الأحد';
                    }else if(nameData == '3'){
                        user_name.value = 'الأثنين';
                    }else if(nameData == '4'){
                        user_name.value = 'الثلاثاء';
                    }else if(nameData == '5'){
                        user_name.value = 'الأربعاء';
                    }else if(nameData == '6'){
                        user_name.value = 'الخميس';
                    }else if(nameData == '7'){
                        user_name.value = 'الجمعة';
                    }
                mob = document.getElementById('phone');             

                if(phoneData == '1'){
                        mob.value = 'السبت';
                    }else if(phoneData == '2'){
                        mob.value = 'الأحد';
                    }else if(phoneData == '3'){
                        mob.value = 'الأثنين';
                    }else if(phoneData == '4'){
                        mob.value = 'الثلاثاء';
                    }else if(phoneData == '5'){
                        mob.value = 'الأربعاء';
                    }else if(phoneData == '6'){
                        mob.value = 'الخميس';
                    }else if(phoneData == '7'){
                        mob.value = 'الجمعة';
                    }
                
                
                user_email = document.getElementById('email');
                user_email.value = emailData;
                user_country = document.getElementById('country');
                user_country.value = countryData;
                
               
               // window.alert(data);
    }
    function editAdd(data,nameData,emailData) {
        ids = document.getElementById('idsAdd');
        ids.value = data;
        user_name = document.getElementById('oldaddress');
        user_name.value = nameData;
        another_adds = document.getElementById('anotheradd');
        another_adds.value = emailData;
               
    }
    function addtonew(data) {
        
        ids = document.getElementById('productIds');
        ids.value = data;
               
    }
    function addtonew1(data) {
        $("#asignproduct").modal("show");
        ids = document.getElementById('productIds');
        ids.value = data;
               
    }
    function addtosale(data) {
        ids = document.getElementById('productSaleId');
        ids.value = data;
               
    }
    function addtosale1(data) {
        $("#asignSale").modal("show");
        ids = document.getElementById('productSaleId');
        ids.value = data;
               
    }
    function addtoAll(data) {
        ids = document.getElementById('productSaleId1');
        ids.value = data;
    }
    function addtoAll1(data) {
        $("#asignToall").modal("show");
        ids = document.getElementById('productSaleId1');
        ids.value = data;
               
    }
    function testImgContainer() {
        const containers = $(".img");
        
        containers.each(function(index) {
            if($(containers[index]).find("img").length) {
                let src = $(containers[index]).find("img").attr("src");
                if(src.search(/jpg/i) != -1 || src.search(/gif/i) != -1 || src.search(/jpeg/i) != -1 || src.search(/png/i) != -1) {
                    $(containers[index]).show();
                    $(containers[index]).parent().find(".main").addClass("hidden");
                } else {
                    $(containers[index]).hide();
                    $(containers[index]).parent().find(".main").removeClass("hidden");
                }
            } else {
                $(containers[index]).hide();
                $(containers[index]).parent().find(".main").removeClass("hidden");
            }
        });
    }
    testImgContainer();
    function editPro1(data,nameData,phoneData,emailData,countryData,saleData,disData,percentageData,imgData1,imgData2,imgData3,imgData4,imgData5) {
        
            $("#editProduct").modal("show");
            
           	newdiscription1 = disData.replace(/\+/g,' ');
 		newname1 = nameData.replace(/\+/g,' ');
 		
                id =  document.getElementById('ids16');
                id.value  = data;
                olddayname = document.getElementById('productName16');
                olddayname.value =newname1;

                oldprice = document.getElementById('productPrice16');
                oldprice.value = phoneData;
                
                olddesc = document.getElementById('description16');
                olddesc.value = emailData;

                oldnewDate = document.getElementById('myDate16');
                oldnewDate.value =   countryData;

                oldsaleDate = document.getElementById('saleDeadLine16');
                oldsaleDate.value =   saleData;

                olddis = document.getElementById('discount16');
                olddis.value =   newdiscription1;

                oldpercentage = document.getElementById('percentage16');
                oldpercentage.value =   percentageData;
             

                if(imgData1 !== 'null'){
                    oldimage1 = document.getElementById('image16');
                    oldimage1.src =   '/image/'+imgData1;
                    $(oldimage1).parent().parent().find(".main").addClass("hidden");
                    
                }
                if(imgData2 !== 'null'){
                    oldimage2 = document.getElementById('image17');
                    oldimage2.src =   '/image/'+imgData2; 
                    $(oldimage2).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData3 !== 'null'){
                    oldimage3 = document.getElementById('image18');
                    oldimage3.src =   '/image/'+imgData3; 
                    $(oldimage3).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData4 !== 'null'){
                    oldimage4 = document.getElementById('image19');
                    oldimage4.src =   '/image/'+imgData4;
                    $(oldimage4).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData5 !== 'null'){
                    oldimage5 = document.getElementById('image20');
                    oldimage5.src =   '/image/'+imgData5;
                    $(oldimage5).parent().parent().find(".main").addClass("hidden");
                }

               // window.alert(data);
                $(".productCode").text($("#ids16").val());
               testImgContainer();
    }
    
    function newproducts(){
        
        $('.anProduct').remove();

        user_id = document.getElementById('userIDS').value;
        $('input:radio').attr("checked", false);
        $('#genders1').prop("checked", true);
        
        $.ajax({
            type:'post',
            url:'/api/add-product',
            data:{save5:'save5',newpro:'newpro',userids:user_id},
            success: function(data){
                console.log(data);
                for(var i = 0; i<data.pro.length; i++){
                    $.ajax({
                        type:'post',
                        url:'/api/add-product',
                        data:{save5:'save5',newphoto:'newphoto',productids:data.pro[i].id,userids:user_id},
                        success: function(data){
                            newdiscription = data.pro[0].discription.replace(/ /g,'+');
                            newname = data.pro[0].name.replace(/ /g,'+');
                            console.log(data);
                            if(data.pro[0].discount == '0'){
        
                                if(data.photo.length == 1)
                                {
                                    
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew1('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li  onclick="addtosale1('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li  onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'",null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  alt="'+newdiscription+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 2){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew1('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li  onclick="addtosale1('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li  onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'",null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 3){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew1('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li  onclick="addtosale1('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li  onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'",null)  data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 4){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew1('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li  onclick="addtosale1('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li  onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'","'+data.photo[3].photo+'") data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else{
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew1('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li  onclick="addtosale1('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li  onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'",null,null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }
                            }else{
                                if(data.photo.length == 1)
                                {
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[i].photo+'","'+data.photo[0].photo+'",null,null,null)data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 2){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[i].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'",null,null)data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 3){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[i].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'",null)data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 4){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[i].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'","'+data.photo[3].photo+'")data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else{
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[i].photo+'",null,null,null,null)data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }
                            }
                           
                            movespecificprod="";
                        }
                    });
                }
                
            },
            error: function(){
                console.log('error');
            }
        });
    }
    function discount(){
        $('.anProduct').remove();
        $('.products > ul').remove();
        user_id = document.getElementById('userIDS').value;
        $('input:radio').attr("checked", false);
        $('#genders1').prop("checked", true);
        

        $.ajax({
            type:'post',
            url:'/api/add-product',
            data:{save5:'save5',sale:'sale',userids:user_id},
            success: function(data){
                
                console.log(data);
                for(var i = 0; i<data.pro.length; i++){
                	    newdiscription = data.pro[0].discription.replace(/ /g,'+');
			    newname = data.pro[0].name.replace(/ /g,'+');
			    if(data.pro[i].discount == '0'){
                        if(data.photo.length == 1)
                        {
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[i].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[i].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[i].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[i].id+',"'+data.pro[i].name+'","'+data.pro[i].price+'","'+data.pro[i].discription+'","'+data.pro[i].end_new+'","'+data.pro[i].end_discount+'","'+data.pro[i].discount+'","'+data.pro[i].pricentage+'","'+data.pro[i].photo+'","'+data.photo[0].photo+'",null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[i].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[i].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[i].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 2){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[i].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[i].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[i].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[i].id+',"'+data.pro[i].name+'","'+data.pro[i].price+'","'+data.pro[i].discription+'","'+data.pro[i].end_new+'","'+data.pro[i].end_discount+'","'+data.pro[i].discount+'","'+data.pro[i].pricentage+'","'+data.pro[i].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'",null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[i].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[i].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[i].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 3){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[i].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[i].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[i].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[i].id+',"'+data.pro[i].name+'","'+data.pro[i].price+'","'+data.pro[i].discription+'","'+data.pro[i].end_new+'","'+data.pro[i].end_discount+'","'+data.pro[i].discount+'","'+data.pro[i].pricentage+'","'+data.pro[i].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'",null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[i].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[i].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[i].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 4){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[i].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[i].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[i].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[i].id+',"'+data.pro[i].name+'","'+data.pro[i].price+'","'+data.pro[i].discription+'","'+data.pro[i].end_new+'","'+data.pro[i].end_discount+'","'+data.pro[i].discount+'","'+data.pro[i].pricentage+'","'+data.pro[i].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'","'+data.photo[3].photo+'")  data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[i].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[i].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[i].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else{
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[i].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[i].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[i].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[i].id+',"'+data.pro[i].name+'","'+data.pro[i].price+'","'+data.pro[i].discription+'","'+data.pro[i].end_new+'","'+data.pro[i].end_discount+'","'+data.pro[i].discount+'","'+data.pro[i].pricentage+'","'+data.pro[i].photo+'",null,null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[i].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[i].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[i].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }
                    }else{
                        $.ajax({
                            type:'post',
                            url:'/api/add-product',
                            data:{save5:'save5',salephoto:'salephoto',productids:data.pro[i].id,userids:user_id},
                            success: function(data){
                               
                                console.log(data);
                            
                        if(data.photo.length == 1)
                        {
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'",null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 2){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'",null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 3){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'",null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 4){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'","'+data.photo[3].photo+'") data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else{
                                salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'",null,null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                
                                $('.products').append(salespecificprod);
                            }
                       
                        },
                        });
                      }  
                    }
                    
                    salespecificprod = "";
           
              }
            });
       }
                
       function specificProduct(Data){
        
        subData = document.getElementsByName('gender');
        user_id = document.getElementById('userIDS').value;
        for (var i = 0; i < subData.length; i++){
            if (subData[i].checked)
            {
                $("#subCatId").val(subData[i].value);
                //console.log(user_id);
            }
        }
        $('.anProduct').remove();
        $.ajax({
            type:'post',
            url:'/api/add-product',
            data:{save5:'save5',subId:Data,userids:user_id},
            success: function(data){
                console.log(data);
                for(var i = 0; i<data.pro.length; i++){
                    $.ajax({
                        type:'post',
                        url:'/api/add-product',
                        data:{save5:'save5',newphoto:'newphoto',productids:data.pro[i].id,userids:user_id},
                        success: function(data){
                            newdiscription = data.pro[0].discription.replace(/ /g,'+');
			    newname = data.pro[0].name.replace(/ /g,'+');
                            console.log(data);
                            if(data.pro[0].discount == '0'){
                                if(data.photo.length == 1)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'",null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 2)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'",null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 3)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'",null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 4)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'","'+data.photo[3].photo+'") data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else{
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'",null,null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+newdiscription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }
                            }else{
                                if(data.photo.length == 1)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'",null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+newname+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 2)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'",null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 3)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'",null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 4)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'","'+data.photo[3].photo+'") data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'",null,null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }
                            }
                            allspecificprod="";
                        }
                    });
                }
                
            },
            error: function(){
                console.log('error');
            }
        });
    }

    function specificProducttwo(Data){
        
        subData = document.getElementsByName('gender');
        user_id = document.getElementById('userIDS').value;
        for (var i = 0; i < subData.length; i++){
            if (subData[i].checked)
            {
                $("#subCatId").val(subData[i].value);
                //console.log(user_id);
            }
        }
        $('.anProduct').remove();
        $.ajax({
            type:'post',
            url:'/api/add-product',
            data:{save5:'save5',subId:Data,userids:user_id},
            success: function(data){
                console.log(data);
                for(var i = 0; i<data.pro.length; i++){
                    $.ajax({
                        type:'post',
                        url:'/api/add-product',
                        data:{save5:'save5',newphoto:'newphoto',productids:data.pro[i].id,userids:user_id},
                        success: function(data){
                            newdiscription = data.pro[0].discription.replace(/ /g,'+');
               			newname2 = 'khale+d';
               			newname	   = data.pro[0].name.replace(/ /g,'+'); 
			    	console.log(newname);
			    	 
			    
                            console.log(data);
                            if(data.pro[0].discount == '0'){
                                if(data.photo.length == 1)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro"('+data.pro[0].id+','+newname+','+data.pro[0].price+','+newdiscription+','+data.pro[0].end_new+','+data.pro[0].end_discount+','+data.pro[0].discount+',"'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'",null,null,null)" data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else if(data.photo.length == 3)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'",null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else if(data.photo.length == 4)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'","'+data.photo[3].photo+'") data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else{
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'",null,null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+newdiscription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }
                            }else{
                                if(data.photo.length == 1)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'",null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else if(data.photo.length == 2)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'",null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else if(data.photo.length == 3)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'",null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else if(data.photo.length == 4)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'","'+data.photo[0].photo+'","'+data.photo[1].photo+'","'+data.photo[2].photo+'","'+data.photo[3].photo+'") data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><li class="editProduct" data-target="#editProduct" onclick=editPro('+data.pro[0].id+',"'+newname+'","'+data.pro[0].price+'","'+newdiscription+'","'+data.pro[0].end_new+'","'+data.pro[0].end_discount+'","'+data.pro[0].discount+'","'+data.pro[0].pricentage+'","'+data.pro[0].photo+'",null,null,null,null) data-toggle="modal">تعديل المنتج</li><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }
                            }
                            allspecificprodtwo="";
                        },
                    });
                }
                
            },
            error: function(){
                console.log('error');
            }
        });
    
    }
    
    function deleteproduct(Data){
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type:'post',
                    url:'/api/add-product',
                    data:{save12:'save12',product_id:Data},
                    success: function(data){
                        console.log(data);
                        $('#productnum'+Data).remove();
                    },
                    error: function(){
                        console.log('error');
                    }
                });            
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
               $(this).parents(".anProduct").remove();
            }

        });
        
    }
    function hiddeSub(data){
        console.log(data);
        $.ajax({
            type:'POST',
            url:'/vendor/hide-sub',
            data:{ids:data},
            success: function(data){
                console.log(data);
            },
            error: function(){
                console.log('error');
            }
        });
    }
    function deletesub(data){
        console.log(data);
        $.ajax({
            type:'POST',
            url:'/api/vendor/delete-sub',
            data:{ids:data},
            success: function(data){
                //const x = "#kinds" + data;
               // console.log(x);
                $("#kinds" + data).remove();
                console.log(data);
            },
            error: function(){
                console.log('error');
            }
        });
    }
    function roomMsg(Data,Sender){
        room_id = document.getElementById('roomIds');
        room_id.value = Data;
        
        blockId = document.getElementById('blockname');
        blockId.value = Sender;
        
        $.ajax({
            type:'POST',
            url:'/api/getmsg',
            data:{roomId:Data},
            success: function(data){
                console.log(data);
                 $('.textBox > p').remove();
                for(i=0; i<data.length; i++){
                    if(data[i].sender_id == Sender){
                        
                        recivermsg = '<p class="secondary text-left">'+data[i].message+'</p>';
                        recivermsg2 = '<p class="secondary text-left">'+data[i].created_at+'</p>';
                        $('.textBox').append(recivermsg);
                        $('.textBox').append(recivermsg2);
                    }else{
                    	sendermsg = '<p class="primary">'+data[i].message+'</p>';
                    	sendermsg2 = '<p class="primary">'+data[i].created_at+'</p>';
                        $('.textBox').append(sendermsg);
                        $('.textBox').append(sendermsg2);

                    }
                }
            },
            error: function(){
                console.log('error');
            }
        });
    }
    function newmsg(){

        $('#newmsgForum1').submit(function(e){
            e.preventDefault();
            var form = $('#newmsgForum1');
            $.ajax({
                type:'POST',
                url:'/vendor/control-panel',
                data:form.serialize(),
                dataType:'json',

                success: function(data){
                    console.log(data);
                    if(data == "block"){
	                    swal();
				swal({
				title: "you are blocked this user",
				text: " "
				});
                    }
                    
                },
                error: function(){
                    console.log('error');
                }
            });
        });
        
    }
    function block(Data){
	    USERID = document.getElementById('blockname').value;
        
            $.ajax({
                type:'POST',
                url:'/api/block',
                data:{vendor_id:"{{Auth::user()->id}}",user_id:USERID,block:Data},

                success: function(data){
                    console.log(data);
                  //  alert('block success');
                    
                },
                error: function(){
                    console.log('error');
                }
            });
        
        
    }
    function addcomment(){
        $('#replaycommentForum').submit(function(e){
          e.preventDefault();
          var form = $('#replaycommentForum');
          $.ajax({
            type:'POST',
            url:'/vendor/control-panel',
            data:form.serialize(),
            dataType:'json',

            success: function(data){
                console.log(data);
            
            },
            error: function(){
                console.log('error');
            }
          });
        });
        
    }
    

</script>
    </body>
<html>