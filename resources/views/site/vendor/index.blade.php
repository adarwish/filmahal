@extends('site.vendor.master')

@section('content')
<!--Start 0f Pofile-settings-->
<section id="profile_settings">
    <div class="navigation">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p class="navigationText text-bold">
                        <a href="#" id="mainPage">الرئيسيه</a> >> اعدادات
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="container vendorProfile">
        <div class="row">
            <div class="col-xs-12 nameEmail">
                <div class="message-header">
                    <div class="img-container">
                        <img src="/image/{{Auth::user()->photo}}" class="img-responsive">
                    </div>              
                    <div class="client-title">
                        <h4>{{Auth::user()->name}}</h4>
                        <p>{{Auth::user()->email}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row settings">
            <div class="col-xs-12 col-md-9 inbox">
                <div class="reviews row">
                    <h4 class="primary text-bold">صندوق الرسائل</h4>
                    <h5><a id="parent_block" onclick="block(1)"><span id="block">Block</span></a>| <a onclick="block(0)"><span id="report" class="error delete">Report</span></a></h5>
                    <input type="hidden" id="blockname" name="blockname" value="0">
                    
                    <div class="clear"></div>
                </div>
                <div class="row">
                    <div class="col-xs-9">
                        <div class="row col-reverse">
                            <div class="col-xs-12 textBox">
                              <div> <p class="primary"></p>
                               <p class="secondary text-left"></p> </div>
                            </div>
                            <div class="col-xs-12 msg">
                                <div class="row">
                                    <form method="POST" id="newmsgForum1">
                                        {{csrf_field()}}
                                        <input type="hidden" name="save18" value="save18">
                                        <input type="hidden" name="roomIds"  id="roomIds" value="0">
                                        <input type="hidden" name="storeId" value="{{$market[0]->id}}">
                                        <div class="col-xs-2">
                                            <div class="row">
                                               <div class="messenger">
                                                    <i class="fa fa-thumbs-o-up fa-lg" aria-hidden="true"></i>
                                                    <i class="fa fa-camera fa-lg" aria-hidden="true">
                                                        <input type="file" class="myInput" accept="image/gif, image/jpeg, image/png, image/jpg">
                                                    </i>
                                               </div>
                                           </div>
                                        </div>
                                        <div class="col-xs-10">
                                            <div class="row">
                                                <textarea id="emoji" name="newmessage"></textarea>
                                            </div>
                                        </div>
                                        <button onclick="newmsg()">send</button>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="col-xs-3 messages-box">
                        <div class="row">
                            <ul class="messages-list">
                                    @foreach($room as $rooms)
                                <?php $sender = DB::table('users')->where('id',$rooms->sender_id)->get();
                                $msg = DB::table('message')->where('room_id',$rooms->id)->get();
                                $reciver = DB::table('users')->where('id',$rooms->reciver_id)->get();
                                $block = DB::table('report_block')->where('user_id',$rooms->sender_id)->where('vendor_id',$rooms->reciver_id)->get();
                                $count_block = count($block); ?>
                                
                                <li onclick="roomMsg('{{$rooms->id}}','{{$rooms->sender_id}}','{{$count_block}}')">
                                    <a href="#">
                                        <div class="client">
                                            <div class="message-header">
                                                <div class="img-container">
                                                    <img src="/image/{{$sender[0]->photo}}" class="img-responsive">
                                                </div>              
                                                <div class="client-title">
                                                    <h5>{{$sender[0]->user_name}}</h5>
                                                    <p>{{$msg[0]->message}}</p>
                                                    <p class="time">
                                                    <?php

                                                    $timestamp = $rooms->created_at;
                                                    $datetime = explode(" ",$timestamp);
                                                    $date = $datetime[0];
                                                    $time = $datetime[1];
                                                    echo $time;?></p>
                                                </div>
                                        
                                            </div>
                                            <div>
                                                
                                            </div>
                                        
                                        </div>
                                    </a>
                                </li>
                                @endforeach

                                    
                                </ul>
                            </div>
                    </div>
                    

                </div>
            </div>
            <div class="col-xs-12 col-md-9 reviewsBox">
                <div class="reviews">
                    <h4 class="primary text-bold">تقييمات المحل</h4>
                    <div class="clear"></div>
                </div>
               @foreach($review as $reviews)
               <?php 
                   
                        $userReview = DB::table('users')->where('id',$reviews->user_id)->get();
                        $vendorReview = DB::table('review')->where('replay',$reviews->id)->get();
                    ?>
                <div class="person-review">
                    <div class="message-header">
                        <div class="img-container">
                         @if($reviews->replay > 0)
                         <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive">
                         @else
                            <img src="/image/{{$reviews->user->photo}}" class="img-responsive">
                            @endif
                        </div>              
                        <div class="client-title">
                             @if($reviews->replay > 0)
                             <?php $store = DB::table('market')->where('user_id',$reviews->vendor_id)->get(); ?>
                             <h4>{{$store[0]->title}}</h4>
                             @else
                        <h4>{{$reviews->user->user_name}}</h4>
                        @endif
                    
                            <p class="timeEn">
                            <?php

                                                    $timestamp = $reviews->created_at;
                                                    $datetime = explode(" ",$timestamp);
                                                    $date = $datetime[0];
                                                    $time = $datetime[1];
                                                    echo $time;?> PM</p>
                        </div>
                    </div>
                    <div class="reviewText">
                        <p>{{$reviews->comment}}</p>
                    </div>
                </div>
                
                @foreach($vendorReview as $vendorReviews)
                <?php 
                   
                        $userReview = DB::table('users')->where('id',$vendorReviews->user_id)->get();
                    ?>
                <div class="person-review">
                    <div class="message-header">
                        <div class="img-container">
                         @if($vendorReviews->replay > 0)
                         <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive">
                         @else
                            <img src="/image/{{$reviews->user->photo}}" class="img-responsive">
                            @endif
                        </div>              
                        <div class="client-title">
                             @if($vendorReviews->replay > 0)
                             <?php $store = DB::table('market')->where('user_id',$vendorReviews->vendor_id)->get(); ?>
                             <h4>{{$store[0]->title}}</h4>
                             @else
                        <h4>{{$userReview[0]->user_name}}</h4>
                        @endif
                    
                            <p class="timeEn">
                            <?php

                                                    $timestamp = $vendorReviews->created_at;
                                                    $datetime = explode(" ",$timestamp);
                                                    $date = $datetime[0];
                                                    $time = $datetime[1];
                                                    echo $time;?> PM</p>
                        </div>
                    </div>
                    <div class="reviewText">
                        <p>{{$vendorReviews->comment}}</p>
                    </div>
                </div>

                
                @endforeach
                
                
                
                

                <div class="textBar">
                    <form method="POST" id="replaycommentForum">
                        {{csrf_field()}}
                        <input type="hidden" name="userId" value="{{$reviews->user_id}}">
                        <input type="hidden" name="oldcomment" value="{{$reviews->id}}">
                        <div class="search">
                            <input type="search" onclick="addcomment()" name="comment" placeholder="اكتب ردك">
                            <div class="img-container">
                                <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive">
                            </div> 
                        </div>
                      <!--  <button>اضف</button>  -->
                    </form>
                </div>
                
                
                @endforeach
            </div>
            <div class="col-xs-12 col-md-9 allSettings">
                    <div class="settingsNav row">
                        <div class="col-xs-3 text-center general" style="background-color: rgb(17, 153, 142); color: rgb(255, 255, 255);">
                            <h4>اعدادات عامه</h4>
                        </div>
                        <div class="col-xs-3 text-center specific" style="background-color: rgb(255, 255, 255); color: rgb(51, 51, 51);">
                            <h4>اعدادات الملف الشخصى</h4>
                        </div>
                        <div class="col-xs-3 text-center pass-con" style="background-color: rgb(255, 255, 255); color: rgb(51, 51, 51);">
                          <h4>اعدادات كلمة المرور</h4>
                      </div>
                    </div>
                    <div class="generalElments">
                        <div class="attrs">
                            <h4 class="primary text-bold attrsNow">فئات المحل الحاليه</h4>
                        </div>
                        <div class="attrsVal">
                            @foreach($categories as $category)
                            <div id="kinds{{$category->id}}" class="val col-xs-12 col-sm-6 col-md-4 text-center">
                                <i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i>
                                <label>{{$category->name}}</label>
                                
                                <div class="popup hidden">
                                    <div class="contain">
                                        <a onclick="deletesub('{{$category->id}}')"><p class="text-bold">اخفاء هذه الفئة</p></a>
                                        <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="clear"></div>

                        <div>
                            <div class="attrs">
                                <h4 class="primary text-bold attrsNow">اضافة فئات اخرى</h4>
                            </div>
                            <div class="add-cates">
                                <p>قم باضافة كل الفئات الموجوده فى محل <i id="cates" class="fa fa-plus-square primary fa-lg" aria-hidden="true"></i></p>
                                
                            </div>

                        </div>
                    </div>

                    <div class="specificElments" id="restInfo" style="display: none;">
                        <form method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="editProf" value="editProf">
                            <div class="photo img-uploader">
                                <div class="main">
                                    <div class="text-center cloud">
                                        <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                        <p>صوره الشخصيه</p>
                                    </div>
                                </div>
                                <div class="absolute">
                                    <input type="file" name="profilephoto" class="myInput" accept="image/gif, image/jpeg, image/png, image/jpg">
                                </div>
                                <div class="img"><img src="/image/{{Auth::user()->photo}}"></div>
                            </div>
                            <div class="infoProfile">
                                <div class="box">
                                    <label for="username" class="text-right">اسم المستخدم</label>
                                    <input type="text" name="username" value="{{Auth::user()->name}}">
                                    <span class="text-bold text-left"></span>
                                </div>

                                <div class="box">
                                    <label for="email" class="text-right">الاميل</label>
                                    <input type="email" name="email" value="{{Auth::user()->email}}">
                                    <span class="text-bold text-left"></span>
                                </div>
                                <?php $marketUser = DB::table('market')->where('user_id',Auth::user()->id)->get(); ?>
                                <div class="box">
                                    <label for="shopName" class="text-right">اسم المحل</label>
                                    <input type="text" name="shopName" value="{{$marketUser[0]->title}}">
                                    <span class="text-bold text-left"></span>
                                </div>
                                
                            </div>

                            <button class="editProfileSettings">حفظ</button>
                        </form></div>
                      
                        <div class="password-config" id="password-config" style="display: none;">
                          <form method="POST" id="pswForum">
                          	{{csrf_field()}}
                          	<input type="hidden" name="changepsw" value="changepsw">
                              <div class="infoProfile">
                                  <div class="box">
                                      <label class="text-right">كلمة المرور القديمة</label>
                                      <input type="password" name="oldPassword" >
                                      <span class="text-bold text-left"></span>
                                  </div>
  
                                  <div class="box">
                                      <label class="text-right">كلمة المرور الجديدة</label>
                                      <input type="password" name="newPassword" >
                                      <span class="text-bold text-left"></span>
                                  </div>
                                  
                              </div>
  
                              <button class="editProfileSettings" onclick="changespsw()">حفظ</button>
                          </form></div>
                      
                      </div>
                        <div id="third-step2" style="display: block;">
                            <form method="POST">
                                {{csrf_field()}}
                                <input type="hidden" name="editCats" value="editCats">
                                <div class="col-xs-9 bg-white">
                                    <div class="choose-kinds">
                                        <h3 class="text-bold">فئات المحل</h3>
                                        <i id="close2" class="fa fa-times fa-2x" aria-hidden="true"></i>
                                        <div class="clear"></div>
                                        <p>اختر جميع الفئات الموجوده فى محل</p>
                                    </div>
                                    <div class="chooseCat">
                                        @foreach($allcats as $allcat)
                                        <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                                            <label>{{$allcat->name}}</label>
                                            <input type="checkbox" name="kinds[]" value="{{$allcat->id}}">
                                        </div>
                                        @endforeach
                                        
                                    </div>
                                    <div>
                                        <p class="text-center error text-bold" id="errorCates"></p>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="next-step step3">
                                        <button>حفظ</button>
                                    </div>
                                </div>
                            </form>
                        </div>
            <div class="col-xs-12 col-md-3">
                <ul class="profile-settings profile-settingss">
                    <li>
                        <a href="#reviewsPeople" class="reviewsPeople" id="reviewsPeople">
                            <i class="ion ion-edit" aria-hidden="true"></i>
                            اراء الناس فى محلك
                        </a>
                    </li>

                    <li>
                        <a href="#boxMessages" class="boxMessages" id="boxMessages">
                            <i class="fa fa-envelope" aria-hidden="true" ></i>
                            صندوق الرسائل
                        </a>
                    </li>

                    <li>
                        <a href="#settingProfile" class="settingProfile" id="settingProfile">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                            اعدادات حسابى
                        </a>
                    </li>

                    <li>
                        <a href="/logout" class="logout">
                            <i class="fa fa-user" ></i>
                            تسجيل الخروج
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End 0f Pofile-settings-->

<!--Start Of Photo Uploader-->
<section id="uploader">
    <div class="container-fluid">
        <div class="row">
            
            <div class="container">
                <div class="row messageContainer">   
                    <div class="messageClicked client col-xs-12">
                        <div class="newMessage text-color">
                            <h3 class="text-bold"><i class="fa fa-envelope fa-lg"></i>رساله جديده</h3>
                        </div>
                        <hr>
                        <div class="message-header">
                            <div class="img-container">
                                <img src="imgs/hosny.jpg" class="img-responsive">
                            </div>              
                            <div class="client-title">
                                <h5>حسنى مبارك</h5>
                                <p>سوف يتواصل معك قريبا من خلال رقم التليفون</p>
                            </div>
                        </div>
                        <form>
                            <div class="messageBox">
                                <i class="fa fa-caret-up"></i>
                                <textarea placeholder="اكتب رسالتك هنا"></textarea>
                            </div>
                            <div class="buttons">
                                <button class="exit">الغاء</button>
                                <button>ارسال</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

            <div class="owl-carousel">
            	<?php $m=3; ?>
            	@for($s=2; 0 <= $s; $s = $s-1)
                <div class="item cover">
                    <form method="POST" enctype="multipart/form-data">
                    	{{csrf_field()}}
                    	<input type="hidden" name="save1" value="save1" >
                        <input type="file" name="vendormainphotos{{$s}}" class="myInput myCover hidden" accept="image/gif, image/jpeg, image/png, image/jpg">
                        <button type="button" style="z-index: 1007665565454; position: absolute;" class="uploadMyCover">رفع</button>
                        <button style="z-index: 1007665565454; position: absolute;">حفظ</button>
                    	
                    </form>
                    @if(count($photo) == 1)
                        @if($s == 0)
                        <div class="img"><img src="/image/{{$photo[0]->photo}}"></div>
                        @else
                        <div class="main">
                            <div class="text-center cloud">
                                <img src="/image/SVG/upload.svg" class="img-responsive">
                            </div>
                            <div class="uploaderWords">
                                <h3 class="text-center text-bold">اضغط هنا لتحميل الصور يجب ان يكون حجم الصورة 600 &#x2716; 1920</h3>
                                <p class="text-center text-bold">يفضل ان تكون الصور لفترينه العرض او منتج جديد او تخفيض</p>
                            </div>
                        </div>
                        @endif
                    @elseif(count($photo) == 2)
                        @if($s == 0)
                        <div class="img"><img src="/image/{{$photo[0]->photo}}"></div>
                        @elseif($s== 2)
                        <div class="img"><img src="/image/{{$photo[1]->photo}}"></div>
                        @else
                         <div class="main">
                            <div class="text-center cloud">
                                <img src="/image/SVG/upload.svg" class="img-responsive">
                            </div>
                            <div class="uploaderWords">
                                <h3 class="text-center text-bold">اضغط هنا لتحميل الصور يجب ان يكون حجم الصورة 600 &#x2716; 1920</h3>
                                <p class="text-center text-bold">يفضل ان تكون الصور لفترينه العرض او منتج جديد او تخفيض</p>
                            </div>
                        </div>
                        @endif
                    @elseif(count($photo)  == 3)
                        <div class="img"><img src="/image/{{$photo[$s]->photo}}"></div>
                    @else
                     <div class="main">
                            <div class="text-center cloud">
                                <img src="/image/SVG/upload.svg" class="img-responsive">
                            </div>
                            <div class="uploaderWords">
                                <h3 class="text-center text-bold">اضغط هنا لتحميل الصور يجب ان يكون حجم الصورة 600 &#x2716; 1920</h3>
                                <p class="text-center text-bold">يفضل ان تكون الصور لفترينه العرض او منتج جديد او تخفيض</p>
                            </div>
                        </div>
                    @endif
                </div>
                @endfor
                
            </div>             
        </div>        
    </div>
</section>
<!--End Of Photo Uploader-->
<?php $counter = count($subcategory);
$marketId = DB::table('market')->where('user_id',Auth::user()->id)->get(); ?>
<!--Start Of Nav-->
<section id="navbar" style="position: relative">
<div class="disabled-vendor"></div>
    <div class="border">
        <div class="container">
            <div class="row">
                <nav>
                    <ul>
                        <li><a id="newOffers" class="newOffers" onclick="newproducts()" href="#newOffers">عروض جديدة</a></li>
                        <li><a id="sales" class="sales" onclick="discount()" href="#sales">تخفيضات</a></li>
                        <li><a id="shopCats" class="actives" onclick="specificProduct('{{$subcategory[$counter-1]->id}}')" href="#shopCats">اقسام المحل</a></li>
                        <li><a id="contactus" class="contactus" href="#contactus">تواصل معنا</a></li>
                        <li><a id="stats" class="stats" href="#statistics">إحصائيات</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="slid-cates">
                <div class="owl-carousel">
                    @foreach($subcategory as $key => $subcategories)
                    <?php 
                    $photos = DB::table('photo')->where('sub_category_id',$subcategories->id)->where('product_id',null)->get(); ?>
                    <div class="item photo">
                        <i class="fa fa-ellipsis-v fa-2x" aria-hidden="true">
                            <img src="/image/SVG/dots.svg" class="img-responsive">
                        </i>
                        <div class="popup hidden">
                            <div class="contain">
                                <p class="text-bold" onclick="hiddeSub('{{$subcategories->id}}')">اخفاء هذه الفئة</p>
                                <i class="fa fa-caret-right" aria-hidden="true"></i>

                            </div>
                        </div>
                        
                            @if(count($photos) == 0)
                            <div class="main">
                                
                                <div class="inputUploader text-center">
                                    <img src="/image/400x400.png" class="img-responsive">
                                </div>
                            
                            </div>
                            @else
                            <div class="img">
                                <img src="/image/{{$photos[0]->photo}}" class="img-responsive cat-image">
                            </div>
                            @endif
                        
                        <div class="brand-name text-center text-bold">
                            <a href="#">{{$subcategories->name}}</a>
                            @if($key == $counter-1)
                            <input type="radio" name="gender" onchange="specificProducttwo('{{$subcategories->id}}')" id="genders1" checked value="{{$subcategories->id}}">
                            @else
                            <input type="radio" name="gender" onchange="specificProducttwo('{{$subcategories->id}}')" id="genders"  value="{{$subcategories->id}}">
                            @endif
                        </div>
                    </div>
                    
                    @endforeach

                </div>
            </div>
            <div class="myMap">
                <div class="pac-card" id="pac-card">
                  <div>
                    <div id="title">
                      ادخل موقعك على خرائط جوجل
                    </div>
                    
                    <div id="type-selector" class="pac-controls">
                      <input type="radio" name="type" id="changetype-all" checked="checked" class="hidden">
                    </div>
                    
                  </div>
                  <div id="pac-container">
                    <input id="pac-input" type="text"
                        placeholder="ادخل موقعك">
                  </div>
                </div>
                <div id="map"></div>
                <div id="infowindow-content">
                  <br>
                  <h5 id="place-name"  class="title"></h5>
                  <p id="place-address"></p>
                </div>
                <div id="addAddress">
                    <div class="title">
                        <h3>اضافه عنوان محلك</h3>
                    </div>
                    <div class="addresses">
                        <ul>
                            <li>
                                <?php $market3 = DB::table('market')->where('user_id',Auth::user()->id)->get();
                                $another_Address = DB::table('contact_info')->where('market_id',$market3[0]->id)->where('address','!=',null)->get(); ?>
                                <span class="places">
                                    <i class="fa fa-map-marker marker" aria-hidden="true"></i>
                                    <span class="text-bold">{{$market3[0]->address}}</span>
                                </span>
                                <a class="edit" href="#" data-target="#editAddress" data-toggle="modal" onclick="editAdd('{{$market3[0]->id}}','{{$market3[0]->address}}','1')">تعديل</a>
                                <div class="clear"></div>
                            </li>
                            
                            <!-- NEW CODE -->
                            
                            <li>
                                <?php $market3 = DB::table('market')->where('user_id',Auth::user()->id)->get(); ?>
                                <span class="places">
                                    <i class="fa fa-map-marker marker" aria-hidden="true"></i>
                                    <span class="text-bold">{{$market3[0]->special_mark}}</span>
                                </span>
                               
                                <div class="clear"></div>
                            </li>
                            
                            <!-- End -->
                            @foreach($another_Address as $another_Add)
                            <li>
                            <span class="places">
                                <i class="fa fa-map-marker marker" aria-hidden="true"></i>
                                <span class="text-bold">{{$another_Add->address}}</span>
                            </span>
                            <a class="edit" href="#" data-target="#editAddress" data-toggle="modal" onclick="editAdd('{{$another_Add->id}}','{{$another_Add->address}}','0')">تعديل</a>
                            <div class="clear"></div>
                            </li>
                            @endforeach
 
                        </ul>

                        <div class="addAnotherAddress">
                            <div class="customize">
                                <a class="primary">اضافه عنوان اخر</a>
                                <i class="fa fa-plus-square" aria-hidden="true" data-target="#newaddress" data-toggle="modal"></i>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="customizeHere col-xs-12">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script defer></script>
</section>
<!--End 0f Nav -->

<!-- Start 0f Main -->
<section id="bigPopUp" style="position:relative">
<div class="disabled-vendor"></div>
    <div class="container">
        <div class="row">
            <div class="wholeSection col-xs-12">
                <div class="row">
                   <!--  <div class="bigArrow">
                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                    </div> -->
                    
                    <div class="mainSection statistics col-xs-12" style="display: none">
                        <div class="row">
                            <div class="col-xs-12 col-md-4 text-center">
                                <h3 class="primary text-bold">عدد الزوار</h3>
                                @if(count($countersview) > 0 )
                               <span class="statsNum"> {{$countersview[0]->views}} </span>
                               @endif
                            </div>
                            <div class="col-xs-12 col-md-4 text-center">
                                <h3 class="primary text-bold">عدد المتابعين</h3>
                                @if(count($countersfollow) > 0 )
                                <span  class="statsNum"> <?php echo count($countersfollow); ?> </span>
                                @endif
                            </div>
                            <div class="col-xs-12 col-md-4 text-center">
                                <h3 class="primary text-bold">عدد الضغط على رقم الموبايل</h3>
                                @if(count($countersphone) > 0 )
                                <span  class="statsNum"> {{$countersphone[0]->phone}} </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="mainSection shopCat col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-md-9 products">
                                   
                            
                                <?php

                                $product2 = DB::table('product')->where('sub_category_id',$subcategory[$counter-1]->id)->where('market_id',$marketId[0]->id)->get(); ?>

                                @foreach($product2 as $products2)

                                
                                <div class="anProduct" id="productnum{{$products2->id}}">
                                    
                                    <!-- 3 icons -->
                                    <i class="fa fa-ellipsis-v fa-2x" aria-hidden="true">
                                         <img src="/image/SVG/dots.svg" class="img-responsive">
                                    </i>
                                    
                                    <div class="popup hidden">
                                        <div class="contain">
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                                        <?php $photos2 = DB::table('photo')->where('product_id',$products2->id)->get(); ?>
                                <ul class="options text-bold">
                                            
                                                            
                                    <li onclick="addtonew1('{{$products2->id}}')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li>
                                        
                                    <li onclick="addtosale1('{{$products2->id}}')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li>
                                    <li onclick="addtoAll1('{{$products2->id}}')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li>
                                   
                                    
                                    <?php
                                    $newdisc = preg_replace('/\s+/', '+', $products2->discription);
                                    $newname = preg_replace('/\s+/', '+', $products2->name);
                                     ?>
                                     <a href="/vendor/edit-product/{{$products2->id}}">   <li class="editProduct">تعديل المنتج</li> </a>
                                    <li class="deleteProduct" onclick="deleteproduct('{{$products2->id}}')">حذف المنتج</li>
                                </ul>

                                        </div>
                                    </div>
                                    <!-- 3 icons -->
                                    <div class="productPhoto">
                                        <img src="/image/{{$products2->photo}}" alt="{{$products2->name}}" class="img-responsive">
                                    </div>
                                    <div class="brand-name text-center text-bold">
                                        <a href="" class="shopName">
                                            <span>{{$products2->name}}</span>
                                            <div class="img-container">
                                                <img src="/image/{{$cinfo[0]->icon}}" class="img-responsive">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="productInfo">
                                        <p class="text-bold text-center nameOfProduct">{{$products2->discription}}</p>
                                        @if($products2->discount == '0')
                                        <p class="text-bold priceOfProduct">{{$products2->price}} جنيه مصرى</p>
                                        @else
                                        <p class="text-bold priceOfProduct">{{$products2->discount}} جنيه مصرى</p>
                                        
                                        <p class="text-bold deleted">{{$products2->price}} جنيه مصرى</p>
                                        @endif
                                    </div>
                                </div>
                          
                                @endforeach
                            </div>
				
                            <div class="uploadProduct col-xs-12 col-md-3">
                                <div class="uploadSec">
                                    <img src="/image/SVG/uploadPhoto.svg" class="img-responsive">
                                    <h3>اضافة منتج</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mainSection newOffer col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 products">
                             
                                <div class="anProduct">
                                    <!-- 3 icons -->
                                    <i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i>
                                    <div class="popup hidden">
                                        <div class="contain">
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                                           <ul class="options text-bold">
                                            <li class="addToNew">اضافه الى عروض جديده</li>
                                            <li class="addToSale">اضافة الى تخفيضات</li>
                                            <li class="addToAll">اضافة الى عروض جديده وتخفيضات</li>
                                            <li class="editProduct">تعديل المنتج</li>
                                            <li class="deleteProduct">حذف المنتج</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- 3 icons -->
                                    <div class="productPhoto">
                                        <img src="imgs/1.jpg" class="img-responsive">
                                    </div>
                                    <div class="brand-name text-center text-bold">
                                        <a href="" class="shopName">
                                            <span>اسم المحل</span>
                                            <div class="img-container">
                                                <img src="imgs/hosny.jpg" class="img-responsive">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="productInfo">
                                        <p class="text-bold text-center nameOfProduct">جاكيت اسود رياضى</p>
                                        <p class="text-bold priceOfProduct">200 جنيه مصرى</p>
                                        <p class="text-bold deleted">300 جنيه مصرى</p>
                                    </div>
                                </div>
                            </div>

                           
                        </div>
                    </div>

                    <div class="mainSection sale col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 products">
                                <div class="anProduct">
                                    <!-- 3 icons -->
                                    <i class="fa fa-ellipsis-v fa-2x" aria-hidden="true">
                                         <img src="/image/SVG/dots.svg" class="img-responsive">
                                    </i>
                                    <div class="popup hidden">
                                        <div class="contain">
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                                           <ul class="options text-bold">
                                            <li class="addToNew">اضافه الى عروض جديده</li>
                                            <li class="addToSale">اضافة الى تخفيضات</li>
                                            <li class="addToAll">اضافة الى عروض جديده وتخفيضات</li>
                                            <li class="editProduct">تعديل المنتج</li>
                                            <li class="deleteProduct">حذف المنتج</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- 3 icons -->
                                    <div class="productPhoto">
                                        <img src="imgs/1.jpg" class="img-responsive">
                                    </div>
                                    <div class="brand-name text-center text-bold">
                                        <a href="" class="shopName">
                                            <span>اسم المحل</span>
                                            <div class="img-container">
                                                <img src="imgs/hosny.jpg" class="img-responsive">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="productInfo">
                                        <p class="text-bold text-center nameOfProduct">جاكيت اسود رياضى</p>
                                        <p class="text-bold priceOfProduct">200 جنيه مصرى</p>
                                        <p class="text-bold deleted">300 جنيه مصرى</p>
                                    </div>
                                </div>
                            </div>

                           
                        </div>
                    </div>


                    <!--Start 0f Add Product Section-->
                    <div class="addProduct col-xs-12" id="uploadProduct">
                        <div class="row">
                            <div  class="X">
                                    <i class="fa fa-times fa-2x"></i>
                            </div>
                            <div class="reverseCols">
                                <div class="col-xs-12 col-md-7 productForm">
                                    
                                    <form method="POST" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        
                                        <input type="hidden" name="subCatId" value="{{$subcategory[$counter-1]->id}}" id="subCatId">
                                        <input type="hidden" name="save3" value="save3">
                                        <div class="inputForm">
                                            <div class="haveLength">
                                                <label>اسم المنتج</label>
                                                <input maxlength="50" type="text" id="productName" name="productname"  placeholder="لا يتعدى الاسم عن 4 كلمات">
                                                <span>50 حرف</span>

                                            </div>
                                        </div>
                        
                                        <div class="inputForm">
                                            <label class="addTo">اضافة الى</label>
                                            <select name="addTo" id="addTo" class="classification">
                                                <option value="0">اختر التصنيف</option>
                                                <option value="1">تخفيضات</option>
                                                <option value="2">عروض جديدة</option>
                                                <option value="3">كلاهما</option>
                                            </select>
                                            <span class="optional">اختيارى</span>
                                        </div>
                                        
                                        <div class="inputForm">
                                            <div class="haveLength">
                                                <label>سعر المنتج</label>
                                                <input type="text" id="productPrice" name="productPrice" value="" placeholder="السعر الاصلى للمنتج">
                                            </div>
                                        </div>
                                        
                                        <div class="salesPercentagePrice hidden">
                                            
                                            <div class="inputForm">
                                                <div class="haveLength">
                                                    
                                                    <div>
                                                        <label>نسبة التخفيض</label>
                                                        <input type="text" name="percentage">
                                                    </div>    
                                                </div>
                                            </div>
                                        
                                            <div class="inputForm">
                                                <div class="haveLength">
                                                    <label>سعر المنتج بعد التخفيض</label>
                                                    <input type="text" id="productPrices" name="productPrices"  placeholder="السعر الجديد">
                                                </div>
                                            </div>

                                            <div class="inputForm">
                                                <div class="haveLength">
                                                    <label class="productStatus">تاريخ الانتهاء من التخفيضات</label>
                                                    <input type="date" id="saleDeadLine" name="saleDate">
                                                </div>
                                            </div> 
                                        </div>
                                        

                                        <div class="inputForm news hidden">
                                            <div class="haveLength">
                                                <label class="productStatus">تاريخ الانتهاء من العروض الجديده</label>

                                                <input type="date" id="myDate" name="newDate"> 
                                            </div>
                                        </div>

                                        <div class="inputForm">
                                            <div class="haveLength">

                                                <label>وصف المنتج</label>
                                                <textarea maxlength="200" id="description" name="description" placeholder="اكتب وصف عن المنتج به معلومات قد تفيد عملائك"></textarea>
                                                <span class="textArea">200 حرف</span>
                                            </div>
                                        </div>
                                        <input type="hidden" name="userIds" id="userIDS" value="{{Auth::user()->id}}">
                                
                                            @foreach($attribute as $attributes)
                                            <div class="properties chooseColor">
                                                <h4>{{$attributes->name}}</h4>
    
                                                @foreach($attributes->properties as $property)
                                                <h5 class="colors">{{$property->name}}</h5>
                                                <?php $aprop = DB::table('sub_properties')->where('att_properties_id',$property->id)->get(); ?>
                                                <div class="clear"></div>
                                                @foreach($aprop as $aprops)
                                                <div class="col-xs-6 col-sm-4 col-md-2 text-center">
                                                    <label>{{$aprops->name}}</label>
                                                    <input type="checkbox" name="colors[]" value="{{$attributes->id}},{{$property->id}},{{$aprops->id}}">
                                                </div><!--id="colorId"-->
                                                @endforeach
                                                @endforeach
                                            </div>
                                            @endforeach
                    
                                        
                                        <div class="clear"></div>
                                        <button class="saveChanges">حفظ التغيرات</button>
                                        <div class="clear"></div>
                                    
                                </div>
                                
                                <div class="col-xs-12 col-md-5 productPhotosUploader">
                                    <div class="productPhotos photo">
                                        <div class="main">
                                            <div class="inputUploader text-center">
                                                <img src="/image/SVG/uploadWhite.svg" class="img-responsive photo-img">
                                                <p class="lead">اضف صورة للمنتج بجوده عاليه</p>
                                                <p class="lead">يحب ان يكون حجم الصوره 400x400</p>
                                            </div>
                                        </div>
                                        <div>
                                            <input type="file" name="image" accept="image/gif, image/jpeg, image/png, image/jpg">
                                        </div>
                                        <div class="img"></div>
                                    </div>
                                    <div class="centerMe">
                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                            
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image2"  accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>
                                            <div class="img"></div>
                                        </div>

                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                            
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image3"  accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>
                                            <div class="img"></div>
                                        </div>

                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image4"  accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>
                                            <div class="img"></div>
                                        </div>

                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image5"  accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>
                                            <div class="img"></div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                    <!--End 0f Add Product Section-->
                </div>
            </div>
        </div>
    </div>
</section>

<section id="restInfo">
    <div class="container-fluid">
        <div class="row">
            <form method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="save4" value="save4" enctype="multipart/form-data">
                <?php 
                $market_id4 = DB::table('market')->where('user_id',Auth::user()->id)->get();
                $contactinfo = DB::table('contact_info')->where('market_id',$market_id4[0]->id)->get();
                
                $phones = explode(',',$contactinfo[0]->phone); ?>
                <div class="col-xs-12 col-md-4 addons">
                    @foreach($phones as $phones1)
                    <div class="boxInput primary phoneBox">
                        
                            <i class="fa fa-phone"></i>
                            <input type="text" name="phone" value="{{$phones1}}" placeholder="ادخل رقم التواصل مع المحل">
                    </div>
                     @endforeach
                    <div class="addAnotherPhone">
                        <div class="customize">
                            <a class="primary">اضافة رقم اخر</a>
                            <i class="fa fa-plus-square primary" aria-hidden="true" data-toggle="modal" data-target="#newphone" ></i>
                        </div>
                    </div>
    
                    

                    <div class="links">
                        <div class="boxInput primary">
                            <i class="fa fa-desktop"></i>
                            <input type="text" name="website" value="{{$contactinfo[0]->site}}" placeholder="ادخل رابط موقعك الالكترونى اذا توفر">
                        </div>

                        <div class="boxInput primary">
                            <i class="fa fa-facebook"></i>
                            <input type="text" name="fb" value="{{$contactinfo[0]->fb}}" placeholder="ادخل رابط فيس بوك اذا توفر">
                        </div>

                        <div class="boxInput primary">
                            <i class="fa fa-youtube"></i>
                            <input type="text" name="yt" value="{{$contactinfo[0]->youtube}}" placeholder="ادخل رابط يوتيوب اذا توفر">
                        </div>

                        <div class="boxInput primary">
                            <i class="fa fa-instagram"></i>
                            <input type="text" name="in" value="{{$contactinfo[0]->inst}}" placeholder="ادل رابط انستجرام اذا توفر">
                        </div>

                        <div class="boxInput primary">
                            <i class="fa fa-twitter"></i>
                            <input type="text" name="tw" value="{{$contactinfo[0]->twitter}}" placeholder="ادخل رابط تويتر اذا توفر">
                        </div>

                        <div class="boxInput primary">
                            <button class="saveAllChanges">حفظ التغيرات</button>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-md-8">
                    <div class="row">
                        <div class="companyBrand col-xs-12 col-md-2">
                            <div class="photo img-uploader">
                                <div class="main">
                                    <div class="text-center cloud">
                                        <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                        <p>شعار المحل</p>
                                    </div>
                                </div>
                                <div class="absolute">
                                    <input type="file" name="companyIcon" class="myInput" accept="image/gif, image/jpeg, image/png, image/jpg">
                                </div>
                                <div class="img"><img class="img-responsive" src="/image/{{$contactinfo[0]->icon}}"></div>
                            </div>
                        </div>
                        
                        <div class="companyDesc col-xs-12 col-md-10">
                            <textarea name="description4" placeholder="اكتب وصف عن الشركه يساعد المستخدم فى معرفة معلومات اكثر عنك">{{$contactinfo[0]->description}}</textarea>
                        </div>
                        <div class="clear"></div>
                    

                        <div class="dt col-xs-12">
                            <div class="row">
                                <div class="delivery col-xs-12 col-md-6">
                                    <h4 class="workTime text-bold">
                                        <i class="fa fa-truck primary" aria-hidden="true"></i>
                                        خدمة التوصيل
                                    </h4>
                                    <select name="state" class="text-bold">
                                        @if($contactinfo[0]->delivery == 1)
                                        <option value="0">غير متاحة</option>
                                        <option value="1" selected="">متاحة</option>
                                        @else
                                        <option value="0" selected="">غير متاحة</option>
                                        <option value="1">متاحة</option>
                                        @endif
                                    </select>
                                </div>
                                <?php $market4 = DB::table('market')->where('user_id',Auth::user()->id)->get(); ?>
                                <div class="col-xs-12 col-md-5">
                                    <h4 class="workTime text-bold">
                                        <i class="fa fa-clock-o primary fa-lg" aria-hidden="true"></i>
                                        مواعيد العمل
                                        <a class="primary editTime" data-toggle="modal" href="#" onclick="editTime('{{$market4[0]->id}}','{{$market4[0]->work_day_from}}','{{$market4[0]->work_day_to}}','{{$market4[0]->work_hours_from}}','{{$market4[0]->work_hours_to}}')" data-target="#edit">تعديل</a>
                                    </h4>
                                    <p class="fromTo secondary text-bold">
                                        من يوم <span id="editDfrom">
                                        @switch($market4[0]->work_day_from)
                                            @case(1)
                                                السبت
                                            @break
                                            @case(2)
                                                الاحد
                                            @break
                                            @case(3)
                                                الأثنين
                                            @break
                                            @case(4)
                                                الثلاثاء
                                            @break
                                            @case(5)
                                                الأربعاء
                                            @break
                                            @case(6)
                                                الخميس
                                            @break
                                            @case(7)
                                                الجمعة
                                            @break
                                        @endswitch
                                    </span>
                                        الى يوم <span id="editDto">
                                        @switch($market4[0]->work_day_to)
                                            @case(1)
                                                السبت
                                            @break
                                            @case(2)
                                                الاحد
                                            @break
                                            @case(3)
                                                الأثنين
                                            @break
                                            @case(4)
                                                الثلاثاء
                                            @break
                                            @case(5)
                                                الأربعاء
                                            @break
                                            @case(6)
                                                الخميس
                                            @break
                                            @case(7)
                                                الجمعة
                                            @break
                                        @endswitch
                                    </span>
                                        
                                        من &nbsp;<span id="editHfrom">{{$market4[0]->work_hours_from}}</span>&nbsp; <span></span>
                                        الى &nbsp;<span id="editHto">{{$market4[0]->work_hours_to}}</span>&nbsp; <span></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>   
    </div>
</section>
<div id="edit" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="title">تعديل مواعيد العمل</h3>
         </div>
         <div class="modal-body">
            <div class="row">

               <div class="col-md-12">
                   <form method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="ids" id="ids">
                    <input type="hidden" name="save6" id="save6" value="save6">
                    <input type="hidden" name="fromeditday" id="fromeditday">
                    <input type="hidden" name="toeditday" id="toeditday">
                       <div class="form-group">
                         <label for="name">من يوم</label>
                         <select name="phone10" id="phone10">
                            <option value="السبت">السبت</option>
                            <option value="الأحد">الاحد</option>
                            <option value="الأثنين">الاتنين</option>
                            <option value="الثلاثاء">الثلاثاء</option>
                            <option value="الأربعاء">الأربعاء</option>
                            <option value="الخميس">الخميس</option>
                            <option value="الجمعة">الجمعة</option>
                        </select>
                       </div>
                       <div class="form-group">
                         <label for="">إلى يوم</label>
                         <select name="phone" id="phone">
                            <option value="السبت">السبت</option>
                            <option value="الأحد">الاحد</option>
                            <option value="الأثنين">الاتنين</option>
                            <option value="الثلاثاء">الثلاثاء</option>
                            <option value="الأربعاء">الأربعاء</option>
                            <option value="الخميس">الخميس</option>
                            <option value="الجمعة">الجمعة</option>
                        </select>
                       </div>
                       <div class="form-group">
                         <label for="">منذ الساعة</label>
                         <input type="text" class="form-control" id="email" name="email" value="0" required="">
                       </div>
                       <div class="form-group">
                         <label for="">إلى الساعة</label>
                         <input type="text" class="form-control"  placeholder="المكان" id="country" name="country" value="0" required="">
                       </div>
                    

                       <button ><strong>الحفظ</strong></button>
                     </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asignToall" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="title">تعديل</h3>
         </div>
         <div class="modal-body">
            <div class="row">

               <div class="col-md-12">
                   <form method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="save15" id="save15" value="save15">
                    <input type="hidden" name="productSaleId1" id="productSaleId1" value="0">
                        <div class="form-group">
                            <label>تاريخ الانتهاء من العرض</label>
                             <input type="date" class="form-control"  id="newDateEnd1" name="newDateEnd1"  required>
                           </div>
                       <div class="form-group">
                        <label>تاريخ الانتهاء من التخفيض</label>
                         <input type="date" class="form-control"  id="saleDateEnd1" name="saleDateEnd1"  required>
                       </div>
                       <div class="form-group">
                        <label>نسبة التخفيض</label>
                         <input type="text" class="form-control"  id="salepercentage1" name="salepercentage1" placeholder="نسبة التخفيض" required>  
                       </div>
                       <div class="form-group">
                        <label>السعر الجديد</label>
                         <input type="text" class="form-control"  id="priceafterpercentage1" name="priceafterpercentage1"  required>
                       </div>
                      
                    

                       <button ><strong>حفظ</strong></button>
                     </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asignSale" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="title">تعديل</h3>
         </div>
         <div class="modal-body">
            <div class="row">

               <div class="col-md-12">
                   <form method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="save14" id="save14" value="save14">
                    <input type="hidden" name="productSaleId" id="productSaleId" value="0">

                       <div class="form-group">
                        <label>تاريخ الانتهاء من التخفيض</label>
                         <input type="date" class="form-control"  id="saleDateEnd" name="saleDateEnd"  required="">
                       </div>
                       <div class="form-group">
                        <label>نسبة التخفيض</label>
                         <input type="text" class="form-control"  id="salepercentage" name="salepercentage"  required="">
                       </div>
                       <div class="form-group">
                        <label>السعر الجديد</label>
                         <input type="text" class="form-control"  id="priceafterpercentage" name="priceafterpercentage"  required="">
                       </div>
                      
                    

                       <button ><strong>حفظ</strong></button>
                     </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asignproduct" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="title">تعديل</h3>
         </div>
         <div class="modal-body">
            <div class="row">

               <div class="col-md-12">
                   <form method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="save13" id="save13" value="save13">
                    <input type="hidden" name="idsTonew" id="productIds" value="0">

                       <div class="form-group">
                        <label>تاريخ الانتهاء من العرض</label>
                         <input type="date" class="form-control"  id="newDateEnd" name="newDateEnd"  required="">
                       </div>
                      
                    

                       <button ><strong>حفظ</strong></button>
                     </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="editAddress" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="title">تعديل العنوان</h3>
         </div>
         <div class="modal-body">
            <div class="row">

               <div class="col-md-12">
                   <form method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="save7" id="save7" value="save7">
                    <input type="hidden" name="anotheradd" value="0" id="anotheradd">
                    <input type="hidden" name="idsAdd" value="0" id="idsAdd">
                   
                       <div class="form-group">
                         <input type="text" class="form-control"  id="oldaddress" name="oldaddress" value="0" required="">
                       </div>
                      
                    

                       <button ><strong>الحفظ</strong></button>
                     </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="newaddress" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="title">اضافة عنوان اخر</h3>
         </div>
         <div class="modal-body">
            <div class="row">

               <div class="col-md-12">
                   <form method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="save8" id="save8" value="save8">

                       <div class="form-group">
                         <input type="text" class="form-control"  id="newaddress" name="newaddress"  required="">
                       </div>
                      
                    

                       <button ><strong>حفظ</strong></button>
                     </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="newphone" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="title">اضافة رقم اخر</h3>
         </div>
         <div class="modal-body">
            <div class="row">

               <div class="col-md-12">
                   <form method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="save9" id="save9" value="save9">

                       <div class="form-group">
                         <input type="text" class="form-control"  id="newphone" name="newphone"  required="">
                       </div>
                      
                    

                       <button ><strong>حفظ</strong></button>
                     </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="editProduct" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="title">تعديل بيانات المنتج</h3>
         </div>
         <div class="modal-body addProduct">
            <div class="row">

               <div class="col-md-12">
                   <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save16" id="save16" value="save16">
                    <input type="hidden" name="ids16" value="0" id="ids16">
                        <div class="row">
                            <div class="reverseCols">
                                <div class="col-xs-12 col-md-7 productForm">
                           
                                        <input type="hidden" name="subCatId16" value="{{$subcategory[$counter-1]->id}}" id="subCatId">
                                        <div class="inputForm">
                                            <div class="haveLength">
                                                <label>اسم المنتج</label>
                                                <input maxlength="50" type="text" id="productName16" name="productname16" value="0"  placeholder="لا يتعدى الاسم عن 4 كلمات">
                                                <span>50 حرف</span>

                                            </div>
                                        </div>

                                        <div class="inputForm">
                                            <label class="addTo">اضافة الى</label>
                                            <select name="addTo16" id="addTo" class="classification">
                                                <option value="0">اختر التصنيف</option>
                                                <option value="1">تخفيضات</option>
                                                <option value="2">عروض جديدة</option>
                                                <option value="3">كلاهما</option>
                                            </select>
                                            <span class="optional">اختيارى</span>
                                        </div>
                                        
                                        <div class="inputForm">
                                            <div class="haveLength">
                                                <label>سعر المنتج</label>
                                                <input type="text" id="productPrice16" name="productPrice16" value="0" placeholder="السعر الاصلى للمنتج">
                                            </div>
                                        </div>
                                        
                                        <div class="salesPercentagePrice hidden">
                                            <div class="inputForm">
                                                <label>نسبة التخفيض</label>
                                                <input type="text" name="percentage16" id="percentage16">
                                            </div>    
    
                                            <div class="inputForm">
                                                <div class="haveLength">
                                                    <label>سعر المنتج بعد التخفيض</label>
                                                    <input type="text" id="discount16" name="discount16"  placeholder="السعر الجديد">
                                                </div>
                                            </div>
    
                                            <div class="inputForm">
                                                <div class="haveLength">
                                                    <label class="productStatus">تاريخ الانتهاء من التخفيضات</label>
                                                    <input type="date" id="saleDeadLine16" name="saleDate16">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="inputForm news hidden">
                                            <div class="haveLength">
                                                <label class="productStatus">تاريخ الانتهاء من العروض الجديده</label>

                                                <input type="date" id="myDate16" name="newDate16"> 
                                            </div>
                                        </div>

                                        

                                        <div class="inputForm">
                                            <div class="haveLength">

                                                <label>وصف المنتج</label>
                                                <textarea maxlength="200" name="description16" id="description16" value="0" placeholder="اكتب وصف عن المنتج به معلومات قد تفيد عملائك"></textarea>
                                                <span class="textArea">200 حرف</span>
                                            </div>
                                        </div>
                                        <input type="hidden" name="userIds" id="userIDS" value="{{Auth::user()->id}}">
                                        @foreach($attribute as $attributes)
                                        <div class="properties chooseColor">
                                            <h4>{{$attributes->name}}</h4>

                                            @foreach($attributes->properties as $property)
                                            <h5 class="colors">{{$property->name}}</h5>
                                            <?php $aprop = DB::table('sub_properties')->where('att_properties_id',$property->id)->get(); ?>
                                            <div class="clear"></div>
                                            @foreach($aprop as $aprops)
                                            <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                                                <label>{{$aprops->name}}</label>
                                                <input type="checkbox" name="colors16" value="{{$attributes->id}},{{$property->id}},{{$aprops->id}}"><!--id="colorId"-->
                                            </div>
                                            @endforeach
                                            @endforeach
                                        </div>
                                        @endforeach
                                        
                                        <div class="clear"></div>
                                    
                                </div>
                                
                                <div class="col-xs-12 col-md-5 productPhotosUploader">
                                    <div class="productPhotos photo">
                                        <div class="main">
                                            <div class="inputUploader text-center">
                                                <img src="/image/SVG/uploadWhite.svg" class="img-responsive photo-img">
                                                <p class="lead">اضف صورة للمنتج بجوده عاليه</p>
                                                <p class="lead">يحب ان يكون حجم الصوره 400x400</p>
                                            </div>
                                        </div>
                                        <div>
                                            <input type="file" name="image16" accept="image/gif, image/jpeg, image/png, image/jpg">
                                        </div>
                                        <div class="img"><img class="img-responsive" id="image16" src="0"></div>
                                    </div>
                                    <p class="text-center text-bold lead">كود المنتج: <span class="productCode"></span></p>
                                    <div class="centerMe">
                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image17"  accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>    
                                            <div class="img"><img id="image17" class="img-responsive" src=""></div>
                                        </div>

                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image18"  accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>
                                            
                                            <div class="img"><img id="image18" class="img-responsive" src=""></div>
                                        </div>

                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                            
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image19"   accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>
                                            
                                            <div class="img"><img id="image19" class="img-responsive" src=""></div>
                                        </div>

                                        <div class="anotherSides photo">
                                            <div class="main">
                                                <div class="inputUploader text-center">
                                                    <img src="/image/SVG/uploadWhite.svg" class="img-responsive">
                                                    <p class="lead">ارفع صورة من زاوية اخرى</p>
                                            	
                                                </div>
                                            </div>
                                            <div>
                                                <input type="file" name="image20"  accept="image/gif, image/jpeg, image/png, image/jpg">
                                            </div>
                                            
                                            <div class="img"><img id="image20" class="img-responsive" src=""></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                      
                    

                       <button ><strong>حفظ</strong></button>
                     </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php $marketId10 = DB::table('market')->where('user_id',Auth::user()->id)->get();
$contact_info10 = DB::table('contact_info')->where('market_id',$marketId10[0]->id)->get(); ?>
@endsection
<script src="{{asset('vendor/js/jquery-3.2.1.min.js')}}"></script>
<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPZjhRY2jUckGQNh0f_tc7jo2dw18GRz4&libraries=places"></script>
<script defer>
 
    function initMap() {
    	store_id = "{{$marketId10[0]->id}}"; 
        $.ajax({
            type:'GET',
            url:'/api/get/location',
            data: {
        	id: store_id
      	    },
            success: function(data){
//                console.log(data);
            if(data[0].lat == null){
                var map = new google.maps.Map(document.getElementById('map'), {

                  center: {lat: 30.0594838, lng: 31.2934839},
                  zoom: 13,
                  
                  styles: [
                  {
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#f5f5f5"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.icon",
                    "stylers": [
                      {
                        "visibility": "off"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#616161"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                      {
                        "color": "#f5f5f5"
                      }
                    ]
                  },
                  {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#bdbdbd"
                      }
                    ]
                  },
                  {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#eeeeee"
                      }
                    ]
                  },
                  {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#757575"
                      }
                    ]
                  },
                  {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#e5e5e5"
                      }
                    ]
                  },
                  {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#9e9e9e"
                      }
                    ]
                  },
                  {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#ffffff"
                      }
                    ]
                  },
                  {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#757575"
                      }
                    ]
                  },
                  {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#dadada"
                      }
                    ]
                  },
                  {
                    "featureType": "road.highway",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#616161"
                      }
                    ]
                  },
                  {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#9e9e9e"
                      }
                    ]
                  },
                  {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#e5e5e5"
                      }
                    ]
                  },
                  {
                    "featureType": "transit.station",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#eeeeee"
                      }
                    ]
                  },
                  {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#c9c9c9"
                      }
                    ]
                  },
                  {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#9e9e9e"
                      }
                    ]
                  }
                ]
                });
            }else{
                var map = new google.maps.Map(document.getElementById('map'), {

                  center: {lat: data[0].lat, lng: data[0].lng},
                  zoom: 13,
                  styles: [
                  {
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#f5f5f5"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.icon",
                    "stylers": [
                      {
                        "visibility": "off"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#616161"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                      {
                        "color": "#f5f5f5"
                      }
                    ]
                  },
                  {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#bdbdbd"
                      }
                    ]
                  },
                  {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#eeeeee"
                      }
                    ]
                  },
                  {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#757575"
                      }
                    ]
                  },
                  {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#e5e5e5"
                      }
                    ]
                  },
                  {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#9e9e9e"
                      }
                    ]
                  },
                  {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#ffffff"
                      }
                    ]
                  },
                  {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#757575"
                      }
                    ]
                  },
                  {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#dadada"
                      }
                    ]
                  },
                  {
                    "featureType": "road.highway",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#616161"
                      }
                    ]
                  },
                  {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#9e9e9e"
                      }
                    ]
                  },
                  {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#e5e5e5"
                      }
                    ]
                  },
                  {
                    "featureType": "transit.station",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#eeeeee"
                      }
                    ]
                  },
                  {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#c9c9c9"
                      }
                    ]
                  },
                  {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#9e9e9e"
                      }
                    ]
                  }
                ]
                });
            }
            var card = document.getElementById('pac-card');
            var input = document.getElementById('pac-input');
            var types = document.getElementById('type-selector');
            var strictBounds = document.getElementById('strict-bounds-selector');

            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

            var autocomplete = new google.maps.places.Autocomplete(input);

            
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            if(data[0].lat == null){
            	    var marker = new google.maps.Marker({
	                position: {lat: 30.0594838, lng: 31.2934839},
	          	map: map,
	            });
            }else{
	            var marker = new google.maps.Marker({
	                position: {lat: data[0].lat, lng: data[0].lng},
	          	map: map,
	            });
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            autocomplete.addListener('place_changed', function() {
              infowindow.close();
              marker.setVisible(false);
              var place = autocomplete.getPlace();
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
              }

              // If the place has a geometry, then present it on a map.
              if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);

              } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
              }
              marker.setPosition(place.geometry.location);
              var latLang = marker.getPosition();//marker.getPosition(place.geometry.location);
              
                $.ajax({
                    type:'POST',
                    url:'/vendor/control-panel',
                    data:{save10:'save10',lat:latLang.lat(),lang:latLang.lng()},
                    success: function(data){
//                        console.log(data);
                        },
                    error: function(){
                        console.log('error');
                    }
                });

             // alert(latLang.lng());
              $("#infowindow-content a").show();
              marker.setVisible(true);

              var address = '';
              if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
              }

            //   infowindowContent.children['place-icon'].src = place.icon;
              infowindowContent.children['place-name'].textContent = place.name;
              infowindowContent.children['place-address'].textContent = address;
              infowindow.open(map, marker);

            });

            // Sets a listener on a radio button to change the filter type on Places
            // Autocomplete.

            $("#pac-input").focus(function() {
                $(this).parent().css("border-bottom", "3px solid #11998e");
            });

            $("#pac-input").focusout(function() {
                $(this).parent().css("border-bottom", "0");
            });

        },
            error: function(){
                console.log('error');
            }
        });
    }
// END 0f MAP
    setTimeout(function(){
        initMap();
    },1000);
    
    function editPro(data,nameData,phoneData,emailData,countryData,saleData,disData,percentageData,imgData1,imgData2,imgData3,imgData4,imgData5) {
               
                id =  document.getElementById('ids16');
                id.value  = data;
                
      		newdiscription1 = emailData.replace(/\+/g,' ');
 		newname1 = nameData.replace(/\+/g,' ');
			    
			    
                olddayname = document.getElementById('productName16');
                olddayname.value = newname1;

                oldprice = document.getElementById('productPrice16');
                oldprice.value = phoneData;
                
                olddesc = document.getElementById('description16');
                olddesc.value = newdiscription1;

                oldnewDate = document.getElementById('myDate16');
                oldnewDate.value =   countryData;

                oldsaleDate = document.getElementById('saleDeadLine16');
                oldsaleDate.value =   saleData;

                olddis = document.getElementById('discount16');
                olddis.value =   newdiscription1;

                oldpercentage = document.getElementById('percentage16');
                oldpercentage.value =   percentageData; 

                if(imgData1 !== null){
                    oldimage1 = document.getElementById('image16');
                    oldimage1.src =   '/image/'+imgData1;
                    $(oldimage1).parent().parent().find(".main").addClass("hidden");
                    
                }
                if(imgData2 !== null){
                    oldimage2 = document.getElementById('image17');
                    oldimage2.src =   '/image/'+imgData2; 
                    $(oldimage2).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData3 !== null){
                    oldimage3 = document.getElementById('image18');
                    oldimage3.src =   '/image/'+imgData3; 
                    $(oldimage3).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData4 !== null){
                    oldimage4 = document.getElementById('image19');
                    oldimage4.src =   '/image/'+imgData4;
                    $(oldimage4).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData5 !== null){
                    oldimage5 = document.getElementById('image20');
                    oldimage5.src =   '/image/'+imgData5;
                    $(oldimage5).parent().parent().find(".main").addClass("hidden");
                }
                $(".productCode").text($("#ids16").val());
                testImgContainer();
               // window.alert(data);
    }
    function editTime(data,nameData,phoneData,emailData,countryData) {

                id =  document.getElementById('ids');
                id.value  = data;
                olddayfrom = document.getElementById('fromeditday');
                olddayfrom.value = nameData;
                olddayto = document.getElementById('toeditday');
                olddayto.value = phoneData;
                user_name = document.getElementById('phone10');
                    if(nameData == '1'){
                        user_name.value = 'السبت';
                    }else if(nameData == '2'){
                        user_name.value = 'الأحد';
                    }else if(nameData == '3'){
                        user_name.value = 'الأثنين';
                    }else if(nameData == '4'){
                        user_name.value = 'الثلاثاء';
                    }else if(nameData == '5'){
                        user_name.value = 'الأربعاء';
                    }else if(nameData == '6'){
                        user_name.value = 'الخميس';
                    }else if(nameData == '7'){
                        user_name.value = 'الجمعة';
                    }
                mob = document.getElementById('phone');             

                if(phoneData == '1'){
                        mob.value = 'السبت';
                    }else if(phoneData == '2'){
                        mob.value = 'الأحد';
                    }else if(phoneData == '3'){
                        mob.value = 'الأثنين';
                    }else if(phoneData == '4'){
                        mob.value = 'الثلاثاء';
                    }else if(phoneData == '5'){
                        mob.value = 'الأربعاء';
                    }else if(phoneData == '6'){
                        mob.value = 'الخميس';
                    }else if(phoneData == '7'){
                        mob.value = 'الجمعة';
                    }
                
                
                user_email = document.getElementById('email');
                user_email.value = emailData;
                user_country = document.getElementById('country');
                user_country.value = countryData;
                
               
               // window.alert(data);
    }
    function editAdd(data,nameData,emailData) {
        ids = document.getElementById('idsAdd');
        ids.value = data;
        user_name = document.getElementById('oldaddress');
        user_name.value = nameData;
        another_adds = document.getElementById('anotheradd');
        another_adds.value = emailData;
               
    }
    function addtonew(data) {
        
        ids = document.getElementById('productIds');
        ids.value = data;
               
    }
    function addtonew1(data) {
        $("#asignproduct").modal("show");
        ids = document.getElementById('productIds');
        ids.value = data;
               
    }
    function addtosale(data) {
        ids = document.getElementById('productSaleId');
        ids.value = data;
               
    }
    function addtosale1(data) {
        $("#asignSale").modal("show");
        ids = document.getElementById('productSaleId');
        ids.value = data;
               
    }
    function addtoAll(data) {
        ids = document.getElementById('productSaleId1');
        ids.value = data;
    }
    function addtoAll1(data) {
        $("#asignToall").modal("show");
        ids = document.getElementById('productSaleId1');
        ids.value = data;
               
    }
    function testImgContainer() {
        const containers = $(".img");
        
        containers.each(function(index) {
            if($(containers[index]).find("img").length) {
                let src = $(containers[index]).find("img").attr("src");
                if(src.search(/jpg/i) != -1 || src.search(/gif/i) != -1 || src.search(/jpeg/i) != -1 || src.search(/png/i) != -1) {
                    $(containers[index]).show();
                    $(containers[index]).parent().find(".main").addClass("hidden");
                } else {
                    $(containers[index]).hide();
                    $(containers[index]).parent().find(".main").removeClass("hidden");
                }
            } else {
                $(containers[index]).hide();
                $(containers[index]).parent().find(".main").removeClass("hidden");
            }
        });
    }
    testImgContainer();
    function editPro1(data,nameData,phoneData,emailData,countryData,saleData,disData,percentageData,imgData1,imgData2,imgData3,imgData4,imgData5) {
        
            $("#editProduct").modal("show");
            
           	newdiscription1 = disData.replace(/\+/g,' ');
 		newname1 = nameData.replace(/\+/g,' ');
 		
                id =  document.getElementById('ids16');
                id.value  = data;
                olddayname = document.getElementById('productName16');
                olddayname.value =newname1;

                oldprice = document.getElementById('productPrice16');
                oldprice.value = phoneData;
                
                olddesc = document.getElementById('description16');
                olddesc.value = emailData;

                oldnewDate = document.getElementById('myDate16');
                oldnewDate.value =   countryData;

                oldsaleDate = document.getElementById('saleDeadLine16');
                oldsaleDate.value =   saleData;

                olddis = document.getElementById('discount16');
                olddis.value =   newdiscription1;

                oldpercentage = document.getElementById('percentage16');
                oldpercentage.value =   percentageData;
             

                if(imgData1 !== 'null'){
                    oldimage1 = document.getElementById('image16');
                    oldimage1.src =   '/image/'+imgData1;
                    $(oldimage1).parent().parent().find(".main").addClass("hidden");
                    
                }
                if(imgData2 !== 'null'){
                    oldimage2 = document.getElementById('image17');
                    oldimage2.src =   '/image/'+imgData2; 
                    $(oldimage2).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData3 !== 'null'){
                    oldimage3 = document.getElementById('image18');
                    oldimage3.src =   '/image/'+imgData3; 
                    $(oldimage3).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData4 !== 'null'){
                    oldimage4 = document.getElementById('image19');
                    oldimage4.src =   '/image/'+imgData4;
                    $(oldimage4).parent().parent().find(".main").addClass("hidden");
                }
                if(imgData5 !== 'null'){
                    oldimage5 = document.getElementById('image20');
                    oldimage5.src =   '/image/'+imgData5;
                    $(oldimage5).parent().parent().find(".main").addClass("hidden");
                }

               // window.alert(data);
                $(".productCode").text($("#ids16").val());
               testImgContainer();
    }
    
    function newproducts(){
        
        $('.anProduct').remove();

        user_id = document.getElementById('userIDS').value;
        $('input:radio').attr("checked", false);
        $('#genders1').prop("checked", true);
        
        $.ajax({
            type:'post',
            url:'/api/add-product',
            data:{save5:'save5',newpro:'newpro',userids:user_id},
            success: function(data){
//                console.log(data);
                for(var i = 0; i<data.pro.length; i++){
                    $.ajax({
                        type:'post',
                        url:'/api/add-product',
                        data:{save5:'save5',newphoto:'newphoto',productids:data.pro[i].id,userids:user_id},
                        success: function(data){
                            newdiscription = data.pro[0].discription.replace(/ /g,'+');
                            newname = data.pro[0].name.replace(/ /g,'+');
//                            console.log(data);
                            if(data.pro[0].discount == '0'){
        
                                if(data.photo.length == 1)
                                {
                                    
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew1('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li  onclick="addtosale1('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li  onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  alt="'+newdiscription+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 2){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew1('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li  onclick="addtosale1('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li  onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 3){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew1('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li  onclick="addtosale1('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li  onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><<a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 4){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew1('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li  onclick="addtosale1('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li  onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else{
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew1('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li  onclick="addtosale1('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li  onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }
                            }else{
                                if(data.photo.length == 1)
                                {
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 2){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><<a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 3){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else if(data.photo.length == 4){
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }else{
                                    movespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(movespecificprod);
                                }
                            }
                            movespecificprod="";
                        }
                    });
                }
                
            },
            error: function(){
                console.log('error');
            }
        });
    }
    function discount(){
        $('.anProduct').remove();
        $('.products > ul').remove();
        user_id = document.getElementById('userIDS').value;
        $('input:radio').attr("checked", false);
        $('#genders1').prop("checked", true);
        

        $.ajax({
            type:'post',
            url:'/api/add-product',
            data:{save5:'save5',sale:'sale',userids:user_id},
            success: function(data){
                
//                console.log(data);
                for(var i = 0; i<data.pro.length; i++){
                	    newdiscription = data.pro[0].discription.replace(/ /g,'+');
			    newname = data.pro[0].name.replace(/ /g,'+');
			    if(data.pro[i].discount == '0'){
                        if(data.photo.length == 1)
                        {
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[i].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[i].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[i].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><<a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[i].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[i].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[i].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 2){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[i].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[i].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[i].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[i].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[i].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[i].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 3){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[i].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[i].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[i].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[i].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[i].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[i].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 4){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[i].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[i].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[i].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[i].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[i].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[i].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else{
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[i].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[i].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[i].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[i].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[i].photo+'" class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[i].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[i].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[i].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }
                    }else{
                        $.ajax({
                            type:'post',
                            url:'/api/add-product',
                            data:{save5:'save5',salephoto:'salephoto',productids:data.pro[i].id,userids:user_id},
                            success: function(data){
                               
//                                console.log(data);
                            
                        if(data.photo.length == 1)
                        {
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 2){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 3){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else if(data.photo.length == 4){
                            salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                            $('.products').append(salespecificprod);
                        }else{
                                salespecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                
                                $('.products').append(salespecificprod);
                            }
                       
                        },
                        });
                      }  
                    }
                    
                    salespecificprod = "";
           
              }
            });
       }
                
       function specificProduct(Data){
        
        subData = document.getElementsByName('gender');
        user_id = document.getElementById('userIDS').value;
        for (var i = 0; i < subData.length; i++){
            if (subData[i].checked)
            {
                $("#subCatId").val(subData[i].value);
                //console.log(user_id);
            }
        }
        $('.anProduct').remove();
        $.ajax({
            type:'post',
            url:'/api/add-product',
            data:{save5:'save5',subId:Data,userids:user_id},
            success: function(data){
//                console.log(data);
                for(var i = 0; i<data.pro.length; i++){
                    $.ajax({
                        type:'post',
                        url:'/api/add-product',
                        data:{save5:'save5',newphoto:'newphoto',productids:data.pro[i].id,userids:user_id},
                        success: function(data){
                            newdiscription = data.pro[0].discription.replace(/ /g,'+');
			    newname = data.pro[0].name.replace(/ /g,'+');
//                            console.log(data);
                            if(data.pro[0].discount == '0'){
                                if(data.photo.length == 1)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 2)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 3)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 4)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else{
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+newdiscription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }
                            }else{
                                if(data.photo.length == 1)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+newname+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 2)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 3)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else if(data.photo.length == 4)
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><<a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }else
                                {
                                    allspecificprod = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprod);
                                }
                            }
                            allspecificprod="";
                        }
                    });
                }
                
            },
            error: function(){
                console.log('error');
            }
        });
    }

    function specificProducttwo(Data){
        
        subData = document.getElementsByName('gender');
        user_id = document.getElementById('userIDS').value;
        for (var i = 0; i < subData.length; i++){
            if (subData[i].checked)
            {
                $("#subCatId").val(subData[i].value);
                //console.log(user_id);
            }
        }
        $('.anProduct').remove();
        $.ajax({
            type:'post',
            url:'/api/add-product',
            data:{save5:'save5',subId:Data,userids:user_id},
            success: function(data){
//                console.log(data);
                for(var i = 0; i<data.pro.length; i++){
                    $.ajax({
                        type:'post',
                        url:'/api/add-product',
                        data:{save5:'save5',newphoto:'newphoto',productids:data.pro[i].id,userids:user_id},
                        success: function(data){
                            newdiscription = data.pro[0].discription.replace(/ /g,'+');
               			newname2 = 'khale+d';
               			newname	   = data.pro[0].name.replace(/ /g,'+'); 
			    	console.log(newname);
			    	 
			    
//                            console.log(data);
                            if(data.pro[0].discount == '0'){
                                if(data.photo.length == 1)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else if(data.photo.length == 3)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else if(data.photo.length == 4)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else{
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li></ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+newdiscription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+newdiscription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }
                            }else{
                                if(data.photo.length == 1)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else if(data.photo.length == 2)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else if(data.photo.length == 3)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else if(data.photo.length == 4)
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }else
                                {
                                    allspecificprodtwo = '<div id="productnum'+data.pro[0].id+'" class="anProduct"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true"><img src="/image/SVG/dots.svg" class="img-responsive"></i><div class="popup hidden"><div class="contain"><i class="fa fa-caret-right" aria-hidden="true"></i><ul class="options text-bold"><li   onclick="addtonew('+data.pro[0].id+')" data-target="#asignproduct" data-toggle="modal" >اضافه الى عروض جديده</li><li    onclick="addtosale('+data.pro[0].id+')" data-target="#asignSale" data-toggle="modal" >اضافة الى تخفيضات</li><li    onclick="addtoAll('+data.pro[0].id+')" data-target="#asignToall" data-toggle="modal">اضافة الى عروض جديده وتخفيضات</li><a href="/vendor/edit-product/'+data.pro[0].id+'"><li class="editProduct">تعديل المنتج</li></a><li class="deleteProduct" onclick="deleteproduct('+data.pro[0].id+')">حذف المنتج</li><ul></div></div><div class="productPhoto"><img src="/image/'+data.pro[0].photo+'" alt="'+data.pro[0].discription+'"  class="img-responsive"></div><div class="brand-name text-center text-bold"><a href="" class="shopName"><span>'+data.pro[0].name+'</span><div class="img-container"><img src="/image/'+data.cinfo[0].icon+'" class="img-responsive"></div></a></div><div class="productInfo"><p class="text-bold text-center nameOfProduct">'+data.pro[0].discription+'</p><p class="text-bold priceOfProduct">'+data.pro[0].discount+' جنيه مصرى</p><p class="text-bold deleted">'+data.pro[0].price+' جنيه مصرى</p></div></div>';
                                    $('.products').append(allspecificprodtwo);
                                }
                            }
                            allspecificprodtwo="";
                        },
                    });
                }
                
            },
            error: function(){
                console.log('error');
            }
        });
    
    }
    
    function deleteproduct(Data){
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type:'post',
                    url:'/api/add-product',
                    data:{save12:'save12',product_id:Data},
                    success: function(data){
//                        console.log(data);
                        $('#productnum'+Data).remove();
                    },
                    error: function(){
                        console.log('error');
                    }
                });            
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
               $(this).parents(".anProduct").remove();
            }

        });
        
    }
    function hiddeSub(data){
//        console.log(data);
        $.ajax({
            type:'POST',
            url:'/vendor/hide-sub',
            data:{ids:data},
            success: function(data){
//                console.log(data);
            },
            error: function(){
                console.log('error');
            }
        });
    }
    function deletesub(data){
//        console.log(data);
        $.ajax({
            type:'POST',
            url:'/api/vendor/delete-sub',
            data:{ids:data},
            success: function(data){
                //const x = "#kinds" + data;
               // console.log(x);
                $("#kinds" + data).remove();
//                console.log(data);
            },
            error: function(){
                console.log('error');
            }
        });
    }
    function roomMsg(Data,Sender,Blocked){
        room_id = document.getElementById('roomIds');
        room_id.value = Data;
        
        blockId = document.getElementById('blockname');
        blockId.value = Sender;
        
        if(Blocked > 0){
            $('#parent_block span').html('unblock');
        }else{
            $('#parent_block span').html('block');
        }
        $.ajax({
            type:'POST',
            url:'/api/getmsg',
            data:{roomId:Data},
            success: function(data){
//                console.log(data);
                $('.textBox > div').remove();
                for(i=0; i<data.length; i++){
                    if(data[i].sender_id == Sender){
                        
//                        recivermsg = '<p class="secondary text-left">'+data[i].message+'</p>';
//                        recivermsg2 = '<p class="secondary text-left">'+data[i].created_at+'</p>';
//                        $('.textBox').append(recivermsg);
//                        $('.textBox').append(recivermsg2);
                        msg = '<div class="msg msg-left"><div class="img-container"><img src="/image/'+data[i].sender.photo+'" class="img-responsive"></div><div class="client-title"><h5>'+data[i].sender.user_name+'</h5></div><div class="clearfix"></div><div class="msgtxt"><p class="secondary text-left">'+data[i].message+'</p><p class="secondary text-left">'+data[i].created_at+'</p></div></div>';
                        $('.textBox').append(msg);
                    }else{
//                    	sendermsg = '<p class="primary">'+data[i].message+'</p>';
//                    	sendermsg2 = '<p class="primary">'+data[i].created_at+'</p>';
//                        $('.textBox').append(sendermsg);
//                        $('.textBox').append(sendermsg2);
                        
                        msg = '<div class="msg msg-right"><div class="img-container"><img src="/image/'+data[i].sender.photo+'" class="img-responsive"></div><div class="client-title"><h5>'+data[i].sender.user_name+'</h5></div><div class="clearfix"></div><div class="msgtxt"><p class="primary">'+data[i].message+'</p><p class="primary">'+data[i].created_at+'</p></div></div>';
                        $('.textBox').append(msg);

                    }
                }
                let height = 0;
                $('.textBox').children().each(function(i, value){
                    height += parseInt($(this).height());
                });
                height += '';
                $('.textBox').animate({scrollTop: height});
            },
            error: function(){
                console.log('error');
            }
        });
    }
    function newmsg(){

        $('#newmsgForum1').submit(function(e){
            e.preventDefault();
            var form = $('#newmsgForum1');
            $.ajax({
                type:'POST',
                url:'/vendor/control-panel',
                data:form.serialize(),
                dataType:'json',

                success: function(data){
//                    console.log(data);
                    if(data == "block"){
	                    swal();
				swal({
				title: "you are blocked this user",
				text: " "
				});
                    }
                    
                },
                error: function(){
                    console.log('error');
                }
            });
        });
        
    }
    function block(Data){
	    USERID = document.getElementById('blockname').value;
        
            $.ajax({
                type:'POST',
                url:'/api/block',
                data:{vendor_id:"{{Auth::user()->id}}",user_id:USERID,block:Data},

                success: function(data){
//                    console.log(data);
                    if(data == 'unblock success'){
                        $('#parent_block span').html('block');
                    }else{
                        $('#parent_block span').html('unblock');
                    }
                  //  alert('block success');
                    
                },
                error: function(){
                    console.log('error');
                }
            });
        
        
    }
    function addcomment(){
        $('#replaycommentForum').submit(function(e){
          e.preventDefault();
          var form = $('#replaycommentForum');
          $.ajax({
            type:'POST',
            url:'/vendor/control-panel',
            data:form.serialize(),
            dataType:'json',

            success: function(data){
//                console.log(data);
            
            },
            error: function(){
                console.log('error');
            }
          });
        });
        
    }
    function changespsw(){
	
	        $('#pswForum').submit(function(e){
	            e.preventDefault();
	            var form = $('#pswForum');
	            $.ajax({
	                type:'POST',
	                url:'/user/profile',
	                data:form.serialize(),
	                dataType:'json',
	
	                success: function(data){
//	                    console.log(data);
	                    if(data == 'psw changed success'){
	                    	swal();
				swal({
				title: "تم تعديل كلمه المرور بنجاح",
				text: " "
				});
	
	                    }else{
	                    	swal();
				swal({
				title: "كلمه المرور قديمه غير صحيحة",
				text: " "
				});
	                    }
	                    
	                },
	                error: function(){
	                    console.log('error');
	                }
	            });
	        });
        
    }

</script>