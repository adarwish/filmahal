<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"> <!--This is added by me-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Vendor Dashboard</title>
     <link rel="apple-touch-icon" sizes="180x180" href="http://filmahal.com/imgs/favIcon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="http://filmahal.com/imgs/favIcon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="http://filmahal.com/imgs/favIcon/favicon-16x16.png">
    <link rel="manifest" href="http://filmahal.com/imgs/favIcon/manifest.json">
    <link rel="mask-icon" href="http://filmahal.com/imgs/favIcon/safari-pinned-tab.svg" color="#38ef7d">
    <meta name="theme-color" content="#ffffff">
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" sizes="32x32" href="{{asset('imgs/favIcon/favicon-32x32.png')}}">
    <link rel="shortcut icon" type="image/png" sizes="16x16" href="{{asset('imgs/favIcon/favicon-16x16.png')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/css/emojionearea.min.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/owlcarousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/sweetalert2.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('vendor/css/main1.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/main0.css')}}">

    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php $dynamic_data = DB::table('dynamic_home')->get(); ?>
    	
    <div class="loaderContainer">
        <div class="loader"></div>
    </div>
    <header class="vendor-header">
        <nav>
            <div class="container">
                <div class="row"> 
                    <div class="right-side col-xs-12 col-md-6">
                        <div class="img-container">
                          <a href="/">
                            <img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo">
                          </a>
                        </div>
                    </div>
                    <ul class="left-side col-xs-12 col-md-6">
                        <li class="notification-icon">
                            <div class="fa fa-bell-o fa-2x" aria-hidden="true">
                                <img src="/image/SVG/bell.svg" class="img-responsive">
                                <i class="fa fa-caret-up" aria-hidden="true"></i>
                            </div>
                            <div class="slide-down">
                                
                                @if(Auth::user())
                                <?php $notifications = DB::table('noti')->where('user_id',Auth::user()->id)->get();
                                $notifications2 = DB::table('noti')->where('user_id',Auth::user()->id)->limit(3)->orderBy('id','DESC')->get();
                                $counter = count($notifications);
                                
                                $old_noti = DB::table('noti_counter')->where('user_id',Auth::user()->id)->get();
                                $counter_noti = count($old_noti);
                                if($counter_noti >0){
                                    DB::table('noti_counter')->where('user_id',Auth::user()->id)->update(['counter'=>$counter]);
                                }else{
                                    DB::table('noti_counter')->insert(['counter'=>$counter,'user_id'=>Auth::user()->id]);
                                }

                                 ?>

                                <ul class="messages-list noti nav-drop-alter">
                                    @if($counter !==0)
                                    @foreach($notifications2 as $noti)
                                    <?php $marketnoti = DB::table('users')->where('id',$noti->action_id)->get();
                                     ?>
                                     @if(count($marketnoti) > 0)
                                    <li class="message">
                                        <a href="#">
                                            <div class="client">
                                                <div class="message-header">
                                                    <div class="img-container">
                                                        <img src="/image/{{$marketnoti[0]->photo}}" class="img-responsive">
                                                    </div>              
                                                    <div class="client-title">
                                                        <h5>{{$marketnoti[0]->user_name}}</h5>
                                                        <p>{{$noti->data}}</p>
                                                        <p class="time">
                                                            <i class="fa fa-phone"></i>
                                                            {{$noti->created_at}}
                                                        </p>
                                                    </div>
                                            
                                                </div>
                                                <div>
                                                    
                                                </div>
                                            
                                            </div>
                                        </a>
                                    </li>
                                    @endif
                                    @endforeach
                                    @endif

                                                            
                                    <li class="more-messages">
                                        <p><a href="/all-notification">شاهد جميع الاشعارات</a></p>
                                        <p><a href="/all-notification"><font id="countnoti">{{$counter}} اشعار اخر</font></a></p>
                                        <div class="clear"></div>
                                    </li>
                                </ul>
                                @endif
                            </div>
                        </li>
                        
                        <li class="messages">
                            <div class="fa fa-envelope-o fa-2x" aria-hidden="true">
                                <img src="/image/SVG/envelope.svg" class="img-responsive">
                                <i class="fa fa-caret-up" aria-hidden="true"></i>
                            </div>
                            
                            <div class="slide-down">
                                
                                @if(Auth::user())
                                <?php $room = DB::table('room')->where('sender_id',Auth::user()->id)->Orwhere('reciver_id',Auth::user()->id)->limit(3)->get();
                              
                                $counter = count($room);
                                
                               
                                 ?>

                                <ul class="messages-list nav-drop-alter">
                                    @if($counter > 0)
                                    @foreach($room as $rooms)
                                    <?php 
                                    $message = DB::table('message')->where('room_id',$rooms->id)->orderBy('id','DESC')->get();
                                    $sender = DB::table('users')->where('id',$message[0]->sender_id)->get();
                                    $reciver = DB::table('users')->where('id',$message[0]->reciver_id)->get();
                                    ?>
                                    <li class="message">
                                        <a href="#">
                                            <div class="client">
                                                <div class="message-header">
                                                    <div class="img-container">
                                                        <img src="/image/{{$sender[0]->photo}}" class="img-responsive">
                                                    </div>              
                                                    <div class="client-title">
                                                        <h5>{{$sender[0]->user_name}}</h5>
                                                        <p>{{$message[0]->message}}</p>
                                                        <p class="time">
                                                            <i class="fa fa-phone"></i>
                                                            <?php

                                                    $timestamp = $rooms->created_at;
                                                    $datetime = explode(" ",$timestamp);
                                                    $date = $datetime[0];
                                                    $time = $datetime[1];
                                                    echo $time;?>
                                                           
                                                        </p>
                                                    </div>
                                            
                                                </div>
                                                <div>
                                                    
                                                </div>
                                            
                                            </div>
                                        </a>
                                    </li>
                                    @endforeach
                                    @endif
                                    <li class="more-messages">
                                        <p><a href="/vendor/control-panel#boxMessages"  class="boxMessages" id="boxMessages">شاهد جميع الرسائل</a></p>
                                        <p><a href="/vendor/control-panel#boxMessages"><font id="countnoti">{{$counter}} رسالة إخرى</font></a></p>
                                        <div class="clear"></div>
                                    </li>
                                    
                                </ul>
                                @endif
                            </div>
                        </li>

                        <li class="profile">
                            <i class="fa fa-angle-down fa-lg" aria-hidden="true">
                                <i class="fa fa-caret-up" aria-hidden="true"></i>
                            </i>
                            <img src="/image/SVG/profile.svg" class="img-responsive">
                            
                            <div>
                                
                                <ul class="profile-settings">
                                    <li>
                                        <a href="#reviewsPeople" class="reviewsPeople" id="reviewsPeople">
                                            <i class="ion ion-edit" aria-hidden="true"></i>
                                            اراء الناس فى محلك
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#boxMessages" class="boxMessages" id="boxMessages">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            صندوق الرسائل
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#settingProfile" class="settingProfile" id="settingProfile">
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                            اعدادات حسابى
                                        </a>
                                    </li>

                                    <li>
                                        <a href="/logout" class="logout">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            تسجيل الخروج
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
        </nav>
        
        
        <div class="container">
            <div class="row text-center">
                <form method="POST" id="searchForum">
                    {{csrf_field()}}
                    <input type="hidden" name="save17" value="save17">
                    <div class="search">
                        <input type="search"  name="search" id="looking" placeholder="أبحث بكود المُنتج ,رقم منتج">
                        <button data-toggle="modal" data-target="#editProduct" onclick="searchfor()"><i class="fa fa-search fa-2x"></i></button>
                    </div>
                </form>            
            </div>
        </div>       
    </header>
    @yield('content')
    <?php $contact_us = DB::table('contact_us')->get(); ?>
    <!--Start Of Footer Section-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="left-section col-xs-12 col-md-6 col-lg-4">
                    <div class="headerWord">
                        <h3 class="text-bold">الدعم الفنى وخدمه العملاء</h3>
                    </div>
                    <div class="contact-us">
                        <div class="phones">

                            <i class="fa fa-phone"></i>

                            <div class="text-color-grey numbers text-bold">
                                <span>
                                    {{$dynamic_data[1]->data}}
                                </span>
                                <span>
                                     {{$dynamic_data[2]->data}}
                                </span>
                            </div>

                        </div>

                        <div class="email">
                            <span class="text-bold">
                                <i class="fa fa-envelope"></i> 
                                <span class="text-color-grey">
                                    {{$contact_us[3]->data}}
                                </span>
                            </span>
                        </div>

                        <div class="whatsApp">
                            <div class="text-bold">
                                <span class="whatsApp-icon"></span> 
                                <span class="text-color-grey">
                                    {{$contact_us[2]->data}}
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="follow-us">
                        <h2>
                            تابعنا
                        </h2>
                       <div class="social-icons">
                            <ul>
                                <a href="{{$dynamic_data[3]->data}}" target="_blank" class="facebook">
                                    <li>
                                        <i class="fa fa-facebook fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$dynamic_data[5]->data}}" target="_blank" class="instagram">
                                    <li>
                                        <i class="fa fa-instagram fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$dynamic_data[7]->data}}" target="_blank" class="googlePlus">
                                    <li>
                                        <i class="fa fa-google-plus fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$dynamic_data[6]->data}}" target="_blank" class="twitter">
                                    <li>
                                        <i class="fa fa-twitter fa-2x"></i>
                                    </li>
                                </a>
                                <a href="{{$dynamic_data[4]->data}}" target="_blank" class="youtube">
                                    <li>
                                        <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
                                    </li>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="center-section col-xs-12 col-md-6 col-lg-4">
                    <div class="headerWord">
                        <h3 class="text-bold">اشترك ليصلك كل جديد</h3>
                    </div>
                    <h2 class="text-bold">
                           <font id="msg"> أرسل لى رساله عندما يكون مُتاح </font>
                    </h2>
                    <form method="POST" id="subsForum">
                        {{csrf_field()}}
                        <input type="hidden" name="sub1" value="sub1">
                        <input type="email" name="emailsub" placeholder="البريد الالكترونى">
                        <button class="text-bold subscribeBtn" onclick="newsubuser()" type="submit">أرسل</button>
                    </form>
                    
                    <div class="categories col-xs-6" style="padding: 0;">
                        <h3 class="text-bold">لأصحاب المحلات</h3>
                        <ul>
                            <li><a class="text-color-grey text-bold" href="/vendor/register" target="_blank">اضف محلك</a></li>
                        </ul>
                    </div>
                    <div class="categories col-xs-6" style="padding-left: 30px; padding-right: 0;">
                        <h3 class="text-bold">أقسام الموقع</h3>
                        <ul>
                            <li><a class="text-color-grey text-bold" href="/mobile" target="_blank">تطبيق الموبيل</a></li>
                            <li><a class="text-color-grey text-bold" href="/technical-support" target="_blank">الدعم الفنى</a></li>
                            <li><a class="text-color-grey text-bold" href="/FAQ" target="_blank">الأسئلة الأكثر شيوعاً</a></li>
                            <li><a class="text-color-grey text-bold" href="/ads-with-us" target="_blank">أعلن معنا</a></li>
                            <li><a class="text-color-grey text-bold" href="/copyright" target="_blank">الملكية الفكرية</a></li>
                            <li><a class="text-color-grey text-bold" href="/blog" target="_blank">المدونة</a></li>
                        </ul>
                    </div>
                    
                </div>
                
                <div class="right-section col-xs-12 col-lg-4">
                    <div class="img-container">
                    <a href="/"><img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo"></a>
                    </div>
                    <h3 class="text-bold">
                        من نحن ؟ ولماذا تنضم الينا ؟
                    </h3>
                    <ul>
                    	<li><a class="text-color-grey text-bold" href="/about-us" target="_blank"><i class="fa fa-check fa-lg"></i>من نحن ؟</a></li>
                        <li><a class="text-color-grey text-bold" href="/terms-condition" target="_blank"><i class="fa fa-check fa-lg"></i>شروط وجود محلك فى الموقع ؟</a></li>
                        <li><a class="text-color-grey text-bold" href="/privileges" target="_blank"><i class="fa fa-check fa-lg"></i>مميزات وجوده معنا ؟</a></li>
                    </ul>
                </div>
            </div> 
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="footer-bottom text-center">
                    <div class="copy-right col-xs-12 col-md-9">
                        <p>Developed By <a style="color: #777; margin-right: 20px;" href="http://www.wasiladev.com" target="_blank">WasilaDev</a></p>
                        <p>جميع الحقوق محفوظه لشركه فى المحل دوت كوم</p>
                    </div>

                    <div class="conditions col-xs-12 col-md-3">
                        <a href="/privacy">سياسة الخصوصية</a>
                        <a href="/rules">احكام وشروط</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<!--End Of Footer Section-->
    
<!--    <script src="{{asset('vendor/js/jquery-3.2.1.min.js')}}"></script>-->
    <script src="{{asset('vendor/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendor/owlcarousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('vendor/js/sweetalert2.min.js')}}"></script>
    <script src="{{asset('vendor/js/watermark.min.js')}}"></script>
    <script src="{{asset('vendor/js/readIMG.js')}}"></script>
    <script src="{{asset('js/js.js')}}"></script>

    <script src="{{asset('vendor/js/emojionearea.min.js')}}"></script>

    <script src="{{asset('vendor/js/main.js')}}"></script>
    <script src="{{asset('js/msgs.js')}}"></script>
    <script>
        function newsubuser(){
            $('#subsForum').submit(function(e){
              e.preventDefault();
              var form = $('#subsForum');
              $.ajax({
                type:'POST',
                url:'/',
                data:form.serialize(),
                dataType:'json',
                success: function(data){
//                    console.log(data);
                    $('#msg').html('تم الأشتراك بنجاح');
                    $("#msg").fadeIn(3000);
                    setTimeout(function() {
                       $('#msg').fadeOut(2000);
                    }, 10000);
                }
              });

            });
        };
    </script>
    <script defer>
    
        function searchfor(){
            //searchVal = document.getElementById('looking').value; 
            $('#searchForum').submit(function(e){
                console.log('1');
                e.preventDefault();
                var form = $('#searchForum');
                $.ajax({
                    type:'post',
                    url:'/vendor/control-panel',
                    data:form.serialize(),
                    dataType:'json',
    //                data:{save17:'save17',search:searchVal},
  //  data,nameData,phoneData,emailData,countryData,saleData,disData,percentageData,imgData1,imgData2,imgData3,imgData4,imgData5
                    success: function(data){
                        console.log(data.products[0].id);
                        if(data.photo.length == 1){
                            editPro(data.products[0].id,data.products[0].name,data.products[0].price,data.products[0].discription,data.products[0].end_new,data.products[0].end_discount,data.products[0].discount,data.products[0].pricentage,data.products[0].photo,data.photo[0].photo,null,null,null);
                        }else if(data.photo.length == 2){
                            editPro(data.products[0].id,data.products[0].name,data.products[0].price,data.products[0].discription,data.products[0].end_new,data.products[0].end_discount,data.products[0].discount,data.products[0].pricentage,data.products[0].photo,data.photo[0].photo,data.photo[1].photo,null,null);
                        }else if(data.photo.length == 3){
                            editPro(data.products[0].id,data.products[0].name,data.products[0].price,data.products[0].discription,data.products[0].end_new,data.products[0].end_discount,data.products[0].discount,data.products[0].pricentage,data.products[0].photo,data.photo[0].photo,data.photo[1].photo,data.photo[2].photo,null);
                        }else if(data.photo.length == 4){
                            editPro(data.products[0].id,data.products[0].name,data.products[0].price,data.products[0].discription,data.products[0].end_new,data.products[0].end_discount,data.products[0].discount,data.products[0].pricentage,data.products[0].photo,data.photo[0].photo,data.photo[1].photo,data.photo[2].photo,data.photo[3].photo);
                        }else{
                            editPro(data.products[0].id,data.products[0].name,data.products[0].price,data.products[0].discription,data.products[0].end_new,data.products[0].end_discount,data.products[0].discount,data.products[0].pricentage,data.products[0].photo,null,null,null,null);
                        }
                    },
                    error: function(){
                        console.log('error');
                    }
                });
            });
        }
    </script>
    @if(Auth::user()->status == 0)
    		
    	@if(Auth::user()->status_view == 1)
    		<script>
    			setTimeout(function() {
    				swal();
				swal({
				title: "1!",
				text: " "
				});
    			}, 5000);
    		 </script>
    	@elseif(Auth::user()->status_view == 2)
    		
    		<script>
    			setTimeout(function() {
    				swal();
				swal({
				title: "2!",
				text: " "
				});
    			}, 5000);
    		 </script>
    	@elseif(Auth::user()->status_view == 3)
    		<script>
    			setTimeout(function() {
    				swal();
				swal({
				title: "3",
				text: " "
				});
    			}, 5000);
    		 </script>
    	@elseif(Auth::user()->status_view == 4)
		
		<script>
    			setTimeout(function() {
    				swal();
				swal({
				title: "4",
				text: " "
				});
    			}, 5000);
    		 </script>    	
    	@else
    		<script>
    			setTimeout(function() {
    				swal();
				swal({
				title: "5",
				text: " "
				});
    			}, 5000);
    		 </script>
    		
    	
    	@endif
    	
    <script defer>
        setTimeout(function() {
         $("input, select, textarea").prop("disabled", true);
        $(".allSettings input").prop("disabled", false);
    }, 1000);
    </script>
    <script defer>
    
    setTimeout(function(){
    	$('.disabled-vendor').addClass('disabled-vendor-active');
    }, 1001)
    
    </script>
    
    @endif
    <script>
    setInterval(function(){
            setTimeout(function getnoti(){
                user_ID = "{{Auth::user()->id}}";
                oldnotificationlength = "{{$counter}}";
                counters = 0;
                $.ajax({
                    type:'POST',
                    url:'/api/get/notification',
                    data:{noti:1,userId:user_ID},
                    success: function(data){
//                        console.log(data);

                        if(data.notifications.length > data.counter[0].counter){
				$('.notification-icon').append('<span class="label label-warning" style="height:25px;">'+data.notifications.length+'</span>');
                            if(data.notifications.length > 3){
				
                                for(i=0; i<3; i++){
                                    $('.message').remove();

                                    $('.more-messages').remove();
                                     
                                    $.ajax({
                                        type:'POST',
                                        url:'/api/get/notification2',
                                        data:{noti:2,userId:user_ID,notiId:data.notifications[i].id},
                                        success: function(data){
//                                              console.log(data);
                                             
                                           notis = '<li class="message"><a href="#"><div class="client"><div class="message-header"><div class="img-container"><img src="/image/'+data.user[0].photo+'" class="img-responsive"></div><div class="client-title"><h5>'+data.user[0].user_name+'</h5><p>'+data.newnotifications[0].data+'</p><p class="time"><i class="fa fa-phone"></i>'+data.newnotifications[0].created_at+'</p></div></div><div></div></div></a></li>';
                                        
                                            $('.noti').append(notis);

                                        }
                                        
                                    });

                                    notis = "";
                                }

                            }else{
                            	$('.notification-icon').append('<span class="label label-warning" style="height:25px;">'+data.notifications.length+'</span>');
                                for(i=0; i<data.notifications.length; i++){
                                   $('.message').remove();

                                    $('.more-messages').remove();
                                    
                                    $.ajax({
                                        type:'POST',
                                        url:'/api/get/notification2',
                                        cache: false,
                                        data:{noti:2,userId:user_ID,notiId:data.notifications[i].id},
                                        success: function(data){
                                             if(counters > 1){
						return;
					     }
                                          notis = '<li class="message"><a href="#"><div class="client"><div class="message-header"><div class="img-container"><img src="/image/'+data.user[0].photo+'" class="img-responsive"></div><div class="client-title"><h5>'+data.user[0].user_name+'</h5><p>'+data.newnotifications[0].data+'</p><p class="time"><i class="fa fa-phone"></i>'+data.newnotifications[0].created_at+'</p></div></div><div></div></div></a></li>';
                                        
                                            $('.noti').append(notis);

                                        }

                                    });
                                    notis = "";
                                }
                            }
                           /* notis2 = '<li class="more-messages"><p><a href="#">شاهد جميع الاشعارات</a></p><p><a href="#"><font id="countnoti">'+oldnotificationlength+' اشعار اخر</font></a></p><div class="clear"></div></li>';
                            $('.noti').append(notis2);*/
                        }
                    },
                    error: function(){
                        console.log('error');
                    }
                  });
                
            }, 1000);
        }, 5000);
    </script>
</body>
</html>