@extends('site.user.master')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-xs-12 reviewsBox">
      <div class="reviews">
        <h4 class="primary text-bold">تقييمات المحل</h4>
        <div class="clear"></div>
      </div>
       @foreach($comment as $comments)
       <?php $user = DB::table('users')->where('id',$comments->user_id)->get(); ?>
      <div class="person-review">
        <div class="message-header">
          <div class="img-container">
            <img src="/image/{{$user[0]->photo}}" class="img-responsive">
          </div>
          <div class="client-title">
            <h4>{{$comments->comment}}</h4>

            <p class="timeEn">
              {{$comments->created_at}}</p>
          </div>
        </div>
       
      </div>
  @endforeach
     
        
      </div>
    </div>
  </div>
</div>



@endsection