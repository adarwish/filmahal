@extends('site.user.master')

@section('content')
<section id="profile_settings">
    <div class="navigation">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p class="navigationText text-bold">
                        <a href="#" id="mainPage">الرئيسيه</a> >> اعدادات
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="container vendorProfile">
        <div class="row">
            <div class="col-xs-12 nameEmail">
                <div class="message-header">
                    <div class="img-container">
                        <img src="/image/{{Auth::user()->photo}}" class="img-responsive">
                    </div>              
                    <div class="client-title">
                        <h4>{{Auth::user()->user_name}}</h4>
                        <p>{{Auth::user()->email}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row settings">

            <div class="col-xs-12 col-md-9 inbox">
                <div class="reviews row">
                    <h4 class="primary text-bold">صندوق الرسائل</h4>
                </div>
                <div class="row">
                    <div class="col-xs-9">
                        <div class="row col-reverse">
                            <div class="col-xs-12 textBox">
                               <div><p class="primary"></p>
                               <p class="secondary text-left"></p></div>
                            </div>
                            <div class="col-xs-12 msg">
                                <div class="row">
                                    <form method="POST" id="newmsgForum1">
                                        {{csrf_field()}}
                                        <input type="hidden" name="save3" value="save3">
                                        <input type="hidden" name="roomIds" id="roomIds" value="0">
                                        <div class="col-xs-2">
                                            <div class="row">
                                                <div class="messenger">
                                                    <i class="fa fa-thumbs-o-up fa-lg" aria-hidden="true"></i>
                                                    <i class="fa fa-camera fa-lg" aria-hidden="true">
                                                        <input type="file" class="myInput" accept="image/gif, image/jpeg, image/png, image/jpg">
                                                    </i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-10">
                                            <div class="row">
                                                <textarea   name="message" id="emoji" ></textarea>

                                                
                                            </div>
                                        </div>
                                        <button class="send-message-chat show" onclick="newmsg()" >إرسال</button>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="col-xs-3 messages-box">
                        <div class="row">
                            <ul class="messages-list">
                                @foreach($room as $rooms)
                                <?php $sender = DB::table('users')->where('id',$rooms->sender_id)->get();
                                $msg = DB::table('message')->where('room_id',$rooms->id)->get();
                                $reciver = DB::table('users')->where('id',$rooms->reciver_id)->get();
                                $storereciver = DB::table('market')->where('user_id',$reciver[0]->id)->get(); ?>
                                <li onclick="roomMsg('{{$rooms->id}}','{{$rooms->sender_id}}')">
                                    <a href="#">
                                        <div class="client">
                                            <div class="message-header">
                                                <div class="img-container">
                                                    <img src="/image/{{$sender[0]->photo }}" class="img-responsive">
                                                </div>              
                                                <div class="client-title">
                                                    <h5>{{$storereciver [0]->title}}</h5>
                                                    <p>{{$msg[0]->message}}</p>
                                                    <p class="time">
                                                    <?php

                                                    $timestamp = $rooms->created_at;
                                                    $datetime = explode(" ",$timestamp);
                                                    $date = $datetime[0];
                                                    $time = $datetime[1];
                                                    echo $time;?></p>
                                                </div>
                                        
                                            </div>
                                            <div>
                                                
                                            </div>
                                        
                                        </div>
                                    </a>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    

                </div>
            </div>

            <div class="col-xs-12 col-md-9 reviewsBox">
                @foreach($follow as $followers)
                    <?php  $photo = DB::table('photo')->where('market_id',$followers->market_id)->where('sub_category_id',null)->where('product_id',null)->get();
                    $product = DB::table('product')->where('market_id',$followers->market_id)->where('new',1)->orderBy('id','ASC')->get();
                    $product2 = DB::table('product')->where('market_id',$followers->market_id)->where('discount','!=',1)->where('pricentage','!=',0)->where('end_discount','!=',0)->orderBy('id','ASC')->get();
                    $contact_phone = DB::table('contact_info')->where('market_id',$followers->market_id)->select('phone')->get();
                    $phones = count($contact_phone);
                    $mplace = DB::table('market_place')->where('market_id',$followers->market_id)->get();
                     ?>
                <table>
                    <thead>
                        <tr>
                            <th>
                                حذف المحل
                                <i  onclick="removefollow('{{$followers->id}}')" class="fa fa-times fa-lg" aria-hidden="true"></i>
                            </th>
                            <th>منتجات جديدة</th>
                            <th>تخفيضات</th>
                            <th>رقم التليفون</th>
                            <th>العنوان</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td>
                              <a href="/store/profile/{{$followers->market_id}}/{{$mplace[0]->city_id}}/{{$mplace[0]->area_id}}/search">  <div class="td">
                                    <div class="img-container">
                                        <img src="/image/{{$photo[0]->photo}}" class="img-responsive">
                                    </div>
                                    <p class="primary text-bold text-center">{{$followers->market->title}}</p> </a>
                                </div>
                            </td> 
                            <td>
                                <div class="td">
                                @if(count($product) >0)
                                    <a href="#" class="text-bold"><?php echo count($product); ?> منتج جديد</a>
                                    
                                    <p class="text-bold">تحديث <?php $date=date_create($product[0]->updated_at);
                                    echo date_format($date,"Y-m-d"); ?> </p>
                                </div>
                                @endif
                            </td>
                            <td>
                            @if(count($product2) >0)
                                <div class="td">
                                    <a href="#" class="text-bold"><?php echo count($product2); ?> منتج في التخفيض</a>
                                    <p class="text-bold">تحديث <?php $date=date_create($product2[0]->updated_at);
                                    echo date_format($date,"Y-m-d"); ?></p>
                                </div>
                             @endif
                            </td>
                            <td>
                                <div class="td">
                                    @for($s=0; $s<$phones; $s++)
                                    <p class="text-bold">{{$contact_phone[$s]->phone}}</p>
                                    @endfor
                                </div>
                            </td>
                            <td>
                                <div class="td">
                                    <p>
                                        
                                        <i class="fa fa-map-marker primary fa-lg" aria-hidden="true"></i>
                                        <p class="rtl">
                                            {{$followers->market->address}}
                                        </p>
                                    </p>
                                </div>
                            </td>
                        </tr>
                       
                    </tbody>
                    
                </table>
                 @endforeach
            </div>

            <div class="col-xs-12 col-md-9 allSettings">
               
                

                <div class="specificElments" id="restInfo">
                    <form method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="save1" value="save1">
                        <div class="photo img-uploader">
                            <div class="main">
                                <div class="text-center cloud">
                                    <i class="fa fa-cloud-upload fa-lg" aria-hidden="true"></i>
                                    <p>صوره الشخصيه</p>
                                </div>
                            </div>
                            <div class="absolute">
                                <input type="file" name="photo"  accept="image/gif, image/jpeg, image/png, image/jpg">
                            </div>
                            <div class="img"><img src="/image/{{Auth::user()->photo}}" class="img-responsive"></div>
                        </div>
                        <div class="infoProfile">
                            <div class="box">
                                <label for="username" class="text-right">اسم المستخدم</label>
                                <input type="text" name="username" value="{{Auth::user()->user_name}}">
                                <span class="text-bold text-left"></span>
                            </div>

                            <div class="box">
                                <label for="email" class="text-right">الاميل</label>
                                <input type="email" name="email" value="{{Auth::user()->email}}">
                                <span class="text-bold text-left"></span>
                            </div>

                           
                        </div>

                        <button class="editProfileSettings">حفظ</button>
                    </div>
                </form>
            </div>

            <div class="col-xs-12 col-md-9 passwordConfig" style="display: none">
              <div class="specificElments" id="restInfo">
                  <form method="POST" enctype="multipart/form-data" id="pswForum">
                  	{{csrf_field()}}
                      <input type="hidden" name="changepsw" value="changepsw">
                      <div class="infoProfile">
                          <div class="box">
                              <label for="oldPassword" class="text-right">كلمة المرور القديمة</label>
                              <input type="password" id="oldPassword" name="oldPassword" value="">
                              <span class="text-bold text-left"></span>
                          </div>

                          <div class="box">
                              <label for="newPassword" class="text-right">كلمة المرور الجديدة</label>
                              <input type="password" id="newPassword" name="newPassword" value="">
                              <span class="text-bold text-left"></span>
                          </div>
                      </div>

                      <button class="editProfileSettings" onclick="changespsw()">حفظ</button>
                  </div>
              </form>
          </div>

            <div id="third-step2">
                        <div class="col-xs-9 bg-white">
                            <div class="choose-kinds">
                                <h3 class="text-bold">فئات المحل</h3>
                                <i id="close2" class="fa fa-times fa-2x" aria-hidden="true"></i>
                                <div class="clear"></div>
                                <p>اختر جميع الفئات الموجوده فى محل</p>
                            </div>
                            <div class="chooseCat">
                                <div class="col-xs-2 col-md-3 text-center">
                                    <label>للرجال</label>
                                    <input type="checkbox" name="kinds" value="mens">
                                </div>
                                <div class="col-xs-2 col-md-3 text-center">
                                    <label>احذية</label>
                                    <input type="checkbox" name="kinds" value="mens">
                                </div>
                                <div class="col-xs-2 col-md-3 text-center">
                                    <label>للرجال</label>
                                    <input type="checkbox" name="kinds" value="mens">
                                </div>
                                <div class="col-xs-2 col-md-3 text-center">
                                    <label>للرجال</label>
                                    <input type="checkbox" name="kinds" value="mens">
                                </div>
                            </div>
                            <div>
                                <p class="text-center error text-bold" id="errorCates"></p>
                            </div>
                            <div class="clear"></div>
                            <div class="next-step step3">
                                <a href="#" id="save2" class="text-center">حفظ</a>
                            </div>
                        </div>
                    </div>

            <div class="col-xs-12 col-md-3">
                <ul class="profile-settings profile-settingss">
                    <li>
                        <a href="#favoriteShops" class="reviewsPeople" id="favoriteShops">
                            <i class="ion ion-heart" aria-hidden="true"></i>
                            محلات اعجبت بها
                        </a>
                    </li>

                    <li>
                        <a href="#boxMessages" class="boxMessages" id="boxMessages">
                            <i class="fa fa-envelope" aria-hidden="true" ></i>
                            صندوق الرسائل
                        </a>
                    </li>

                    <li>
                        <a href="#settingProfile" class="settingProfile" id="settingProfile">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                            اعدادات حسابى
                        </a>
                    </li>
                    
                    <li>
                        <a href="#passwordConfig" class="passCon" id="passCon">
                            <i class="fa fa-key" aria-hidden="true"></i>
                            تعديل كلمة المرور
                        </a>
                    </li>

                    <li>
                        <a href="/logout">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            تسجيل الخروج
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php $ads = DB::table('ads')->get(); ?>
<section class="ads">

    <img src="/image/{{$ads[5]->files}}" alt="تشكيله الشتاء" class="img-responsive">
</section>
<section class="samples-shops">
        <div class="container">
            <div class="row">
                 <div class="owl-carousel owl-theme last-carousel owl-loaded owl-drag">
                    
                    
                     
                     
                <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1180px, 0px, 0px); transition: 0.25s; width: 3304px;"><div class="owl-item cloned" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/wordpress-logo-simplified-rgb.png" class="img-responsive"> 
                    </div></div><div class="owl-item cloned" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/adidas.png" class="img-responsive"> 
                    </div></div><div class="owl-item cloned" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/Nike-Logo-PNG-Picture.png" class="img-responsive"> 
                    </div></div><div class="owl-item cloned" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/PHLIPSE.png" class="img-responsive"> 
                    </div></div><div class="owl-item cloned" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/wordpress-logo-simplified-rgb.png" class="img-responsive"> 
                    </div></div><div class="owl-item active" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/adidas.png" class="img-responsive"> 
                    </div></div><div class="owl-item active" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/Nike-Logo-PNG-Picture.png" class="img-responsive"> 
                    </div></div><div class="owl-item active" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/PHLIPSE.png" class="img-responsive"> 
                    </div></div><div class="owl-item active" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/wordpress-logo-simplified-rgb.png" class="img-responsive"> 
                    </div></div><div class="owl-item cloned active" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/adidas.png" class="img-responsive"> 
                    </div></div><div class="owl-item cloned" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/Nike-Logo-PNG-Picture.png" class="img-responsive"> 
                    </div></div><div class="owl-item cloned" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/PHLIPSE.png" class="img-responsive"> 
                    </div></div><div class="owl-item cloned" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/wordpress-logo-simplified-rgb.png" class="img-responsive"> 
                    </div></div><div class="owl-item cloned" style="width: 226px; margin-right: 10px;"><div class="item">
                        <img src="imgs/shops-logos/adidas.png" class="img-responsive"> 
                    </div></div></div></div><div class="owl-nav disabled"><div class="owl-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div><div class="owl-next"><i class="fa fa-angle-left" aria-hidden="true"></i></div></div><div class="owl-dots disabled"><div class="owl-dot active"><span></span></div><div class="owl-dot"><span></span></div><div class="owl-dot"><span></span></div><div class="owl-dot"><span></span></div></div></div>
            </div>
        </div>
    </section>
<input type="hidden" name="userID" id="userID" value="{{Auth::user()->id}}">
@endsection
<script>
    function roomMsg(Data,Sender){
        room_id = document.getElementById('roomIds');
        room_id.value = Data;

        $.ajax({
            type:'POST',
            url:'/api/getmsg',
            data:{roomId:Data},
            success: function(data){
//                console.log(data);
                 $('.textBox > p').remove();
                for(i=0; i<data.length; i++){
                    if(data[i].sender_id == Sender){
//                        sendermsg = '<p class="primary">'+data[i].message+'</p>';
//                        sendermsg2 = '<p class="primary">'+data[i].created_at+'</p>';
//                        $('.textBox').append(sendermsg);
//                        $('.textBox').append(sendermsg2 );
                        msg = '<div class="msg msg-right"><div class="img-container"><img src="/image/'+data[i].sender.photo+'" class="img-responsive"></div><div class="client-title"><h5>'+data[i].sender.user_name+'</h5></div><div class="clearfix"></div><div class="msgtxt"><p class="primary">'+data[i].message+'</p><p class="primary">'+data[i].created_at+'</p></div></div>';
                        $('.textBox').append(msg);
                    }else{
//                        recivermsg = '<p class="secondary text-left">'+data[i].message+'</p>';
//                        recivermsg2 = '<p class="secondary text-left">'+data[i].created_at+'</p>';
//                        $('.textBox').append(recivermsg);
//                        $('.textBox').append(recivermsg2);
                        msg = '<div class="msg msg-left"><div class="img-container"><img src="/image/'+data[i].sender.photo+'" class="img-responsive"></div><div class="client-title"><h5>'+data[i].sender.user_name+'</h5></div><div class="clearfix"></div><div class="msgtxt"><p class="secondary text-left">'+data[i].message+'</p><p class="secondary text-left">'+data[i].created_at+'</p></div></div>';
                        $('.textBox').append(msg);

                    }
                }
                let height = 0;
                $('.textBox').children().each(function(i, value){
                    height += parseInt($(this).height());
                });
                height += '';
                $('.textBox').animate({scrollTop: height});
            },
            error: function(){
                console.log('error');
            }
        });
    }
    function newmsg(){
	
	        $('#newmsgForum1').submit(function(e){
	            e.preventDefault();
	            var form = $('#newmsgForum1');
	            $.ajax({
	                type:'POST',
	                url:'/user/profile',
	                data:form.serialize(),
	                dataType:'json',
	
	                success: function(data){
//	                    console.log(data);
	                    if(data == 'block'){
	                    	swal();
				swal({
				title: "this vendor blocked you",
				text: " "
				});
	
	                    }
	                    
	                },
	                error: function(){
	                    console.log('error');
	                }
	            });
	        });
        
    }
    function removefollow(Data){
        $.ajax({
            type:'POST',
            url:'/user/profile',
            data:{followId:Data,save2:2},

            success: function(data){
//                console.log(data);
                
            },
            error: function(){
                console.log('error');
            }
        });
    }
    function changespsw(){
	
	        $('#pswForum').submit(function(e){
	            e.preventDefault();
	            var form = $('#pswForum');
	            $.ajax({
	                type:'POST',
	                url:'/user/profile',
	                data:form.serialize(),
	                dataType:'json',
	
	                success: function(data){
//	                    console.log(data);
	                    if(data == 'psw changed success'){
	                    	swal();
				swal({
				title: "تم تعديل كلمه المرور بنجاح",
				text: " "
				});
	
	                    }else{
	                    	swal();
				swal({
				title: "كلمه المرور قديمه غير صحيحة",
				text: " "
				});
	                    }
	                    
	                },
	                error: function(){
	                    console.log('error');
	                }
	            });
	        });
        
    }
    
</script>

                     