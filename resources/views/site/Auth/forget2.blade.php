<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="shortcut icon" href="{{asset('imgs/favIcon/favicon-32x32.png')}}" type="image/png">
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('auth/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('auth/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('auth/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('auth/css/main.css')}}">
    
    <script defer src="{{asset('auth/js/jquery-3.2.1.min.js')}}"></script>
    <script defer src="{{asset('auth/js/bootstrap.min.js')}}"></script>
    
    <script defer src="{{asset('auth/js/main.js')}}"></script>
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="img-container"><a href="/"><img src="{{asset('imgs/Logo.png')}}" class="img-responsive"></a></div>
    	<form method="POST" id="forgetForum"  class="forget-two" style="margin: 0 auto;">
    	 {{csrf_field()}}
    		<label> ادخل الكود المرسل إلى بريدك الالكتروني </label>
    		<input type="text" class="form-control" id="forgotCode" name="code">
    		<span></span>
    		<br>
    		<label> ادخل كلمة السر الجديدة </label>
    		<input type="password" class="form-control" id="forgotPassword" name="newpass">
    		<span></span>
    		<font id="msg" color="green"></font>
    		<button id="forgotSendTwo" onclick="validCode()" class="inlove">إرسال</button>
    	</form>


         <script>
	        function validCode(){
	            $('#forgetForum').submit(function(e){
	              e.preventDefault();
	              var form = $('#forgetForum');
	              $.ajax({
	                type:'POST',
	                url:'/forgetpassword-step2',
	                data:form.serialize(),
	                dataType:'json',
	                success: function(data){
	                    console.log(data);
	                    if(data == 'true'){
	                        location.href = "/login";
	                    }else{
	                        $('#msg').html('خطأ');
	                        $("#msg").fadeIn(1000);
	                        setTimeout(function() {
	                           $('#msg').fadeOut(1000);
	                        }, 2000);
	                    }
	                  
	                }
	              });
	            });
	        };
        </script>
    
       	
<footer>
        <div class="container-fluid">
            <div class="row">
                <div class="footer-bottom text-center">
                    <div class="copy-right col-xs-12 col-md-9">
                        <p>Developed By <a style="color: #888; text-decoration: none; font-weight: 400;" href="http://wasiladev.com" target="_blank">WasilaDev</a></p>
                        <p>جميع الحقوق محفوظه لشركه فى المحل دوت كوم</p>
                    </div>

                    <div class="conditions col-xs-12 col-md-3">
                        <a href="/privacy">سياسة الخصوصية</a>
                        <a href="/rules">احكام وشروط</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

</body>
</html>
    	