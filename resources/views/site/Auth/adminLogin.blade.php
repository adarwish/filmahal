<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="shortcut icon" href="{{asset('imgs/favIcon/favicon-32x32.png')}}" type="image/png">
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('auth/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('auth/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('auth/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('auth/css/main.css')}}">
    
    <script defer src="{{asset('auth/js/jquery-3.2.1.min.js')}}"></script>
    <script defer src="{{asset('auth/js/bootstrap.min.js')}}"></script>
    
    <script defer src="{{asset('auth/js/main.js')}}"></script>
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="wrapper">
        <header>
            <div class="container">
                <div class="row">
                    <div class="img-container">
                        <a href="/"><img src="{{asset('imgs/Logo.png')}}" class="img-responsive"></a>
                    </div>
                </div>
            </div>
        </header>

        <section>
            <div class="container">
                <div class="row">
                    <div class="align-form">
                        <form class="col-xs-12" method="POST" id="loginForm">
                            {{csrf_field()}}
                            <input type="hidden" name="login" value="login">
                            <div class="form">

                                <div class="box">
                                    <label for="username" class="text-right">اسم المستخدم</label>
                                    <input id="username" type="text" name="username" placeholder="ادخل اسم المستخدم، رقم التليفون">
                                    <span class="text-bold text-left"></span>
                                </div>

                                <div class="box" id="pass">
                                    <span class="img">
                                        <img src="/auth/vendor/imgs/showdisable.png" class="img-responsive">
                                    </span>
                                    <label for="password" class="text-right">كلمة السر</label>
                                    <input  type="password" name="password" placeholder="*** *** *** ***">
                                    <span class="text-bold text-left"></span>
                                </div>

                             <p class="text-left"><a href="/forgetpassword">نسيت كلمة السر؟</a></p>
                                <font color="red" id="msglogin"></font>
                                <button onclick="validlogin()"  type="submit">تسجيل الدخول</button>
                            </div>
                            
                        </form>
                       
                    </div>
                </div>
            </div>
        </section>
    </div>
    
    
    
<!--Start Of Footer Section-->
 <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="footer-bottom text-center">
                    <div class="copy-right col-xs-12 col-md-9">
                        <p>Developed By <a style="color: #888; text-decoration: none; font-weight: 400;" href="http://wasiladev.com" target="_blank">WasilaDev</a></p>
                        <p>جميع الحقوق محفوظه لشركه فى المحل دوت كوم</p>
                    </div>

                    <div class="conditions col-xs-12 col-md-3">
                        <a href="/privacy">سياسة الخصوصية</a>
                        <a href="/rules">احكام وشروط</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<!--End Of Footer Section-->
<script>
    function validlogin(){
        $('#loginForm').submit(function(e){
          console.log('run');
          e.preventDefault();

          var form = $('#loginForm');
          $.ajax({
            type:'POST',
            url:'/login',
            data:form.serialize(),
            dataType:'json',
            success: function(data){
                console.log(data);
                if(data.admin == 1){
                  
                    location.href = '/admin.home.mahalatmasr2018@fmax0*';
                
                }else{
                    $('#msglogin').html('خطأ في الدخول');
                    $("#msglogin").fadeIn(500);
                    setTimeout(function() {
                       $('#msglogin').fadeOut(3000);
                    }, 10000);
                }
              
            },
            error: function(){
                console.log('error');
            }
          });
        });
       // console.log('error');
    };
</script>
</body>
</html>