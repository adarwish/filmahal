<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="shortcut icon" href="{{asset('imgs/favIcon/favicon-32x32.png')}}" type="image/png">
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/auth/user/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/auth/user/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('/auth/user/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/auth/user/css/main.css')}}">
    
    <script defer src="{{asset('/auth/user/js/jquery-3.2.1.min.js')}}"></script>
    <script defer src="{{asset('/auth/user/js/bootstrap.min.js')}}"></script>
    
    <script defer src="{{asset('/auth/user/js/main.js')}}"></script>
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="wrapper">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="align-form">
                            <form method="POST" class="col-xs-12" id="registerForum" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="form">

                                    <div class="user">
                                    <!--<div class="img-container">
                        <a href="/"><img src="{{asset('imgs/Logo.png')}}" class="img-responsive"></a>
                    </div>-->
                            <div class="form-group">
        				    
    					    <!--<i class="fa fa-user-circle primary" aria-hidden="true"></i>-->
                                <img src="{{asset('image/user-circle.jpg')}}" alt="ارفع صورة" class="fa-user-circle" id="user-image-auth">
                                <input id="userPhoto" type="file" name="photo">
    					    </div>
                                    </div>

                                    <div class="box">
                                        <label for="username" class="text-right">اسم المستخدم</label>
                                        <input id="username" type="text" name="username" placeholder="اسم المستخدم ,ادخل رقم التليفون" required>
                                        <span class="text-bold text-left"></span>
                                    </div>

                                    <div class="box">
                                        <label for="email" class="text-right">البريدالألكترونى</label>
                                        <input id="email" type="text" name="email" placeholder="example@example.com" required>
                                        <span class="text-bold text-left"></span>
                                    </div>

                                    <div class="box">
                                        <label for="password" class="text-right">كلمة السر</label>
                                        <input id="password" type="password" name="password" placeholder="*** *** *** ***" required>
                                        <span class="text-bold text-left"></span>
                                    </div>
                                    <h3><font color="green" id="msg">{{$msg}}</font></h3>
                                    <button >انشاء حساب</button>

                                    <div class="hr">
                                        <hr>
                                    </div>

                                     <div class="facebook-login">
                                      <a href="/socialLogin">   <p class="secondary text-bold">انشاء حساب باستخدام الفيس بوك </a>
                                            <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                        </p>                                       
                                    </div> 

                                    <div  class="have-account">
                                        <span>لدى حساب بالفعل</span>
                                        <a href="/login">تسجيل الدخول</a>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-12 col-md-5 left left-form-register">
                        <div class="sale">
                        <?php $ads = DB::table('ads')->get(); ?>
                            <img src="/image/{{$ads[7]->files}}" class="img-responsive">
                        </div>
                        <div class="nowYouCan">
                            <h3 class="text-bold primary">
                                دلوقتى تقدر
                            </h3>
                        </div>
                        <ul>
                            <li><i class="fa-checkk"><img src="/auth/vendor/imgs/check.svg"></i> <span>تابع التخفيضات والمنتجات الجديده فى محلك المفضل</span></li>
                            <li><i class="fa-checkk"><img src="/auth/vendor/imgs/check.svg"></i> <span>تعمل متابعه لمحلاتك المفضلة</span></li>
                            <li><i class="fa-checkk"><img src="/auth/vendor/imgs/check.svg"></i> <span>تعمل شير لمحل معين وسط اصدقائك</span></li>
                            <li><i class="fa-checkk"><img src="/auth/vendor/imgs/check.svg"></i> <span>تكتب تقييم لاى محل</span></li>
                        </ul>
                        <div class="video" role="button" data-toggle="modal" data-target="#exampleModal">
                            <p class="text-bold">شاهد الفيديو التوضيحى</p>
                            <i class="fa fa-play-circle-o fa-3x" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php $ads = DB::table('ads')->get();
$setting = DB::table('setting')->get(); 
$counter = count($setting);?>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="video-wrapper">
             			<video loop class="video-responsive videoVideo" width="100%" controlsList="nodownload">
                   		    <source src="/video/{{$setting[3]->photo}}">
                   		    المتصفح الخاص بك لا يدعن خاصية الفيديو
             			</video>
             			<div class="playpause"></div>
           		    </div>
                        </div>
                    </div>
                </div>
            </div>
    
<!--Start Of Footer Section-->
 <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="footer-bottom text-center">
                    <div class="copy-right col-xs-12 col-md-9">
                        <p>Developed By <a style="color: #777; margin-right: 20px;" href="http://www.wasiladev.com" target="_blank">WasilaDev</a></p>
                        <p>جميع الحقوق محفوظه لشركه فى المحل دوت كوم</p>
                    </div>

                    <div class="conditions col-xs-12 col-md-3">
                        <a href="/privacy">سياسة الخصوصية</a>
                        <a href="/rules">احكام وشروط</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    
<!--End Of Footer Section-->
    <script>
        function validlogin(){
            $('#registerForum').submit(function(e){
              console.log('run');
              e.preventDefault();

              var form = $('#registerForum');
              $.ajax({
                type:'POST',
                url:'/user/register',
                data:form.serialize(),
                dataType:'json',
                success: function(data){
                    console.log(data);
                    if(data == 'success'){
                        location.href = '/';
                    }else{
                        $('#msglogin').html('مستخدم موجود بالفعل');
                        $("#msglogin").fadeIn(3000);
                        setTimeout(function() {
                           $('#msglogin').fadeOut(2000);
                        }, 10000);
                    }
                  
                },
                error: function(){
                    console.log('error');
                }
              });
            });
           // console.log('error');
        };
    </script>
</body>
</html>