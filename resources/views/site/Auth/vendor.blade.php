<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="shortcut icon" href="{{asset('imgs/favIcon/favicon-32x32.png')}}" type="image/png">
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('auth/vendor/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('auth/vendor/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('auth/vendor/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('auth/vendor/css/main.css')}}">
    
    <script defer src="{{asset('auth/vendor/js/jquery-3.2.1.min.js')}}"></script>
    <script defer src="{{asset('auth/vendor/js/bootstrap.min.js')}}"></script>
    
    <script defer src="{{asset('auth/vendor/js/main.js')}}"></script>
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="thanks next-step hidden">
        <div class="container">
            <div class="thanksMessage">
                <img src="{{asset('auth/vendor/imgs/eyes.svg')}}" class="img-responsive">
                <h3 class="primary text-bold">شكرا لانضمامك معنا</h3>
                <p class="secondary text-bold text-right">سوف يتم مراجعة البيانات والموافقة على طلب الانضمام وارسالها على بريدك الالكترونى فى خلال 24 ساعة</p>
                <button type="button">شكراً</button>
            </div>
        </div>    
    </div>
    <div class="">
        <header>
            <div class="container">

                    <div class="img-container">
                        <a href="/"><img src="{{asset("image/".$image->value)}}" class="img-responsive"></a>
                    </div>

                    <div class="steps col-xs-12">
                        <div class="step">
                            <div class="num">
                                <span>1</span>
                            </div>
                            <p>
                                معلومات عن صاحب المحل
                            </p>
                            <hr>
                        </div>

                        <div class="step">
                            <div class="num">
                                <span>2</span>
                            </div>
                            <p>
                                معلومات عن نشاط المحل
                            </p>
                            <hr>
                        </div>

                        <div class="step">
                            <div class="num">
                               <span>3</span>
                            </div>
                            <p>
                                أبدا فى عرض منتجاتك
                            </p>
                        </div>
                    </div>
                    <span class="auth-divider"></span>
                </div>

        </header>

    <!--Start Of HowItWorks Section-->
        <section class="howItWorks" id="howItWorks">
            <div class="container">

                    <div class="tools">
                        <h3 class="text-bold text-color">
                            فى المحل هيساعدك تتغلب على منافسيك
                        </h3>
                        <ul>
                            <li><i class="text-color fa fa-check fa-lg"><img src="/auth/vendor/imgs/check.svg"></i> <span>اعرض منتجاتك امام الاف الزوار يوميا</span></li>
                            <li><i class="text-color fa fa-check fa-lg"><img src="/auth/vendor/imgs/check.svg"></i> <span>اضف الجديد باستمرار لتحصل على مشاهدات اعلى</span></li>
                            <li><i class="text-color fa fa-check fa-lg"><img src="/auth/vendor/imgs/check.svg"></i> <span>يصل زوار الموقع اليك من خلال ارقام التليفون,  والرسائل,  خريطة جوجل, علامات مميزة لموقعك</span></li>
                        </ul>

                        <div class="account-buttons">
                            <div class="create-account">
                                <a href="#" id="create-account">انشاء حساب</a>
                            </div>
                            <div class="have-account">
                                <span class="text-bold">بالفعل لدى حسب</span>
                                <a href="/login">تسجيل دخول</a>
                            </div>
                        </div>
                    </div>

                </div>

        </section>
    <!--End Of HowItWorks Section-->

        <section id="step1">
            <div class="container">
                <div class="row">

                    <form id="myForm" method="POST">
                        {{csrf_field()}}
                        <div id="first_step">
                            <div class="right-forms col-xs-12 col-md-6 padding-left">
                                <div>
                                    <label>الاسم كاملا</label>
                                    <input id="fullName" name="fullName" type="text" placeholder="احمد ناصر عبد العاطى">
                                    <span></span>

                                </div>

                                <div>
                                    <label>البريد الالكترونى</label>
                                    <input id="email" name="email" type="text" placeholder="ahmed.mohamed17@gmail.com">
                                    <span></span>
                                </div>

                                <div>
                                    <label>رقم الهاتف</label>
                                    <input id="phone" name="phone" type="text" placeholder="01151073476" maxlength="11">
                                    <span></span>
                                </div>
                            </div>

                            <div class="right-forms col-xs-12 col-md-6 padding-right">


                                <div>
                                    <label>اسم المستخدم</label>
                                    <input id="userName" name="userName"  type="text" placeholder="احمد ناصر">
                                    <span></span>
                                </div>


                                <div id="pass">
                                    <span class="img">
                                        <img src="/auth/vendor/imgs/showdisable.png" class="img-responsive">
                                    </span>
                                    <label>كلمة السر</label>
                                    <input id="password" type="password" name="password" placeholder="*** *** *** ***" maxlength="16">
                                    <span></span>
                                </div>

                                <div class="next-step next-step0">
                                    <a href="#" id="next-step0">الخطوة السابقة</a>
                                     <a href="#" id="next-step2">الخطوة التالية</a>
                                </div>  
                            </div>
                        </div>

                        <div id="second-step">
                            <div class="right-forms col-xs-12 col-md-6 padding-left">
                                <div class="normal-form">
                                    <label>اسم المحل</label>
                                    <input id="shopName" type="text" name="marketName" placeholder="الاسم الموجود على لافته المحل" maxlength="20">
                                    <span>20 حرف</span>

                                </div>

                                <div class="normal-form">
                                    <label>اسم باللغة الأنجليزية</label>
                                    <input id="shopNameEN" type="text" name="marketEnName" placeholder="هذا الحقل اختيارى" maxlength="20">
                                    <span>20 حرف</span>
                                </div>

                                <div class="normal-form">
                                    <label>اسم الشهرة</label>
                                    <input id="famousName" type="text" name="nickName" placeholder="اسم مشهور به المحل بين عملائك" maxlength="20">
                                    <span>20 حرف</span>
                                </div>

                                <div class="work-times normal-form">
                                    <label>مواعيد العمل</label>
                                    <div class="choose-time">
                                        <div class="days">
                                            <div class="resp respx">
                                                <span class="from">من يوم</span>
                                                <div class="customizeArrow">
                                                    <i class="fa fa-chevron-down secondary" aria-hidden="true"></i>
                                                    <select name="from">
                                                        <option value="1" selected>السبت</option>
                                                        <option value="2">الأحد</option>
                                                        <option value="3">الأثنين</option>
                                                        <option value="4">الثلاثاء</option>
                                                        <option value="5">الأربعاء</option>
                                                        <option value="6">الخميس</option>
                                                        <option value="7">الجمعه</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="resp">
                                                <span class="to-day">الى يوم</span>
                                                <div class="customizeArrow">
                                                    <i class="fa fa-chevron-down secondary" aria-hidden="true"></i>
                                                    <select name="to">
                                                        <option value="1">السبت</option>
                                                        <option value="2">الأحد</option>
                                                        <option value="3">الأثنين</option>
                                                        <option value="4">الثلاثاء</option>
                                                        <option value="5">الأربعاء</option>
                                                        <option value="6">الخميس</option>
                                                        <option value="7" selected>الجمعه</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear fixClear"></div>
                                        <div class="time">
                                            
                                            <div class="resp respx">
                                                <span class="from">من</span>
                                                <div class="customizeArrow customizeArrowTime">
                                                <i class="fa fa-chevron-down secondary" aria-hidden="true"></i>
                                                    <select name="h1" class="hoursOfDay">
                                                        <!--Added By JS-->
                                                    </select>
                                                </div>
                                                <div class="customizeArrow customizeArrowFormat">
                                                    <i class="fa fa-chevron-down secondary" aria-hidden="true"></i>
                                                    <select name="modeOpening">
                                                        <option value="صباحاً">صباحاً</option>
                                                        <option value="مساءً">مساءً</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="resp">
                                                <span class="to">الى</span>
                                                <div class="customizeArrow customizeArrowTime">
                                                    <i class="fa fa-chevron-down secondary" aria-hidden="true"></i>
                                                    <select name="h2" class="hoursOfDay">
                                                        <!--Added By JS-->
                                                    </select>
                                                </div>
                                                <div class="customizeArrow customizeArrowFormat">
                                                    <i class="fa fa-chevron-down secondary" aria-hidden="true"></i>
                                                <select name="modeClosing">
                                                        <option value="صباحاً">صباحاً</option>
                                                        <option value="مساءً">مساءً</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="right-forms col-xs-12 col-md-6 padding-right">


                                <div>
                                    <div class="address">
                                        <label>عنوان المحل</label>
                                        <div class="resp">
                                            <span class="from">محافظة</span>
                                            <div class="customizeArrow customizeArrowPlace">
                                                <i class="fa fa-chevron-down secondary" aria-hidden="true"></i>
                                                <select name="governorate" id="governorate" onchange="getarea()">
                                                    @foreach($city as $city)
                                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                                    @endforeach   
                                                </select>
                                            </div>
                                        </div>
                                        <div class="resp">
                                            <span class="area">منطقة</span>
                                            <div class="customizeArrow customizeArrowPlace">
                                                <i class="fa fa-chevron-down secondary" aria-hidden="true"></i>
                                                <select name="area" id="area">
                                                    @foreach($area as $area)
                                                    <option value="{{$area->id}}">{{$area->name}}</option>
                                                    @endforeach 
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="main-address">
                                        <label>العنوان الرئيسى</label>
                                        <p class="enter-main-address">ادخل عنوان الفرع الرئيسى, واضافة باقى الفروع ستكون من داخل حسابك</p>
                                        <textarea id="mainAddress" name="mainaddress" placeholder='مثال "مدينة نصر ,النادى الاهلى ,محمد توفيق وهبه, عمارة 10'  maxlength="100"></textarea>
                                        <span>100 حرف</span>
                                    </div>

                                    <div>
                                        <label>علامات مميزة</label>
                                        <p class="enter-main-address">اكتب علامات مميزة لكى تساعد المستخدم فى العثور عليك</p>
                                        <textarea id="specialMarks" name="markaddress" placeholder='اول اما تنزل ادخل يمين هتلاقى سمكرى على اول الشارع وحديقة الورد على يمينك احنا رقم 10' maxlength="100"></textarea>
                                        <span>100 حرف</span>
                                    </div>
                                </div>

                                <div class="next-step step3">
                                    <a href="#" id="prev-step">الخطوة السابقة</a>
                                    <a href="#" id="next-step3">الخطوة التالية</a>
                                </div>  
                            </div>
                        </div>

                        <div id="third-step">
                            <div class="right-forms col-xs-12 col-md-6">
                                <div class="shop-cates">
                                    <label>فئات المحل</label>
                                    <div class="add-cates">
                                        <p class="secondary">قم باضافة كل الفئات الموجوده فى محل</p>
                                        <i id="cates" class="fa fa-plus-square fa-2x" aria-hidden="true"></i>
                                        <span id="catsValidation"></span>
                                    </div>
                                    <ul id="cates-list"></ul>
                                </div>
                            </div>

<!--
                            <div class="right-forms col-xs-12 col-md-6">
                                <div class="shop-cates">
                                    <label>الفئة المستهدفة</label>
                                    <div class="add-cates">
                                        <p>حدد المستفيد من هذه الفئات رجال, نساء, اطفال</p>
                                        <i id="kinds" class="fa fa-plus-square fa-2x" aria-hidden="true"></i>
                                    </div>
                                    <ul id="kinds-list"></ul>
                                </div>
                            </div>
-->

                            <div class="col-xs-12">
                                <div class="must-selected">
                                    <div class="cons">
                                        <label>لقد قرأت ووافقت على <a href="/rules">شروط وسياسة الموقع</a> </label>

                                        <input type="checkbox" name="conditions" value="1" id="cons">
                                        <span></span>
                                    </div>
                                    <div class="subscribe">
                                        <label> ابقى متواصلا لمعرفه الجديد عن <a href="#">فى المحل.كوم</a> </label>

                                        <input type="checkbox" name="subscribe" value="1" id="subscribe">
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <div class="next-step step3">
                                    <a href="#" id="prev-to-secondStep">الخطوة السابقة</a>
                                    <button type="button" id="thanks">تسجيل المحل</button>
                                    <button id="submit-shop" type="submit">تسجيل المحل</button>
                                    
                                </div>  
                            </div>
                        </div>
                        <div id="third-step2">
                            <div class="col-xs-12 bg-white">
                                <div class="choose-kinds">
                                    <h3 class="text-bold">فئات المحل</h3>
                                    <i id="close2" class="fa fa-times fa-2x" aria-hidden="true"></i>
                                    <div class="clear"></div>
                                    <p class="secondary lead">اختر جميع الفئات الموجوده فى محل</p>
                                </div>
                                <div class="chooseCat">

                                    @foreach($category as $cats)
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <label>{{$cats->name}}</label>
                                        <input  type="checkbox" name="kinds[]" value="{{$cats->id}}">
                                    </div>
                                    @endforeach
                                </div>
                                <div class="clear"></div>
                                <div>
                                    <p class="text-center error text-bold" id="errorCates"></p>
                                </div>
                                <div class="clear"></div>
                                <div class="next-step step3">
                                    <a href="#" id="save2" class="text-center">حفظ الفئات</a>
                                </div>
                            </div>
                        </div>



                    </form>
                </div>
            </div>
        </section>
    </div>
    <footer style="
    position: fixed;
    width:  100%;
    bottom:  0;">
        <div class="container-fluid">
            <div class="row">
                <div class="footer-bottom text-center">
                    <div class="conditions col-xs-12 col-md-3">
                        <a href="/privacy">سياسة الخصوصية</a>
                        <a href="/rules">احكام وشروط</a>
                    </div>
                    
                    <div class="copy-right col-xs-12 col-md-7">
                        <p>Developed By WasilaDev</p>
                        <p>جميع الحقوق محفوظه لشركه فى المحل دوت كوم</p>
                    </div>

                    
                </div>
            </div>
        </div>
    </footer>
<!--End Of Footer Section-->
    <script>

        function getarea(){
            cities = document.getElementById('governorate').value;
            console.log(cities);
              $.ajax({
                type:'GET',
                url:'/vendor/cities',
                data:{city:cities},
                success: function(data){
                    console.log(data);
                    $('#area > option').remove();
                    for(var i = 0; i < data.area.length; i++){
                        addoption = '<option value='+data.area[i].id+' class="level-0">'+data.area[i].name+'</option>';
                        $('#area').append(addoption);
                    }
                },
                error: function(){
                    console.log('error');
                }
              });
           // console.log('error');
        };
    </script>
</body>
</html>