 <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title></title>
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="shortcut icon" type="image/png" sizes="32x32" href="{{asset('imgs/favIcon/favicon-32x32.png')}}">
    <link rel="shortcut icon" type="image/png" sizes="16x16" href="{{asset('imgs/favIcon/favicon-16x16.png')}}">
    
    <link rel="stylesheet" href="{{asset('profiles/css/main0.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/pace-theme-flash.css')}}">
    <link rel="stylesheet" href="{{asset('css/blog.css')}}">
    
    
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <?php $dynamic_data = DB::table('dynamic_home')->get(); ?>
<!--Start Of Header Section-->
    <header>
        <nav class="container">
            <div class="row"> 
        <!-- <ul class="left-side col-xs-12 col-lg-6">   
              <li class="drop-down">
                    <select id="governorate" onchange="getarea()">
                    	<option value="{{$city1[0]->id}}">{{$city1[0]->name}}</option>
                        @foreach($city as $cities)
                        <option value="{{$cities->id}}">{{$cities->name}}</option>
                        @endforeach
                    </select>
                </li>
                <li class="button-shop">
                    @if(Auth::guest())
                    <a href="/vendor/register" class="add-shop">
                        أضف محلك
                        <i class="fa fa-plus"></i>
                    </a>
                    @endif
                </li>
            </ul> -->
            
            <ul class="right-side col-xs-12" id="contentPagesHeader">
            	<li class="header-higher">
            	    <a href="/" style="box-shadow: none">
            	        <img src="{{asset("image/".$image->value)}}" alt="Filmahal">
            	    </a>
            	</li>
                @if(Auth::guest())
                <ul class ="user-authentication header-higher">
                  <li><a href="/user/register" class="sign-up">انشاء حساب</a></li>
                  <li><a href="/login" class="sign-in">تسجيل الدخول</a></li>
                </ul>
                @else
                	@if(Auth::user()->vendor == 1)
                	<ul class ="user-authentication header-higher">
                	  <li><a href="/vendor/control-panel" class="sign-up">{{Auth::user()->user_name}}</a></li>
	                  <li><a href="/logout" class="sign-in">تسجيل خروج</a></li>
	                </ul>
                	@elseif(Auth::user()->admin == 1)
                	<ul class ="user-authentication header-higher">
                	  <li><a href="/admin.home.mahalatmasr2018@fmax0*" class="sign-up">{{Auth::user()->user_name}}</a></li>
	                  <li><a href="/logout" class="sign-in">تسجيل خروج</a></li>
	                </ul>
                	@else
                	<ul class ="user-authentication header-higher">
	                  <li><a href="/user/profile" class="sign-up">{{Auth::user()->user_name}}</a></li>
	                  <li><a href="/logout" class="sign-in">تسجيل خروج</a></li>
	                </ul>
	                @endif
                @endif
            </ul>
                </div>
        </nav>
        <div class="clear"></div>
    </header>
<!--End Of Header Section-->
    
  @yield('content')
<!--Start Of Footer Section-->
<?php $contact_us = DB::table('contact_us')->get(); ?>
    <footer>
        <div class="container">
            <div class="row">
                <div class="left-section col-xs-12 col-md-6 col-lg-4">
                    <div class="headerWord">
                        <h3 class="text-bold">الدعم الفنى وخدمه العملاء</h3>
                    </div>
                    <div class="contact-us">
                        <div class="phones">

                            <i class="fa fa-phone"></i>

                            <div class="text-color-grey numbers text-bold">
                                <span>
                                    {{$contact_us[0]->data}}
                                </span>
                                <span>
                                    {{$contact_us[1]->data}}
                                </span>
                            </div>

                        </div>

                        <div class="email">
                            <span class="text-bold">
                                <i class="fa fa-envelope"></i> 
                                <span class="text-color-grey">
                                    {{$contact_us[3]->data}}
                                </span>
                            </span>
                        </div>

                        <div class="whatsApp">
                            <div class="text-bold">
                                <span class="whatsApp-icon"></span> 
                                <span class="text-color-grey">
                                    {{$contact_us[2]->data}}
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="follow-us">
                        <h2>
                            تابعنا
                        </h2>
                        <div class="social-icons">
                            <ul>
                                <a href="{{$contact_us[4]->data}}" target="_blank" class="facebook">
                                    <li>
                                        <i class="fa fa-facebook fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$contact_us[5]->data}}" target="_blank" class="instagram">
                                    <li>
                                        <i class="fa fa-instagram fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$contact_us[6]->data}}" target="_blank" class="googlePlus">
                                    <li>
                                        <i class="fa fa-google-plus fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$contact_us[7]->data}}" target="_blank" class="twitter">
                                    <li>
                                        <i class="fa fa-twitter fa-2x"></i>
                                    </li>
                                </a>
                                <a href="{{$contact_us[8]->data}}" target="_blank" class="youtube">
                                    <li>
                                        <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
                                    </li>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="center-section col-xs-12 col-md-6 col-lg-4">
                    <div class="headerWord">
                        <h3 class="text-bold"><font id="msg">اشترك ليصلك كل جديد</font></h3>
                    </div>
                    
                    <div class="send-message">
                        <form method="POST" id="subsForum">
                            {{csrf_field()}}
                            <input type="hidden" name="sub1" value="sub1">
                            <input type="email" name="emailsub" placeholder="البريد الالكترونى">
                            <button class="text-bold" onclick="newsubuser()" type="submit">أرسل</button>
                        </form>
                    </div>
                    
                    <div class="categories col-xs-6" style="padding: 0;">
                        <h3 class="text-bold">لأصحاب المحلات</h3>
                        <ul>
                            <li><a class="text-color-grey text-bold" href="/vendor/register" target="_blank">اضف محلك</a></li>
                        </ul>
                    </div>
                    <div class="categories col-xs-6" style="padding-left: 30px; padding-right: 0;">
                        <h3 class="text-bold">أقسام الموقع</h3>
                       
                        <ul>
                            <li><a class="text-color-grey text-bold" href="/mobile" target="_blank">تطبيق الموبيل</a></li>
                            <li><a class="text-color-grey text-bold" href="/technical-support" target="_blank">الدعم الفنى</a></li>
                            <li><a class="text-color-grey text-bold" href="/FAQ" target="_blank">الأسئلة الأكثر شيوعاً</a></li>
                            <li><a class="text-color-grey text-bold" href="/ads-with-us" target="_blank">أعلن معنا</a></li>
                            <li><a class="text-color-grey text-bold" href="/copyright" target="_blank">الملكية الفكرية</a></li>
                            <li><a class="text-color-grey text-bold" href="/blog" target="_blank">المدونة</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="right-section col-xs-12 col-lg-4">
                    <div class="img-container">
                    <a href="/"><img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo"></a>
                    </div>
                    <h3 class="text-bold">
                        من نحن ؟ ولماذا تنضم الينا ؟
                    </h3>
                    <ul>
                        <li><a class="text-color-grey text-bold" href="/about-us" target="_blank"><i class="fa fa-check fa-lg"></i>من نحن ؟</a></li>
                        <li><a class="text-color-grey text-bold" href="/terms-condition" target="_blank"><i class="fa fa-check fa-lg"></i>شروط وجود محلك فى الموقع ؟</a></li>
                        <li><a class="text-color-grey text-bold" href="/privileges" target="_blank"><i class="fa fa-check fa-lg"></i>مميزات وجوده معنا ؟</a></li>
                    </ul>
                </div>
            </div> 
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="footer-bottom text-center">
                    <div class="copy-right col-xs-12 col-md-9">
                        <p>Developed By <a style="color: #777; margin-right: 20px;" href="http://www.wasiladev.com" target="_blank">WasilaDev</a></p>
                        <p>جميع الحقوق محفوظه لشركه فى المحل دوت كوم</p>
                    </div>

                    <div class="conditions col-xs-12 col-md-3">
                        <a href="/privacy">سياسة الخصوصية</a>
                        <a href="/rules">احكام وشروط</a>
                    </div>
                </div>
            </div>
        </div>
        </footer>
        
        
        

    <div id="preloader">
        <div class="text">

            <img src="{{asset("image/".$image->value)}}" alt="Filmahal Logo">

        </div>
    </div>



        
        
<!--End Of Footer Section-->
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/pace.min.js')}}"></script>
    <script src="{{asset('js/js.js')}}"></script>

    <script>
        function newsubmobuser(){
            $('#subsmobForum').submit(function(e){
              e.preventDefault();
              var form = $('#subsmobForum');
              $.ajax({
                type:'POST',
                url:'/',
                data:form.serialize(),
                dataType:'json',
                success: function(data){
                    console.log(data);
                    $('#msg2').html('تم الارسال بنجاح');
                    $("#msg2").fadeIn(3000);
                    setTimeout(function() {
                       $('#msg2').fadeOut(2000);
                    }, 10000);
                }
              });
            });
        };
        function newsubuser(){
            $('#subsForum').submit(function(e){
              e.preventDefault();
              var form = $('#subsForum');
              $.ajax({
                type:'POST',
                url:'/',
                data:form.serialize(),
                dataType:'json',
                success: function(data){
                    console.log(data);
                    $('#msg').html('تم الأشتراك بنجاح');
                    $("#msg").fadeIn(3000);
                    setTimeout(function() {
                       $('#msg').fadeOut(2000);
                    }, 10000);
                }
              });
            });
        };
        function getarea(){
            cities = document.getElementById('governorate').value;
            console.log(cities);
              $.ajax({
                type:'GET',
                url:'/vendor/cities',
                data:{city:cities},
                success: function(data){
                    console.log(data);
                    $('#area > option').remove();
                    for(var i = 0; i < data.area.length; i++){
                        addoption = '<option value='+data.area[i].id+' class="level-0">'+data.area[i].name+'</option>';
                        $('#area').append(addoption);
                    }
                    addoption="";
                },
                error: function(){
                    console.log('error');
                }
              });
           // console.log('error');
        };
        
        $(document).ready(function(){
            $('.container').addClass('container-fluid').removeClass('container');
        });
    </script>
    
    
    <script>
    
    
    paceOptions = {

    ajax: true,
    document: true,
    eventLag: false

};

Pace.on('done', function () {

    'use strict';

    $("body, html").css("overflow", "auto");

    $('#preloader').delay(500).fadeOut(800, function () {

        $(this).remove();

    });
});
    
    
    </script>
    
</body>
</html>