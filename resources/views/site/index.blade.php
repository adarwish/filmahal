<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title></title>
<!--    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    
    
    <link rel="stylesheet" href="/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="/owlcarousel/assets/owl.theme.default.min.css">
    
    <link rel="stylesheet" href="{{asset('css/pace-theme-flash.css')}}">
    
    <link rel="stylesheet" href="/css/main.css">
    
    
    
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('owlcarousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/pace.min.js')}}"></script>
    <script src="{{asset('profiles/js/typeahead.bundle.min.js')}}"></script>
    <script defer src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/js.js')}}"></script>
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <?php $dynamic_data = DB::table('dynamic_home')->get(); ?>
<!--Start Of Header Section-->
    <header>
        <nav class="container">
            <div class="row"> 
            <ul class="left-side col-xs-12 col-lg-6">   
                <li class="drop-down">
                    <select id="governorate" onchange="getarea()">
                    	<option value="{{$city1[0]->id}}">{{$city1[0]->name}}</option>
                        @foreach($city as $cities)
                        <option value="{{$cities->id}}">{{$cities->name}}</option>
                        @endforeach
                    </select>
                </li>
                <li class="button-shop">
                    @if(Auth::guest())
                    <a href="/vendor/register" class="add-shop">
                        أضف محلك
                        <i class="fa fa-plus"></i>
                    </a>
                    @endif
                </li>
            </ul>
            
            <ul class="right-side col-xs-12 col-lg-6">
                @if(Auth::guest())
                <li><a href="/user/register" class="sign-up">انشاء حساب</a></li>
                <li><a href="/login" class="sign-in">تسجيل الدخول</a></li>
                @else
                	@if(Auth::user()->vendor == 1)
                	<li><a href="/vendor/control-panel" class="sign-up">{{Auth::user()->user_name}}</a></li>
	                <li><a href="/logout" class="sign-in">تسجيل خروج</a></li>
                	@elseif(Auth::user()->admin == 1)
                	<li><a href="/admin.home.mahalatmasr2018@fmax0*" class="sign-up">{{Auth::user()->user_name}}</a></li>
	                <li><a href="/logout" class="sign-in">تسجيل خروج</a></li>
                	@else
                	
	                <li><a href="/user/profile" class="sign-up">{{Auth::user()->user_name}}</a></li>
	                <li><a href="/logout" class="sign-in">تسجيل خروج</a></li>
	                @endif
                @endif
            </ul>
                </div>
        </nav>
        <div class="clear"></div>
        
        <div class="container">
            <div class="row text-center">
                <div class="img-container">
                    <a href="/"><img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo"></a>
                </div>
                
<!--                <h1 class="title text-bold">{{$dynamic_data[0]->data}}</h1>-->
                <h1 class="title text-bold">أول وأكبر محرك بحث عن المحلات في مصر</h1>
                <form method="POST">
                    {{csrf_field()}}
                    <div class="search">
                        <input type="search" class="search-profile" id="searching" name="search" placeholder="أبحث باسم محل ,فئه معينة ,بأسم منتج">
                        <i class="fa fa-search fa-2x"></i>

                        
                    </div>
                  
                    <div class="example">
                        <p class="ex text-bold">مثال: الفردوس ,ملابس ,جاكيت</p>
                    </div>

                    <div class="customize-search">
                        <select id="area" name="getarea" class="text-bold towns" >
                            <option value="">اختار مدينه</option>
                            @foreach($area as $areas)
                            <option value="{{$areas->id}}">{{$areas->name}}</option>
                            @endforeach
                        </select>

                        <input type="submit" name="searchbutton" value="عرض المحلات" class="text-bold towns">
                    </div>
                </form>
                
                    <a class="arrow-bottom text-center" href="#howItWorks">
                        <i class="fa fa-chevron-down fa-2x"></i>
                    </a>             
            </div>
        </div>       
    </header>
<!--End Of Header Section-->
    
<!--Start Of HowItWorks Section-->
    <section class="howItWorks" id="howItWorks">
        <div class="container">
            <div class="headerWord">
                <h1 class="text-bold text-center">
                    <span class="bottom-divider">ابحث</span> عن أي محل في مصر
                </h1>
                
                <!-- <div class="vertical">
                    <hr>
                </div> -->
            </div>
            <div class="tools">
                <h2 class="text-bold">
                    فى المحل .كوم
                </h2>
                <p>هو اكبر محرك بحث عن المحلات فى مصر تقدر من خلاله توصل لأي مكان وأنت في مكانك</p>
                <ul>
                    <li><i class="fa fa-check fa-lg"></i> <span>ابحث بأسم محل معين وتابع احدث منتجاته</span></li>
                    <li><i class="fa fa-check fa-lg"></i> <span>ابحث بأسم فئه معينة وشوف المحلات اللى بتبيعها</span></li>
                    <li><i class="fa fa-check fa-lg"></i> <span>ابحث بأسم منطقة وشوف المحلات اللى فيها</span></li>
                    <li><i class="fa fa-check fa-lg"></i> <span>ابحث بأسم منتج وشوف المحلات اللى بتبيعه</span></li>
                </ul>
            </div>  
        </div>
         <?php $ads = DB::table('ads')->get();
$setting = DB::table('setting')->get(); 
$counter = count($setting);?>
        <div class="play-button">
        	<img class="" id="videoM" src="/image/SVG/play-button-white.svg" role="button" data-toggle="modal" data-target="#exampleModal">
        	
        </div>
        <!--Image Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="video-wrapper">
             			<video loop class="video-responsive video" width="100%" controlsList="nodownload">
                   		    <source src="/video/{{$setting[2]->photo}}">
                   		    المتصفح الخاص بك لا يدعم عرض الفيديو
             			</video>
             			<div class="playpause"></div>
           		    </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
<!--End Of HowItWorks Section-->
   

<!--Start Of Advertisment Section-->
    <section class="ads">
        <img src="/image/{{$ads[0]->files}}" class="img-responsive" alt="تشكيله الشتاء">
    </section>
<!--End Of Advertisment Section-->

<!--Start Of Mobile-App Section-->
    <section class="app">
        <div class="container">
            <div class="row">
                <div class="headerWord">
                  <h1 class="text-bold text-center"><span class="bottom-divider" style="border-color: #11998e"> قريبًا </span>تطبيق الموبايل </h1>

                   <!-- <div class="vertical">
                        <hr>
                    </div>-->
                </div>
            
            
            <div class="mobile-app col-xs-12 col-md-6">
                <img class="img-responsive" src="imgs/Flat%20Cooming%20Soon.svg">
            </div>
            
            <div class="tools col-xs-12 col-md-6">
                <h2 class="text-bold">
                       <font id="msg2"> أرسل لى رساله عندما يكون مُتاح </font>
                </h2>
                <div class="send-message col-xs-12">
                    <form method="POST" id="subsmobForum">
                        {{csrf_field()}}
                        <input type="hidden" name="sub2" value="sub2">
                        <input type="email" name="emailsubmob" placeholder="البريد الالكترونى">
                        <button class="text-bold" onclick="newsubmobuser()" type="submit">أرسل</button>
                    </form>
                    
                    <div class="stores">
                        
                        <div class="android">
                            <img src="imgs/en_badge_web_generic.png" class="img-responsive">
                        </div>
                        
                        <div class="ios">
                            <img src="imgs/Apple%20icon.svg" class="img-responsive">
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
<!--End Of Mobile-App Section-->
    <?php $logos = DB::table('icon_home')->get(); ?>
<!--Start Of Samples-Shops Section-->
    <section class="samples-shops">
        <div class="container">
            <div class="row">
                 <div class="owl-carousel owl-theme">
                 @foreach($logos as $logo)
                    <div class="item">
                        <img src="/image/{{$logo->icon}}" class="img-responsive"> 
                    </div>
                 @endforeach 
                </div>
            </div>
        </div>
    </section>
<!--End Of Samples-Shops Section-->
    <?php $contact_us = DB::table('contact_us')->get(); ?>
<!--Start Of Footer Section-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="left-section col-xs-12 col-md-6 col-lg-4">
                    <div class="headerWord">
                        <h3 class="text-bold">الدعم الفنى وخدمه العملاء</h3>
                    </div>
                    <div class="contact-us">
                        <div class="phones">

                            <i class="fa fa-phone"></i>

                            <div class="text-color-grey numbers text-bold">
                                <span>
                                    {{$contact_us[0]->data}}
                                </span>
                                <span>
                                    {{$contact_us[1]->data}}
                                </span>
                            </div>

                        </div>

                        <div class="email">
                            <span class="text-bold">
                                <i class="fa fa-envelope"></i> 
                                <span class="text-color-grey">
                                    {{$contact_us[3]->data}}
                                </span>
                            </span>
                        </div>

                        <div class="whatsApp">
                            <div class="text-bold">
                                <span class="whatsApp-icon"></span> 
                                <span class="text-color-grey">
                                    {{$contact_us[2]->data}}
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="follow-us">
                        <h2>
                            تابعنا
                        </h2>
                        <div class="social-icons">
                            <ul>
                                <a href="{{$contact_us[4]->data}}" target="_blank" class="facebook">
                                    <li>
                                        <i class="fa fa-facebook fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$contact_us[5]->data}}" target="_blank" class="instagram">
                                    <li>
                                        <i class="fa fa-instagram fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$contact_us[6]->data}}" target="_blank" class="googlePlus">
                                    <li>
                                        <i class="fa fa-google-plus fa-2x"></i>
                                    </li>
                                </a>
                                
                                <a href="{{$contact_us[7]->data}}" target="_blank" class="twitter">
                                    <li>
                                        <i class="fa fa-twitter fa-2x"></i>
                                    </li>
                                </a>
                                <a href="{{$contact_us[8]->data}}" target="_blank" class="youtube">
                                    <li>
                                        <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
                                    </li>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="center-section col-xs-12 col-md-6 col-lg-4">
                    <div class="headerWord">
                        <h3 class="text-bold"><font id="msg">اشترك ليصلك كل جديد</font></h3>
                    </div>
                    
                    <div class="send-message">
                        <form method="POST" id="subsForum">
                            {{csrf_field()}}
                            <input type="hidden" name="sub1" value="sub1">
                            <input type="email" name="emailsub" placeholder="البريد الالكترونى">
                            <button class="text-bold" onclick="newsubuser()" type="submit">أرسل</button>
                        </form>
                    </div>
                    
                    <div class="categories col-xs-6" style="padding: 0;">
                        <h3 class="text-bold">لأصحاب المحلات</h3>
                        <ul>
                            <li><a class="text-color-grey text-bold" href="/vendor/register" target="_blank">اضف محلك</a></li>
                        </ul>
                    </div>
                    <div class="categories col-xs-6" style="padding-left: 30px; padding-right: 0;">
                        <h3 class="text-bold">أقسام الموقع</h3>
                        <ul>
                            <li><a class="text-color-grey text-bold" href="/mobile" target="_blank">تطبيق الموبيل</a></li>
                            <li><a class="text-color-grey text-bold" href="/technical-support" target="_blank">الدعم الفنى</a></li>
                            <li><a class="text-color-grey text-bold" href="/FAQ" target="_blank">الأسئلة الأكثر شيوعاً</a></li>
                            <li><a class="text-color-grey text-bold" href="/ads-with-us" target="_blank">أعلن معنا</a></li>
                            <li><a class="text-color-grey text-bold" href="/copyright" target="_blank">الملكية الفكرية</a></li>
                            <li><a class="text-color-grey text-bold" href="/blog" target="_blank">المدونة</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="right-section col-xs-12 col-lg-4">
                    <div class="img-container">
                    <a href="/"><img class="img-responsive" src="{{asset("image/".$image->value)}}" alt="FilMahal Logo"></a>
                    </div>
                    <h3 class="text-bold">
                        من نحن ؟ ولماذا تنضم الينا ؟
                    </h3>
                    <ul>
                        <li><a class="text-color-grey text-bold" href="/about-us" target="_blank"><i class="fa fa-check fa-lg"></i>من نحن ؟</a></li>
                        <li><a class="text-color-grey text-bold" href="/terms-condition" target="_blank"><i class="fa fa-check fa-lg"></i>شروط وجود محلك فى الموقع ؟</a></li>
                        <li><a class="text-color-grey text-bold" href="/privileges" target="_blank"><i class="fa fa-check fa-lg"></i>مميزات وجوده معنا ؟</a></li>
                    </ul>
                </div>
            </div> 
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="footer-bottom text-center">
                    <div class="copy-right col-xs-12 col-md-9">
                        <p>Developed By <a style="color: #777; margin-right: 20px;" href="http://www.wasiladev.com" target="_blank">WasilaDev</a></p>
                        <p>جميع الحقوق محفوظه لشركه فى المحل دوت كوم</p>
                    </div>

                    <div class="conditions col-xs-12 col-md-3">
                        <a href="/privacy">سياسة الخصوصية</a>
                        <a href="/rules">احكام وشروط</a>
                    </div>
                </div>
            </div>
        </div>
        </footer>
        
        
    <div id="preloader">
        <div class="text">

            <img src="{{asset("image/".$image->value)}}" alt="Filmahal Logo">

        </div>
    </div>

        
<!--End Of Footer Section-->



    <script>
        $(document).ready(function() {
            cities = document.getElementById('searching').value;
            console.log(cities);
              $.ajax({
                type:'POST',
                url:'/autocomplete',
                data:{searchVal:cities},
                success: function(data){
                        var a = [data[0].title];
                        for(var i = 0; i< data.length; i++){
                          a.push(data[i].title);
                        }
                        console.log(data);
                        var arabicPhrases = new Bloodhound({
                            datumTokenizer: Bloodhound.tokenizers.whitespace,
                            queryTokenizer: Bloodhound.tokenizers.whitespace,
                            local: a

                        });
                        

                        $('.search-profile').typeahead({
                        hint: false
                        },
                        {
                        name: 'search',
                        source: arabicPhrases,
                        remote: 'city.php?query=%QUERY',
                        templates: {
                            empty: [
                            '<div class="empty-message">',
                                'لا توجد نتائج',
                            '</div>'
                            ].join('\n'),
                        }
                        });
                },
                error:function(){
                  console.log('error');
                }
              });

        });
        function newsubmobuser(){
            $('#subsmobForum').submit(function(e){
              e.preventDefault();
              var form = $('#subsmobForum');
              $.ajax({
                type:'POST',
                url:'/',
                data:form.serialize(),
                dataType:'json',
                success: function(data){
                    console.log(data);
                    $('#msg2').html('تم الارسال بنجاح');
                    $("#msg2").fadeIn(3000);
                    setTimeout(function() {
                       $('#msg2').fadeOut(2000);
                    }, 10000);
                }
              });
            });
        };
        function newsubuser(){
            $('#subsForum').submit(function(e){
              e.preventDefault();
              var form = $('#subsForum');
              $.ajax({
                type:'POST',
                url:'/',
                data:form.serialize(),
                dataType:'json',
                success: function(data){
                    console.log(data);
                    $('#msg').html('تم الأشتراك بنجاح');
                    $("#msg").fadeIn(3000);
                    setTimeout(function() {
                       $('#msg').fadeOut(2000);
                    }, 10000);
                }
              });
            });
        };
        
        function getarea(){
            cities = document.getElementById('governorate').value;
            console.log(cities);
              $.ajax({
                type:'GET',
                url:'/vendor/cities',
                data:{city:cities},
                success: function(data){
                    console.log(data);
                    $('#area > option').remove();
                    for(var i = 0; i < data.area.length; i++){
                        addoption = '<option value='+data.area[i].id+' class="level-0">'+data.area[i].name+'</option>';
                        $('#area').append(addoption);
                    }
                    addoption="";
                },
                error: function(){
                    console.log('error');
                }
              });
           // console.log('error');
        };
    </script>
    
           <script>
    
    
    paceOptions = {

    ajax: true,
    document: true,
    eventLag: false

};

Pace.on('done', function () {

    'use strict';

    $("body, html").css("overflow", "visible");

    $('#preloader').delay(500).fadeOut(800, function () {

        $(this).remove();

    });
});
    
    
    </script>
    
</body>
</html>