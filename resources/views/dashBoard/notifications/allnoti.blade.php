@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
        <div class="admin-notifications">
            <div class="header-word">
                <h2>جميع الإشعارات:</h2>
            </div>
                <ul class="messages-list noti">
                    @foreach($allnoti as $allnotis)
                    <?php $username = DB::table('users')->where('id',$allnotis->action_id)->get() ?>
                    @if(count($username) > 0)
                                 <li class="message">
                                        <a href="#">
                                            <div class="client">
                                                <div class="message-header">
                                                    <div class="img-container">
                                                        <img src="/image/400x400.png" class="img-responsive">
                                                    </div>              
                                                    <div class="client-title">
                                                        <h5>{{$username[0]->user_name}}</h5>
                                                        <p>{{$allnotis->data}}</p>
                                                        <p class="time">
                                                            <i class="fa fa-phone"></i>
                                                           {{$allnotis->created_at}}
                                                        </p>
                                                    </div>
                                            
                                                </div>
                                                <div>
                                                    
                                                </div>
                                            
                                            </div>
                                        </a>
                                    </li>
                                    @endif
                                 @endforeach                                       
                                 
                                </ul>
        </div>
    </section>
</div>
@endsection