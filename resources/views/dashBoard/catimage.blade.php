@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
        <div class="cat-image-page">
            <div class="header-word">
                <h2>صور الفئات:</h2>
            </div>
            <table id="datatable-catimages" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>الفئة الفرعية</th>
                        <th>الصورة</th>
                        <th data-orderable="false">تعديل</th>
                    </tr>
                </thead>
                 
                <tbody>
                @foreach($sub as $key => $subs)
                <?php $photo = DB::table('photo')->where('sub_category_id',$subs->id)->get(); ?>
                    <tr>
                    
                        <td>{{$key+1}}</td>
                        <td>{{$subs->name}}</td>
                        <td>
                        @if(count($photo) > 0 )
                            <img class="logo-img" id="newImg{{$key}}" src="/image/{{$photo[0]->photo}}" role="button" data-toggle="modal" data-target="#exampleModal" onclick="changeImg('{{$key}}')">
                        @endif
                        </td>
                        <td>
                            <form method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="subId" value="{{$subs->id}}" />
                                <div class="form-group edit-logo btn">
                                    <input type='file' id='logo-input' name="logo" accept="image/*">    
                                    تعديل
                                </div>
                                <button>حفظ</button>
                            </form>
                        </td>
                    </tr>
                 @endforeach
                </tbody>
            </table>
            <!--Image Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img id="oldImg" src="" class="modal-img">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
<script>
function changeImg(Data){
	img1 = document.getElementById('newImg'+Data);
	img2 = document.getElementById('oldImg');
	img2.src = img1.src;
}
</script>