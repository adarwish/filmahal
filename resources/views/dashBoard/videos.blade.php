@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
        <div class="cat-image-page">
            <div class="header-word">
                <h2>الفيديوهات:</h2>
            </div>
            <div class="col-sm-6">
                <form class="landing-video" method="POST" enctype="multipart/form-data">
                	{{csrf_field()}}
                    <label>فيديو الصفحة الرئيسية: </label>
                    <div class="form-group upload-video btn">
                        <input type="file" name="landing_video">
                        <i class="fas fa-video"></i>
                    </div>
                    <button>حفظ</button>
                </form>
            </div>
            <div class="col-sm-6">
                <form class="user-video" method="POST" enctype="multipart/form-data" >  
                	{{csrf_field()}}
                    <label>الفيديو التعريفي: </label>
                    <div class="form-group upload-video btn">
                        <input type="file" name="user_video">
                        <i class="fas fa-video"></i>
                    </div>
                    <button>حفظ</button>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection