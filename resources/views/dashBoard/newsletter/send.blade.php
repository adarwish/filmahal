@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
        <div class="newsletter">
            <div class="header-word">
                <h2>إرسال نشرة بريدية:</h2>
            </div>
            
                <div id="formStep1" class="col-lg-12">
                    <div class="col-md-12 content-rte">
                        <textarea id="summernote" name="something" required></textarea>
                    </div>
                    <h3><font id="msg" color="green"></font></h3>
                    <button class="btn btn-primary content-btn" onclick="emails()">ارسال</button>
                </div>
                <div id="formStep2" class="col-lg-12">
                    <div class="col-md-12">
                    <table id="datatable-subscribers" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                        <button type="button" id="select-all-news">اختيار الكل</button>
                        <button type="button" id="none-all-news" class="hidden">محو الكل</button>
                        <thead>
                            <tr>
                                <th class="text-center delete select-checkbox sorting_disabled" rowspan="1" colspan="1" aria-label="" style="width: 12px;"></th>
                                <th>البريد الإلكتروني</th>
                            </tr>
                        </thead>                                        
                            <tbody>
                                @foreach($subscribe as $subscribes)
                                <tr id="{{$subscribes->id}}">
                                    <td></td>
                                    <td class="sorting_1"><p>{{$subscribes->email}}</p></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            <div class="clearfix"></div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
<script>
function emails(){
    msg = document.getElementById('summernote').value;
    $('.selected').each(function () {
        const id = $(this).attr('id');
        console.log(id);
        $.ajax({
            type:'POST',
            url:'/admin.send-news.mahalatmasr2018@fmax0*',
            data:{something:msg,ids:id},
            dataType:'json',
            success: function(data){
                console.log(data);
                $('#msg').html('تم الارسال بنجاح');
                setTimeout(function(){ 
                    $('#msg').html('');
                    
                }, 2000);
            },
            error: function(){
                console.log('error');
            }
        });
    });
      
        
};
</script>
@endsection