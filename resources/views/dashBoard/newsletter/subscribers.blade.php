@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
        <div class="newsletter">
            <div class="header-word">
                <h2>إرسال نشرة بريدية:</h2>
            </div>
           <form>
                <div class="col-md-12">
                    <table id="datatable-subscribers" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                        <button type="button" id="select-all-news">اختيار الكل</button>
                        <button type="button" id="none-all-news" class="hidden">محو الكل</button>
                        <button type="button" onclick="deleteGroup()" id="delete-selected" class="btn-danger" style="margin-bottom: 20px; margin-right: 50px;">مسح المحدد</button>
                <thead>
                    <tr>
                        <th class="text-center delete select-checkbox sorting_disabled" rowspan="1" colspan="1" aria-label="" style="width: 12px;"></th>
                        <th>البريد الإلكتروني</th>
                       
                        <th>مسح</th>
                    </tr>
                </thead>                                        
                <tbody>
                @foreach($subscribe as $subscribes)
                    <tr id="{{$subscribes->id}}">
                        <td></td>
                        <td class="sorting_1"><p>{{$subscribes->email}}</p></td>
                        
                        <td><a class="delete" onclick="deletes('{{$subscribes->id}}')">مسح</a></td>
                    </tr>
                 @endforeach
                </tbody>
              </table>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('scripts')
<script>
function deletes(Data){
          $.ajax({
            type:'POST',
            url:'/api/delete/subs',
            data:{id:Data},
            success: function(data){
                console.log(data);
                $('#'+Data).remove();
                
            },
            error: function(){
            	console.log('error');
            }
          });
};
function deleteGroup(){
    $('.selected').each(function () {
        const ids = $(this).attr('id');
        console.log(ids);
        $.ajax({
            type:'POST',
            url:'/api/delete/subs',
            data:{id:ids},
            dataType:'json',
            success: function(data){
                console.log(data);
            },
            error: function(){
                console.log('error');
            }
        });
    });
      
        
};
</script>


@endsection