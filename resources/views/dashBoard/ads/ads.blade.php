@extends('dashBoard.master')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content container-fluid rtl">
        <div id="sixth-step" style="display: block">
            <div class="header-word">
                <h2>قسم الاعلانات:</h2>
                <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save1" value="save1">
                    <h4 class="text-bold">الصفحة الرئيسية</h4>
                    <div class="ads cover">
                        <div>
                            <input type="file" name="file" accept="image/gif, image/jpeg, image/png, image/jpg" id="mainPage">
                        </div>
                        <div class="ad-container img">
                            <img src="/image/{{$ads[0]->files}}" class="img-responsive" data-name="mainPage">
                        </div>
                    </div>
                    <button type="submit">حفظ</button>
                </form>
                <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save2" value="save2">
                    <h4 class="text-bold">صفحة المحل اسفل التعليقات رقم 1</h4>
                    <div class="ads cover">
                        <div>
                            <input type="file" name="file" accept="image/gif, image/jpeg, image/png, image/jpg" id="save2">
                        </div>
                        <div class="ad-container img">
                            <img src="/image/{{$ads[1]->files}}" class="img-responsive" data-name="save2">
                        </div>
                    </div>
                    <button type="submit">حفظ</button>
                </form>
                <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save3" value="save3">
                    <h4 class="text-bold">2 صفحة المحل اسفل التعليقات رقم</h4>
                    <div class="ads cover">
                        <div>
                            <input type="file" name="file" accept="image/gif, image/jpeg, image/png, image/jpg" id="save3">
                        </div>
                        <div class="ad-container img">
                            <img src="/image/{{$ads[2]->files}}" class="img-responsive" data-name="save3">
                        </div>
                    </div>
                    <button type="submit">حفظ</button>
                </form>
                <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save4" value="save4">
                    <h4 class="text-bold">3 صفحة المحل اسفل التعليقات رقم</h4>
                    <div class="ads cover">
                        <div>
                            <input type="file" name="file" accept="image/gif, image/jpeg, image/png, image/jpg" id="save4">
                        </div>
                        <div class="ad-container img">
                            <img src="/image/{{$ads[3]->files}}" class="img-responsive" data-name="save4">
                        </div>
                    </div>
                    <button type="submit">حفظ</button>
                </form>
                <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save5" value="save5">
                    <h4 class="text-bold">صفحة المحل منتصف الصفحة </h4>
                    <div class="ads cover">
                        <div>
                            <input type="file" name="file" accept="image/gif, image/jpeg, image/png, image/jpg" id="save5">
                        </div>
                        <div class="ad-container img">
                            <img src="/image/{{$ads[4]->files}}" class="img-responsive" data-name="save5">
                        </div>
                    </div>
                    <button type="submit">حفظ</button>
                </form>
                <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save6" value="save6">
                    <h4 class="text-bold">صفحة المستخدم منتصف الصفحة</h4>
                    <div class="ads cover">
                        <div>
                            <input type="file" name="file" accept="image/gif, image/jpeg, image/png, image/jpg" id="save6">
                        </div>
                        <div class="ad-container img">
                            <img src="/image/{{$ads[5]->files}}" class="img-responsive" data-name="save6">
                        </div>
                    </div>
                    <button type="submit">حفظ</button>
                </form>
                 <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save7" value="save7">
                    <h4 class="text-bold">صفحة البحث منتصف الصفحة</h4>
                    <div class="ads cover">
                        <div>
                            <input type="file" name="file" accept="image/gif, image/jpeg, image/png, image/jpg" id="save7">
                        </div>
                        <div class="ad-container img">
                            <img src="/image/{{$ads[6]->files}}" class="img-responsive" data-name="save7">
                        </div>
                    </div>
                    <button type="submit">حفظ</button>
                </form>
                 <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save8" value="save8">
                    <h4 class="text-bold"> صفحة التسجيل</h4>
                    <div class="ads cover">
                        <div>
                            <input type="file" name="file" accept="image/gif, image/jpeg, image/png, image/jpg" id="save8">
                        </div>
                        <div class="ad-container img">
                            <img src="/image/{{$ads[7]->files}}" class="img-responsive" data-name="save8">
                        </div>
                    </div>
                    <button type="submit">حفظ</button>
                </form>
                 <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save9" value="save9">
                    <h4 class="text-bold">9</h4>
                    <div class="ads cover">
                        <div>
                            <input type="file" name="file" accept="image/gif, image/jpeg, image/png, image/jpg" id="save9">
                        </div>
                        <div class="ad-container img">
                            <img src="/image/{{$ads[8]->files}}" class="img-responsive" data-name="save9">
                        </div>
                    </div>
                    <button type="submit">حفظ</button>
                </form>

                 <form method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="save10" value="save10">
                    <h4 class="text-bold">10</h4>
                    <div class="ads cover">
                        <div>
                            <input type="file" name="file" accept="image/gif, image/jpeg, image/png, image/jpg" id="save10">
                        </div>
                        <div class="ad-container img">
                            <img src="/image/{{$ads[9]->files}}" class="img-responsive" data-name="save10">
                        </div>
                    </div>
                    <button type="submit">حفظ</button>
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
  