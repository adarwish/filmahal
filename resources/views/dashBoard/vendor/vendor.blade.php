@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
<!--
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
-->
<!--
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>اختيار الفئات</a></li>

        <li class="active">فئات</li>
      </ol>
-->
    </section>

    <!-- Main content -->
    <section class="content container-fluid rtl">

        <div id="seventh-step" style="display: block">
            <form method="POST" style="text-align: center; margin-top: 20px">
              {{csrf_field()}}
                <input type="hidden" value="100" name="vendor">
                <button type="submit" value="vendall" name="all" class="" style="margin-right: 0;">الكل</button>
                <button type="submit" value="vendreject" name="all" class="">المرفوضة</button>
                <button type="submit" value="vendwaiting" name="all" class="">المعلقة</button>
                <button type="submit" value="vendtoday" name="all" class="">اليوم</button>
            </form>
            <div class="header-word">
               <h2>أصحاب المحلات:</h2>
            </div>
            <select class="action-list">
                    <option value="0" selected>اوامر</option>
                    
                    <option value="5">اضافه مستخدم</option>
                    <option value="6">تعديل مستخدم</option>
                    <option value="3">تفعيل</option>
                    <option value="7">تجميد</option>
                    <option value="4">رفض</option>
                    <option value="15" class="delete">مسح</option>
            </select>
            <button type="button" id="select-all-users" class="">اختيار الكل</button>
            <button type="button" id="none-all-users" class="hidden">محو الكل</button>
            <table id="example5" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td></td>
                        <th>اسم المُستخدم</th>
                        <th>اميل المستخدم</th>
                        <th>اسم المحل</th>
                        <th>هاتف المحل</th>
                        <th>تاريخ التسجيل</th>
                        <th>مشاهده</th>
                        <th>الحاله</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($user as $users)
                	<?php if(count($users->market) > 0){
                	
                		$cityId = DB::table('market_place')->where('market_id',$users->market[0]->id)->get();
                		} ?>
                    <tr id={{$users->id}}>
                        <td><input type="hidden" name="ids" value="{{$users->id}}" id="ids"></td>
                        <td>{{$users->user_name}}</td>
                        <td>{{$users->email}}</td>
                        @if(count($users->market) >0 )
                        <?php $cinfo = DB::table('contact_info')->where('market_id',$users->market[0]->id)->get(); ?>
                        <td>{{$users->market[0]->title}}</td>
                        @if(count($cinfo) >0 )
                        <td>{{$cinfo[0]->phone}}</td>
                        @else
                        <td>لا يوجد</td>
                        @endif
                        <td>{{$users->created_at}}</td>
                        @if(count($cityId) > 0)
                       	<td><a href="/store/profile/{{$users->market[0]->id}}/{{$cityId[0]->city_id}}/{{$cityId[0]->area_id}}/{{$users->market[0]->title}}"> مشاهده</a></td> 
                       	@else
                       	<td><a href="/store/profile/{{$users->market[0]->id}}/{{$cityId[0]->city_id}}/{{$cityId[0]->area_id}}/{{$users->market[0]->title}}"> مشاهده</a></td> 
                       	@endif
                        @else
                        <td>لا يوجد</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        @endif
                        @if($users->status == 0)
                        <td>غير نشط</td>
                        @elseif($users->status == 1)
                        <td>نشط</td>
                        @elseif($users->status == 2)
                        <td>مرفوض</td>
                        @elseif($users->status == 3)
                        <td>مجمد</td>
                        @elseif($users->status == 3)
                        <td>مجمد</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
            <button type="button" class="btn btn-info btn-lg denyUser myModel" data-toggle="modal" data-target="#denyUser">Open Modal</button>
        <div id="denyUser" class="modal fade" role="dialog">
            <form method="POST" id="rejectForum">
                {{csrf_field()}}
                <input type="hidden" name="vendor" value="3">
                <input type="hidden" name="id" id="textArea" value="3">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">سبب الرفض</h4>
                        </div>
                        <div class="modal-body">
                            <textarea name="rejectarea" placeholder="اختيارى"></textarea>
                        </div>
                        <div class="modal-footer">
                            <h3><font color="green" id="msg"></font></h3>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            
                            <button class="btn btn-info" onclick="rejected()">save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>
        <!-- Trigger the modal with a button -->
        

        <button type="button" class="btn btn-info btn-lg editUser myModel" data-toggle="modal" data-target="#editUser">Open Modal</button>
        <div id="editUser" class="modal fade" role="dialog">
            <form method="POST" id="editForum">
                {{csrf_field()}}
                <input type="hidden" name="vendor" value="4">
                <input type="hidden" name="id" id="editId" value="3">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">تعديل بيانات المستخدم</h4>
                        </div>
                        <div class="modal-body">
                            <div class="textBox">
                                <label>اسم المستخدم:</label>
                                <input type="text" name="name" id="name">
                            </div>

                            <div class="textBox">
                                <label>اميل المستخدم:</label>
                                <input type="email" name="email" id="email">
                            </div>

                            <div class="textBox">
                                <label>باسورد جديد:</label>
                                <input type="password" name="psw" id="psw">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <h3><font color="green" id="msg1"></font></h3>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button class="btn btn-info" onclick="editable()">save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
<!-- END 0f NEWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWww -->
    </section>
    <!-- /.content -->
    <section class="model">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 messageModel">
                    <div class="header-word">
                <h2>اضف خصائص:</h2>
            </div>
            
            <div class="containerOfInputs col-xs-12">
                <form id="attrs">
                    <div class="col-md-12 bigBox">
                        <div>
                            <div class="nameOfAttributes">
                                <div>
                                    <label>اسم المجموعه</label>
                                    <input type="text" name="collectionName">
                                </div>
                            </div>
                            <div class="addAttr">
                                <ul class="list-attr"></ul>
                                <label><a href="#" id="addAttr">اضف خاصيه</a></label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-md-6 box1">
                        <div class="boxOfAddAttr">
                            <label>اسم الخاصيه:</label>
                            <input type="text" name="newAttr" id="getAttrName">
                        </div>
                        <div class="howChoose">
                            <select name="choose" id="chooseFilter">
                                <option value="0">كيفية الاختيار</option>
                                <option value="1">عده اختيارات</option>
                                <option value="2">اختيار واحد</option>
                                <option value="3">اسم</option>
                            </select>
                        </div>
                        <button type="button" id="applyAttr">حفظ الخاصيه</button>
                        <button type="button" id="applyEdit">تعديل</button>
                        <button type="button" id="cancel">الغاء</button>
                    </div>
                    
                    <div id="textAttr" class="col-xs-12 col-md-6 box3">
                            <label>اسم</label>
                            <input type="text" name="attrName">
                        </div>
                    <div id="cates-branches" class="col-xs-12 col-md-6 box2">
                        <!-- <select name="cates-list">
                            <option value="clothes">ملابس</option>
                        </select> -->
                        <div class="boxAddRemove no-drop">
                            <input type="text" name="">
                            <i class="ion ion-plus-circled add-branches" aria-hidden="true"></i>
                            <i class="ion ion-minus-circled remove-branches" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <br>
                        <button id="saveCollection">حفظ المجموعه</button>
                        <button id="cancelMe">اغلاق</button>
                    </div>
                </form>                
            </div>
                </div>
            </div>
        </div>
    </section>
  </div>
@endsection
<script>
    function rejected(){
        $('#rejectForum').submit(function(e){
          e.preventDefault();
          var form = $('#rejectForum');
          $.ajax({
            type:'POST',
            url:'/api/active/vendor',
            data:form.serialize(),
            dataType:'json',
            success: function(data){
                console.log(data);
                $('#msg').html('تم الأرسال بنجاح');
                $("#msg").fadeIn(3000);
                setTimeout(function(){ 
                    location.reload();
                    
                }, 2000);
                setTimeout(function() {
                   $('#msg').fadeOut(2000);
                }, 10000);
            }
          });
        });
    };
    function editable(){
        $('#editForum').submit(function(e){
          e.preventDefault();
          var form = $('#editForum');
          $.ajax({
            type:'POST',
            url:'/api/active/vendor',
            data:form.serialize(),
            dataType:'json',
            success: function(data){
                console.log(data);
                $('#msg1').html('تم التعديل بنجاح');
                $("#msg1").fadeIn(3000);
                setTimeout(function(){ 
                    location.reload();
                    
                }, 2000);
                setTimeout(function() {
                   $('#msg').fadeOut(2000);
                }, 10000);
            }
          });
        });
    };
</script>