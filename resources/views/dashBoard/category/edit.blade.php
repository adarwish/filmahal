@extends('dashBoard.master')

@section('content')

<div class="content-wrapper">
    
    <section class="content container-fluid rtl">

      

         <div class="shreet">
                <p class="nameOfShreet text-bold">اسم المجموعه: <span>المجموعه</span></p>
        </div>
        <form method="POST">
            {{csrf_field()}}
            <table id="example3" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        
                        <th>اسم الخاصيه</th>
                        <th>قيم الخاصيه</th>
                        <th>مسح الكل</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($prop as $propty)
                    <tr>
                        <?php $subprop = DB::table('sub_properties')->where('att_properties_id',$propty->id)->get(); ?>
                        <td><input type="text" value="{{$propty->name}}" name="editpropname[]">
                        <input type="hidden" value="{{$propty->id}}" name="editpropId[]"></td>
                        <td>
                            @foreach($subprop as $subprops)
                            <div id="subpropdiv{{$subprops->id}}">
                                <input type="text" value="{{$subprops->name}}" name="editsubpropname[]">
                                <a onclick="deletesubprop('{{$subprops->id}}','{{$propty->id}}'),('{{$id}}')"><i class="ion ion-close-circled delete fa-lg"></i></a>
                                <input type="hidden" value="{{$subprops->id}}" name="editsubpropId[]">
                                
                            </div>
                            @endforeach
                        </td>
                        <td><a href="/deleteAttAll/{{$propty->id}}/{{$id}}">مسح الكل</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <br>
            <button>حفظ</button>
        </form>
    </section>
  </div>


@endsection
<script>
    function deletesubprop(Data,Data2,Data3){
        $.ajax({
            type:'GET',
            url:'/deletesubprops/',
            data:{id:Data,propID:Data2,attID:Data3},
            success: function(data){
                console.log(data);
                $('#subpropdiv'+Data).children().remove();
               
            },
            error: function(){
                console.log('error');
            }
        });
    }
</script>