@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
 

    <!-- Main content -->
<section class="content container-fluid rtl">

  <script>
  	window.onload = function () {
  		const currentURL = document.URL;
  		const second = document.getElementById("second-step");
  		const third = document.getElementById("third-step");
                $('.active').removeClass('active');
                $('#attributes').addClass('active');
  		if (currentURL.includes("add")) {
  			second.classList.add("show");
                        $('#second').addClass('active');
  			third.classList.remove("show");
  			return;
  		} else if (currentURL.includes("all")) {
  			second.classList.remove("show");
  			third.classList.add("show");
                        $('#third').addClass('active');
  			return;
  		} else {
  			second.classList.add("show");
  			third.classList.remove("show");
                        $('#second').addClass('active');
  		}
  		
  	}
  	window.onhashchange = function () {
  		const currentURL = document.URL;
  		const second = document.getElementById("second-step");
  		const third = document.getElementById("third-step");
                $('.active').removeClass('active');
                $('#attributes').addClass('active');
  		if (currentURL.includes("add")) {
  			second.classList.add("show");
                        $('#second').addClass('active');
  			third.classList.remove("show");
  			return;
  		} else if (currentURL.includes("all")) {
  			second.classList.remove("show");
  			third.classList.add("show");
                        $('#third').addClass('active');
  			return;
  		} else {
  			second.classList.add("show");
  			third.classList.remove("show");
                        $('#second').addClass('active');
  		}
  	}
  </script>


  <div id="second-step">
      <div class="header-word">
          <h2>اضف خصائص:</h2>
      </div>
      
      <div class="containerOfInputs col-xs-12">
          <form id="attrs">
             
              <input type="hidden" name="save1" value="save1">
              <div class="col-md-12 bigBox">
                  <div>
                      <div class="nameOfAttributes">
                          <div>
                              <label>اسم المجموعه</label>
                              <input type="text" name="collectionName" id="collectionName">
                              <input type="hidden" value="0" name="collectionId" id="collectionId">
                          </div>
                      </div>
                      <div class="addAttr">
                          <ul class="list-attr"></ul>
                          <label><a href="#" id="addAttr">اضف خاصيه</a></label>
                      </div>
                  </div>
              </div>
              
              <div class="col-xs-12 col-md-6 box1">
                  <div class="boxOfAddAttr">
                      <label>اسم الخاصيه:</label>
                      <input type="text" name="newAttr" id="getAttrName">
                  </div>
                  <div class="howChoose">
                      <select name="choose" id="chooseFilter">
                          <option value="0">كيفية الاختيار</option>
                          <option value="1">عده اختيارات</option>
                          <option value="2">اختيار واحد</option>
                          <option value="3">اسم</option>
                      </select>
                  </div>
                  <button id="applyAttr">حفظ الخاصيه</button>
                  <button  id="applyEdit">تعديل</button>
                  <button  id="cancel">الغاء</button>
              </div>
              
              <div id="textAttr" class="col-xs-12 col-md-6 box3">
                      <label>اسم</label>
                      <input type="text" name="attrName">
                  </div>
              <div id="cates-branches" class="col-xs-12 col-md-6 box2">
                  <!-- <select name="cates-list">
                      <option value="clothes">ملابس</option>
                  </select> -->
                  <div class="boxAddRemove no-drop" id="no-drop">
                      <input type="text" name="">
                      <i class="ion ion-plus-circled add-branches" aria-hidden="true"></i>
                      <i class="ion ion-minus-circled remove-branches" aria-hidden="true"></i>
                  </div>
                  
              </div>
              <div class="col-xs-12">
                  <br>
                  <a id="saveCollection">حفظ المجموعه</a>
              </div>
          </form>                
      </div>
      <div class="clearfix"></div>
  </div>

  
  <div id="third-step">
      <div class="header-word">
          <h2>جدول المجموعات:</h2>
      </div>
      <select class="action-list">
              <option value="0" selected>اوامر</option>
              <option value="1">تعديل</option>
              <option value="2" class="delete">مسح</option>
      </select>
      <button type="button" id="select-all-attrs" class="">اختيار الكل</button>
      <button type="button" id="none-all-attrs" class="hidden">محو الكل</button>
        <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
              <tr>
                  <td></td>
                  <th>اسم المجموعه</th>
                  <th>عدد الخصائص</th>
                  <th>تعديل</th>
              </tr>
          </thead>
          <tbody>
              @foreach($att as $atts)
              <?php $counter = count($atts->properties); ?>
              <tr id="{{$atts->id}}">
                  <td><input type="hidden" name="ids" id="ids" value="{{$atts->id}}"></td>
                  <td>{{$atts->name}}</td>
                  <td>{{$counter}}</td>
                  <td><a href="/admin.attributes-values/{{$atts->id}}.mahalatmasr2018@fmax0*">تعديل</a></td>
              </tr>
              @endforeach
             
          </tbody>
      </table>
  </div>
  
</scetion>

</div>
@endsection
<script>
 /*   $.ajax({
        type:'POST',
        url:'/admin/getEditAtt',
        data:{
            id: $(".selected .select-checkbox input").val()
        },
        $('#valprops > li').remove();
        success:function (data) {
            console.log(data);
            
             editvalprop = '<li>data.propval[i].name</li>';
             $('#valprops).append(editvalprop);
        },
        error:function(){
            console.log('error');
        }
    });*/
</script>