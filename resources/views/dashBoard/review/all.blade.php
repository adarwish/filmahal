@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
<!--
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
-->
<!--
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>اختيار الفئات</a></li>

        <li class="active">فئات</li>
      </ol>
-->
    </section>
    <!-- Main content -->
    <section class="content container-fluid rtl">

        <div id="seventh-step" style="display: block">
            <form method="POST" style="text-align: center;">
              {{csrf_field()}}
                <input type="hidden" value="100" name="vendor">
                <button type="submit" value="comall" name="all" class="" style="margin-right: 0;">الكل</button>
                <button type="submit" value="comreject" name="all" class="">المرفوضة</button>
                <button type="submit" value="comwaiting" name="all" class="">المعلقة</button>
                <button type="submit" value="comtoday" name="all" class="">اليوم</button>  
            </form>
            <div class="header-word">
               <h2>التعليقات:</h2>
            </div>
            <select class="action-list">
                    <option value="0" selected>اوامر</option>
                    <option value="88">تفعيل</option>
                    <option value="99">تجميد</option>
                    <option value="100" class="delete">مسح</option>
            </select>
            <button type="button" id="select-all-users" class="">اختيار الكل</button>
            
            <button type="button" id="none-all-users" class="hidden">محو الكل</button>
            <table id="example4" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td></td>
                        <th>اسم المستخدم</th>
                        <th>اسم المحل</th>
                        <th>التعليق</th>
                        <th>بريد المستخدم</th>
                        <th>بريد المحل</th>
                        <th>الحاله</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($review as $reviews)
                <?php $store = DB::table('market')->where('id',$reviews->market_id)->get();
                ?>
                    <tr id={{$reviews->id}}>
                        <td><input type="hidden" name="ids" value="{{$reviews->id}}" id="ids"></td>
                        <td>{{$reviews->user->user_name}}</td>
                        <td>{{$store[0]->title}}</td>
                       
                      
                        <td>{{$reviews->comment}}</td>
                     
                        <td>{{$reviews->user->email}}</td>
                        
                        <td>{{$reviews->market->user->email}}</td>                      
                        @if($reviews->status == 0)
                        <td>غير نشط</td>
                        @elseif($reviews->status == 1)
                        <td>نشط</td>
                        @elseif($reviews->status == 2)
                        <td>مرفوض</td>
                       
                        
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
            <button type="button" class="btn btn-info btn-lg denyUser myModel" data-toggle="modal" data-target="#denyUser">Open Modal</button>
        <div id="denyUser" class="modal fade" role="dialog">
            <form method="POST" id="rejectForum">
                {{csrf_field()}}
                <input type="hidden" name="vendor" value="7">
                <input type="hidden" name="id" id="textArea" value="3">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">سبب الرفض</h4>
                        </div>
                        <div class="modal-body">
                            <textarea name="rejectarea" placeholder="اختيارى"></textarea>
                        </div>
                        <div class="modal-footer">
                            <h3><font color="green" id="msg"></font></h3>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            
                            <button class="btn btn-info" onclick="rejected()">save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>
        <!-- Trigger the modal with a button -->
        

        <button type="button" class="btn btn-info btn-lg editUser myModel" data-toggle="modal" data-target="#editUser">Open Modal</button>
        <div id="editUser" class="modal fade" role="dialog">
            <form method="POST" id="editForum">
                {{csrf_field()}}
                <input type="hidden" name="vendor" value="4">
                <input type="hidden" name="id" id="editId" value="3">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">تعديل بيانات المستخدم</h4>
                        </div>
                        <div class="modal-body">
                            <div class="textBox">
                                <label>اسم المستخدم:</label>
                                <input type="text" name="name" id="name">
                            </div>

                            <div class="textBox">
                                <label>اميل المستخدم:</label>
                                <input type="email" name="email" id="email">
                            </div>

                            <div class="textBox">
                                <label>باسورد جديد:</label>
                                <input type="password" name="psw" id="psw">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <h3><font color="green" id="msg1"></font></h3>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button class="btn btn-info" onclick="editable()">save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
<!-- END 0f NEWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWww -->
    </section>
    <!-- /.content -->
    <section class="model">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 messageModel">
                    <div class="header-word">
                <h2>اضف خصائص:</h2>
            </div>
            
            <div class="containerOfInputs col-xs-12">
                <form id="attrs">
                    <div class="col-md-12 bigBox">
                        <div>
                            <div class="nameOfAttributes">
                                <div>
                                    <label>اسم المجموعه</label>
                                    <input type="text" name="collectionName">
                                </div>
                            </div>
                            <div class="addAttr">
                                <ul class="list-attr"></ul>
                                <label><a href="#" id="addAttr">اضف خاصيه</a></label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-md-6 box1">
                        <div class="boxOfAddAttr">
                            <label>اسم الخاصيه:</label>
                            <input type="text" name="newAttr" id="getAttrName">
                        </div>
                        <div class="howChoose">
                            <select name="choose" id="chooseFilter">
                                <option value="0">كيفية الاختيار</option>
                                <option value="1">عده اختيارات</option>
                                <option value="2">اختيار واحد</option>
                                <option value="3">اسم</option>
                            </select>
                        </div>
                        <button type="button" id="applyAttr">حفظ الخاصيه</button>
                        <button type="button" id="applyEdit">تعديل</button>
                        <button type="button" id="cancel">الغاء</button>
                    </div>
                    
                    <div id="textAttr" class="col-xs-12 col-md-6 box3">
                            <label>اسم</label>
                            <input type="text" name="attrName">
                        </div>
                    <div id="cates-branches" class="col-xs-12 col-md-6 box2">
                        <!-- <select name="cates-list">
                            <option value="clothes">ملابس</option>
                        </select> -->
                        <div class="boxAddRemove no-drop">
                            <input type="text" name="">
                            <i class="ion ion-plus-circled add-branches" aria-hidden="true"></i>
                            <i class="ion ion-minus-circled remove-branches" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <br>
                        <button id="saveCollection">حفظ المجموعه</button>
                        <button id="cancelMe">اغلاق</button>
                    </div>
                </form>                
            </div>
                </div>
            </div>
        </div>
    </section>
  </div>
  
  
  
  				<div class="modal fade edit-post-window" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                      <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                        <div class="modal-header">
                            <div id="titledetails"><h5 class="modal-title" id="exampleModalLongTitle"></h5></div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                                              <!-- Modal Content -->
                                         
                                          <div class="products-modal">
                                           <div class="products-inputs">
                                            	<div class="input"><label>الاسم</label><input type="text" value="0" id="proname" disabled></div>
				                <div class="input"><label>الوصف</label><input type="text" value="0" id="desc" disabled></div>
				                <div class="input"><input type="text" value="0" id="prodiscount" disabled></div>
				                <div class="input"><label>السعر بعد التخفيض</label><input type="text" value="0" id="prodiscount2" disabled></div>
				                <div class="input"><label>السعر الأصلي</label><input type="text" value="0" id="price" disabled></div>
				                <div class="input"><label>الحالة</label><input type="text" value="0" id="pronew" disabled></div>
				                <div class="input"><label>نسبة التخفيض</label><input type="text" value="0" id="percantage" disabled></div>
                                             </div>
                                                
                                                <div class="images">
				                    <img src="0" id="photo1">
				                    <img src="0" id="photo2">
				                    <img src="0" id="photo3">
				                    <img src="0" id="photo4">
				                    <img src="0" id="photo5">
                                                    <div class="clearfix"></div>
                                                </div>
<div class="clearfix"></div>
                                          </div>
                                        </div>
                                     </div>
                                </div>
@endsection
<script>
    function rejected(){
        $('#rejectForum').submit(function(e){
          e.preventDefault();
          var form = $('#rejectForum');
          $.ajax({
            type:'POST',
            url:'/api/active/vendor',
            data:form.serialize(),
            dataType:'json',
            success: function(data){
                console.log(data);
                $('#msg').html('تم الأرسال بنجاح');
                $("#msg").fadeIn(3000);
                setTimeout(function(){ 
                    location.reload();
                    
                }, 2000);
                setTimeout(function() {
                   $('#msg').fadeOut(2000);
                }, 10000);
            }
          });
        });
    };
    function productinfo(Data1,Data2,Data3,Data4,Data5,Data6,Data7,Data8,Data9,Data10,Data11){
    	a =  document.getElementById('proname');
    	a.value  = Data1;
    	
    	b =  document.getElementById('desc');
    	b.value = Data2;
    	
    	CG = document.getElementById('prodiscount');
    	MG = document.getElementById('percantage');
    	KM = document.getElementById('prodiscount2');
    	
    	if(Data3 == 0){
    		CG.value = "";
    		MG.value = "";
    		KM.value = "";
    	}else{
    		CG.value = "تخفيضات";
    		MG.value = Data7;
    		KM.value = Data3;
    	}
    	
    	
    	d = document.getElementById('price');
    	d.value = Data4;
    	
    	L = document.getElementById('pronew');
    	if(Data5 == 0){
    		L.value = "";
    	}else{
    		L.value = "منتج جديد";
    	}
    	
    	f = document.getElementById('photo1');
    	f.src = "/image/"+Data6;
    	
    	g = document.getElementById('photo2');
    	g.src = "/image/"+Data8;
    	
    	h = document.getElementById('photo3');
    	h.src = "/image/"+Data9;
    	
    	r = document.getElementById('photo4');
    	r.src = "/image/"+Data10;
    	
    	k = document.getElementById('photo5');
    	k.src = "/image/"+Data11;
    	
    	
    	
    }
</script>
