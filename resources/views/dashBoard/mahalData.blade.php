@extends('dashBoard.master')

@section('content')
<?php $homeData = DB::table('dynamic_home')->get(); ?>
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
        <div class="data-page">
            <div class="header-word">
                <h2>مدخلات الصفحة الرئيسية:</h2>
            </div>
            <form method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="col-md-6 col-xs-12 blog-right">
                    <div class="form-group">
                        <label>عنوان</label>
                        <input type="text" name="homeTitle" class="form-control" value="{{$homeData[0]->data}}" style="margin-bottom: 20px;">
                    </div>
                    <div class="form-group">
                        <label>أرقام الموبايل</label>
                        <input type="text" name="mobileFooter1" value="{{$homeData[1]->data}}" class="form-control" style="margin-bottom: 20px;">
                        <input type="text" name="mobileFooter2" value="{{$homeData[2]->data}}" class="form-control" style="margin-bottom: 20px;">
                    </div>
                    <div class="form-group">
                        <label>روابط التواصل الاجتماعي</label>
                        <input type="text" name="social1" value="{{$homeData[3]->data}}" class="form-control" style="margin-bottom: 20px;" placeholder="Facebook">
                        <input type="text" name="social2" value="{{$homeData[4]->data}}" class="form-control" style="margin-bottom: 20px;" placeholder="Youtube">
                        <input type="text" name="social3" value="{{$homeData[5]->data}}" class="form-control" style="margin-bottom: 20px;" placeholder="Instagram">
                        <input type="text" name="social4" value="{{$homeData[6]->data}}" class="form-control" style="margin-bottom: 20px;" placeholder="Twitter">
                        <input type="text" name="social5" value="{{$homeData[7]->data}}" class="form-control" style="margin-bottom: 20px;" placeholder="Google+">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 blog-left">
                    <h2>صورة اللوجو</h2>
                    <div class="slider-img">
                        <div class="form-group">
                            <input id='slider1' name="filamahlLogo" type="file" class="dropify"> 
                        </div>
                    </div>
                </div>
                <button>Save</button>
            </form>
        </div>
    </section>
</div>
@endsection