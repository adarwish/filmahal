@extends('dashBoard.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">

</section>

<!-- Main content -->
<section class="content container-fluid rtl">

 
    <div id="first-step">
        <div class="header-word">
            <h2>ادخل الفئات:</h2>
        </div>
        <form class="col-sm-8" method="POST" id="adminCatControl">
        	{{csrf_field()}}
            <input type="hidden" name="save2" value="2">
            <div id="example_wrapperr" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                            <button type="submit" onclick="editelement()"  id="apply" class="hidden">تنفيذ</button>
                            <!--Start First-->
                            <div class="actions">
                            <div class="acitonsthings">
                                <select id="action-list">
                                    <option value="0" selected>أوامر</option>
                                    <option value="1">تعديل</option>
                                    <option value="2" class="delete">مسح</option>
                                </select>
                                <button type="button" id="select-all">اختيار الكل</button>
                                <button type="button" id="none-all" class="hidden">محو الكل</button>
                            </div>
                        </div><!-- END First   -->
                            <thead>
                                <tr role="row">
                                    <th class="text-center delete select-checkbox sorting_disabled" rowspan="1" colspan="1" aria-label="" style="width: 12px;">

                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 113px;">فئات</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 194px;">الوصف</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 82px;">العدد</th>
                                </tr>
                            </thead>

                            <tbody>
                            	<?php 
                            	$counter=0;
                            	 ?>
                            	@foreach($category as $categories)

	                            	<?php $counter++;
	                            	$counter1 =0;
                                     ?>
	                                <tr role="row" id="c{{$categories->id}}">
	                                    <td></td>
	                                    <td class="sorting_1" data-search="{{$categories->name}}" ><input type="text" name="newcat[]" value="{{$categories->name}}" disabled>
	                                    <input type="hidden" name="catIds[]" value="{{$categories->id}}"  disabled>
	                                    </td>
	                                    <td data-search="{{$categories->description}}"><textarea name="textarea1[]" disabled>{{$categories->description}}</textarea></td>
	                                    <td>{{$counter}}</td>
	                                    
	                                </tr>
	                                <?php $subcategory = $categories->sub_category; ?>
	                                @foreach($subcategory as $subcategory)

	                            	<?php $counter1++ ?>
	                                <tr role="row" id="s{{$subcategory->id}}">
	                                    <td></td>
	                                    <td class="sorting_1" data-search="{{$subcategory->name}}"><input type="text" value="{{$subcategory->name}}" name="newsub[]" disabled>
	                                    <input type="hidden" name="subsIds[]" value="{{$subcategory->id}}" disabled>
	                                	</td>
	                                    

	                                    <td data-search="{{$subcategory->description}}"><textarea name="textarea2[]" disabled>{{$subcategory->description}}</textarea></td>
	                                    <td>{{$counter}}.{{$counter1}}</td>
	                                </tr>
                                    <?php $subsubcategory = $subcategory->target;
                                    $counter2 =0; ?>
                                        @foreach($subsubcategory as $subsubcategory)
                                        <?php $counter2++ ?>
                                        <tr role="row" id="t{{$subsubcategory->id}}">
                                            <td></td>
                                            <td class="sorting_1"><input type="text" value="{{$subsubcategory->name}}" name="newsubsub[]" disabled>
                                            <input type="hidden" name="subsubsIds[]" value="{{$subsubcategory->id}}" disabled>
                                            </td>                                            
                                            <td><textarea name="textarea3[]" disabled>{{$subsubcategory->description}}</textarea></td>
                                            <td>{{$counter}}.{{$counter1}}.{{$counter2}}</td>
                                           
                                        </tr>
                                        @endforeach
	                                @endforeach
                                @endforeach
                                                             
                            </tbody>
                        </table>
                    </div>
                </div>
        </form>
                
            </div>
            
            <form class="col-xs-12 col-md-4">   
                <div class="containerOfInputs ">
                    
                    <div id="inputs-cates">
                        <label>اسم الفئه:</label>
                        <input id="add-cates" type="text" name="cates">

                        <select id="select-cates" name="selectcates">
                            <option class="none" value="00">فئة رئيسيه</option>
                            
                           	@foreach($category as $cat)
                           		<?php $subs = $cat->sub_category; ?>
                            	<option value="0{{$cat->id}}" class="level-0">{{$cat->name}}</option>
                            	@foreach($subs as $subcat)
                            		<?php $subsub = $subcat->target; ?>
                            		<option value="1{{$subcat->id}}" class="level-1">&nbsp;&nbsp;&nbsp;{{$subcat->name}}</option>
                            		@foreach($subsub as $targets)
                            			
	                            		<option value="2{{$subcat->id}}" class="level-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$targets->name}}</option>
	                            	@endforeach
                            	@endforeach
                            @endforeach

                        </select>
                        
                        <select name="attrs" id="attrsChoose" >
                            <option value="0">اختر مجموعه</option>
                            @foreach($attribute as $attributes)
                                <option value="{{$attributes->id}}">{{$attributes->name}}</option>
                            @endforeach
                        </select>
                        <div class="choosen"></div>
                    </div>
                    <!--<h3><font id="msg" color="green"></font></h3>
                    <p class="error" id="errorMessage"></p>-->
        	
                   <!-- <button id="save" data-toggle="modal" aria-label="Show SweetAlert2 success message">حفظ</button> -->
                   
                   <a name="save" onclick="newelement()" value="save" class="btn btn-primary" id="enableSave">حفظ</a>
                </div>
        </form>
    

    </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
<script>

	function newelement(){
        addcates = document.getElementById('add-cates').value;
        selectcate = document.getElementById('select-cates').value;
        selectatt = document.getElementById('attrsChoose').value;//$("#attrsChoose").val();
      //  console.log(selectatt);
          $.ajax({
            type:'POST',
            url:'/admin.home.mahalatmasr2018@fmax0*',
            data:{save:1,selectcates:selectcate,cates:addcates,selectatts:selectatt},
            success: function(data){
                console.log(data);

                   // $('#msg').html('تم الحفظ بنجاح');
                    $('#select-cates > option').remove();

                    for(var i = 0; i < data.cat.length; i++){
                        addoption = '<option value='+0+data.cat[i].id+' class="level-0">'+data.cat[i].name+'</option>';
                        $('#select-cates').append(addoption);
                        for(var s=0; s<data.sub.length; s++){
                            if(data.sub[s].category_id == data.cat[i].id){
                                addoptintwo = '<option value='+1+data.sub[s].id+' class="level-1">&nbsp;&nbsp;&nbsp;'+data.sub[s].name+'</option>';
                                $('#select-cates').append(addoptintwo);
                                for(var m=0; m<data.subsub.length; m++){
                                    if(data.subsub[m].sub_id == data.sub[s].id){
                                        addoptinthird = '<option value='+2+data.subsub[m].id+' class="level-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+data.subsub[m].name+'</option>';
                                        $('#select-cates').append(addoptinthird);
                                    }
                                }
                            }
                        }
                        
                    }
 
              
            },
            error: function(){
            	console.log('error');
            }
          });
       // console.log('error');
    };
    function apply() {
        var inputs = $("tr").find("input");
        var areas = $("tr").find("textarea");
        
        inputs.each(function(index) {
            if(!$(inputs[index]).prop("disabled") && $(inputs[index]).is("[type=text]")) {
               // $(inputs[index]).prop("disabled", true);
                    $(inputs[index]).attr("disabled", true);
                console.log($(inputs[index]).val());
            }
           
        });
        
        areas.each(function(index) {
           if(!$(areas[index]).prop("disabled")) {
                    $(areas[index]).attr("disabled", true);
            };
        });
        $("#action-list option:first").prop("selected", true);
    }
    function editelement(){
        $('#adminCatControl').submit(function(e){
          e.preventDefault();
          var form = $('#adminCatControl');
          $.ajax({
            type:'POST',
            url:'/admin.home.mahalatmasr2018@fmax0*',
            data:form.serialize(),
            success: function(data){
                console.log(data);
                apply();                           
            },
            error: function(){
                console.log('error');
                apply();
            }
          });
        });
       // console.log('error');
    };
</script>