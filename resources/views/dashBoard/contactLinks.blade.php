@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
        <div class="links-page">
            <div class="header-word">
                <h2>روابط التواصل:</h2>
            </div>
            <div>
                <form method="POST" class="col-sm-6 col-xs-12">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>أرقام الهاتف</label>
                        <input type="text" name="mobile1" value="{{$contact[0]->data}}" id="mobile1">
                        <input type="text" name="mobile2" value="{{$contact[1]->data}}" id="mobile2">
                    </div>
                    <div class="form-group">
                        <label>البريد الالكتروني</label>
                        <input type="email" name="email" value="{{$contact[3]->data}}">
                    </div>
                    <div class="form-group">
                        <label>رقم الواتساب</label>
                        <input type="text" name="whatsapp" value="{{$contact[2]->data}}">
                    </div>
                    <div class="form-group">
                        <label>روابط التواصل الاجتماعي</label>
                        <input type="text" name="facebook" placeholder="Facebook..." value="{{$contact[4]->data}}">
                        <input type="text" name="instagram" placeholder="Instagram..." value="{{$contact[5]->data}}">
                        <input type="text" name="google" placeholder="Google+..." value="{{$contact[6]->data}}">
                        <input type="text" name="twitter" placeholder="Twitter..." value="{{$contact[7]->data}}">
                        <input type="text" name="youtube" placeholder="Youtube..." value="{{$contact[8]->data}}">
                    </div>
                    <button class="btn btn-primary">حفظ</button>
                </form>
                <form method="POST" action="/api/imageLogo" enctype="multipart/form-data" class="col-sm-6 col-xs-12">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>اللوجو</label>
                        <input type="file" name="image"  data-default-file="{{asset("image/".$image->value)}}" class="dropify" accept="image/*">
                        <button type="button" onClick="submitHandler()" class="btn btn-primary">تغيير</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection
<script>
function changeImg(Data){
	img1 = document.getElementById('newImg'+Data);
	img2 = document.getElementById('oldImg');
	img2.src = img1.src;
}

function submitHandler(data){
    let url = "/api/imageLogo";
    let _token = $('input[name="_token"]').val();
    let img = $('body').find('input[name="image"]').prop('files')[0];
    
    var form_data = new FormData();
    form_data.append('IMAGE', img);
    	  	
    $.ajax({
        headers: { 
            "X-CSRF-TOKEN" : _token
        },
        type:'POST',
        url:url,
        data: form_data,
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false,
    
        success:function (data) {
            console.log(data);
        },
        error:function(){
        console.log(error);
        }
        });
}
</script>