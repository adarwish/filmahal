@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl all-logos-page">
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        

                    </div>

                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>جميع اللوجوهات</h2>


                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="datatable-logos" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>اللوجو</th>
                                                <th data-orderable="false">تعديل/مسح</th>
                                               
                                            </tr>
                                        </thead>
                                        
                                        <?php $counter = 0; ?>
                                        
                                        <tbody>
                                           @foreach($icon as $icons) 
                                            <tr>
                                                <td>1</td>
                                                <td><img class="logo-img" src="/image/{{$icons->icon}}"></td>
                                                <td>
                                            
                                                  
                                                      <form method="POST" enctype="multipart/form-data">
                                                  	{{csrf_field()}}
                                                  	<div class="form-group edit-logo btn">
                                                  	<input type="hidden" value="{{$icons->id}}" name="iconsId">
                                                      	<input type='file' id='logo-input' name="logo" accept="image/*">    
                                                      	تعديل
                                                      	
                                                      	
                                                      	</div>
                                                      	<button>حفظ</button>
                                                      	</form>
                                                  
                                                                                                    
                                                  <a href="/icons/delete/{{$icons->id}}" class="btn delete">
                                                    مسح
                                                  </a>
                                                </td>
                                            </tr>
					   @endforeach
                                        </tbody>
                                    </table>
                                    
                            </div>
                        </div>


                </div>
            </div>
            <!-- /page content -->
            
               
        </div>
    </div>

    </section>
</div>
    @endsection