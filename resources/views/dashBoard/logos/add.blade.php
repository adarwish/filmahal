@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
        <div class="add-logo-page">
            <div class="header-word">
                <h2>اضف لوجو:</h2>
            </div>
            <form method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <h3>اختر صورة</h3>
                <div class="form-group">
                    <!--<input type='file' id='logo-input' name="logo" accept="image/*">    
                    <i class="fa fa-image"></i>-->
                    <input id='logo-input' name="logo" type="file" class="dropify" data-height="">
                </div>
                <button class="btn btn-primary">حفظ</button>
            </form>
        </div>
    </section>
</div>
@endsection