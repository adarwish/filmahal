<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>لوحة التحكم</title>
  <link rel="shortcut icon" href="{{asset('imgs/favIcon/favicon-32x32.png')}}" type="image/png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  
  <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="{{asset('admin/dist/css/summernote.css')}}">
  <link rel="stylesheet" href="{{asset('admin/dist/css/dropify.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.css')}}">
  <link rel="stylesheet" href="{{asset('admin/dist/css/skins/_all-skins.css')}}">
  
  <link rel="stylesheet" href="{{asset('admin/dist/css/skins/skin-blue.css')}}">
  <link rel="stylesheet" href="{{asset('profiles/css/main0.css')}}">
  
  <script defer src="{{asset('admin/dist/js/sweetalert2.all.min.js')}}"></script>
  <script defer src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <script defer src="{{asset('admin/dist/js/sweetalert2.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('admin/dist/css/sweetalert2.min.css')}}">
  <!-- Google Font -->
<!--  <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">-->
  <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,700&amp;subset=arabic" rel="stylesheet">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  
  <link rel="stylesheet" type="text/css" href="{{asset('admin/dist/DataTables/datatables.min.css')}}"/>

  <script defer src="{{asset('admin/dist/DataTables/datatables.min.js')}}"></script>    

  <script defer src="{{asset('admin/dist/js/inputs.js')}}"></script>
  <script defer src="{{asset('js/js.js')}}"></script>
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="{{asset('imgs/Logo.png')}}" style="max-width: 50px; max-height: 50px;"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>لوحة التحكم</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              	 <i class="fa fa-bell-o"></i>
	            	</a>
	            	<ul class="dropdown-menu">
		              <li>
		                <!-- Inner Menu: contains the notifications -->
		                <!--<ul class="menu">
		                  <li>
		                    <a href="#">
		                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
		                    </a>
		                  </li>
		                </ul>-->
		                @if(Auth::check())
                                <?php $notifications = DB::table('noti')->where('user_id',Auth::user()->id)->get();
                                $notifications2 = DB::table('noti')->where('user_id',Auth::user()->id)->limit(3)->orderBy('id','DESC')->get();
                                $counter = count($notifications);
                                
                                $old_noti = DB::table('noti_counter')->where('user_id',Auth::user()->id)->get();
                                $counter_noti = count($old_noti);
                                if($counter_noti >0){
                                    DB::table('noti_counter')->where('user_id',Auth::user()->id)->update(['counter'=>$counter]);
                                }else{
                                    DB::table('noti_counter')->insert(['counter'=>$counter,'user_id'=>Auth::user()->id]);
                                }

                                 ?>

                                <ul>
                                    @if($counter !==0)
                                    @foreach($notifications2 as $noti)
                                    <?php $marketnoti = DB::table('market')->where('user_id',$noti->action_id)->get();
                                    if(count($marketnoti) > 0){
                                    $contact_info = DB::table('contact_info')->where('market_id',$marketnoti[0]->id)->get();
                                    } ?>
                                    @if(count($marketnoti) > 0)
                                    <li class="message">
                                        <a href="#">
                                            <div class="client">
                                                <div class="message-header">
                                                    <div class="img-container">
                                                        <img src="/image/{{$contact_info[0]->icon}}" class="img-responsive">
                                                    </div>              
                                                    <div class="client-title">
                                                        <h5>{{$marketnoti[0]->title}}</h5>
                                                        <p>{{$noti->data}}</p>
                                                        <p class="time">
                                                            <i class="fa fa-phone"></i>
                                                            {{$noti->created_at}}
                                                        </p>
                                                    </div>
                                            
                                                </div>
                                                <div>
                                                    
                                                </div>
                                            
                                            </div>
                                        </a>
                                    </li>
                                    @endif
                                    @endforeach
                                    @endif

                                                            
                                    <li class="more-messages">
                                        <p><a href="/admin.all-notification.mahalatmasr2018@fmax0*">شاهد جميع الاشعارات</a></p>
                                        <p><a href="/admin.all-notification.mahalatmasr2018@fmax0*"><font id="countnoti">{{$counter}} اشعار اخر</font></a></p>
                                        <div class="clear"></div>
                                    </li>
                                </ul>
                                @endif
		            </li>
		        </ul>
                            <!--<div class="slide-down">
                               
                            </div>-->
          </li>
          
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="/admin/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">الأدمن</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  اسم الأدمن
                  <small>Member since Feb. 2018</small>
                </p>
              </li>
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <!--<div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>-->
                <div class="pull-right">
                  <a href="/logout" class="btn btn-default btn-flat">تسجيل خروج</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-right image">
          <img src="/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-right info">
          <p>الأدمن</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> متاح</a>
        </div>
      </div>

      

     <!-- Sidebar Menu -->
<ul class="sidebar-menu" data-widget="tree">
  <!-- Optionally, you can add icons to the links -->

  <li class="active treeview" id="firstt">
    <a href="#">
      <i class="fa fa-link"></i>
      <span>فئات</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li id="cattts">
        <a href="/admin.home.mahalatmasr2018@fmax0*">
          <span>ادخل الفئات</span>
        </a>
      </li>
      <!-- NEW  -->
      <li id="catimages">
        <a href="/admin.sub-categories/photos.mahalatmasr2018@fmax0*">
          <span>صور الفئات الفرعية</span>
        </a>
      </li>

    </ul>
  </li>

  <li class="treeview" id="attributes">
    <a href="#" class="attributesPage">
      <i class="fa fa-link"></i>
      <span>خصائص</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <!-- NEW  -->
      <li id="second">
        <a href="/admin.attributes.mahalatmasr2018@fmax0*#add">
          <span> ادخل خصائص</span>
        </a>
      </li>
      <!-- NEW  -->
      <li id="third">
        <a href="/admin.attributes.mahalatmasr2018@fmax0*#all">
          <span>جدول المجموعات</span>
        </a>
      </li>

    </ul>
  </li>

    <li class="treeview" id="area">
      <a href="#">
        <i class="fa fa-link"></i>
        <span>المناطق</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li id="fourth">
          <a href="/admin.cities-areas.mahalatmasr2018@fmax0*#add">
            <span>اضافة المناطق</span>
          </a>
        </li>
        <li id="fifth">
          <a href="/admin.cities-areas.mahalatmasr2018@fmax0*#all">
            <span>جدول المناطق</span>
          </a>
        </li>
      </ul>
      </li>

      <li class="treeview" id="blog">
          <a href="#" class="citiesPage">
            <i class="fa fa-link"></i>
            <span>المدونة</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="add-blog">
              <a href="/admin.add-blog.mahalatmasr2018@fmax0*">
                <span>اضافة مدونة</span>
              </a>
            </li>
            <li id="blog-cates">
              <a href="/admin.blog-category.mahalatmasr2018@fmax0*">
                <span>تصنيفات المدونة</span>
              </a>
            </li>
            <li id="all-blog">
              <a href="/admin.allPosts.mahalatmasr2018@fmax0*">
                <span>جميع المدونات</span>
              </a>
            </li>
          </ul>
          </li>

      <li id="vendors">
        <a href="/admin.all-vendors.mahalatmasr2018@fmax0*">
          <i class="fa fa-link"></i>
          <span>اصحاب المحلات</span>
        </a>
      </li>
      <li id="users">
        <a href="/admin.all-users.mahalatmasr2018@fmax0*">
          <i class="fa fa-link"></i>
          <span>المستخدمين</span>
        </a>
      </li>
      <li id="products">
        <a href="/admin.all-products.mahalatmasr2018@fmax0*">
          <i class="fa fa-link"></i>
          <span>المنتجات</span>
        </a>
      </li>
      <li id="ads">
        <a href="/admin.all-Ads.mahalatmasr2018@fmax0*">
          <i class="fa fa-link"></i>
          <span>اعلانات</span>
        </a>
      </li>
      <li id="ads">
        <a href="/admin.all-comments.mahalatmasr2018@fmax0*">
          <i class="fa fa-link"></i>
          <span>تعليقات</span>
        </a>
      </li>
      <li class="treeview" id="logos">
      <a href="#">
        <i class="fa fa-link"></i>
        <span>اللوجوهات</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li id="add-logo">
          <a href="/admin.add-logo.mahalatmasr2018@fmax0*">
            <span>اضافة لوجو</span>
          </a>
        </li>
        <li id="all-logo">
          <a href="/admin.Logos.mahalatmasr2018@fmax0*">
            <span>جميع اللوجوهات</span>
          </a>
        </li>
      </ul>
      </li>
      <li class="treeview" id="content-pages">
      <a href="#">
        <i class="fa fa-link"></i>
        <span>صفحات المحتوى</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li id="who-are-we">
          <a href="/admin.about.mahalatmasr2018@fmax0*">
            <span>من نحن</span>
          </a>
        </li>
        <li id="vendor-terms">
          <a href="/admin.terms.mahalatmasr2018@fmax0*">
            <span>شروط وجود محلك في الموقع</span>
          </a>
        </li>
        <li id="privileges">
          <a href="/admin.privileges.mahalatmasr2018@fmax0*">
            <span>مميزات وجودك معنا</span>
          </a>
        </li>
        <li id="property">
          <a href="/admin.intellectual-property.mahalatmasr2018@fmax0*">
            <span>الملكية الفكرية</span>
          </a>
        </li>
        <li id="add-ad">
          <a href="/admin.ads-with-us.mahalatmasr2018@fmax0*">
            <span>أعلن معنا</span>
          </a>
        </li>
        <li id="faq">
          <a href="/admin.FAQ.mahalatmasr2018@fmax0*">
            <span>الأسئلة الأكثر شيوعا</span>
          </a>
        </li>
        <li id="support">
          <a href="/admin.technical-support.mahalatmasr2018@fmax0*">
            <span>الدعم الفني</span>
          </a>
        </li>
        <li id="mobile-app">
          <a href="/admin.mobile.mahalatmasr2018@fmax0*">
            <span>تطبيق الموبايل</span>
          </a>
        </li>
        <li id="rules">
          <a href="/admin.rules.mahalatmasr2018@fmax0*">
            <span>أحكام وشروط</span>
          </a>
        </li>
        <li id="privacy">
          <a href="/admin.privacy.mahalatmasr2018@fmax0*">
            <span>سياسة الخصوصية</span>
          </a>
        </li>
      </ul>
      </li>
      <li id="videos">
        <a href="/admin.videos.mahalatmasr2018@fmax0*">
          <i class="fa fa-link"></i>
          <span>الفيديوهات</span>
        </a>
      </li>
      <li class="treeview" id="newsletter">
      <a href="#">
        <i class="fa fa-link"></i>
        <span>النشرة البريدية</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li id="send-news">
          <a href="/admin.send-news.mahalatmasr2018@fmax0*">
            <span>اضافة نشرة بريدية</span>
          </a>
        </li>
        <li id="subscribers">
          <a href="/admin.subscribers.mahalatmasr2018@fmax0*">
            <span>المشتركين</span>
          </a>
        </li>
      </ul>
      </li>
      <li id="contactLinks">
        <a href="/admin.contact_links.mahalatmasr2018@fmax0*">
          <i class="fa fa-link"></i>
          <span>روابط التواصل</span>
        </a>
      </li>

</ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

<!--Script-->
<script>
        const vendors = document.getElementById("vendors");
        const users = document.getElementById("users");
        const products = document.getElementById("products");
        const ads = document.getElementById("ads");
  	window.onload = function () {
  		const currentURL = document.URL;
                $(".active").removeClass("active");
  		if (currentURL.includes("home")) {
                        $('#firstt').addClass('active');
                        $('#cattts').addClass('active');
                        console.log("home");
                } else if (currentURL.includes("sub-categories/photos")) {
  			$('#firstt').addClass('active');
                        $('#catimages').addClass('active');
  			return;
  		} else if (currentURL.includes("add-blog")) {
  			$('#blog').addClass('active');
                        $('#add-blog').addClass('active');
  			return;
  		} else if (currentURL.includes("blogCates")) {
  			$('#blog').addClass('active');
                        $('#blog-cates').addClass('active');
  			return;
  		} else if (currentURL.includes("allPosts")) {
  			$('#blog').addClass('active');
                        $('#all-blog').addClass('active');
  			return;
  		} else if (currentURL.includes("all-vendors")) {
  			$('#vendors').addClass('active');
  			return;
  		} else if (currentURL.includes("all-users")) {
  			$('#users').addClass('active');
  			return;
  		} else if (currentURL.includes("all-products")) {
  			$('#products').addClass('active');
  			return;
  		} else if (currentURL.includes("all-Ads")) {
  			$('#ads').addClass('active');
  			return;
  		} else if (currentURL.includes("add-logo")) {
  			$('#logos').addClass('active');
  			$('#add-logo').addClass('active');
  			return;
  		} else if (currentURL.includes("Logos")) {
  			$('#logos').addClass('active');
  			$('#all-logo').addClass('active');
  			return;
  		} else if (currentURL.includes("who-are-we")) {
  			$('#content-pages').addClass('active');
  			$('#who-are-we').addClass('active');
  			return;
  		} else if (currentURL.includes("vendor-terms")) {
  			$('#content-pages').addClass('active');
  			$('#vendor-terms').addClass('active');
  			return;
  		} else if (currentURL.includes("privileges")) {
  			$('#content-pages').addClass('active');
  			$('#privileges').addClass('active');
  			return;
  		} else if (currentURL.includes("intellectual-property")) {
  			$('#content-pages').addClass('active');
  			$('#property').addClass('active');
  			return;
  		} else if (currentURL.includes("place-your-ad")) {
  			$('#content-pages').addClass('active');
  			$('#add-ad').addClass('active');
  			return;
  		} else if (currentURL.includes("FAQ")) {
  			$('#content-pages').addClass('active');
  			$('#faq').addClass('active');
  			return;
  		} else if (currentURL.includes("support")) {
  			$('#content-pages').addClass('active');
  			$('#support').addClass('active');
  			return;
  		} else if (currentURL.includes("mobile-app")) {
  			$('#content-pages').addClass('active');
  			$('#mobile-app').addClass('active');
  			return;
  		} else if (currentURL.includes("privacy")) {
  			$('#content-pages').addClass('active');
  			$('#privacy').addClass('active');
  			return;
  		} else if (currentURL.includes("rules")) {
  			$('#content-pages').addClass('active');
  			$('#rules').addClass('active');
  			return;
  		} else if (currentURL.includes("videos")) {
  			$('#videos').addClass('active');
  			return;
  		} else if (currentURL.includes("send-news")) {
  			$('#newsletter').addClass('active');
  			$('#send-news').addClass('active');
  			return;
  		} else if (currentURL.includes("subscribers")) {
  			$('#newsletter').addClass('active');
  			$('#subscribers').addClass('active');
  			return;
  		} else if (currentURL.includes("contact_links")) {
  			$('#contactLinks').addClass('active');
  			return;
  		} else {console.log("nothing...");}
  		
  	}
  	window.onhashchange = function () {
  		const currentURL = document.URL;
  		
  	}
  </script>

  @yield('content')


<footer class="main-footer">
    <!-- To the right -->
    <!-- Default to the left -->
        <div class="text-center">
            Developed By <a href="http://www.wasiladev.com" target="_blank">WasilaDev</a> جميع الحقوق محفوظه لشركه فى المحل دوت كوم
        </div>
        <div class="text-center web-conditions">
            <a>سياسة الخصوصية</a>
            <a>احكام وشروط</a>
        </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{asset('admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- AdminLTE App -->
<script defer src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<script src="{{asset('admin/dist/js/summernote.js')}}"></script>
<script src="{{asset('admin/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('admin/dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('js/js.js')}}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
     <script>
        setInterval(function(){
            setTimeout(function getnoti(){
                user_ID = "{{Auth::user()->id}}";
                oldnotificationlength = "{{$counter}}";
                
                $.ajax({
                    type:'POST',
                    url:'/api/get/notification',
                    data:{noti:1,userId:user_ID},
                    success: function(data){
                        // console.log(data);

                        if(data.notifications.length > data.counter[0].counter){
				$('.notification-icon').append('<span class="label label-warning" style="height:25px;">'+data.notifications.length+'</span>');
				
                            if(data.notifications.length > 3){

                                for(i=0; i<3; i++){
                                    $('.message').remove();

                                //    $('.more-messages').remove();
                                   	
                                    $.ajax({
                                        type:'POST',
                                        url:'/api/get/notification',
                                        cache: false,
                                        data:{noti:2,userId:user_ID,notiId:data.notifications[i].id},
                                        success: function(data){
                                           // console.log(data);
                                           notis = '<li class="message"><a href="#"><div class="client"><div class="message-header"><div class="img-container"><img src="/image/'+data.contact_info[0].icon+'" class="img-responsive"></div><div class="client-title"><h5>'+data.marketnoti[0].title+'</h5><p>'+data.newnotifications[0].data+'</p><p class="time"><i class="fa fa-phone"></i>'+data.newnotifications[0].created_at+'</p></div></div><div></div></div></a></li>';
                                        
                                            $('.noti').append(notis);
					   
                               
                                        }
                                        
                                        
                                    });
					
                                    notis = "";
                                }
                                


                            }else{
                            $('.notification-icon').append('<span class="label label-warning" style="height:25px;">'+data.notifications.length+'</span>');
                                for(i=0; i<data.notifications.length; i++){
                                    $('.message').remove();

                                //    $('.more-messages').remove();
                                    $.ajax({
                                        type:'POST',
                                        url:'/api/get/notification',
                                        cache: false,
                                        data:{noti:2,userId:user_ID,notiId:data.notifications[i].id},
                                        success: function(data){
                                          
                                          notis = '<li class="message"><a href="#"><div class="client"><div class="message-header"><div class="img-container"><img src="/image/'+data.contact_info[0].icon+'" class="img-responsive"></div><div class="client-title"><h5>'+data.marketnoti[0].title+'</h5><p>'+data.newnotifications[0].data+'</p><p class="time"><i class="fa fa-phone"></i>'+data.newnotifications[0].created_at+'</p></div></div><div></div></div></a></li>';
                                        
                                            $('.noti').append(notis);

                                        }

                                    });
                                    notis = "";
                                }
                            }
                           /* notis2 = '<li class="more-messages"><p><a href="#">شاهد جميع الاشعارات</a></p><p><a href="#"><font id="countnoti">'+oldnotificationlength+' اشعار اخر</font></a></p><div class="clear"></div></li>';
                            $('.noti').append(notis2);*/
                        }
                    },
                    error: function(){
                        console.log('error');
                    }
                  });
                
            }, 1000);
        }, 5000);
    </script>
    @yield('scripts')
</body>
</html>