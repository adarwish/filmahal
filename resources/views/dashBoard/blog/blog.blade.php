@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">

                    <div class="row add-blog-page">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>أضف مدونة</h2>


                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                
                                  <div class="slider" id="new-post">
                                    <form method="POST" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="col-md-6 col-xs-12 blog-right">
                                          <div class="form-group">
                                            <label>عنوان المدونة</label>
                                            <input type="text" name="title" class="form-control" style="margin-bottom: 20px;">
                                            <label>تصنيفات المدونة</label>
                                            <!--<select data-placeholder="ابحث باسم التصنيف..." multiple class="chosen-select" name="test">-->
                                            <select id="cats" name="test" onchange="cat();">
                                              <option selected disabled>اختر التصنيفات</option>
                @foreach($cat as $cats)
                
                <option value="{{$cats->id}}" data-name="{{$cats->name}}">{{$cats->name}}</option>
                @endforeach
              </select>
                </div>
                <div id="selected-categories"></div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 blog-left">
                                          <h2>اختر صورة</h2>
                                          <div class="slider-img">
                                              <div class="form-group">
                                                <input id='slider1' name="blogimage" onchange="readURL(event)" type="file" class="dropify" required> 
                                            </div>
                                            
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label>تفاصيل المدونة</label>
                                      <div class="form-group">
                                                <textarea id="summernote"  name="something" required></textarea>
                                      </div>
                                        </div>
                                        <div class="col-md-12">
                                           <div id="submit-slider"> 
                                           
                                          <button  class="btn">نشر </button>
                                          
                                     </div> 
                                     
                                     </div>
                                      
                                    </form>
                                   
                                  </div>
                                </div>
                            </div>
                        </div>


                    </div>

                
    </section>
</div>
@endsection
