@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        

                    </div>

                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>جميع المدونات</h2>


                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="datatable-posts" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>عنوان المدونة</th>
                                                <th>تصنيفات المدونة</th>
                                                <th>التاريخ</th>
                                                <th data-orderable="false">عرض/تعديل</th>
                                               
                                            </tr>
                                        </thead>
                                        
                                        <?php $counter = 0; ?>
                                        
                                        <tbody>
                                        @foreach($blog as $blogs)
                                            <?php $catsId = DB::table('blogs_category')->where('blog_id',$blogs->id)->get(); ?>
                                            <tr>
                                                <td>{{++$counter}}</td>
                                                <td>{{$blogs->title}}</td>
                                                
                                                <td>@foreach($catsId as $catsIds) <?php $cats = DB::table('blog_cat')->where('id',$catsIds->cblog_id)->get(); ?> {{$cats[0]->name}},@endforeach</td>
                                                
                                                <td>{{$blogs->created_at->format('Y-m-d')}}</td>
                                                <td>
                                            
                                                  <a href="#" class="edit-modal btn" type="button" onclick="editBlogs('{{$blogs->id}}','{{$blogs->title}}','{!!$blogs->paragraph!!}','{{$blogs->photo}}')" data-toggle="modal" data-target=".edit-post-window">
                                                    تعديل
                                                  </a>
                                                  
                                                  <a href="/blog/post/{{$blogs->id}}" class="btn">
                                                    عرض
                                                  </a>
                                                  
                                                  <a href="/blog/delete/{{$blogs->id}}" class="btn delete">
                                                    مسح
                                                  </a>
                                                </td>
                                            </tr>
                                         @endforeach

                                        </tbody>
                                    </table>
                                    
                                    <div class="modal fade edit-post-window" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                      <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                              <!-- Modal Content -->
                                          <div>
                                          <div class="slider" id="new-post">
                                            <form method="POST" enctype="multipart/form-data">
                                              {{ csrf_field() }}
                                              <input id="ids" name="invisible" type="hidden" value="">
                                                <div class="col-md-6 col-xs-12">
                                                  <div class="form-group">
                                                    <label>Post Title</label>
                                                    <input id="tin" value="" name="title" type="text" class="form-control">
                                                  </div>
                                                  <label>Write Your Post</label>
                                                  <div class="form-group">
                                                      <textarea id="aboutedit" name="aboutedit">
                                                  
                                                       </textarea>
                                                      <div class="center"> 
                                                        <a id="textarea-undo" data-toggle="tooltip" data-placement="top" title="Undo">
                                                          <i class="icon-ios-undo"></i>
                                                        </a> 
                                                        <a id="textarea-redo" data-toggle="tooltip" data-placement="top" title="Redo">
                                                          <i class="icon-ios-redo"></i>
                                                        </a> 
                                                      </div> 
                                                     
        
                                                  </div>
                                                 
                                                </div>
                                                <div class="col-md-5 col-md-offset-1 col-xs-12">
                                                  <h2>Update the featured image</h2>
                                                  <div class="slider-img">
                                                    <div class="form-group">
                                                      <input type='file' id='slider1' name="background" onchange="readURL(event)" accept="image/*">    
                                                      <i class="fa fa-image"></i>
                                                    </div>
                                                    
                                                  </div>
                                                  <div class="old-post-img">
                                                    <img src="image/{{}}" alt="">
                                                  </div>
                                                </div>
                                                   
                                      
                                                <button class="btn btn-primary">Update </button>
                         
                                                <div class="clearfix"></div>
                                            </form>
                                            
                                          </div>

                                          <!-- End of Modal -->
                                        </div>
                                      </div>
                                    </div>                                 
                                </div>
                            </div>
                        </div>


                </div>
            </div>
            <!-- /page content -->
            
               
        </div>
    </div>







    <!-- Start Edit Model Model -->



    <div class="modal fade stat-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                    <h4 class="modal-title" id="myModalLabel2">Change Order Status</h4>
                </div>
                <div class="modal-body">



                    <form class="form-horizontal form-label-left">


                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Product Status</label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <select class="form-control">
                            <option>in progress</option>
                            <option>delivered</option>

                          </select>
                            </div>
                        </div>



                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>

                    </form>


                </div>

            </div>
        </div>
    </div>

    <!-- End Edit Model Model -->


     


    <!-- edit model -->

    </section>
</div>
    @endsection
    
    <script>
   
    function editBlogs(idD,title,paragraph,img){
      id =  document.getElementById('ids');
      id.value  = idD;
      
      t =  document.getElementById('tin');
      t.value = title;
      
      //b = document.getElementById('aboutedit');
      $(".note-editable").text(paragraph);
      $("#aboutedit").text(paragraph);
      
      //var i = document.getElementById('slider1');
      //i.value = img;

      $('.old-post-img > div').remove();
      $('.old-post-img').html('<img src="/image/'+img+'" alt="">')

      
    }
    
    </script>
