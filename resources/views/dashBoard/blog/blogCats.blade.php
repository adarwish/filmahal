@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
        <div class="blog">
            <div class="header-word">
                <h2>تصنيفات المدونة:</h2>
            </div>
            <div class="col-md-8">
            <form method="POST" id="editForum">
                 {{csrf_field()}}
                 <input type="hidden" name="save1" value="save1">
              <table id="datatable-blogCats" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
              
              <button type="submit" onclick="edit()" id="apply" class="hidden">تنفيذ</button>
              
                <div class="actions">
                    <div class="acitonsthings">
                        <select id="action-list" style="float: right;">
                            <option value="0" selected>أوامر</option>
                            <option value="1">تعديل</option>
                            <option value="222"  class="delete">مسح</option>
                        </select>
                        <button type="button" id="select-all-blog">اختيار الكل</button>
                        <button type="button" id="none-all-blog" class="hidden">محو الكل</button>
                    </div>
                </div>
                <thead>
                    <tr>
                        <th class="text-center delete select-checkbox sorting_disabled" rowspan="1" colspan="1" aria-label="" style="width: 12px;"></th>
                        <th>اسم التصنيف</th>
                        <th>عدد المقالات</th>
                    </tr>
                </thead>                                        
                <tbody>
                
                @foreach($cat as $cats)
                
                    <tr id="{{$cats->id}}">
                        <td> <input type="hidden" name="catId" value="{{$cats->id}}"></td>
                        <td class="sorting_1" data-search="تصنيف 1" ><input type="text" id="catID" name="categoryName" value="{{$cats->name}}" disabled></td>
                        <?php $categories = DB::table('blogs_category')->where('cblog_id',$cats->id)->get(); ?>
                        <td>{{count($categories)}}</td>
                    </tr>
                @endforeach  
                
                </tbody>
               
              </table>
              </form>
            </div>
            <form class="col-md-4" method="POST">
                    {{csrf_field()}}
                    <div class="">
                        <div id="inputs-cates">
                            <label>أضف تصنيف:</label>
                            <input id="add-cat" type="text" name="catname">
                            <button class="btn btn-primary">حفظ</button>
                        </div>
                    </div>
            </form>
        </div>
    </section>
</div>
@endsection

<script>
function edit(){
            $('#editForum').submit(function(e){
              e.preventDefault();
              $('.selected').each(function(){
                var id = $(this).attr("id");
                var categoryName = $('#catID').val();
                
              $.ajax({
                type:'POST',
                url:'/admin.blog-category.mahalatmasr2018@fmax0*',
                data:{
                    i:id,catName:categoryName,save1:1
                },
                dataType:'json',
                success: function(data){
                    console.log(data);
                    
                }
              });
            });
        });
}



</script>