@extends('dashBoard.master')

@section('content')
<div class="right_col" role="main">
                <div class="">
                    
                                                   <!--Not the one-->
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>اضف مدونة</h2>


                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                
                                  <div class="slider" id="new-post">
                                    <form method="POST" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="col-md-6 col-xs-12">
                                          <div class="form-group">
                                            <label>عنوان المدونة</label>
                                            <input type="text" name="title" class="form-control">
                                          </div>
                                          <label>تفاصيل المدونة</label>
                                          <div class="form-group">
                                            <textarea id="aboutedit" name="textblog"></textarea> <div class="center"> <a id="textarea-undo" data-toggle="tooltip" data-placement="top" title="Undo"><i class="icon-ios-undo"></i></a> <a id="textarea-redo" data-toggle="tooltip" data-placement="top" title="Redo"><i class="icon-ios-redo"></i></a> </div> 

                                          </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                                <h2>اختر صورة</h2>
                                          <div class="slider-img">
                                              
                                            <div class="form-group">
                                                <input type='file' id='slider1' name="blogimage" onchange="readURL(event)" accept="image/*">  
                                                <i class="fa fa-image"></i>  
                                            </div>
                                            
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                           <div id="submit-slider"> 
                                           
                                          <button  class="btn">نشر </button>
                                          
                                     </div> 
                                     
                                     </div>
                                      
                                    </form>
                                   
                                  </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            
@endsection
