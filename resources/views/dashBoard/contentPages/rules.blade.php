@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
        <div class="rules-page content-page">
            <div class="header-word">
                <h2>أحكام وشروط:</h2>
            </div>
            <form method="POST">
            {{csrf_field()}}
                <div class="col-md-12 pull-right">
                    <textarea id="summernote"  name="something" required><?php $setting = DB::table('setting')->get(); echo $setting[12]->paragraph; ?></textarea>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-primary">حفظ</button>
            </form>
        </div>
    </section>
</div>
@endsection