@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">
            <!-- page content -->
        <div class="privileges-page content-page">
            <div class="header-word">
                <h2>مميزات وجودك معنا:</h2>
            </div>
            <form method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="col-md-12 content-image">
                    <input type="file" class="dropify" name="photo" data-height="270">
                </div>
                <div class="col-md-12 content-rte">
                    <textarea id="summernote"  name="something" required><?php $setting = DB::table('setting')->get(); echo $setting[6]->paragraph; ?> </textarea>
                </div>
                <button class="btn btn-primary content-btn">حفظ</button>
            </form>
        </div>
    </section>
</div>
@endsection