@extends('dashBoard.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->


<!-- Main content -->
<section class="content container-fluid rtl">

  <?php $cityName = DB::table('cities')->where('id',$id)->get(); ?>

    <div class="shreet">
            <p class="nameOfShreet text-bold">محافظة: <span>{{$cityName[0]->name}}</span></p>
    </div>
    <form method="POST">
        {{csrf_field()}}
        <table id="example3" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    
                    <th>اسم المدينه</th>
                    <th>مسح الكل</th>
                </tr>
            </thead>
            <tbody>
                @foreach($city as $cities)
                <tr>
                    <td>
                        <div>
                            <input type="text" name="editcityname[]" value="{{$cities->name}}">
                            
                            <input type="hidden" name="editcityId[]" value="{{$cities->id}}">
                        </div>
                     </td>
                    <td><a href="/deleteAreaEdit/{{$cities->id}}/{{$id}}">مسح المدينه</a></td>
                </tr>
                @endforeach
               
            </tbody>
        </table>
        <br>
        <button>حفظ</button>
    </form>
</section>
</div>
@endsection