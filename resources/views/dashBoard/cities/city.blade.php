@extends('dashBoard.master')

@section('content')
<div class="content-wrapper">
    <section class="content container-fluid rtl">

  <script>
  	window.onload = function () {
  		const currentURL = document.URL;
  		const fourth = document.getElementById("fourth-step");
  		const fifth = document.getElementById("fifth-step");
                $('.active').removeClass('active');
                $('#area').addClass('active');
  		if (currentURL.includes("add")) {
  			fourth.classList.add("show");
  			fifth.classList.remove("show");
                        $('#fourth').addClass('active');
  			return;
  		} else if (currentURL.includes("all")) {
  			fourth.classList.remove("show");
  			fifth.classList.add("show");
                        $('#fifth').addClass('active');
  			return;
  		} else {
  			fourth.classList.add("show");
  			fifth.classList.remove("show");
                        $('#fourth').addClass('active');
  		}
  	}
  	window.onhashchange = function () {
  		const currentURL = document.URL;
  		const fourth = document.getElementById("fourth-step");
  		const fifth = document.getElementById("fifth-step");
                $('.active').removeClass('active');
                $('#area').addClass('active');
  		if (currentURL.includes("add")) {
  			fourth.classList.add("show");
  			fifth.classList.remove("show");
                        $('#fourth').addClass('active');
  			return;
  		} else if (currentURL.includes("all")) {
  			fourth.classList.remove("show");
  			fifth.classList.add("show");
                        $('#fifth').addClass('active');
  			return;
  		} else {
  			fourth.classList.add("show");
  			fifth.classList.remove("show");
                        $('#fourth').addClass('active');
  		}
  	}
  </script>
        <div id="fourth-step">
            <div class="header-word">
                <h2>ادخل المناطق لكل محافظة:</h2>
            </div>
            
            <div class="containerOfInputs">
                <form method="POST" id="newareaForum">
                    {{csrf_field()}}
                    <input type="hidden" name="save1" value="save1">
                    <div class="col-xs-12 col-md-4">
                        <select name="governorates" id="gov">
                            <option value="-1">محافظات</option>
                        </select>
                    </div>
        
                    <div class="col-xs-12 col-md-8 govs">
                        <div class="boxAddRemove no-drop">
                            <input type="text" id="area" name="area[]">
                            <i class="ion ion-plus-circled add-branches" aria-hidden="true"></i>
                            <i class="ion ion-minus-circled remove-branches" aria-hidden="true"></i>
                        </div>
                    </div>
                    
                    <div class="container">
                        <div class="row">
                            
                        </div>
                    </div>
        
                    <div class="col-xs-12">
                        <h3><font color="green" id="msg"></font></h3>
                        <button onclick="newareas()">حفظ</button>
                        <button type="cancel" id="govCancel">الغاء</button>
                    </div>
                </form>
            </div>
        </div>
        
        <div id="fifth-step">
            <div class="header-word">
               <h2>جدول المناطق:</h2>
            </div>
            <select class="edit-area">
                    <option value="0" selected>اوامر</option>
                    <option value="1">تعديل</option>
                    <option value="2" class="delete">مسح</option>
            </select>
             <button type="button" id="select-all-govs" class="">اختيار الكل</button>
            <button type="button" id="none-all-govs" class="hidden">محو الكل</button>
            <table id="example2" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td></td>
                        <th>اسم المحافظه</th>
                        <th>عدد المناطق</th>
                        <th>تعديل</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($city as $cities)
                     <?php $counter = count($cities->area); ?>
                    <tr id="{{$cities->id}}">
                        <td></td>
                        <td>{{$cities->name}}</td>
                        <td>{{$counter}}</td>
                        <td><a href="/admin.edit-areas/{{$cities->id}}.mahalatmasr2018@fmax0*">تعديل</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
       
    </section>
</div>
@endsection

<script>
    function newareas(){
        $('#newareaForum').submit(function(e){
          e.preventDefault();
          var form = $('#newareaForum');
          $.ajax({
            type:'POST',
            url:'/admin.cities-areas.mahalatmasr2018@fmax0*',
            data:form.serialize(),
            success: function(data){
                console.log(data);
                 $('#area').val('');
                 $('#msg').html("تم إضافة المنطقة بنجاح");                        
            },
            error: function(){
                console.log('error');
                
            }
          });
        });
       // console.log('error');
    };
</script>