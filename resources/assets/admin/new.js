//check if the trip is every day or only one day
$('#price').on('change', function () {
    var select = $(this).val();
    console.log(select);
    if (select == 1) {
        $('.price1').removeClass('hidden');
        $('.price2').addClass('hidden');
    } else{
        $('.price2').removeClass('hidden');
        $('.price1').addClass('hidden');
    }
});

$('#car_type').on('change' , function(){
    var select = $(this).val();
    if(select == 1){
        $('.price1').removeClass('hidden');
        $('.price2').addClass('hidden');
    }else{
        $('.price1').addClass('hidden');
        $('.price2').removeClass('hidden');
    }
});
// /**
//  * DropZone Code
//  */
// Dropzone.options.imageUpload = {
//     maxFilesize: 10000,
//     acceptedFiles: ".jpeg,.jpg,.png,.gif",
//     addRemoveLinks: true,
//     init: function (file) {
//         this.on("removedfile", function (file) {
//             var url = 'deleteDropzoneImage';
//             $.ajax({
//                 type: 'POST',
//                 url: url,
//                 data: {name: file.name, _token: $('#csrf-token').val()},
//                 dataType: 'json',
//                 success: function (data) {
//                     document.getElementById('image_' + data.name).remove();

//                 }
//             });
//         });
//     },
//     success: function (file, done) {
//         $('#dropzone_image').append('<input id="image_' + file.name + '" type="hidden" name="imgs[]" value="' + done.success + '" />');
//         // console.log(done.success);

//     }
// };
// --------------------------Trigger File upload browsing Section ---------------------------
$(document).on('click', '.btn-product-image', function () {
    var btn = $(this);
    var uploadInp = btn.next('input[type=file]');
    uploadInp.change(function () {
        if (validateImgFile(this)) {
            btn.html('');
            previewURL(btn, this);
        }
    }).click();
});
function previewURL(btn, input) {
    if (input.files && input.files[0]) {
        // collecting the file source
        var file = input.files[0];
        // preview the image
        var reader = new FileReader();
        reader.onload = function (e) {
            var src = e.target.result;
            btn.attr('src', src);
        };
        reader.readAsDataURL(file);
    }
}
//validating the file
function validateImgFile(input) {
    if (input.files && input.files[0]) {
        // collecting the file source
        var file = input.files[0];
        // validating the image name
        if (file.name.length < 1) {
            alert("The file name couldn't be empty");
            return false;
        }
        // validating the image size
        else if (file.size > 2000000) {
            alert("The file is too big");
            return false;
        }
        // validating the image type
        else if (file.type != 'image/png' && file.type != 'image/jpg' && file.type != 'image/gif' && file.type != 'image/jpeg') {
            alert("The file does not match png, jpg or gif");
            return false;
        }
        return true;
    }
}

/***************************************************************************
 * Custom logging function
 * @param mixed data
 * @returns void
 **************************************************************************/
function _(data) {
    console.log(data);
}


//translation 
var tripId = $('#tripId');
var id = $('.trans-btn');
$(id).on('click', function () {
    var data = $(this).attr('data-id');
    tripId.val(data);
});
$('.editPrice').on('click' ,function () {
   var dataId = $(this).data('id');
   $('#tripPriceId').val(dataId);
});

$('#datePricker').on('change' ,function () {
    var date = $(this).val();
    var tripId = $('#tripPriceId').val();
    console.log(tripId);
    $.ajax({
        url : 'dayCruiseTrip/prices',
        data : {'date':date ,'_token' :$(this).data('token'),'id':tripId},
        type: 'post',
        success: function (data) {
            $('#Price-template').html('');
            $('#Price-template').html(data);
        },
        errors: function () {
            alert('error');
        }
    });

    $.ajax({
        url : 'cabine/prices',
        data : {'date':date ,'_token' :$(this).data('token'),'id':tripId},
        type: 'post',
        success: function (data) {
            $('#Price-template').html('');
            $('#Price-template').html(data);
        },
        errors: function () {
            alert('error');
        }
    });

    $.ajax({
        url : 'cars/prices',
        data : {'date':date ,'_token' :$(this).data('token'),'id':tripId},
        type: 'post',
        success: function (data) {
            $('#Price-template').html('');
            $('#Price-template').html(data);
        },
        errors: function () {
            alert('error');
        }
    });
});

