(function ($) {
    "use strict";
    var regForm = {
        initialized: false,
        initialize: function () {
            if (this.initialized)
                return;
            this.initialized = true;
            this.build();
            this.events();
        },
        build: function () {
            this.validations();
        },
        events: function () {
        },
        validations: function () {
            var regform = $(".reg-form"),
                    url = regform.attr("action");
            
            regform.validate({
                submitHandler: function (form) {
                    // Loading State
                    var submitButton = $(this.submitButton);
                   
                        submitButton.button("جاري");
                
                    // Ajax Submit
                    $.ajax({
                        method: "POST",
                        url: url,
                        data: $(form).serialize(),
                        dataType: "json",
                        success: function (data) {
                            if (data.response == "success") {
                                var message = data.message;
                                $(".reg-alert-success").text(message);
                                $(".reg-alert-success").stop().removeClass("hidden").hide().fadeIn();
                                $(".reg-alert-error").stop().addClass("hidden");
                                // Reset Form
                                $(".reg-form .form-control")
                                        .val("")
                                        .blur()
                                        .parent()
                                        .removeClass("has-success")
                                        .removeClass("has-error")
                                        .find("label.error")
                                        .remove();
                                if (($(".reg-alert-success").position().top - 80) < $(window).scrollTop()) {
                                    $("html, body").animate({
                                        scrollTop: $(".reg-alert-success").offset().top - 80
                                    }, 300);
                                }
                                location.reload();
                            } else if (data.response == 'error') {
                                var message = data.message;
                                $(".reg-alert-error").text(message);
                                $(".reg-alert-error").stop().removeClass("hidden").hide().fadeIn();
                                $(".reg-alert-success").stop().addClass("hidden");
                                setTimeout(function () {
                                    $(".reg-alert-error").stop().fadeOut().addClass("hidden");
                                    footHeight();
                                    $("html, body").animate({
                                        scrollTop: $(document).height()
                                    }, 300);
                                }, 3000);
                            }
                            footHeight();
                            $("html, body").animate({
                                scrollTop: $(document).height()
                            });
                        },
                        complete: function () {
                            submitButton.button("reset");
                            footHeight();
                            $("html, body").animate({
                                scrollTop: $(document).height()
                            }, 300);
                        }
                    });
                },
                rules: {
                    f_name: {
                        required: true
                    },
                   
                    email: {
                        required: true,
                        
                    },
                    phone: {
                        required: true,
                        
                    },
                    address: {
                        required: true
                    },
                     shop_type: {
                        required: true
                    },
                     shop_name: {
                        required: true
                    },
                    shop_location: {
                        required: true
                    },
                    agree: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 6,
                        equalTo: "#reg_pass"
                    }
                },
                messages: {
                    password: {
                        minlength: 'الرقم السرى يجب ان يكون 6 احرف على الاقل'
                    },
                    password_confirmation: {
                        minlength: 'الرقم السرى يجب ان يكون 6 احرف على الاقل',
                        equalTo: 'تأكيد الرقم السرى غير متطابق مع الرقم الاصلى'
                    }
                    
                },
                highlight: function (element) {
                    $(element)
                            .parent()
                            .removeClass("has-success")
                            .addClass("has-error");
                    if (typeof $.fn.isotope !== 'undefined') {
                        $('.filter-elements').isotope('layout');
                    }
                },
                success: function (element) {
                    $(element)
                            .parent()
                            .removeClass("has-error")
                            .addClass("has-success")
                            .find("label.error")
                            .remove();
                }
            });
            $.ajaxSetup(
                    {
                        headers:
                                {
                                    'X-CSRF-Token': $('input[name="_token"]').val()
                                }
                    });
        }
    };
    regForm.initialize();
})(jQuery);

/* Login Form Script */
(function ($) {

    /* Login form
     ================*/
    var headLoginForm = {
        initialized: false,
        initialize: function () {
            if (this.initialized)
                return;
            this.initialized = true;
            this.build();
            this.events();
        },
        build: function () {
            this.validations();
        },
        events: function () {
        },
        validations: function () {
            var headLoginForm = $("#head-log-form"),
                    url = headLoginForm.attr("action");
            headLoginForm.validate({
                submitHandler: function (form) {

                    // Ajax Submit
                    $.ajax({
                        method: "POST",
                        url: url,
                        data: $(form).serialize(),
                        dataType: "json",
                        success: function (data) {
                            if (data.response === "success") {
                                console.log('success');

                                var alertSelector = '#headLogActionSuccess';
                                var alertOpSelector = '#headLogActionError';
                            } else if (data.response === "error") {
                                console.log('error');

                                var alertSelector = '#headLogActionError';
                                var alertOpSelector = '#headLogActionSuccess';
                            }
                            $(alertSelector).text(data.message);
                            $(alertSelector).hide().removeClass('hidden').fadeIn();
                            setTimeout(function () {
                                $(alertSelector).fadeOut().addClass('hidden');
                            }, 3000);
                            $(alertOpSelector).fadeOut().addClass('hidden');
                            if (data.response === "success") {
                                location.reload();
                            }
                        }
                    });
                },
                rules: {
                    email: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }
                },
                messages: {
                    email: {
                        required: 'من فضلك ادخل الايميل'
                    },
                    password: {
                        required: 'من فضلك ادخل الرقم السرى',
                        minlength: 'الرقم السرى يجب ان يكون 6 احرف او اكثر'
                    }
                },
                highlight: function (element) {
                    $(element)
                            .parent()
                            .removeClass("has-success")
                            .addClass("has-error");
                    if (typeof $.fn.isotope !== 'undefined') {
                        $('.filter-elements').isotope('layout');
                    }
                },
                success: function (element) {
                    $(element)
                            .parent()
                            .removeClass("has-error")
                            .addClass("has-success")
                            .find("label.error")
                            .remove();
                }
            });
            $.ajaxSetup(
                    {
                        headers:
                                {
                                    'X-CSRF-Token': $('input[name="_token"]').val()
                                }
                    });
        }
    };
    headLoginForm.initialize();
})(jQuery);