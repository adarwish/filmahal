(function () {

    /***************************************************************************
    * AJAX Setup for processing
    ***************************************************************************/
    //var baseUrl = '/tourism';
    var csrf = new FormData($('#csrf')[0]);
    var loading = $('#loading').html();

    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="_token"]').attr('content')
        }
    });
    var views = $('.views');
    views.on('click', function () {
        var views_count = 0;
        views_count ++ ;
        $("#counter").html(views_count);
    });
     
     
//      $('.first_limit , .last_limit').change(function (e) {
//        e.preventDefault();
//        var $this = $(this);
//        var form = $this.closest('form');
//        $('#filters_data').LoadingOverlay('show');
//        $.ajax({
//            url: form.attr('action'),
//            data: {'type':$(this).data('type'),'id':$(this).data('id'),'first_limit':$('.first_limit').val(),'last_limit':$('.last_limit').val()},
//            success:function (data) {
//                $('#filters_data').html(data).LoadingOverlay('hide');
//            }
//        }).done(function (data) {
//            $('#filters_data').html(data).LoadingOverlay('hide');
//
//        }).fail(function () {
//            alert('Internal Server Error.');
//        });
//    });

    $('.price-range-chosse').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        var form = $this.closest('form');
        $('#filters_data').LoadingOverlay('show');
        $.ajax({
            url: form.attr('action'),
            data: {'type':$(this).data('type'),'id':$(this).data('id'),'first_limit':$('.first_limit').val(),'last_limit':$('.last_limit').val()},
            success:function (data) {
                $('#filters_data').html(data).LoadingOverlay('hide');
            }
        }).done(function (data) {
            $('#filters_data').html(data).LoadingOverlay('hide');

        }).fail(function () {
            alert('Internal Server Error.');
        });
    });
      /***************************************************************************
    * Form Add Ajax  submit buttons
    **************************************************************************/

    $(document).on('submit', ".ajax-form", function (e) {
        e.preventDefault();
        var $this = $(this);
        var url = $this.attr('action');
        var ajaxSubmit = $this.find('.ajax-submit');
        var ajaxSubmitHtml = ajaxSubmit.html();
        var altText = loading;
        var notification = 'm';
        if (ajaxSubmit.data('loading') !== undefined) {
            altText = ajaxSubmit.data('loading');
        }
        //ajaxSubmit.prop('disabled', true).html(altText);
        var formData = new FormData(this);
        if ($this.find('.tiny-editor').length) {
            for (var i = 0; i < tinymce.editors.length; i++) {
                formData.append('editor' + (i + 1), tinymce.editors[i].getContent());
            }
        }
        if ($this.data('url') !== undefined) {
            url = $this.data('url');
        }
        if ($this.data('notification') !== undefined) {
            notification = $this.data('notification');
        }
        request(url, formData, function (result) {
            noty({
                text: result.msg,
                type: result.status,
                layout      : 'topRight',
                timeout     : 200,
                closeWith   : ['click'],
                maxVisible: 10,
                animation: {
                    // open: 'animated bounceInLeft',
                    // close: 'animated bounceOutLeft',
                    open: {height: 'toggle'}, 
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 1500 // opening & closing animation speed
                },
                callback: {
                    afterClose : function () {
                        location.reload(true);
                    },
                },
            });

        }, function () {
            noty({
                text: 'Internal Server Error',
                type: 'error',
                timeout     : 200,
                closeWith   : ['click'],
                animation: {
                    open: {height: 'toggle'}, 
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 1500 // opening & closing animation speed
                }
            });
        }); 
    });
    
    $(document).on('submit', ".new-form", function (e) {
        e.preventDefault();
        var $this = $(this);
        var url = $this.attr('action');
        var ajaxSubmit = $this.find('.new-submit');
        var ajaxSubmitHtml = ajaxSubmit.html();
        var altText = loading;
        var notification = 'm';
        if (ajaxSubmit.data('loading') !== undefined) {
            altText = ajaxSubmit.data('loading');
        }
        //ajaxSubmit.prop('disabled', true).html(altText);
        var formData = new FormData(this);
        if ($this.find('.tiny-editor').length) {
            for (var i = 0; i < tinymce.editors.length; i++) {
                formData.append('editor' + (i + 1), tinymce.editors[i].getContent());
            }
        }
        if ($this.data('url') !== undefined) {
            url = $this.data('url');
        }
        alert(url);
        if ($this.data('notification') !== undefined) {
            notification = $this.data('notification');
        }
        request(url, formData, function (result) {
            noty({
                text: result.msg,
                type: result.status,
                layout      : 'topRight',
                timeout     : 200,
                closeWith   : ['click'],
                maxVisible: 10,
                animation: {
                    // open: 'animated bounceInLeft',
                    // close: 'animated bounceOutLeft',
                    open: {height: 'toggle'}, 
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 2000 // opening & closing animation speed
                },
                callback: {
                    afterClose : function () {
                    //location.replace("http://starwebers.com/filmahal/profile/index")

                    },
                },
            });

        }, function () {
            noty({
                text: 'Internal Server Error',
                type: 'error',
                timeout     : 200,
                closeWith   : ['click'],
                animation: {
                    open: {height: 'toggle'}, 
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 5000 // opening & closing animation speed
                }
            });
        }); 
    });
    /////////////////////////////////////
    var AddModalBtn = $('.addBTN');
    AddModalBtn.on('click', function () {
        var AddModalForm = AddModalBtn.closest('form');
        var formData = new FormData(AddModalForm[0]);
        if (typeof tinymce !== "undefined" && tinymce.editors.length) {
            for (var i = 0; i < tinymce.editors.length; i++) {
                formData.append('content' + (i + 1), tinymce.editors[i].getContent());
            }
        }
        request(AddModalForm.attr('action'), formData,
            // on request success handler
            function (result) {
                if (result.status) {
                    swal({title: "success.", text: result.data, type: "success"}, function () {
                        location.reload(true);
                    });
                } else {
                    swal('wrong.', result.data, 'error');
                }
            },
            // on request failure handler
            function () {
                alert('Internal Server Error.');
            });
    });
    ////////////////////////////////////////////////////////////
    $('.approveBTN').click(function () {
        var url =$(this).data('url');
        var id = $(this).data('id');
        $(this).remove();
        $.ajax({
            url:url,
            type :'POST',
            data :{id:id,_token: $('#csrf-token').val()},
            dataType: 'json',
            success:function (data) {
                location.reload(true);
                console.log(data);
            }
        });
    });

    /**
     * Admin delete
     */
    $('.removeBTN').click(function () {
        var url =$(this).data('url');
        var id = $(this).data('id');
        $.ajax({
            url:url,
            type :'POST',
            data :{id:id,_token: $('#csrf-token').val()},
            dataType: 'json',
            success:function (data) {
                location.reload(true);
                $('#type_'+id).remove();
                console.log(data);
            }
        });
    });


    /**
     *  Video Change
     */

    $('.video_change').click(function (e) {
        e.preventDefault();
        var url =$(this).data('url');
        $.ajax({
            url :url,
            type:'GET',
            dataType: 'json',
            success:function (data) {
                $('#video_content').attr('src',data.name);
                console.log(data);
            },
            error:function (data) {
                console.log(data);
            }
        });
    });

    /**
     * Home page Menu sub_sub Cat
     */
    $('.sub_subCat').click(function () {
        var url =$(this).data('url');
        $.ajax({
            url :url,
            type:'GET',
            dataType: 'json',
            success:function (data) {
                $('#sub_sub_content_menu').html('')
                for (i=0;i<data.length;i++){
                    $('#sub_sub_content_menu').append('<li><a href="#">'+data[i].name+'</a></li>');
                }
            },
            error:function (data) {
                console.log(data);
            }
        });
    });


    /**
     * Home Category Section
     */
    var cat_btn_clike =$('.sub_home_cat');

    cat_btn_clike.click(function (e) {
        console.log($(this).data('id'));
        e.preventDefault();
        var id= $(this).data('id');
        var url ='catSection/'+id;
        $('.show_cat_product_data').attr('id','prodact_data_cat'+id);
        $.ajax({
            url :url,
            type :'GET',
            success :function (data) {
                $('#prodact_data_cat'+id).html(data);
            },
            error:function (data) {
                console.log(data);
                // insert_data_to_div.html(data);
            }
        });
    });

    /**
     * Notification Admin
     */
    function not_count() {
        var url =$('#notify_count').data('url');
        $.ajax({
            url :url,
            type :'GET',
            success :function (data) {
                $('.num_noty').html(data.length);
                $('#noty_content').html('');
                for (i=0 ; i<data.length; i++){
                    $('#noty_content').append('<li><a href="#">'+
                        '<i class="fa fa-comments"></i><img src="storage/uploads/products/'+data[i].products.ad_logo+'" style="width: 50px;height: 30px;" />'
                        +data[i].member.f_name+' اضاف اشعار جديد </br> للمنتج '+data[i].product_name[0].ad_name
                        +'</a></li>');
                }
            },
            error :function (data) {
                console.log(data);
            }
        });
    }

    // window.setInterval(function () {
    //     not_count();
    // },3000)
    
    /**
     * chosse_sub_cat_name
     */
    $('#choose_main_cat_name').change(function () {
        console.log($(this).val());
        $('#chosse_sub_cat_name').html('');
        $.ajax({
            url : 'showSubCat',
            data :{'id':$(this).val()},
            success :function (data) {
                $('#chosse_sub_cat_name').append(data);
            },
            error:function (data) {
                console.log(data);
            }
        });
    });

    $(document.getElementById('choose_main_cat2_name')).change(function () {
        console.log($(this).val());
        $.ajax({
            url : 'showSubCat',
            data :{'id':$(this).val()},
            success :function (data) {
                $('#chosse_sub_cat_name').append(data);
            },
            error:function (data) {
                console.log(data);
            }
        });
    });

    /******************************************************/
    $('.editBTN').click(function(){
        var id =$(this).data('id');
        var url ='users/info';
        $.ajax({
            url  :url,
            method :'post',
            data :{'id': id},
            success :function(data){
                $('#username').val(data.username);
                $('#fullname').val(data.fullname);
                $('#address').val(data.address);
                $('#phone').val(data.phone);
                $('#email').val(data.email);
                $('#age').val(data.age);
                $('#gender').val(data.gender);
                $('#image').attr('src',"storage/uploads/avatars/" + data.avatar);
                $('#job').val(data.job);
               


              $('#edit-modals').modal('show');
            }
        });
     });


     /***************************************************************************
    * Show Edit General Modal
    **************************************************************************/
    var editModal = $('#edit-modal');
    $(document).on('click', '.edit-modal-btn', function () {
        var $this = $(this);
        var url = $this.data('url');
        request(url, csrf, function (data) {
            if (data.status === 'success') {
                editModal.find('.modal-content').html(data.content);
                editModal.modal('toggle');
            } else
            if (data.status === 'error') {
                swal(data.title, data.msg, "error");
            } else
            if (data.status === 'warning') {
                swal(data.title, data.msg, "warning");
            }
        }, function () {
            alert('Internal Server Error.');
        });
    });

    /***************************************************************************
    * Post Add  General Modal
    **************************************************************************/
    $(document).on('click', '#add-modal-submit', function () {
        var $this = $(this);
        var form = $this.closest('form');
        request(form.attr('action'), new FormData(form[0]), function (data) {
            if (data.status === 'success') {
                swal({title: data.title, text: data.msg, type: "success"}, function () {
                    location.reload(0);
                });
            } else
            if (data.status === 'error') {
                swal(data.title, data.msg, "error");
            } else
            if (data.status === 'warning') {
                swal(data.title, data.msg, "warning");
            }
        }, function () {
            alert('Internal Server Error.');
        });
    });
    
    $(document).on('click', '.edit-modal-submit', function () {
        var $this = $(this);
        var form = $this.closest('form');
        request(form.attr('action'), new FormData(form[0]), function (data) {
            if (data.status === 'success') {
                swal({title: data.title, text: data.msg, type: "success"}, function () {
                    location.reload(0);
                });
            } else
            if (data.status === 'error') {
                swal(data.title, data.msg, "error");
            } else
            if (data.status === 'warning') {
                swal(data.title, data.msg, "warning");
            }
        }, function () {
            alert('Internal Server Error.');
        });
    });
    /***************************************************************************
    * Modal View Modal
    **************************************************************************/

    $(document).on('click', '.btn-modal-view', function () {
        var $this = $(this);
        var url = $this.data('url');
        var originalHtml = $this.html();
        //$this.prop('disabled', true).html('loading...');
        request(url, null, function (data) {
            $this.prop('disabled', false).html(originalHtml);
            $('#common-modal').html(data).modal('toggle');
        }, function () {
            alert('Error');
        }, 'get');
    });

     /***************************************************************************
    * Ajax Pagination Controller
    **************************************************************************/
    var tableData = $('#ajax-table');
    $(document).on('click', '#ajax-table .pagination a', function (e) {
        var $this = $(this);
        tableData.html(loading);
        $.ajax({
            url: $this.attr('href'),
        }).done(function (data) {
            tableData.html(data);
        }).fail(function () {
            alert('Internal Server Error.');
        });
        e.preventDefault();
    });

    /***************************************************************************
    * Ajax Pagination For Products and wishlist Controller
    **************************************************************************/
    var productsArea = $('#products-area');
    $(document).on('click', '#products-area .pagination a', function (e) {
        e.preventDefault();
        var $this = $(this);
        productsArea.show();
        $.ajax({
            url: $this.attr('href'),
            data: $this.closest('form').serialize()
        }).done(function (data) {
            productsArea.html(data).hide();
        }).fail(function () {
            alert('Internal Server Error.');
        });
    });
    /***************************************************************************
    * Search input events for filtered table
    **************************************************************************/
    var inputSearch = $('#input-search');
    $(document).on('click', '.btn-search', function () {
        var form = $(this).closest('form');
        var search = (inputSearch.val().length) ? "/" + inputSearch.val() : "";
        tableData.html(loading);
        request(form.attr('action') + "/search" + search, null, function (data) {
            tableData.html(data);
        }, function () {
            alert('Internal Server Error');
        }, 'get');
    });
    /**************************************************************************
    * Actions Of Filters Buttons
    ***************************************************************************/

    $(document).on('change', '.btn-filter', function () {
        var $this = $(this);
        var filter = $this.data('filter');
        tableData.html(loading);
        var form = $this.closest('form');
        request(form.attr('action') + "/filter/" + filter, null, function (data) {
            tableData.html(data);
        }, function () {
            alert('Internal Server Error.');
        }, 'get');
    });
    /**************************************************************************
    * Events Action Buttons for the tables
    **************************************************************************/

    $(document).on('click', '.btn-action', function (e) {
        var $this = $(this);
        var action = $this.data('action');
        var form = $this.closest('form');
        request(form.attr('action') + "/action/" + action, new FormData(form[0]), function (data) {
            if (data.status === 'success') {
                notify(data.status, data.title, data.msg, function () {
                    $('input[data-filter=all]').change();
                });
            } else {
                notify(data.status, data.title, data.msg);
            }
        }, function () {
            alert('Internal Server Error.');
        });
        e.preventDefault();
    });

    /***************************************************************************
    * Check ALL Button For Table Rows
    ***************************************************************************/

    $(document).on('click', '#chk-all', function () {
        $('.chk-box').prop('checked', this.checked);
    });
    /***************************************************************************
    * Common Ajax Delete Section
    **************************************************************************/

    $(document).on('click', ".ajax-delete", function (e) {
        e.preventDefault();
        var $this = $(this);
        var url = $this.data('url');
        var originalHtml = $this.html();
        var altText = loading;
        var notification = 'm';
        if ($this.data('loading') !== undefined) {
            altText = $this.data('loading');
        }
        $this.prop('disabled', true).html(altText);

        if ($this.data('notification') !== undefined) {
            notification = $this.data('notification');
        }

        request(url, csrf, function (result) {
            notify(result.status, result.title, result.msg, notification);
            $this.prop('disabled', false).html(originalHtml);
            $this.closest('.ajax-target').remove();

        }, function () {
            alert('Internal Server Error.');
        });
    });


    var commonModal = $('#common-modal');
    var deleteModalTemplate = $('#delete-modal-template').html();
    $(document).on('click', '.modal-delete-btn', function (e) {
        var url = $(this).attr('data-url');
        var txt = deleteModalTemplate;
        txt = txt.replace(new RegExp('{url}', 'g'), url);
        commonModal.html(txt).modal('toggle');
        e.preventDefault();
    });

     $('.btndelet').click(function (e) {

        var txt = $('#template-modal').html();
        var url = $(this).attr('data-url');
        txt = txt.replace(new RegExp('{url}', 'g'), url);
        $('#delete-modal .modal-dialog').html(txt);
        $('#delete-modal').modal('show');
        e.preventDefault()
    });
    /////////////////////// Categories And Menus  ///////////////////////

    $('#data_review').on('submit', ".ajax-form", function (e) {
        e.preventDefault();
        var $this = $(this);
        var url = $this.attr('action');
        var ajaxSubmit = $this.find('.ajax-submit');
        var ajaxSubmitHtml = ajaxSubmit.html();
        var altText = loading;
        var notification = 'm';
        if (ajaxSubmit.data('loading') !== undefined) {
            altText = ajaxSubmit.data('loading');
        }
        //ajaxSubmit.prop('disabled', true).html(altText);
        var formData = new FormData(this);
        if ($this.find('.tiny-editor').length) {
            for (var i = 0; i < tinymce.editors.length; i++) {
                formData.append('editor' + (i + 1), tinymce.editors[i].getContent());
            }
        }
        if ($this.data('url') !== undefined) {
            url = $this.data('url');
        }
        if ($this.data('notification') !== undefined) {
            notification = $this.data('notification');
        }
            
            $.ajax({
            url: url, //server script to process data
            type: 'POST',
            xhr: function () {  // custom xhr
                myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // if upload property exists
                    myXhr.upload.addEventListener('progress', progressHandler, false); // progressbar
                }
                return myXhr;
            },
            // Ajax events
            success: function(data){

                $('#view_review').append(data);
            },
            
            // Form data
            data: formData,
            // Options to tell jQuery not to process data or worry about the content-type
            cache: false,
            contentType: false,
            processData: false
        }, 'json');

    });

    
    ///////////////////////////////////// End Admin Panel Ajax  ////////////////////////////////////////
    
    //////////////////////////////////////// Site Ajax  //////////////////////////////////////////////////

    /***************************************************************************
    * Form Add Ajax  submit buttons
    **************************************************************************/

    $(document).on('submit', ".new-form", function (e) {
        e.preventDefault();
        var $this = $(this);
        var url = $this.attr('action');
        var ajaxSubmit = $this.find('.new-submit');
        var ajaxSubmitHtml = ajaxSubmit.html();
        var altText = loading;
        var notification = 'm';
        if (ajaxSubmit.data('loading') !== undefined) {
            altText = ajaxSubmit.data('loading');
        }
        //ajaxSubmit.prop('disabled', true).html(altText);
        var formData = new FormData(this);
        if ($this.find('.tiny-editor').length) {
            for (var i = 0; i < tinymce.editors.length; i++) {
                formData.append('editor' + (i + 1), tinymce.editors[i].getContent());
            }
        }
        if ($this.data('url') !== undefined) {
            url = $this.data('url');
        }
        if ($this.data('notification') !== undefined) {
            notification = $this.data('notification');
        }
        request(url, formData, function (result) {
            if (result.status) {
                swal({title: "نجاح", text: result.data, type: "success"}, function () {
                    location.reload(true);
                });
            } else {
                swal('خطأ.', result.data, 'error');
            }
            },
            // on request failure handler
            function () {
                alert('خطأ فى السرفر.');
            });
    });
   

//////////////////////////////////// End Site Ajax  //////////////////////////////////////////////////
 
























    
    /****************************************************************************
    * Function Preview Url for file
    * @param  Image btn   [description]
    * @param  Input input [description]
    * @return Src      [description]
    ***************************************************************************/
    function previewURL(btn, input) {

        if (input.files && input.files[0]) {

            // collecting the file source
            var file = input.files[0];
            // preview the image
            var reader = new FileReader();
            reader.onload = function (e) {
                var src = e.target.result;
                btn.attr('src', src);
            };
            reader.readAsDataURL(file);
        }
    }

    /***************************************************************************
    * mark active page
    **************************************************************************/
    $('a[href="' + window.location.href + '"],a[href="' + window.location.href + 'home"]').closest('li').addClass('active');
    /***************************************************************************
    * validating the file
    **************************************************************************/

    function validateImgFile(input) {
        if (input.files && input.files[0]) {

            // collecting the file source
            var file = input.files[0];
            // validating the image name
            if (file.name.length < 1) {
                alert("The file name couldn't be empty");
                return false;
            }
            // validating the image size
            // else if (file.size > 300000) {
            //     alert("The file is too big");
            //     return false;
            // }
            // validating the image type
            else if (file.type != 'image/png' && file.type != 'image/jpg' && file.type != 'image/gif' && file.type != 'image/jpeg') {
                alert("The file does not match png, jpg or gif");
                return false;
            }
            return true;
        }
    }

    /***************************************************************************
    * Custom Ajax request function
    * @param string url
    * @param mixed|FormData data
    * @param callable(data) completeHandler
    * @param callable errorHandler
    * @param callable progressHandler
    * @returns void
    **************************************************************************/
    function _(data) {
        console.log(data);
    }

    function request(url, data, completeHandler, errorHandler, progressHandler) {
        if (typeof progressHandler === 'string' || progressHandler instanceof String) {
            method = progressHandler;
        } else {
            method = "POST"
        }

        $.ajax({
            url: url, //server script to process data
            type: method,
            xhr: function () {  // custom xhr
                myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // if upload property exists
                    myXhr.upload.addEventListener('progress', progressHandler, false); // progressbar
                }
                return myXhr;
            },
            // Ajax events
            success: completeHandler,
            error: errorHandler,
            // Form data
            data: data,
            // Options to tell jQuery not to process data or worry about the content-type
            cache: false,
            contentType: false,
            processData: false
        }, 'json');
    }

    /***********************************************************************
    * Notify with a message in shape of fancy alert
    **********************************************************************/

    function notify(status, title, msg, type) {
        status = (status == 'error' ? 'danger' : status);
        var callable = null;
        var template = null;
        var icons = {
            'danger': 'fa-ban',
            'success': 'fa-check',
            'info': 'fa-info',
            'warning': 'fa-warning'
        };
        if ($.isFunction(type)) {
            callable = type;
            type = 'modal';
        }

        if (!type || type == 'm') {
            type = 'modal';
        } else if (type == 'f') {
            type = 'flash';
        }

        template = $("#alert-" + type).html();
        template = template.replace(new RegExp('{icon}', 'g'), icons[status]);
        template = template.replace(new RegExp('{status}', 'g'), status);
        template = template.replace(new RegExp('{title}', 'g'), title);
        template = template.replace(new RegExp('{msg}', 'g'), msg);
        switch (type) {
            case 'modal':
            var modal = $(template).modal('toggle');
            if ($.isFunction(callable)) {
                modal.on("hidden.bs.modal", callable);
            }
            return;
            default:
            $('#alert-box').html(template);
        }

    }

    /***********************************************************************
    * loading new file image 
    **********************************************************************/
 
    $(document).on('click', '.file-generate', function () {
        var $this = $(this);
        var fileBox = $this.closest('.file-box');
        var newBox = $('div.file-box:first').clone();
        newBox.find('img').prop('src' , '');
        newBox.find('.caption').append('<button type="button" class="file-remove btn btn-danger"><i class="fa fa-minus fa-lg" aria-hidden="true"></i></button>');
        fileBox.after(newBox);

    });

    $(document).on('click', '.file-remove', function () {
        var $this = $(this);
        $this.closest('.file-box').remove();
    });

   
    $(document).on('click', '.file-btn', function () {
        $(this).closest('.file-box').find('input[type=file]').click();
    });
    $(document).on('change', '.file-box input[type=file]', function () {
        var fileBtn = $(this).closest('.file-box').find('.file-btn');
        if (validateImgFile(this)) {
            previewURL(fileBtn, this);
        }
    });
   

    /////////////////////login button ///////////////////////////////

    /***************************************************************************
    * Select2 Plugin For tags
    **************************************************************************/
    if ((tagsList = $('#select-tags')).length) {
        tagsList.select2({
            tags: true,
            dir: "rtl",
            tokenSeparators: [',', ' '],
            theme: "classic",
            multiple: true,
            ajax: {
                url: tagsList.data('url'),
                type: "GET",
                dataType: "json",
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                }
            }
        });
    }
    /***************************************************************************
    * identify Tinymce
    **************************************************************************/
    if (typeof tinymce !== "undefined") {
        /*Text area Editors
        =========================*/
        tinymce.init({
            selector: '.tiny-editor',
            height: 350,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc',
            ],
            toolbar: 'newdocument | bold | italic | underline | strikethrough | alignleft | aligncenter | alignright | alignjustify | styleselect | formatselect | fontselect | fontsizeselect | cut | copy | paste | bullist | numlist | outdent | indent | blockquote | undo redo | removeformat | subscript | superscript | link unlink | image | charmap | pastetext | print | anchor | pagebreak | spellchecker | searchreplace | save cancel | table | ltr rtl | emoticons | template | forecolor backcolor | insertfile | preview | hr | visualblocks | visualchars | code | fullscreen | insertdatetime | media | nonbreaking | inserttable tableprops deletetable cell row column | visualaid | selectall',
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],
            fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 16pt 18pt 20pt 22pt 24pt 26pt 28pt 30pt 32pt 34pt 36pt 38pt 40pt 42pt 44pt 46pt 48pt 50pt 52pt 54pt 56pt 58pt 60pt 62pt 64pt 66pt 68pt 70pt 72pt 74pt 76pt 78pt 80pt 82pt 84pt 86pt 88pt 90pt 92pt 94pt 96pt 98pt 100pt 102pt 104pt 106pt 108pt 110pt",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    }

    /******************************************************************************************
     *  Abd Elghany    Sub Category
     ******************************************************************************************/

        //get the master id and the new language parameters
        //add the new data with new language with the same master id
    var TransModalBtn = $('.transBTN');
    var modelName = $('.trans').attr('href');
    var TransModalForm = TransModalBtn.closest('form');

    TransModalBtn.on('click', function () {

        console.log(TransModalForm.attr('action'));
        request(TransModalForm.attr('action'), new FormData(TransModalForm[0]),
            // on request success handler
            function (result) {
                if (result.status) {
                    swal({title: "success.", text: result.data, type: "success"}, function () {
                        location.reload(true);
                    });
                } else {
                    swal('wrong.', result.data, 'error');
                }
            },
            // on request failure handler
            function () {
                alert('Internal Server Error.');
            });
    });

//get the language model
    var selectLang = $('.select-lang');
    var transBtn = $('.trans-btn');
    var newlangHiddenMaster = $("#newlang-insert-master_id");
    var newlangHiddenLang = $("#newlang-insert-lang");
    var newlangModalBody = $('#newlang-modal-body');
    var newlangModalBodyTemplate = $('#newlang-modal-body-template').html();
    var newlangForm = $('#trans-form');
    selectLang.on('change', function () {
        var lang = $(this).find('option:selected').val();
        transBtn.attr('data-lang', lang);
    });

    transBtn.on('click', function () {
        var masterId = $(this).attr('data-id');
        var lang = $(this).attr('data-lang');
        newlangHiddenMaster.val(masterId);
        newlangHiddenLang.val(lang);
    });

    var selectLang1 = $('.select-lang1');
    var transBtn1 = $('.trans-btn1');
    var newlangHiddenMaster1 = $("#newlang-insert-master_id1");
    var newlangHiddenLang1 = $("#newlang-insert-lang1");
    var newlangModalBody1 = $('#newlang-modal-body1');
    var newlangModalBodyTemplate1 = $('#newlang-modal-body-template1').html();
    var newlangForm1 = $('#trans-form1');
    $(document).on('change', '.select-lang1', function () {
        var lang = $(this).find('option:selected').val();
        //transBtn.attr('data-lang', lang);
        var btn1 = $(this).closest('tr').find('.trans-btn1');
        var url1 = btn1.attr('data-url1');
        var id1 = btn1.attr('data-id1');

        btn1.attr('href', url1 + "?lang=" + lang + "&master_id=" + id1);
    });

    selectLang1.change();

    transBtn1.on('click', function () {
        var masterId1 = $(this).attr('data-id1');
        var lang1 = $(this).attr('data-lang1');

        newlangHiddenMaster1.val(masterId1);
        newlangHiddenLang1.val(lang1);

        var data = new FormData();
        data.append('id', masterId1);
        data.append('lang', lang1);

        request($(this).attr('href'), data,
            // on request success handler
            function (result) {
                if (result.status) {
                    swal({title: "success.", text: result.data, type: "success"}, function () {
                        location.reload(true);
                    });
                } else {
                    swal('wrong.', result.data, 'error');
                }
            },
            // on request failure handler
            function () {
                alert('Internal Server Error.');
            }, 'get');
    });


})();

