<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
Route::get('admin/login','admin@getLogin');
//Route::get('admin/home','admin@getAdmin')->middleware('auth');
Route::get('admin.home.mahalatmasr2018@fmax0*','admin@getAdmin')->middleware('auth');
Route::post('admin.home.mahalatmasr2018@fmax0*','admin@postAdmin');


Route::get('admin/attributes','admin@getAttr')->middleware('auth');
Route::post('admin/attributes','admin@postAttr');

Route::get('admin/sub-categories/photos','admin@getphotoCats')->middleware('auth');
Route::post('admin/sub-categories/photos','admin@postphotoCats')->middleware('auth');

//Route::post('admin/subAtt','admin@getsubatt');
//Route::get('admin/subAttprop','admin@getsubattprop');
Route::get('/admin/all-notification','notificationController@getallNoti2');

Route::get('admin/add-logo', 'admin@getlogo')->middleware('auth');
Route::post('admin/add-logo', 'admin@postlogo');
Route::get('admin/Logos', 'admin@getlogos')->middleware('auth');
Route::post('/admin/Logos','admin@updatelogos');
Route::get('/icons/delete/{i}','admin@deleteIcon'); 



Route::get('admin/FAQ','admin@getFAQ')->middleware('auth');
Route::post('admin/FAQ','admin@postFAQ');

Route::get('admin/about','admin@getAbout')->middleware('auth');
Route::post('admin/about','admin@postAbout');

Route::get('admin/terms','admin@getTerms')->middleware('auth');
Route::post('admin/terms','admin@postTerms');

Route::get('admin/technical-support','admin@getSupport')->middleware('auth');
Route::post('admin/technical-support','admin@postSupport');

Route::get('admin/mobile','admin@getMobile')->middleware('auth');
Route::post('admin/mobile','admin@postMobile');

Route::get('admin/privileges','admin@getPrivileges')->middleware('auth');
Route::post('admin/privileges','admin@postPrivileges');

Route::get('admin/intellectual-property','admin@getProperty')->middleware('auth');
Route::post('admin/intellectual-property','admin@postProperty');

Route::get('admin/ads-with-us','admin@getAdsUs')->middleware('auth');
Route::post('admin/ads-with-us','admin@postAdsUs');

Route::get('admin/videos','admin@getVideos')->middleware('auth');
Route::post('admin/videos','admin@postVideos');


Route::get('admin/rules','admin@getrules')->middleware('auth');
Route::post('admin/rules','admin@postrules');

Route::get('admin/privacy','admin@getprivacy')->middleware('auth');
Route::post('admin/privacy','admin@postprivacy');

Route::get('admin/send-news','admin@getsendNews')->middleware('auth');
Route::post('admin/send-news','admin@postsendNews');

Route::get('admin/subscribers','admin@getsub')->middleware('auth');
Route::post('admin/subscribers','admin@postsub');




Route::get('admin/add-blog', 'admin@getblog')->middleware('auth');
Route::post('admin/add-blog', 'admin@postblog');
Route::get('admin/blog-category', 'admin@getblogcat')->middleware('auth');
Route::post('admin/blog-category', 'admin@postblogcat');
Route::get('admin/allPosts', 'admin@getPosts')->middleware('auth');
Route::post('/admin/allPosts','admin@updatePost');
Route::get('/blog/delete/{i}','admin@deleteBlog'); 
Route::get('/blog','admin@blogView');
Route::get('/blog/post/{i}','admin@eachPost'); 

Route::get('admin/attributes-values/{i}','admin@getattvalues');
Route::post('admin/attributes-values/{i}','admin@postattvalues');
Route::get('deleteAttAll/{i}/{m}','admin@deleteAttribute');
Route::get('deletesubprops','admin@deletesubproperty');
Route::post('admin/deleteSelectedAtt','admin@deleteSelectedAtt');


Route::get('admin/cities-areas','admin@getcity')->middleware('auth');
Route::post('admin/cities-areas','admin@postcity');
Route::get('admin/edit-areas/{i}','admin@geteditArea');
Route::post('admin/edit-areas/{i}','admin@posteditArea');
Route::get('deleteAreaEdit/{i}/{m}','admin@deleteEditarea');

Route::get('admin/all-comments','admin@getcomments')->middleware('auth');


Route::get('admin/all-vendors','admin@getvendor')->middleware('auth');
Route::post('admin/all-vendors','admin@active');

Route::get('admin/all-users','admin@getuser')->middleware('auth');
Route::post('admin/all-users','admin@active');

Route::get('admin/all-products','admin@getproduct')->middleware('auth');
Route::post('admin/all-products','admin@active');

Route::get('admin/all-Ads','admin@getads')->middleware('auth');
Route::post('admin/all-Ads','admin@postads');

Route::get('admin/dynamic-home','admin@getdaynamic')->middleware('auth');
Route::post('admin/dynamic-home','admin@postdaynamic');



// Route::get('/', function () {
//     return view('site.index1');
// });
Route::get('/','landingController@getlanding');
Route::post('autocomplete','landingController@autocompleteResult');
Route::post('/','landingController@getsearchresult');
Route::get('logout', function () {
    Auth::logout();
    return redirect('/');
});
Route::get('/login',function(){
    return view('site.Auth.login');
})->name('login');
Route::post('/login','RegisterLogin@RLogin');
//Route::post('/','admin@sendmail');
Route::get('/about-us','userController@getabout');
Route::get('/terms-condition','userController@getabout2');
Route::get('/privileges','userController@getabout3');
Route::get('/technical-support','userController@getabout4');
Route::get('/copyright','userController@getabout5');
Route::get('/ads-with-us','userController@getabout6');
Route::get('/FAQ','userController@getabout7');
Route::get('/mobile','userController@getabout8');
Route::get('/rules','userController@getabout9');
Route::get('/privacy','userController@getabout10');




// vendor controllers
Route::get('vendor/register','vendorController@getVendorRegister');
Route::post('vendor/register','vendorController@register');
Route::get('vendor/cities','vendorController@getctiy');
Route::post('vendor/cities','vendorController@getctiy');
Route::get('vendor/control-panel','vendorController@getCpanel');
Route::post('vendor/control-panel','vendorController@postCpanel');
Route::get('vendor/control-panel/location','vendorController@postCpanel');
Route::get('vendor/control-panel/profile','vendorController@getprofile');
Route::post('vendor/hide-sub','vendorController@hideSub');
Route::get('vendor/edit-product/{i}','vendorController@getEdit');
Route::post('vendor/edit-product/{i}','vendorController@postEdit');


// end vendor controller
Route::get('/all-notification','notificationController@getallNoti');

// user controller
Route::get('user/register','userController@getregister');
Route::post('user/register','userController@postregister');
Route::get('user/profile','userController@show');
Route::post('user/profile','userController@edit');
Route::get('store/profile/{i}/{city}/{area}/{searchval}','userController@getStore');
Route::post('store/profile/{i}/{city}/{area}/{searchval}','userController@addcomment');
Route::get('/{i}/product/{m}/{city}/{area}/{searchval}','userController@getProDetails');
Route::post('/{i}/product/{m}/{city}/{area}/{searchval}','userController@addcomment');
Route::get('/store/{i}/{city}/{area}/{searchval}/all-comments','userController@getallComment');
Route::post('/store/{i}/{city}/{area}/{searchval}/all-comments','userController@addcomment');
Route::get('/unsubscribe-us/{email}','userController@unsubscribe');
// end user controller

//forget

Route::get('/forgetpassword', function(){
    return view('site.Auth.forget');
});
Route::get('/forgetpassword-step2', function(){
    return view('site.Auth.forget2');
});
Route::post('/forgetpassword', 'notificationController@forget');
Route::post('/forgetpassword-step2', 'notificationController@forgetStep2');

Route::get('/callback', 'RegisterLogin@handleProviderCallback');
Route::get('/socialLogin','RegisterLogin@redirectToProvider');



// like

Route::post('like/comment','notificationController@like');
