<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
Route::get('admin.login.mahalatmasr2018@fmax0*','admin@getLogin');
//Route::get('admin/home','admin@getAdmin')->middleware('auth');
Route::get('admin.home.mahalatmasr2018@fmax0*','admin@getAdmin')->middleware('auth');
Route::post('admin.home.mahalatmasr2018@fmax0*','admin@postAdmin');


Route::get('admin.attributes.mahalatmasr2018@fmax0*','admin@getAttr')->middleware('auth');
Route::post('admin.attributes.mahalatmasr2018@fmax0*','admin@postAttr');

Route::get('admin.sub-categories/photos.mahalatmasr2018@fmax0*','admin@getphotoCats')->middleware('auth');
Route::post('admin.sub-categories/photos.mahalatmasr2018@fmax0*','admin@postphotoCats')->middleware('auth');

//Route::post('admin/subAtt','admin@getsubatt');
//Route::get('admin/subAttprop','admin@getsubattprop');
Route::get('/admin.all-notification.mahalatmasr2018@fmax0*','notificationController@getallNoti2');

Route::get('admin.add-logo.mahalatmasr2018@fmax0*', 'admin@getlogo')->middleware('auth');
Route::post('admin.add-logo.mahalatmasr2018@fmax0*', 'admin@postlogo');
Route::get('admin.Logos.mahalatmasr2018@fmax0*', 'admin@getlogos')->middleware('auth');
Route::post('/admin.Logos.mahalatmasr2018@fmax0*','admin@updatelogos');
Route::get('/icons/delete/{i}','admin@deleteIcon'); 



Route::get('admin.FAQ.mahalatmasr2018@fmax0*','admin@getFAQ')->middleware('auth');
Route::post('admin.FAQ.mahalatmasr2018@fmax0*','admin@postFAQ');

Route::get('admin.about.mahalatmasr2018@fmax0*','admin@getAbout')->middleware('auth');
Route::post('admin.about.mahalatmasr2018@fmax0*','admin@postAbout');

Route::get('admin.terms.mahalatmasr2018@fmax0*','admin@getTerms')->middleware('auth');
Route::post('admin.terms.mahalatmasr2018@fmax0*','admin@postTerms');

Route::get('admin.technical-support.mahalatmasr2018@fmax0*','admin@getSupport')->middleware('auth');
Route::post('admin.technical-support.mahalatmasr2018@fmax0*','admin@postSupport');

Route::get('admin.mobile.mahalatmasr2018@fmax0*','admin@getMobile')->middleware('auth');
Route::post('admin.mobile.mahalatmasr2018@fmax0*','admin@postMobile');

Route::get('admin.privileges.mahalatmasr2018@fmax0*','admin@getPrivileges')->middleware('auth');
Route::post('admin.privileges.mahalatmasr2018@fmax0*','admin@postPrivileges');

Route::get('admin.intellectual-property.mahalatmasr2018@fmax0*','admin@getProperty')->middleware('auth');
Route::post('admin.intellectual-property.mahalatmasr2018@fmax0*','admin@postProperty');

Route::get('admin.ads-with-us.mahalatmasr2018@fmax0*','admin@getAdsUs')->middleware('auth');
Route::post('admin.ads-with-us.mahalatmasr2018@fmax0*','admin@postAdsUs');

Route::get('admin.videos.mahalatmasr2018@fmax0*','admin@getVideos')->middleware('auth');
Route::post('admin.videos.mahalatmasr2018@fmax0*','admin@postVideos');


Route::get('admin.rules.mahalatmasr2018@fmax0*','admin@getrules')->middleware('auth');
Route::post('admin.rules.mahalatmasr2018@fmax0*','admin@postrules');

Route::get('admin.privacy.mahalatmasr2018@fmax0*','admin@getprivacy')->middleware('auth');
Route::post('admin.privacy.mahalatmasr2018@fmax0*','admin@postprivacy');

Route::get('admin.send-news.mahalatmasr2018@fmax0*','admin@getsendNews')->middleware('auth');
Route::post('admin.send-news.mahalatmasr2018@fmax0*','admin@postsendNews');

Route::get('admin.subscribers.mahalatmasr2018@fmax0*','admin@getsub')->middleware('auth');
Route::post('admin.subscribers.mahalatmasr2018@fmax0*','admin@postsub');

Route::get('admin.contact_links.mahalatmasr2018@fmax0*','admin@getLinks')->middleware('auth');
Route::post('admin.contact_links.mahalatmasr2018@fmax0*','admin@postLinks');



Route::get('admin.add-blog.mahalatmasr2018@fmax0*', 'admin@getblog')->middleware('auth');
Route::post('admin.add-blog.mahalatmasr2018@fmax0*', 'admin@postblog');
Route::get('admin.blog-category.mahalatmasr2018@fmax0*', 'admin@getblogcat')->middleware('auth');
Route::post('admin.blog-category.mahalatmasr2018@fmax0*', 'admin@postblogcat');
Route::get('admin.allPosts.mahalatmasr2018@fmax0*', 'admin@getPosts')->middleware('auth');
Route::post('/admin.allPosts.mahalatmasr2018@fmax0*','admin@updatePost');
Route::get('/blog/delete/{i}','admin@deleteBlog'); 
Route::get('/blog','admin@blogView');
Route::get('/blog/post/{i}','admin@eachPost'); 

Route::get('admin.attributes-values/{i}.mahalatmasr2018@fmax0*','admin@getattvalues')->middleware('auth');
Route::post('admin.attributes-values/{i}.mahalatmasr2018@fmax0*','admin@postattvalues');
Route::get('deleteAttAll/{i}/{m}','admin@deleteAttribute');
Route::get('deletesubprops','admin@deletesubproperty');
Route::post('admin/deleteSelectedAtt','admin@deleteSelectedAtt');


Route::get('admin.cities-areas.mahalatmasr2018@fmax0*','admin@getcity')->middleware('auth');
Route::post('admin.cities-areas.mahalatmasr2018@fmax0*','admin@postcity');
Route::get('admin.edit-areas/{i}.mahalatmasr2018@fmax0*','admin@geteditArea')->middleware('auth');
Route::post('admin.edit-areas/{i}.mahalatmasr2018@fmax0*','admin@posteditArea');
Route::get('deleteAreaEdit/{i}/{m}','admin@deleteEditarea');

Route::get('admin.all-comments.mahalatmasr2018@fmax0*','admin@getcomments')->middleware('auth');
Route::post('admin.all-comments.mahalatmasr2018@fmax0*','admin@active');

Route::get('admin.all-vendors.mahalatmasr2018@fmax0*','admin@getvendor')->middleware('auth');
Route::post('admin.all-vendors.mahalatmasr2018@fmax0*','admin@active');

Route::get('admin.all-users.mahalatmasr2018@fmax0*','admin@getuser')->middleware('auth');
Route::post('admin.all-users.mahalatmasr2018@fmax0*','admin@active');

Route::get('admin.all-products.mahalatmasr2018@fmax0*','admin@getproduct')->middleware('auth');
Route::post('admin.all-products.mahalatmasr2018@fmax0*','admin@active');

Route::get('admin.all-Ads.mahalatmasr2018@fmax0*','admin@getads')->middleware('auth');
Route::post('admin.all-Ads.mahalatmasr2018@fmax0*','admin@postads');

Route::get('admin.dynamic-home.mahalatmasr2018@fmax0*','admin@getdaynamic')->middleware('auth');
Route::post('admin.dynamic-home.mahalatmasr2018@fmax0*','admin@postdaynamic');



// Route::get('/', function () {
//     return view('site.index1');
// });
Route::get('/','landingController@getlanding');
Route::post('autocomplete','landingController@autocompleteResult');
Route::post('/','landingController@getsearchresult');
Route::get('logout', function () {
    Auth::logout();
    return redirect('/');
});
Route::get('/login',function(){
    return view('site.Auth.login');
})->name('login');
Route::post('/login','RegisterLogin@RLogin');
//Route::post('/','admin@sendmail');
Route::get('/about-us','userController@getabout');
Route::get('/terms-condition','userController@getabout2');
Route::get('/privileges','userController@getabout3');
Route::get('/technical-support','userController@getabout4');
Route::get('/copyright','userController@getabout5');
Route::get('/ads-with-us','userController@getabout6');
Route::get('/FAQ','userController@getabout7');
Route::get('/mobile','userController@getabout8');
Route::get('/rules','userController@getabout9');
Route::get('/privacy','userController@getabout10');




// vendor controllers
Route::get('vendor/register','vendorController@getVendorRegister');
Route::post('vendor/register','vendorController@register');
Route::get('vendor/cities','vendorController@getctiy');
Route::post('vendor/cities','vendorController@getctiy');
Route::get('vendor/control-panel','vendorController@getCpanel');
Route::post('vendor/control-panel','vendorController@postCpanel');
Route::get('vendor/control-panel/location','vendorController@postCpanel');
Route::get('vendor/control-panel/profile','vendorController@getprofile');
Route::post('vendor/hide-sub','vendorController@hideSub');
Route::get('vendor/edit-product/{i}','vendorController@getEdit');
Route::post('vendor/edit-product/{i}','vendorController@postEdit');

// end vendor controller
Route::get('/all-notification','notificationController@getallNoti');

// user controller
Route::get('user/register','userController@getregister');
Route::post('user/register','userController@postregister');
Route::get('user/profile','userController@show');
Route::post('user/profile','userController@edit');
Route::get('store/profile/{i}/{city}/{area}/{searchval}','userController@getStore');
Route::post('store/profile/{i}/{city}/{area}/{searchval}','userController@addcomment');
Route::get('/{i}/product/{m}/{city}/{area}/{searchval}','userController@getProDetails');
Route::post('/{i}/product/{m}/{city}/{area}/{searchval}','userController@addcomment');
Route::get('/store/{i}/{city}/{area}/{searchval}/all-comments','userController@getallComment');
Route::post('/store/{i}/{city}/{area}/{searchval}/all-comments','userController@addcomment');
Route::get('/unsubscribe-us/{email}','userController@unsubscribe');
// end user controller

//forget

Route::get('/forgetpassword', function(){
    return view('site.Auth.forget');
});
Route::get('/forgetpassword-step2', function(){
    return view('site.Auth.forget2');
});
Route::post('/forgetpassword', 'notificationController@forget');
Route::post('/forgetpassword-step2', 'notificationController@forgetStep2');

Route::get('/callback', 'RegisterLogin@handleProviderCallback');
Route::get('/socialLogin','RegisterLogin@redirectToProvider');



// like

Route::post('like/comment','notificationController@like');
