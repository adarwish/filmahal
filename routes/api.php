<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('add-product','vendorController@postCpanel');
Route::post('active/vendor','admin@active');
Route::post('active/comment','admin@comments');
Route::post('admin/home','admin@postAdmin');
Route::post('vendor/delete-sub','vendorController@deleteSub');

Route::post('product/attributes','userController@getproductattr');
Route::post('/store/specific','userController@getspecificpro');
Route::post('review/users/','userController@getreviewusers');
Route::get('get/location','userController@getlocation');
Route::post('get/notification','notificationController@usernotification');
Route::post('get/notification2','notificationController@usernotification2');
Route::post('noti/phone','notificationController@usernotification3');
Route::post('getmsg','userController@getmsgs');
Route::post('block','notificationController@block');
Route::post('like/comment','notificationController@like');
Route::post('delete/subs','admin@deletesub');
Route::post('admin/blog-category','admin@postblogcat');

Route::post('filterBlog','admin@getBlogFilter');
//Route::post('email/subs','admin@postsendNews');

//Route::post('store/profile','userController@addcomment');

Route::post('imageLogo','admin@imageLogo');
Route::get('allitems','admin@all');
Route::get('rejectitems','admin@reject');
Route::get('waitingitems','admin@waiting');
Route::get('todayitems','admin@today');