$(function() {
    $(document).ready(function () {
        $('#summernote').summernote();
    });
    
    $('.dropify').dropify({
        messages: {
            'default': 'اسحب واسقط الصورة هنا او اضغط',
            'replace': 'اسحب واسقط او اضغط لتغيير الصورة',
            'remove':  'مسح',
            'error':   'هناك خطأ!'
        }
    });
    
    $(".chosen-select").chosen({
        no_results_text: "لا يوجد!",
        rtl: true,
        width: "100%"
    });

    
    /* $("input[type='file']").change(function() {
        readIMG(this, 1349, 471);
    }); */
    
    const inputFiles = $('input:file');

    $.each(inputFiles, thumbinal);

    function thumbinal() {
    $(this).bind("input", function () {
        var reader = new FileReader();
        var id = $(this).attr('id');
        reader.onload = function (e) {
            var img = document.querySelector('[data-name="' + id + '"]');
                img.src = e.target.result;
            };
            reader.readAsDataURL(this.files[0]);
    });
    }
    
    $('#example').DataTable({

        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0,
            
        } ]
        ,
        
        select: {
            style:    'multi',
            selector: 'td:first-child'
        },
        
        order: [[ 3, 'asc' ]],
        "language": {
          
    "sProcessing":   "جارٍ التحميل...",
    "sLengthMenu":   "أظهر _MENU_ مدخلات",
    "sZeroRecords":  "لم يعثر على أية سجلات",
    "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
    "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
    "sInfoPostFix":  "",
    "sSearch":       "ابحث:",
    "sUrl":          "",
    "oPaginate": {
        "sFirst":    "الأول",
        "sPrevious": "السابق",
        "sNext":     "التالي",
        "sLast":     "الأخير"
    }

       },
        
        responsive: true
        
    } );
    
    $("#example3").DataTable({
        order: [[ 1, 'asc' ]],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Arabic.json",
        "sProcessing":   "جارٍ التحميل...",
    "sLengthMenu":   "أظهر _MENU_ مدخلات",
    "sZeroRecords":  "لم يعثر على أية سجلات",
    "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
    "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
    "sInfoPostFix":  "",
    "sSearch":       "ابحث:",
    "sUrl":          "",
    "oPaginate": {
        "sFirst":    "الأول",
        "sPrevious": "السابق",
        "sNext":     "التالي",
        "sLast":     "الأخير"
    }
        },
        responsive: true,
    });
    
    // NEW
    $("#example1, #example2").DataTable({
        order: [[ 2, 'asc' ]],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Arabic.json",
        "sProcessing":   "جارٍ التحميل...",
    "sLengthMenu":   "أظهر _MENU_ مدخلات",
    "sZeroRecords":  "لم يعثر على أية سجلات",
    "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
    "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
    "sInfoPostFix":  "",
    "sSearch":       "ابحث:",
    "sUrl":          "",
    "oPaginate": {
        "sFirst":    "الأول",
        "sPrevious": "السابق",
        "sNext":     "التالي",
        "sLast":     "الأخير"
    }
        },
        responsive: true,
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        }]
    });
    $("#example4, #example6").DataTable({
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        order: [[ 2, 'asc' ]],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Arabic.json",
        "sProcessing":   "جارٍ التحميل...",
    "sLengthMenu":   "أظهر _MENU_ مدخلات",
    "sZeroRecords":  "لم يعثر على أية سجلات",
    "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
    "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
    "sInfoPostFix":  "",
    "sSearch":       "ابحث:",
    "sUrl":          "",
    "oPaginate": {
        "sFirst":    "الأول",
        "sPrevious": "السابق",
        "sNext":     "التالي",
        "sLast":     "الأخير"
    }
        },
        responsive: true,
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        },
        {
            targets: 1,
            width: "10%"
        }]
        
    });
    $("#example5").DataTable({
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        order: [[ 2, 'asc' ]],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Arabic.json",
        "sProcessing":   "جارٍ التحميل...",
    "sLengthMenu":   "أظهر _MENU_ مدخلات",
    "sZeroRecords":  "لم يعثر على أية سجلات",
    "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
    "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
    "sInfoPostFix":  "",
    "sSearch":       "ابحث:",
    "sUrl":          "",
    "oPaginate": {
        "sFirst":    "الأول",
        "sPrevious": "السابق",
        "sNext":     "التالي",
        "sLast":     "الأخير"
    }
        },
        responsive: true,
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0,
            width: "20px"
        },
        {
            targets: 2,
            width: "200px"
        }]
        
    });
    
    $('#datatable-blogCats, #datatable-subscribers').DataTable({

        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0,
            
        } ]
        ,
        
        select: {
            style:    'multi',
            selector: 'td:first-child'
        },
        
        order: [[ 1, 'asc' ]],
        "language": {
          
    "sProcessing":   "جارٍ التحميل...",
    "sLengthMenu":   "أظهر _MENU_ مدخلات",
    "sZeroRecords":  "لم يعثر على أية سجلات",
    "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
    "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
    "sInfoPostFix":  "",
    "sSearch":       "ابحث:",
    "sUrl":          "",
    "oPaginate": {
        "sFirst":    "الأول",
        "sPrevious": "السابق",
        "sNext":     "التالي",
        "sLast":     "الأخير"
    }

       }
        
    } );
    
    
    
    $("#add").click(function() {
        addInput(this, "cates");
    });
    
    $(".add-branches").click(function() {
        if($(this).parent().parent().prop("id") == "cates-branches") {
            addInput(this, "#cates-branches");
        } else {
            addInput(this, ".govs");
        }
        
    });

    function addInput(self, container) {
        const boxAddRemove = $(self).parent().clone(true);
        $(boxAddRemove).find("input").val("");
        $(boxAddRemove).removeClass("no-drop").appendTo(container);
    }
    
    $(".remove-branches").click(function() {
        if(!$(this).parent().hasClass("no-drop")) {
            removeInput(this);
        }
    });

    function removeInput(self) {
        const boxAddRemove = $(self).parent();
        $(boxAddRemove).remove();
    }
    
     $("#add-property").click(function() {
         addInput(this, "properties");
    })
    
    // function addInput(ele, name) {
    //     $(ele).before('<input type="text" name=' + name + '>');
    // }
    
    function implement(ele, naver) {
        $(ele).removeClass("hidden").addClass("show");
        $(naver).parent().children().removeClass("active");
        $(naver).addClass("active");
        
    }

    /*
    $("#first").click(function(ev) {
        ev.preventDefault();
        $("#first-step").removeClass("hidden");
        $("#second-step, #third-step, #fourth-step, #fifth-step, #sixth-step, #seventh-step").addClass("hidden");
        implement("#first-step", this);
    });
    
   $("#second").click(function(ev) {
     ev.preventDefault();
      $("#first-step, #third-step, #fourth-step, #fifth-step, #sixth-step, #seventh-step").addClass("hidden");
       implement("#second-step", this);
   });
   
   $("#third").click(function(ev) {
      ev.preventDefault();
        $("#first-step, #second-step, #fourth-step, #fifth-step, #sixth-step, #seventh-step").addClass("hidden");
        implement("#third-step", this);
   });

   $("#fourth").click(function(ev) {
      //ev.preventDefault();
        $("#govCancel").trigger("click");
        $("#first-step, #second-step, #third-step, #fifth-step, #sixth-step, #seventh-step").addClass("hidden");
        implement("#fourth-step", this);
   });

   $("#fifth").click(function(ev) {
    ev.preventDefault();
      $("#first-step, #second-step, #third-step, #fourth-step, #sixth-step, #seventh-step").addClass("hidden");
     implement("#fifth-step", this);
   });

   $("#sixth").click(function(ev) {
    ev.preventDefault();
      $("#first-step, #second-step, #third-step, #fourth-step, #fifth-step, #seventh-step").addClass("hidden");
     implement("#sixth-step", this);
   });

   $("#seventh").click(function(ev) {
    ev.preventDefault();
      $("#first-step, #second-step, #third-step, #fourth-step, #fifth-step, #sixth-step").addClass("hidden");
     implement("#seventh-step", this);
   });
   
    */
//    $("#save1").click(function(ev) {
//        ev.preventDefault();
//            var i = 0;
//            var inputs = $("#inputs-cates input");
//            $("#inputs-cates input").each(function(){
//                
//                if(!($(this).val().trim())) {
//                    return false;
//                } else {
//                    $(this).val($(this).val().trim());
//                    ++i;
//                }
//            });
//        
//            if(i == inputs.length) {
//                $("#errorMessage").text("");
//                swal({title:"تم الحفظ بنجاح",
//                    type: "success"});
//            } else {
//                swal({title:"احد المدخلات فارغه",
//                    type: "error"});
//            }
//    });
    
    $("#save").click(function(ev) {
       //ev.preventDefault(); //****************This Prevent Default Action of Button (حفظ)********************
        var self = $(this);
        var kind = $("#select-cates").find(":selected");
        var classVal = kind.attr("class");
        var add = $("#add-cates").val().trim();
        var regex = /[\d]/g;
        var count = "";
        if(add && classVal == "none" ) {
            $("#select-cates").append('<option class="level-0" value="">' + add + '</option>');
        } else if(add) {
            count = classVal.match(regex)[0];
            count = parseInt(count);
            kind.after('<option class="level-' + (++count) + '">' + ("&nbsp;".repeat(count*3)) + add + '</option>');
        }
    });
    
    
    function edit(self) {
//        console.log(self);
        var children = $(self).parent().children();
        var cell_0 = children[1];
        var children_cell_0 = $(children[1]).children();
        var child_0 = $(children_cell_0)[0];
        var child_1 = $(children_cell_0)[1];
        var cell_1 = children[2];
        if($(cell_0).children().is("input")) {
        
            // var dataCell_0 = $(cell_0).val();
            // var dataCell_1 = $(cell_1).val();

            // self.value_0 = cell_0;
            //  console.log(cell_0);
            $(child_0).attr("disabled", false);
            $(child_1).attr("disabled", false);
            $(cell_1).children().attr("disabled", false);
            
            $("#apply").removeClass("hidden");
        }
    }
    
    function apply() {
        var inputs = $("tr").find("input");
        var areas = $("tr").find("textarea");
        
        inputs.each(function(index) {
            if(!$(inputs[index]).prop("disabled") && $(inputs[index]).is("[type=text]")) {
               // $(inputs[index]).prop("disabled", true);
               setTimeout(function() {
                    $(inputs[index]).attr("disabled", true);
               }, 1000);
                console.log($(inputs[index]).val());
            }
           
        });
        
        areas.each(function(index) {
           if(!$(areas[index]).prop("disabled")) {
                setTimeout(function() {
                    $(areas[index]).attr("disabled", true);
               }, 1000);
            };
        });
        $("#action-list option:first").prop("selected", true);
    }
    
    function deleteSelected(self, num) {
        
        self.parent().remove();
        console.log(num);
        for(var i=0; i<self.length; i++){
            var x = self[i];
            console.log($(x).parent().attr("id"));
            $.ajax({
            type:'POST',
            url:'/api/admin/home',
            data:{delete:num,Ids:$(x).parent().attr("id")},
            success: function(data){
                console.log(data);
        
            },
            error: function(){
                console.log('error');
            }
          });
        }

    }
    function deleteSelected22(self, num) {
        
        self.parent().remove();
        console.log(num);
        for(var i=0; i<self.length; i++){
            var x = self[i];
            console.log($(x).parent().attr("id"));
            $.ajax({
                type:'POST',
                url:'/api/admin/blog-category',
                data:{save2:'save2',id:$(x).parent().attr("id")},
            success: function(data){
                  console.log(data);
                        
           },
           error: function(){
                console.log('error');
            }
          });
        }

    }
    function deleteSelected1(self, num) {
        
        self.parent().remove();
        console.log(num);
        for(var i=0; i<self.length; i++){
            var x = self[i];
            console.log($(x).parent().attr("id"));
            $.ajax({
            type:'POST',
            url:'/admin/home',
            data:{delete:num,Ids:$(x).parent().attr("id")},
            success: function(data){
                console.log(data);
        
            },
            error: function(){
                console.log('error');
            }
          });
        }

    }
    
    
    $("#action-list").change(function() {
        var selected = $(this).find("option:selected");
        var value = $(selected).attr("value");
        var checked = $(".selected").find(".select-checkbox");
        
        if(value == "1") {
           checked.each(function(index) {
               edit(this);
           });
        } 
        else if(value == "2") {
            deleteSelected(checked, 1);

            $(".select-info").remove();
        }
        else if(value == "222") {
            deleteSelected22(checked, 1);

            $(".select-info").remove();
        }
        
    });
    
    
//     $(".select-checkbox").click(function() {
//         $(this).parent().toggleClass(" selected");
//         $("#action-list option:first").prop("selected", true);
//         var children = $(this).parent().children();
//         var cell_0 = children[1];
// //        console.log(cell_0);
//         var cell_1 = children[2];
//         var dataCell_0 = $(cell_0).children().val();
//         var dataCell_1 = $(cell_1).children().val();
        
//         if(!$(cell_0).children().is("input")) {
//             $(cell_0).replaceWith('<td class="sorting_1">' + dataCell_0 + '</td>');
//             $(cell_1).replaceWith('<td class="sorting_1">' + dataCell_1 + '</td>');
//         }
        
//     });

$("#example .select-checkbox").click(function() {
        if(!$(this).parent().parent().is("thead")) {
            if(!$(this).parent().hasClass("selected")) {
                $(this).parent().toggleClass(" selected");
                $("#action-list option:first").prop("selected", true);
                var children = $(this).parent().children();
                var cell_0 = children[1];
        //        console.log(cell_0);
                var cell_1 = children[2];
                var dataCell_0 = $(cell_0).children().val();
                var dataCell_1 = $(cell_1).children().val();

                if(!$(cell_0).children().is("input")) {
                    $(cell_0).replaceWith('<td class="sorting_1">' + dataCell_0 + '</td>');
                    $(cell_1).replaceWith('<td class="sorting_1">' + dataCell_1 + '</td>');
                }
            } else {
                $(this).parent().find("input").prop("disabled", "true");
                $(this).parent().find("textarea").prop("disabled", "true");
            }
            setTimeout(calculate, 1);
        }
        
        
    });
    
    $("#datatable-blogCats .select-checkbox").click(function() {
        if(!$(this).parent().parent().is("thead")) {
            if(!$(this).parent().hasClass("selected")) {
                $(this).parent().toggleClass(" selected");
                $("#action-list option:first").prop("selected", true);
                var children = $(this).parent().children();
                var cell_0 = children[1];
        //        console.log(cell_0);
                var cell_1 = children[2];
                var dataCell_0 = $(cell_0).children().val();
                var dataCell_1 = $(cell_1).children().val();

                if(!$(cell_0).children().is("input")) {
                    $(cell_0).replaceWith('<td class="sorting_1">' + dataCell_0 + '</td>');
                    $(cell_1).replaceWith('<td class="sorting_1">' + dataCell_1 + '</td>');
                }
            } else {
                $(this).parent().find("input").prop("disabled", "true");
                $(this).parent().find("textarea").prop("disabled", "true");
            }
            setTimeout(calculate, 1);
        }
        
        
    });
    
   /* $("#apply").click(function() {
        apply();
        $(this).addClass("hidden");
    });*/
    
    
    function calculate() {
        const len = $("tbody .selected");
            const lenMain = $("tbody tr");
            console.log(len.length, lenMain.length);
            if(len.length == lenMain.length) {
                
                $("#none-all").removeClass("hidden");
                $("#select-all").addClass("hidden");
            } else {
                $("#none-all").addClass("hidden");
                $("#select-all").removeClass("hidden");
            }
    }
    
     $("button").click(function() {
        if($(this).attr("id") == "select-all") { 
            $("#example tbody tr").addClass(" selected");
            $(this).addClass("hidden");
            $("#none-all").removeClass("hidden");
        } else if($(this).attr("id") == "select-all-attrs") {
            $("#example1 tbody tr").addClass(" selected");
            $(this).addClass("hidden");
            $("#none-all-attrs").removeClass("hidden");
        } else if($(this).attr("id") == "select-all-govs") {
            $("#example2 tbody tr").addClass(" selected");
            $(this).addClass("hidden");
            $("#none-all-govs").removeClass("hidden");
        } else if($(this).attr("id") == "select-all-users") {
            $("#example4 tbody tr").addClass(" selected");
            $(this).addClass("hidden");
            $("#none-all-users").removeClass("hidden");
        } else if($(this).attr("id") == "select-all-blog") { 
            $("#datatable-blogCats tbody tr").addClass(" selected");
            $(this).addClass("hidden");
            $("#none-all-blog").removeClass("hidden");
        } else if($(this).attr("id") == "select-all-news") { 
            $("#datatable-subscribers tbody tr").addClass(" selected");
            $(this).addClass("hidden");
            $("#none-all-news").removeClass("hidden");
        }
    });
    
    $("button").click(function() {
        if($(this).attr("id") == "none-all") {
            $("#example tbody tr").removeClass(" selected");
            const inputs = $("#example tbody tr").find("input");
            const textareas = $("#example tbody tr").find("textarea");
            $(inputs).prop("disabled", "true");
            $(textareas).prop("disabled", "true");
            $(this).addClass("hidden");
            $("#select-all").removeClass("hidden");
        } else if($(this).attr("id") == "none-all-attrs") {
            $("#example1 tbody tr").removeClass(" selected");
            const inputs = $("#example1 tbody tr").find("input");
            const textareas = $("#example1 tbody tr").find("textarea");
            $(inputs).prop("disabled", "true");
            $(textareas).prop("disabled", "true");
            $(this).addClass("hidden");
            $("#select-all-attrs").removeClass("hidden");
        } else if($(this).attr("id") == "none-all-govs") {
             $("#example2 tbody tr").removeClass(" selected");
            const inputs = $("#example2 tbody tr").find("input");
            const textareas = $("#example2 tbody tr").find("textarea");
            $(inputs).prop("disabled", "true");
            $(textareas).prop("disabled", "true");
            $(this).addClass("hidden");
            $("#select-all-govs").removeClass("hidden");
        } else if($(this).attr("id") == "none-all-users") {
            $("#example4 tbody tr").removeClass(" selected");
            $("#select-all-users").removeClass("hidden");
            $(this).addClass("hidden");
       } else if($(this).attr("id") == "none-all-blog") {
            $("#datatable-blogCats tbody tr").removeClass(" selected");
            const inputs = $("#datatable-blogCats tbody tr").find("input");
            const textareas = $("#datatable-blogCats tbody tr").find("textarea");
            $(inputs).prop("disabled", "true");
            $(textareas).prop("disabled", "true");
            $(this).addClass("hidden");
            $("#select-all-blog").removeClass("hidden");
        } else if($(this).attr("id") == "none-all-news") {
            $("#datatable-subscribers tbody tr").removeClass(" selected");
            const inputs = $("#datatable-subscribers tbody tr").find("input");
            const textareas = $("#datatable-subscribers tbody tr").find("textarea");
            $(inputs).prop("disabled", "true");
            $(textareas).prop("disabled", "true");
            $(this).addClass("hidden");
            $("#select-all-news").removeClass("hidden");
        }
    });
    
    $(".pagination .paginate_button a").click(function(ev) {
        ev.preventDefault();
        console.log("work");
        $("#none-all").addClass("hidden");
        $("#select-all").removeClass("hidden");
    });
    
    
    
    
    $("#chooseFilter").change(function() {
        const val = $(this).val();
        switch(val) {
            case "0":
                $(".box2, .box3").hide();
                break;
            case "1":
                filterHideShow("#textAttr", "#cates-branches");
                $(".boxAddRemove input").prop("name", "checkbox");
                break;
            case "2":
                filterHideShow("#textAttr", "#cates-branches");
                $(".boxAddRemove input").prop("name", "radio");
                break;
            case "3":
                filterHideShow("#cates-branches", "#textAttr");
                break;
        }
    });

    function filterHideShow(hide, show) {
        $(hide).hide();
        $(show).show();
    }


    // Start 0f function to add attributes to list
    function addToList(attributes) {
        const list = $(".list-attr");
        $(list).children().remove();    //Delete All "li" in "ul" and re-build it again with new values added.
        let i = 0;
        for(const attr of attributes) {
            $(list).append(`<li><span>${attr.attrName}</span> <input type="hidden" value="${i}"><a href="#">تعديل</a><span class="delete">مسح</span></li>`);
            i++;
        }
            
    }
    // End 0f function to add attributes to list

    // Start 0f ADD Attribute
        
    $("#addAttr").click(function(ev) {
        ev.preventDefault();
        const box1 = $(".box1");
        const box2 = $(".box2");
        const box3 = $(".box3");
        const box1Inputs  =$(".box1 input");
        const box2Inputs  =$(".box2 input");
        const box3Inputs  =$(".box3 input");
        const box2RemoveAdd = $(".no-drop").eq(0).clone(true);
        $(box2RemoveAdd).find("input").val("");
        $(box1Inputs).each(function(index) {
            $(box1Inputs[index]).val("");
            console.log("box1");
        });
        $(box2Inputs).each(function(index) {
            $(box2Inputs[index]).val("");
            console.log("box2");
        });
        $(box3Inputs).each(function(index) {
            $(box3Inputs[index]).val("");
            console.log("box3");
        });
        $(box2).children().remove();
        $(box2).append(box2RemoveAdd);
        $(box1, box2, box3).hide();
        $(box1).show();
        $("#chooseFilter").val("0").trigger('change');
        $("#applyEdit").hide();
        $("#applyAttr").show();
    });
// END 0f ADD Attribute

// Start 0f Cancel Adding or Editing Attribute
    $("#cancel").click(function(ev) {
        ev.preventDefault();
        $(".box1, .box2, .box3").hide();
    });
// End 0f Cancel Adding or Editing Attribute

// Start 0f Save Attribute
    let attributes = [];
    let flag;
    $("#applyAttr").click(function(ev) {
        ev.preventDefault();
        const attrName = $(".box1 input").val();
        const attrChoose = $("#chooseFilter").val();
        let attrValues = [];

        if(saveAttr()) {
            if(attrChoose == "1" || attrChoose == "2") {
                const inputs = $(".box2 .boxAddRemove input");
                for(const input of inputs) {
                    // Add All Values 0f Inputs
                    attrValues.push(
                        $(input).val()
                    );
                }
            } else if(attrChoose == "3") {
                // Name Value 0f Attribute
                attrValues = $(".box3 input").val();
            }

            attributes.push({
                "attrName": attrName,
                "attrChoose": attrChoose,
                "attrValues": attrValues
            });
            
        addToList(attributes);
            $(".box1, .box2, .box3").find("input").val("");
            $(".box1, .box2, .box3").hide();
            flag = true;
        } else {
            //error
            swal(
                "خطأ",
                'بعض المُدخلات فارغه',
                'error'
              )
              flag = false;
        }
    });
// End 0f Save Attribute

// Start 0f Easy Edit ;)
let indexOfArr;
$(".list-attr").on("click", "a", function(ev) {
    ev.preventDefault();
    $("#applyEdit").show();
    $("#applyAttr, .delete").hide();
    // indexOfArr = $(this).parent().find("input").val();
    // const content = attributes[indexOfArr];
    // const len = content.attrValues.length;
    // let box = $(".boxAddRemove").eq(0).clone(true);
    
    // $(".box1").show();
    // $(".box1").find("input").val(content.attrName);
    // $("#chooseFilter").val(content.attrChoose).trigger('change');
    
    
    // if(len > 1) {
    //     if(content.attrChoose == "1" || content.attrChoose == "2") {
    //         $(".box2").children().remove();
    //         for(const val of content.attrValues) {
    //             $(box).appendTo(".box2");
    //             $(box).find("input").val(val);
    //             $("#textAttr input").val("");
    //             box = $(".boxAddRemove").eq(0).clone(true).prop("id", false);
    //         }
    //     } else if(content.attrChoose == "3") {
    //         $(".box2").children().remove();
    //         $(box).find("input").val("");
    //         $(".box2").append(box);
    //         $("#textAttr input").val(content.attrValues);
    //         }
    // } else {
    //     if(content.attrChoose == "1" || content.attrChoose == "2") {
    //         $(".box2").children().remove();
    //         $(box).find("input").val("");
    //         $(box).appendTo(".box2");
    //         $("#textAttr input").val("");
    //         $(box).find("input").val(content.attrValues);
    //         console.log("here");
    //     } else if(content.attrChoose == "3") {
    //         $(".box2").children().remove();
    //         $(box).find("input").val("");
    //         $(".box2").append(box);
    //         $("#textAttr input").val(content.attrValues);
    //     } 
    // }
});

//*************************************/
$("#applyEdit").click(function() {
    $(".delete").show();
    const myAttr = attributes[indexOfArr];
    let myValues;
    let x;
    let y;
    
    const box = $(".boxAddRemove").eq(0).clone(true);
    myAttr.attrChoose = $("#chooseFilter").val();
    myAttr.attrValues = [];
    if($("#getAttrName").val().trim()) {
        myAttr.attrName = $("#getAttrName").val().trim();
        x = true;
    } else {
        swal(
            "خطأ",
            ' اسم الخاصيه فارغ',
            'error'
          );
          x = false;
    }
    if(myAttr.attrChoose != "3") {
        myValues = $(".boxAddRemove input");
        for(const val of myValues) {
            if($(val).val()) {
                myAttr.attrValues.push($(val).val());
            } else {
                //error
                swal(
                    "خطأ",
                    ' احدى قيم الخاصيه فارغه ',
                    'error'
                  );
                  x = false;
                  break;
            }
            y = true;
        }
    } else {
        $(".box2").children().remove();
        $(".box2").append(box);
        if($("#textAttr input").val()) {
            myAttr.attrValues = $("#textAttr input").val();
            y = true;
        }else {
            swal(
                "خطأ",
                'قيمة الخاصيه فارغه',
                'error'
              );
              y = false;
        }
    }

   if(x && y) {
    addToList(attributes);
    
    $(".box1, .box2, .box3").find("input").val("");
    
    $(".box1, .box2, .box3").hide();
   }
    
});
// End 0f Easy Edit ;)

// Start 0f Deleting Element
$(".list-attr").on("click", ".delete", function(ev) {
    ev.preventDefault();
    const removedIndex = $(this).parent().find("input").val();
    $(this).parent().remove();
    attributes.splice(removedIndex,1);
    if(attributes.length) {
        addToList(attributes);
    }
});
// End 0f Deleting Element

function validation(ev) {
    const attrName = $(".nameOfAttributes div input").val();
    if(attrName) {
        if(!flag && !$(".list-attr").children().length) {
            ev.preventDefault();
            swal(
                "خطأ",
                'لم يتم ادخال اى خواص',
                'error'
              );
        } else {
             $(".model").hide();
             $("#cancel").trigger("click");
            for(const attr of attributes) {
        $.ajax({
            type:'POST',
            url:'/admin.attributes.mahalatmasr2018@fmax0*',
            data:{collectionName:$('#collectionName').val(),newAttr: attr.attrName,choose: attr.attrChoose,subprop: attr.attrValues,save1:'save1',collectionedit:$("#collectionId").val()},
            success: function(data){
                console.log(data);
            swal();
                        swal({
                            title: "تم اضافة المجموعة بنجاح",
                            text: " "
                        });
            },
            error: function(){
                console.log('error');
            }
        });
    }
        }
    } else {
        ev.preventDefault();
        //Error Must Have Collection Name
        swal(
            "خطأ",
            'يجب عليك ادخال اسم لمجموعه الخصائص',
            'error'
          );
        
    }
}

$("#cancelMe").click(function(ev) {
        ev.preventDefault();
        $(".model").hide();
        $("#cancel").trigger("click");
    });

function saveAttr() {
    const attrName = $("#getAttrName").val().trim();
    const chooseFilter = $("#chooseFilter").val().trim();
    if(attrName) {
        if(chooseFilter == "0") {
            //Error you must choose one
            swal(
                "خطأ",
                'يجب اختيار طريقة التصنيف',
                'error'
              )
            $("#textAttr input").val("");
            $(".boxAddRemove").find("input").val("");
            return false;
        } else if(chooseFilter == "1" || chooseFilter == "2") {
            $("#textAttr input").val("");
            const boxs = $(".boxAddRemove").find("input");
            for(const box of boxs) {
                if(!$(box).val()) {
                    //Error it empty
                    swal(
                        "خطأ",
                        'احدى قيم الخاصيه فارغ',
                        'error'
                      );
                      return false;
                      break;
                }
            }
            return true;
        } else if(chooseFilter == "3") {
            $(".boxAddRemove").find("input").val("");
            if(!$("#textAttr input").val()) {
                //Error it empty
                swal(
                    "خطأ",
                    'قيمه الخاصيه فارغه',
                    'error'
                  )
                  return false;
            } else {
                return true;
            }
        }
        return true;
    } else {
        //Error AttrName EMPTY
        swal(
            "خطأ",
            'اسم الخاصيه فارغ',
            'error'
          )
          return false;
    }
}

$("#saveCollection").click(function(ev) {
    validation(ev);
});

$(".action-list").change(function() {
        var selected = $(this).find("option:selected");
        var value = $(selected).attr("value");
        var checked = $(".selected").find(".select-checkbox");

        if(value == "2") {
            deleteSelected(checked, 2);
            $(".select-info").remove();
            const ids = $(".selected input[name='ids']");
            
            setTimeout(function() {
                $(".action-list").val("0").prop("selected", true)
            }, 500  );
            $(".nameOfAttributes div input").val("");
            $("#addAttr").trigger("click");
            $("#cancel").trigger("click");
        } else if(value == "1") {
            const inputVal = $(".select-checkbox input").val();
            $("#collectionId").val(inputVal);
            $.ajax({
                type:'POST',
                url:'/admin/subAtt',
                data:{id:inputVal},
                success: function(data){
                    console.log(data);
                    $('#valprops > li').remove();
                    $('#collectionNameEdit').val($(checked).next().text());
                    for(var i=0; i<data.properties.length; i++){
                        editvalprop = '<li>'+data.properties[i].name+'<a href="#">تعديل</a><span class="delete">مسح</span></li>';
                        $('#valprops').append(editvalprop);
                        
                    }
                },
                error: function(){
                    console.log('error');
                }
            });
           $(".model").show();
          //  addToList(attributes);
             //Add Here Collection 0f Attributes
        }
        else if(value == "4" && checked.length == 1) {
            $(".denyUser").trigger("click");
            $("#textArea").val($(".selected input[type='hidden']").val());
            
        } 
        else if(value == "5") {
            location.href = "https://filmahal.com/vendor/register"; 
        }
        else if(value == "6" && checked.length == 1) {
            $(".editUser").trigger("click");
            $("#editId").val($(".selected input[type='hidden']").val());
            $("#name").val($(".selected td").eq(1).text());
            $("#email").val($(".selected td").eq(2).text());
        }
        else if(value == "3" && checked.length == 1) {
            swal({
                title: 'تفعيل ؟',
                text: "هل انت متأكد من تفعيل ذلك الحساب",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'تفعيل',
                cancelButtonText: 'اغلاق'
              }).then((result) => {
                if (result.value) {
                    const ids = $(".selected input[name='ids']");
                   // console.log(ids);
                    $(ids).each(function(index) {
                       // console.log($(ids[index]).val());
                            $.ajax({
                                type:'POST',
                                url:'/api/active/vendor',
                                data:{vendor:1,id: $(ids[index]).val() },
                                success: function(data){
                                    console.log(data);
                                    setTimeout(function(){ 
                                        location.reload();
                                        
                                    }, 2000);
                                },
                                error: function(){
                                    console.log('error');
                                }
                             });
                    });
                    
                  swal(
                    'تم التفعيل',
                    '',
                    'success'
                  )
                }
              });
        }else if(value == "16" && checked.length == 1) {
            swal({
                title: 'تفعيل ؟',
                text: "هل انت متأكد من تفعيل ذلك الحساب",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'تفعيل',
                cancelButtonText: 'اغلاق'
              }).then((result) => {
                if (result.value) {
                    const ids = $(".selected input[name='ids']");
                   // console.log(ids);
                    $(ids).each(function(index) {
                        console.log($(ids[index]).val());
                            $.ajax({
                                type:'POST',
                                url:'/api/active/vendor',
                                data:{vendor:1,id: $(ids[index]).val() },
                                success: function(data){
                                    console.log(data);
                                    setTimeout(function(){ 
                                        location.reload();
                                        
                                    }, 2000);
                                },
                                error: function(){
                                    console.log('error');
                                }
                             });
                    });
                    
                  swal(
                    'تم التفعيل',
                    '',
                    'success'
                  )
                }
              });
        }
        else if(value == "8" && checked.length == 1) {
            swal({
                title: 'تفعيل ؟',
                text: "هل انت متأكد من تفعيل ذلك الحساب",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'تفعيل',
                cancelButtonText: 'اغلاق'
              }).then((result) => {
                if (result.value) {
                    const ids = $(".selected input[name='ids']");
                    console.log(ids);
                    $(ids).each(function(index) {
                      // console.log($(ids[index]).val());
                            $.ajax({
                                type:'POST',
                                url:'/api/active/vendor',
                                data:{vendor:5,id: $(ids[index]).val() },
                                success: function(data){
                                    console.log(data);
                                    setTimeout(function(){ 
                                        location.reload();
                                        
                                    }, 2000);
                                },
                                error: function(){
                                    console.log('error');
                                }
                             });
                    });
                    
                  swal(
                    'تم التفعيل',
                    '',
                    'success'
                  )
                }
              });
        }
        else if(value == "88" && checked.length == 1) {
            swal({
                title: 'تفعيل ؟',
                text: "هل انت متأكد من تفعيل ذلك التعليق",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'تفعيل',
                cancelButtonText: 'اغلاق'
              }).then((result) => {
                if (result.value) {
                    const ids = $(".selected input[name='ids']");
                    console.log(ids);
                    $(ids).each(function(index) {
                      // console.log($(ids[index]).val());
                            $.ajax({
                                type:'POST',
                                url:'/api/active/comment',
                                data:{comment:1,id: $(ids[index]).val() },
                                success: function(data){
                                    console.log(data);
                                    setTimeout(function(){ 
                                        location.reload();
                                        
                                    }, 2000);
                                },
                                error: function(){
                                    console.log('error');
                                }
                             });
                    });
                    
                  swal(
                    'تم التفعيل',
                    '',
                    'success'
                  )
                }
              });
        }
        else if(value == "99" && checked.length == 1) {
            swal({
                title: 'تجميد ؟',
                text: "هل انت متأكد من تجميد ذلك التعليق",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'تجميد',
                cancelButtonText: 'اغلاق'
              }).then((result) => {
                if (result.value) {
                    const ids = $(".selected input[name='ids']");
                    $(ids).each(function(index) {
                        //console.log($(ids[index]).val());
                            $.ajax({
                                type:'POST',
                                url:'/api/active/comment',
                                data:{comment:2,id: $(ids[index]).val() },
                                success: function(data){
                                    console.log(data);
                                    setTimeout(function(){ 
                                        location.reload();
                                        
                                    }, 2000);
                                },
                                error: function(){
                                    console.log('error');
                                }
                             });
                    });
                  swal(
                    'تم التجميد',
                    '',
                    'success'
                  )
                }
              });
        }
        else if(value == "9" && checked.length == 1) {
            swal({
                title: 'تجميد ؟',
                text: "هل انت متأكد من تجميد ذلك الحساب",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'تجميد',
                cancelButtonText: 'اغلاق'
              }).then((result) => {
                if (result.value) {
                    const ids = $(".selected input[name='ids']");
                    $(ids).each(function(index) {
                        //console.log($(ids[index]).val());
                            $.ajax({
                                type:'POST',
                                url:'/api/active/vendor',
                                data:{vendor:6,id: $(ids[index]).val() },
                                success: function(data){
                                    console.log(data);
                                    setTimeout(function(){ 
                                        location.reload();
                                        
                                    }, 2000);
                                },
                                error: function(){
                                    console.log('error');
                                }
                             });
                    });
                  swal(
                    'تم التجميد',
                    '',
                    'success'
                  )
                }
              });
        }
        else if(value == "10") {
            deleteSelected(checked, 5);
            $(".select-info").remove();
            const ids = $(".selected input[name='ids']");
            
            setTimeout(function() {
                $(".action-list").val("0").prop("selected", true)
            }, 500  );
            $(".nameOfAttributes div input").val("");
            $("#addAttr").trigger("click");
            $("#cancel").trigger("click");
        }
        else if(value == "100") {
            deleteSelected(checked, 55);
            $(".select-info").remove();
            const ids = $(".selected input[name='ids']");
            
            setTimeout(function() {
                $(".action-list").val("0").prop("selected", true)
            }, 500  );
            $(".nameOfAttributes div input").val("");
            $("#addAttr").trigger("click");
            $("#cancel").trigger("click");
        }
        else if(value == "11" && checked.length == 1) {
            $(".denyUser").trigger("click");
            $("#textArea").val($(".selected input[type='hidden']").val());
            
        }
        else if(value == "7" && checked.length == 1) {
            swal({
                title: 'تجميد ؟',
                text: "هل انت متأكد من تجميد ذلك الحساب",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'تجميد',
                cancelButtonText: 'اغلاق'
              }).then((result) => {
                if (result.value) {
                    const ids = $(".selected input[name='ids']");
                    $(ids).each(function(index) {
                        //console.log($(ids[index]).val());
                            $.ajax({
                                type:'POST',
                                url:'/api/active/vendor',
                                data:{vendor:0,id: $(ids[index]).val() },
                                success: function(data){
                                    console.log(data);
                                    setTimeout(function(){ 
                                        location.reload();
                                        
                                    }, 2000);
                                },
                                error: function(){
                                    console.log('error');
                                }
                             });
                    });
                  swal(
                    'تم التجميد',
                    '',
                    'success'
                  )
                }
              });
        }else if(value == "15") {
            deleteSelected(checked, 4);
            $(".select-info").remove();
            const ids = $(".selected input[name='ids']");
            
            setTimeout(function() {
                $(".action-list").val("0").prop("selected", true)
            }, 500  );
            $(".nameOfAttributes div input").val("");
            $("#addAttr").trigger("click");
            $("#cancel").trigger("click");
        }


        setTimeout(function() {
            $(".action-list").val("0").prop("selected", true)
        }, 500);
    });
    
    // Start 0f List 0f Governorates
const governorates = ["الإسكندرية", "الإسماعيلية", "أسوان", "أسيوط", "الأقصر", "البحر الأحمر", "البحيرة", "بني سويف", "بورسعيد", "جنوب سيناء",
"الجيزة", "الدقهلية", "دمياط", "سوهاج", "السويس", "الشرقية", "شمال سيناء", "الغربية", "الفيوم", "القاهرة", "القليوبية", "قنا", "كفر الشيخ",
"مطروح", "المنوفية", "المنيا", "الوادي الجديد"].sort();

for(let i = 0; i < governorates.length; ++i) {
    $(`<option value=${governorates[i]}>${governorates[i]}</option>`).appendTo("#gov");
}
// End 0f List 0f Governorates

// Start 0f edit-area
$(".edit-area").change(function() {
        var selected = $(this).find("option:selected");
        var value = $(selected).attr("value");
        var checked = $(".selected").find(".select-checkbox");

        if(value == "2") {
            if(checked.length != 0) {
               
                deleteSelected(checked, 3);
                setTimeout(function() {
                    $(".edit-area").val("0").prop("selected", true)
                }, 500);
            
                // $("#addAttr").trigger("click");
                // $("#cancel").trigger("click");
            } else {
                $(".edit-area").val("0").prop("selected", true)
            }
        } else if(value == "1") {
            if(checked.length != 0) {
                $("#govCancel").show();
                setTimeout(function() {
                    $(".edit-area").val("0").prop("selected", true)
                }, 500);
                $("#fourth").trigger("click");
                const myGov = governorates.indexOf($(checked).next().text());
                $("#gov").val(myGov).prop("selected", true);
                const gov = governorates[myGov];
            } else {
                $(".edit-area").val("0").prop("selected", true)
            }
        }
});
// End 0f edit-area

//Cancel 0f Edit
$("#govCancel").click(function(ev) {
    ev.preventDefault();
    $(this).hide();
    const no_drop = $("#fourth-step .no-drop").clone(true);
    $(no_drop).find("input").val("");
    $("#fourth-step .boxAddRemove").remove();
    $(".edit-area").val("0").prop("selected", true);
    $(".govs").append(no_drop);
    $("#fifth").trigger("click");
    $("#fourth-step").removeClass("selected");
});

let areas = [];
$("#saveAreas").click(function(ev) {
    ev.preventDefault();
    areas.push({govNum: $("#gov").val(), 
                areas: $("#fourth-step .boxAddRemove input").val()
            });
});


/*
$(".home").click(function(ev) {
    ev.preventDefault();
    location.href = "http://filmahal.com/admin/home";
});
*/
/*
$(".attributesPage").click(function(ev) {
    ev.preventDefault();
    location.href = "http://filmahal.com/admin/attributes"; 
});
*/
/*
$(".citiesPage").click(function(ev) {
    ev.preventDefault();
    location.href = "http://filmahal.com/admin/cities-areas";
});
*/
/*
$(".users").click(function(ev) {
    ev.preventDefault();
    location.href = "http://filmahal.com/admin/users";
});
*/
$("#enableSave").click(function(ev) {
    if(!$("#add-cates").val().trim() && $("#attrsChoose").val() == 0) {
        ev.preventDefault();
        swal(
          'خطأ',
          'يجب اضافه اسم للفئه واختيار مجموعه للفئه',
          'error'
          )
    }
    else {
        if(!$("#add-cates").val().trim()) {
        ev.preventDefault();
        swal(
            'خطأ',
          'يجب اضافه اسم للفئه',
          'error'
          )
        }
        if($("#attrsChoose").val() == 0) {
        ev.preventDefault();
        swal(
          'خطأ',
          'يجب اختيار مجموعه',
          'error'
        )
        }
    }
    
});



});

function cat() {
    "use strict";
    var select = document.getElementById("cats"),
        categories = document.getElementById("selected-categories"),
        newCat = document.createElement("div"),
        inputShow = document.createElement("input"),
        input = document.createElement("input"),
        close = document.createElement("span"),
        dataName = select.options[select.selectedIndex].getAttribute("data-name");
    categories.appendChild(newCat);
    newCat.appendChild(inputShow);
    newCat.appendChild(input);
    newCat.appendChild(close);
    inputShow.setAttribute("disabled", "disabled");
    inputShow.classList.add("cat");
    inputShow.setAttribute("value", dataName);
    input.classList.add("hidden");
    input.setAttribute("name", "categories[]");
    input.setAttribute("value", select.value);
    close.innerHTML = "&times;";
    close.classList.add("closeit");
    console.log(dataName);

    close.onclick = function closeit() {
        newCat.parentNode.removeChild(newCat);
    };
}